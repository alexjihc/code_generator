#!/usr/bin/env python
import os
import re
import sys
import json
import time
import pathlib
import fileinput

# definition_path: C:\Users\a3q\Documents\generatorV1\definition
base_path        = os.getcwd()
definition_path  = os.getcwd() + '\definition'

from config import *
from dataset_generator import *


class Interface_Generator():
    # Key value pair used in implementation generator. e.g. {"C": methodInterface}
    # native and logical lists used in implementation generator
    method_implementation = {}
    native_fields  = []
    logical_fields = []

    def __init__(self):
        c  = Config()
        dg = Dataset_Generator()
        be = BE()
        # se = Service()
        # fi = Fields_Generate()

        # lists for fields used in implementation generator
        self.native_fields  = dg.native_fields
        self.logical_fields = dg.logical_fields

        # config variables
        is_be               = c.get('is_be')
        bl_name             = c.get('bl_name')
        bl_dir_name         = c.get('bl_dir_name')
        bl_wsnamespace      = c.get('bl_wsnamespace')
        bl_entityname       = c.get('bl_entityname')
        bl_parent           = c.get('bl_parent')
        bl_interface        = c.get('bl_interface')
        be_keyfields        = c.get('be_keyfields')
        be_browseuri        = c.get('be_browseuri')
        be_source           = c.get('be_source')
        be_securitycontext  = c.get('be_securitycontext')

        # if O is found, then get all optional methods from config.json
        for optional_method in bl_interface:
            if 'O' in optional_method:
                bl_methods   = c.get('bl_method')

        #  get example and import lists from config.py
        example_lists    = c.example_lists()
        import_lists     = c.import_lists()

        # create default variables
        # add slash to business entity e.g. inventory.parts -> inventory/parts
        bl_interface_name = bl_dir_name.replace('.', '/')

        # remove comma e.g. ['SiteCode PartNumber WorkOrderNumber']
        keyfields = be_keyfields.split(',')
        
        # lowercase interface name e.g. partsReceiveWorkOrder
        lowercase_interface = bl_name[0].lower() + bl_name[1:]

        # set dataset_definition if be is true else false
        if is_be:
            dataset_definition = 'beInterfaceDefinition.txt'
        else:
            dataset_definition = 'serviceInterfaceDefinition.txt'

        # Change directory one up to generatorv1/
        os.chdir('..')        
        
        pathlib.Path(bl_name).mkdir(parents=True, exist_ok=True) 
        os.chdir(os.getcwd() + "\\" + bl_name)  

        # create interface
        interface = "I" + bl_name + ".cls"
        f_interface = open(interface, 'w+')

        # copy interface definition
        with open(os.path.join(definition_path, dataset_definition), 'r') as f_interfaceDefinition:
            readlines = f_interfaceDefinition.read()
            id_readlines = readlines
            f_interfaceDefinition.close()
            
        f_interface.write(id_readlines)
        f_interface.close()

        self.copy_imports(bl_name, bl_interface, import_lists, interface)
        self.replace_inline(os.getcwd(), bl_name, bl_dir_name, bl_wsnamespace, bl_entityname, be_securitycontext, be_browseuri, bl_interface_name, bl_parent, interface)

        # variables
        temp_strings            = ""
        result_strings          = ""

        # check interface and add result to a string
        for interface_method in bl_interface:
            if interface_method == "C":
                result_strings = self.generic_interface_method(result_strings, temp_strings, interface_method, c.create(), bl_name, bl_dir_name, bl_interface_name, lowercase_interface)                

            if interface_method == "R":
                result_strings = self.generic_interface_method2(result_strings, temp_strings, interface_method, c.fetch(), example_lists, be, keyfields, bl_name, bl_dir_name, bl_interface_name, lowercase_interface)

            if interface_method == "U":   
                result_strings = self.generic_interface_method(result_strings, temp_strings, interface_method, c.update(), bl_name, bl_dir_name, bl_interface_name, lowercase_interface)

            if interface_method == "D":
                result_strings = self.generic_interface_method(result_strings, temp_strings, interface_method, c.delete(), bl_name, bl_dir_name, bl_interface_name, lowercase_interface)

            if interface_method == "I":
                result_strings = self.generic_interface_method2(result_strings, temp_strings, interface_method, c.initialize(), example_lists, be, keyfields, bl_name, bl_dir_name, bl_interface_name, lowercase_interface)

            if interface_method == "V":                
                result_strings = self.generic_interface_method(result_strings, temp_strings, interface_method, c.isvalid(), bl_name, bl_dir_name, bl_interface_name, lowercase_interface)

            if interface_method == "E":
                result_strings = self.generic_interface_method2(result_strings, temp_strings, interface_method, c.exists(), example_lists, be, keyfields, bl_name, bl_dir_name, bl_interface_name, lowercase_interface)

            if interface_method == "G":
                result_strings = self.generic_interface_method(result_strings, temp_strings, interface_method, c.getcontrolstates(), bl_name, bl_dir_name, bl_interface_name, lowercase_interface)                

            if interface_method == "O":
                
                for i in range(len(bl_methods)):
                    temp_strings += temp_strings.join(map(str,c.optionalmethod()))    
                    temp_strings = self.replace_common_strings(temp_strings, bl_name, bl_dir_name, bl_interface_name, lowercase_interface)
                    
                    for key, value in bl_methods[i].items():       
                        # variable
                        input_example_position = 0
                        param_position         = 0
                        
                        import_results         = ""
                        param_results          = ""
                        indent_results         = ""
                        example_results        = ""
                        method_param_results   = ""

                        param_input_output     = []
                        param_items            = []

                        temp_strings = self.replace_method_strings(temp_strings, value)

                        input_strings = temp_strings
                        
                        input_example_position  = self.get_input_example_position(input_example_position, input_strings)
                        param_position          = self.get_param_position(param_position, input_strings)
                
                        import_results, param_results, param_items, param_input_output = self.get_import_and_param_results_for_optional_method(value)
                                                
                        indent_results       = self.get_indent_result(" ", indent_results, input_example_position)
                        example_results      = self.get_example_result_for_optional_method(example_results, param_items, param_input_output, example_lists, indent_results)
                        
                        method_param_results = self.get_method_param_result(method_param_results, param_position, param_items, param_input_output)
                        
                        temp_strings = temp_strings.replace("[dataset_import]", import_results)
                        temp_strings = temp_strings.replace("[param]", param_results)
                        temp_strings = temp_strings.replace("[input_example]", example_results)
                        temp_strings = temp_strings.replace("[input_output_param]", method_param_results)
                        temp_strings = temp_strings.replace("bl_name", bl_name)
                        temp_strings = temp_strings.replace("bl_interface_name", bl_interface_name)

                        self.optional_implementation_methods(temp_strings, interface_method, value)
                        
                        result_strings += temp_strings               
                        temp_strings = self.empty_strings(temp_strings) 


            if interface_method == "GL":
                result_strings = self.generic_interface_method(result_strings, temp_strings, interface_method, c.getlist(), bl_name, bl_dir_name, bl_interface_name, lowercase_interface)

            if interface_method == "FL":
                result_strings = self.generic_interface_method(result_strings, temp_strings, interface_method, c.fetchlist(), bl_name, bl_dir_name, bl_interface_name, lowercase_interface)

            if interface_method == "CC":
                result_strings = self.generic_interface_method(result_strings, temp_strings, interface_method, c.createconfirmation(), bl_name, bl_dir_name, bl_interface_name, lowercase_interface)

            if interface_method == "UC":
                result_strings = self.generic_interface_method(result_strings, temp_strings, interface_method, c.updateconfirmation(), bl_name, bl_dir_name, bl_interface_name, lowercase_interface)
            
            if interface_method == "DC":
                result_strings = self.generic_interface_method(result_strings, temp_strings, interface_method, c.deleteconfirmation(), bl_name, bl_dir_name, bl_interface_name, lowercase_interface)

            if interface_method == "HC":
                result_strings = self.generic_interface_method(result_strings, temp_strings, interface_method, c.handleconfirmation(), bl_name, bl_dir_name, bl_interface_name, lowercase_interface)


        # Open interface and replace main-content
        with open(os.path.join(base_path, bl_name, interface), 'r') as f_interface:
            readFile = f_interface.read()            
            main_content = '[Main-Content]'
            content = readFile.replace(main_content, result_strings)

        # Update interface
        with open(os.path.join(base_path, bl_name, interface), 'w') as f_interface:                
            f_interface.write(content)



##### METHOD DEFINITIONS #####

    # empty string variables
    def empty_strings(self, string_to_empty):
        string_to_empty = ""
        return string_to_empty


    # lowercase keyfields e.g. ['siteCode', 'partNumber', 'workOrderNumber']
    def keyfields_lowercase(self, lowercase_keyfield_list, keyfields):
        for keyfield in keyfields:
            lowercase_keyfields = keyfield[0].lower() + keyfield[1:]
            lowercase_keyfield_list.append(lowercase_keyfields)

        return lowercase_keyfield_list


    # Grab method line used in implementation generator e.g. method public void Create(input dataset-handle dsPartsReceiveWorkOrder).
    def implementation_methods(self, temp_strings, interface_method):
        if "*/" in temp_strings:
            methodPosition  = temp_strings.index('method')
            methodInterface = temp_strings[methodPosition:]

        self.method_implementation.update({interface_method: methodInterface})


    # Grab optional method line used in implementation generator e.g. method public void CheckParts(input dataset-handle dsPartsReceiveWorkOrder).
    def optional_implementation_methods(self, temp_strings, interface_method, value):
        if "*/" in temp_strings:
            methodPosition  = temp_strings.index('method')
            methodInterface = temp_strings[methodPosition:]
            method_name     = "O_" + value['method_name']

        self.method_implementation.update({method_name: methodInterface})


    # copy imports
    def copy_imports(self, bl_name, bl_interface, import_lists, interface):
        bl_imports                  = []
        check_methods               = ""
        check_confirmation_methods  = ""
        result_strings              = ""
        import_strings              = ""

        for method in bl_interface:
            # add imports e.g. {com/qad/assetmgmt/inventory/parts/dsPartsReceiveWorkOrder.i}
            if check_methods not in bl_imports:
                if method == 'C' or method == 'R' or method == 'U' or method == 'D' or method == 'I' or method == 'V' or method == 'G' or method == 'O' or method == 'GL':
                    bl_imports.append(import_lists[0])
                    check_methods = import_lists[0]

            # add imports for confirmation container e.g. {com/qad/assetmgmt/inventory/parts/dsPartsReceiveWorkOrderConfirmation.i}
            if check_confirmation_methods not in bl_imports:
                    if method == 'CC' or method == 'UC' or method == 'DC':
                        bl_imports.append(import_lists[1])
                        check_confirmation_methods = import_lists[1]

            # add imports for get list e.g. {com/qad/qra/base/dsFilter.i}
            if method == 'GL':
                    bl_imports.append(import_lists[3])
            
            # add imports for fetch list e.g. {com/qad/qra/base/dsKeyField.i}
            if method == 'FL':
                bl_imports.append(import_lists[4])

        for importcontent in bl_imports:
            import_strings += "   " + importcontent + "\n"
            result_strings = import_strings.rstrip()

            
        with open(os.path.join(base_path, bl_name, interface), 'r') as f_interface:
            readFile = f_interface.read()            
            import_content = '[Import-Content]'
            content = readFile.replace(import_content, result_strings)

        with open(os.path.join(base_path, bl_name, interface), 'w') as f_interface:                
            f_interface.write(content)   


    # replace inline
    def replace_inline(self, get_dir, bl_name, bl_dir_name, bl_wsnamespace, bl_entityname, be_securitycontext, be_browseuri, bl_interface_name, bl_parent, interface):
        
        for line in fileinput.input(os.path.join(get_dir, interface), inplace=True):
            print(line.replace('bl_name', bl_name), end='')
        
        for line in fileinput.input(os.path.join(get_dir, interface), inplace=True):
            print(line.replace('bl_dir_name', bl_dir_name), end='')

        for line in fileinput.input(os.path.join(get_dir, interface), inplace=True):
            print(line.replace('bl_wsnamespace', bl_wsnamespace), end='')  

        for line in fileinput.input(os.path.join(get_dir, interface), inplace=True):
                print(line.replace('bl_entityname', bl_entityname), end='')

        for line in fileinput.input(os.path.join(get_dir, interface), inplace=True):
            print(line.replace('be_securitycontext', be_securitycontext), end='')

        for line in fileinput.input(os.path.join(get_dir, interface), inplace=True):
            print(line.replace('be_browseuri', be_browseuri), end='')     

        for line in fileinput.input(os.path.join(get_dir, interface), inplace=True):
            print(line.replace('bl_interface_name', bl_interface_name), end='')

        for line in fileinput.input(os.path.join(get_dir, interface), inplace=True):
            print(line.replace('bl_parent', bl_parent), end='')
     

    # replace common strings
    def replace_common_strings(self, temp_strings, bl_name, bl_dir_name, bl_interface_name, lowercase_interface):
        temp_strings = temp_strings.replace("bl_name", bl_name)
        temp_strings = temp_strings.replace("bl_dir_name", bl_dir_name)
        temp_strings = temp_strings.replace("bl_interface_name", bl_interface_name)
        temp_strings = temp_strings.replace("lowercase_interface", lowercase_interface)

        return temp_strings


    # replace method strings
    def replace_method_strings(self, temp_strings, value):
        temp_strings = temp_strings.replace("method_name", value['method_name'])
        temp_strings = temp_strings.replace("method_description", value['description'])
        temp_strings = temp_strings.replace("method_return", value['method_return'])

        return temp_strings


    # get position of the [input_example] e.g. partsReceiveWorkOrderService:Fetch([input_example]
    def get_input_example_position(self, input_example_position, input_strings):
        input_lines = input_strings.splitlines()

        for n, line in enumerate(input_lines, 1):
            if "[input_example]" in line:
                input_example_position = line.index('[')

        return input_example_position


    # get position of the [input_keyfield] e.g. method public void Fetch([input_keyfield]
    def get_input_keyfield_position(self, input_keyfield_position, input_strings):
        input_lines = input_strings.splitlines()

        for n, line in enumerate(input_lines, 1):
            if "[input_keyfield]" in line:
                input_keyfield_position = line.index('[')

        return input_keyfield_position


    # get position of the [input_output_param] e.g. method public method_return method_name([input_output_param]).
    def get_param_position(self, param_position, input_strings):
        input_lines = input_strings.splitlines()

        for n, line in enumerate(input_lines, 1):
            if "[input_output_param]" in line:
                param_position = line.index('[')

        return param_position


    # get position of the first example field line
    def get_field_position(self, line_to_check, index_delimiter, index_position, input_strings):
        input_lines = input_strings.splitlines()

        for n, line in enumerate(input_lines, 1):
            if line_to_check in line:
                index_position = line.index(index_delimiter)

        return index_position + 1

    
    # get position of the input fieldName for Isvalid method
    # IN THE FUTURE, MAY SWITCH TO get_field_position function for flexibility
    def get_isvalid_field_position(self, input_isvalid_position, input_strings):
        input_lines = input_strings.splitlines()

        for n, line in enumerate(input_lines, 1):
            if ":IsValid" in line:
                input_isvalid_position = line.index('(')

        return input_isvalid_position + 1

    
    # replace [output] with proper indented line to match first parameter position
    # e.g  method public void Fetch([input_example],
    # e.g.                           output dataset-handle dsPartsReceiveWorkOrder).
    def replace_output(self, temp_strings, input_example_position, input_strings):
        input_lines = input_strings.splitlines()

        for n, line in enumerate(input_lines, 1):
            if "[output]" in line:
                # Indent output dataset line
                indent = " "
                pre_line = "    *   "
                output_example_line_length = len(pre_line)
                output_example_indent_position = input_example_position - output_example_line_length
                output_example_indent = ""

                for i in range(output_example_indent_position):
                    output_example_indent += indent

                output_result = output_example_indent + "output"
                temp_strings = temp_strings.replace("[output]", output_result)

        return temp_strings


    # replace [input-output] from isvalid e.g.    *   input-output dataset dsbl_name by-reference).
    def replace_input_output_isvalid(self, temp_strings, input_strings, input_isvalid_position, bl_name):
        input_lines = input_strings.splitlines()

        for n, line in enumerate(input_lines, 1):
            if "[isvalid-line]" in line:
                indent = " "
                pre_line = "    *   "
                input_output_length = len(pre_line)
                input_output_position = input_isvalid_position - input_output_length
                input_output_indent = ""

                for i in range(input_output_position):
                    input_output_indent += indent

                input_output_result = pre_line + input_output_indent + "input-output dataset dsbl_name by-reference)."

                temp_strings = temp_strings.replace("[isvalid-line]", input_output_result)
                temp_strings = temp_strings.replace("bl_name", bl_name)

        return temp_strings

        
    # return indent line based on indent space e.g.     * 
    def get_indent_result(self, indent_space, indent_result, input_position):        
        indent_result = ""
        pre_line      = "    *"
        line_length   = len(pre_line)
        indent_count  = input_position - line_length

        for i in range(indent_count):
            indent_result += indent_space

        return pre_line + indent_result 


    # return param result e.g. * @param siteCode
    def get_param_result(self, param_results, keyfields):
        keyfields_strings   = ""

        # if keyfields is empty
        if '' in keyfields:     
            param_results = ""            
        else:                    
        # loop through keyfields and add param e.g. * @param siteCode @param partNumber
            for keyfield in keyfields:
                lowercase_keyfields = keyfield[0].lower() + keyfield[1:]
                param_results += keyfields_strings.join("    * @param " + lowercase_keyfields + "\n")

        param_results = param_results.rstrip('\n')
        return param_results


    # return multiple results - import, param, param_items, param_input_output
    def get_import_and_param_results_for_optional_method(self, value):
        param_string            = ""
        param_strings           = ""
        param_dataset_string    = ""
        param_input_output      = []
        param_items             = []
        generic_import = "{com/qad/assetmgmt/bl_interface_name/"
        import_results  = ""
        param_results   = ""

        # from "param" key from optional_method in config.json
        # convert param key value to string "bl_name" "returnMessage" "siteCode"
        for i in range(len(value['param'])):
            param_key = list(value['param'][i].keys())
            param_string = param_string.join(param_key)

            # check if param is bl_name then create import statement for dataset
            if param_string == "bl_name":
                param_string = "ds" + param_string
                param_dataset_string = param_dataset_string.join("    *\n    *   " + generic_import + param_string + ".i}" + "\n")
                
            # import result e.g.  *   {com/qad/assetmgmt/bl_interface_name/dsbl_name.i}
            #               e.g.  *
            import_results = param_dataset_string + "    *"

            # param result e.g.     * @param dsbl_name
            param_results += param_strings.join("    * @param " + param_string + "\n")

            # add to list e.g. [('siteCode', 'character'), ('bl_name', 'dataset-handle'), ('returnMessage', 'character')]
            param_items += list(value['param'][i].items())
            
            # from "input_output" key from optional_method in config.json
            # add to list e.g. ['input', 'input', 'output', 'output']
            param_input_output.append(value['input_output'][i])

        import_results = import_results.rstrip()
        return import_results, param_results, param_items, param_input_output
        

    # return param lines with correct position for optional methods
    # e.g. method public void CheckPartsForWorkOrder(input siteCode as character,    
    #                                                output dataset-handle dsPartsReceiveWorkOrder,
    #                                                output returnMessage as character).
    def get_method_param_result(self, method_param_results, param_position, param_items, param_input_output):
        indent = " "
        param_indent = ""
        dataset_string = ""
        method_param_strings = ""

        for i in range(param_position):
            param_indent += indent

        for i in range(len(param_items)):
            if i < 1:                
                # check if first index has bl_name
                # e.g. ('siteCode', 'character')
                # e.g. ('bl_name', 'dataset-handle')
                if param_items[i][0] == "bl_name":
                    dataset_string = "ds" + param_items[i][0]

                    # e.g. input-output dataset-handle dsbl_name,
                    method_param_results += method_param_strings.join(param_input_output[i] + " " + param_items[i][1] + " " + dataset_string + ",\n")
                else:
                    # e.g. input siteCode as character,
                    method_param_results += method_param_strings.join(param_input_output[i] + " " + param_items[i][0] + " as " + param_items[i][1] + ",\n")
            else:
                if param_items[i][0] == "bl_name":
                    dataset_string = "ds" + param_items[i][0]

                    # e.g. *indented* output dataset-handle dsbl_name,
                    method_param_results += method_param_strings.join(param_indent + param_input_output[i] + " " + param_items[i][1] + " " + dataset_string + ",\n")
                else:
                    # e.g. *indented* input partNumber as character,
                    method_param_results += method_param_strings.join(param_indent + param_input_output[i] + " " + param_items[i][0] + " as " + param_items[i][1] + ",\n")
                    
        method_param_results = method_param_results.rstrip(',\n')
        return method_param_results


    # return data type e.g [character, character, integer']
    def get_field_datatype(self, field_datatype, be, keyfields):
        field_results = ""

        for keyfield in keyfields:
            # check be_dataset regardless of service because keyfield usually tied to source and service_dataset does not have source
            if keyfield in be.be_dataset[keyfield]:
                field_results += be.be_dataset[keyfield]

        field_results = field_results.splitlines()
        for n, line in enumerate(field_results, 1):
            if "field " in line:
                # splits into list. e.g. ['field', 'SiteCode', 'as', 'character', 'initial', '?']
                field_line = line.split()
                # add list e.g. [character, character, integer']
                field_datatype.append(field_line[3])

        return field_datatype


    # return example fields e.g. input 'AIR-010'
    def get_example_result(self, example_results, example_lists, indent_results, interface_method, keyfields):
        line_strings = ""

        # if keyfields is empty
        if '' in keyfields:
            example_results = ""
        else:                    
            for i in range(len(keyfields)):
                # for first line of example fields e.g. input '10-100',
                if i < 1:
                    if keyfields[i] == 'SiteCode':
                        example_results += line_strings.join("input " + "'" + example_lists[0] + "',\n")
                    elif keyfields[i] == 'StoresRequisitionNumber':
                        example_results += line_strings.join("input " + example_lists[1] + ",\n")
                    elif keyfields[i] == 'LineNumber':
                        example_results += line_strings.join("input " + example_lists[2] + ",\n")
                    elif keyfields[i] == 'PartNumber':
                        example_results += line_strings.join("input " + "'" + example_lists[3] + "',\n")
                    elif keyfields[i] == 'EquipmentNumber':
                        example_results += line_strings.join("input " + "'" + example_lists[4] + "',\n")
                    elif keyfields[i] == 'WorkOrderNumber':
                        example_results += line_strings.join("input " + example_lists[2] + ",\n")
                    elif keyfields[i] == 'ProjectNumber':
                        example_results += line_strings.join("input " + "'" + example_lists[6] + "',\n")
                    elif keyfields[i] == 'JobNumber':
                        example_results += line_strings.join("input " + "'" + example_lists[7] + "',\n")
                    else:
                        example_results += line_strings.join("input " + "'" + example_lists[5] + "',\n")
                else:
                    # rest of the example fields e.g. *    input 'AIR-010'
                    if keyfields[i] == 'SiteCode':
                        example_results += line_strings.join(indent_results + "input " + "'" + example_lists[0] + "',\n")
                    elif keyfields[i] == 'StoresRequisitionNumber':
                        example_results += line_strings.join(indent_results + "input " + example_lists[1] + ",\n")
                    elif keyfields[i] == 'LineNumber':
                        example_results += line_strings.join(indent_results + "input " + example_lists[2] + ",\n")
                    elif keyfields[i] == 'PartNumber':
                        example_results += line_strings.join(indent_results + "input " + "'" + example_lists[3] + "',\n")
                    elif keyfields[i] == 'EquipmentNumber':
                        example_results += line_strings.join(indent_results + "input " + "'" + example_lists[4] + "',\n")
                    elif keyfields[i] == 'WorkOrderNumber':
                        example_results += line_strings.join(indent_results + "input " + example_lists[2] + ",\n")
                    elif keyfields[i] == 'ProjectNumber':
                        example_results += line_strings.join(indent_results + "input " + "'" + example_lists[6] + "',\n")
                    elif keyfields[i] == 'JobNumber':
                        example_results += line_strings.join(indent_results + "input " + "'" + example_lists[7] + "',\n")
                    else:
                        example_results += line_strings.join(indent_results + "input " + "'" + example_lists[5] + "',\n")

        if interface_method == "E":
            example_results = example_results.rstrip(',\n')    
        else:
            example_results = example_results.rstrip('\n')
        return example_results


    # pass in  param ex. ['output'] ['input-output'] and return example fields e.g. input 'AIR-010'
    def get_example_result_for_optional_method(self, example_results, param_items, param_input_output, example_lists, indent_results):
        line_strings = ""
        # print(param_items)

        for i in range(len(param_items)):            
            # for first line of example fields e.g. input '10-100',
            # check param item e.g. ('siteCode', 'character')
            if param_input_output[i] != 'output':
                if i < 1:
                    if param_items[i][0] == 'siteCode':
                        example_results += line_strings.join(param_input_output[i] + " '" + example_lists[0] + "',\n")
                    elif param_items[i][0] == 'storesRequisitionNumber':
                        example_results += line_strings.join(param_input_output[i] + " " + example_lists[1] + ",\n")
                    elif param_items[i][0] == 'lineNumber':
                        example_results += line_strings.join(param_input_output[i] + " " + example_lists[2] + ",\n")
                    elif param_items[i][0] == 'partNumber':
                        example_results += line_strings.join(param_input_output[i] + " '" + example_lists[3] + "',\n")
                    elif param_items[i][0] == 'equipmentNumber':
                        example_results += line_strings.join(param_input_output[i] + " '" + example_lists[4] + "',\n")
                    elif param_items[i][0] == 'workOrderNumber':
                        example_results += line_strings.join(param_input_output[i] + " " + example_lists[2] + ",\n")
                    elif param_items[i][0] == 'projectNumber':
                        example_results += line_strings.join(param_input_output[i] + " '" + example_lists[6] + "',\n")
                    elif param_items[i][0] == 'jobNumber':
                        example_results += line_strings.join(param_input_output[i] + " '" + example_lists[7] + "',\n")
                    elif param_items[i][0] == 'bl_name':
                        example_results += line_strings.join(param_input_output[i] + " " + example_lists[11] + ",\n")
                    elif param_items[i][0] == 'returnMessage':
                        example_results += line_strings.join(param_input_output[i] + " " + example_lists[12] + ",\n")
                    else:
                        example_results += line_strings.join(param_input_output[i] + " '" + example_lists[5] + "',\n")
                else:
                    # rest of the example fields e.g. *    input 'AIR-010'
                    if param_items[i][0] == 'siteCode':
                        # print(param_items[i][i])
                        example_results += line_strings.join(indent_results + param_input_output[i] + " '" + example_lists[0] + "',\n")
                    elif param_items[i][0] == 'storesRequisitionNumber':
                        example_results += line_strings.join(indent_results + param_input_output[i] + " " + example_lists[1] + ",\n")
                    elif param_items[i][0] == 'lineNumber':
                        example_results += line_strings.join(indent_results + param_input_output[i] + " " + example_lists[2] + ",\n")
                    elif param_items[i][0] == 'partNumber':
                        example_results += line_strings.join(indent_results + param_input_output[i] + " '" + example_lists[3] + "',\n")
                    elif param_items[i][0] == 'equipmentNumber':
                        example_results += line_strings.join(indent_results + param_input_output[i] + " '" + example_lists[4] + "',\n")
                    elif param_items[i][0] == 'workOrderNumber':
                        example_results += line_strings.join(indent_results + param_input_output[i] + " " + example_lists[2] + ",\n")
                    elif param_items[i][0] == 'projectNumber':
                        example_results += line_strings.join(indent_results + param_input_output[i] + " '" + example_lists[6] + "',\n")
                    elif param_items[i][0] == 'jobNumber':
                        example_results += line_strings.join(indent_results + param_input_output[i] + " '" + example_lists[7] + "',\n")
                    elif param_items[i][0] == 'bl_name':
                        example_results += line_strings.join(indent_results + param_input_output[i] + " " + example_lists[11] + ",\n")
                    elif param_items[i][0] == 'returnMessage':
                        example_results += line_strings.join(indent_results + param_input_output[i] + " " + example_lists[12] + ",\n")
                    else:
                        example_results += line_strings.join(indent_results + param_input_output[i] + " '" + example_lists[5] + "',\n")

            # if param is 'output'
            # e.g. output dataset PartsReturnWorkOrder by-reference
            # e.g. output returnMessage
            else:
                if i < 1:
                    if param_items[i][0] == 'bl_name':
                        example_results += line_strings.join(param_input_output[i] + " dataset " + param_items[i][0] + " by-reference,\n")
                    else: 
                        example_results += line_strings.join(param_input_output[i] + " " + param_items[i][0] + ",\n")
                else:
                    if param_items[i][0] == 'bl_name':
                        example_results += line_strings.join(indent_results + param_input_output[i] + " dataset " + param_items[i][0] + " by-reference,\n")
                    else:
                        example_results += line_strings.join(indent_results + param_input_output[i] + " " +  param_items[i][0] + ",\n")                    

        example_results = example_results.rstrip(',\n')
        return example_results

    
    # return keyfield and datatype as input parameter e.g. input siteCode as character,
    def get_keyfield_result(self, keyfield_results, input_keyfield_position, lowercase_keyfield_list, field_datatype, interface_method, keyfields):
        indent                = " "
        keyfields_strings     = ""
        input_keyfield_indent = ""

        for i in range(input_keyfield_position):
            input_keyfield_indent += indent

        for i in range(len(keyfields)):                    
            if i < 1:
                keyfield_results += keyfields_strings.join("input " + lowercase_keyfield_list[0] + " as " + field_datatype[0] + ",\n")
            else:
                keyfield_results += keyfields_strings.join(input_keyfield_indent + "input " + lowercase_keyfield_list[i] + " as " + field_datatype[i] + ",\n")

        if interface_method == "E":
            keyfield_results = keyfield_results.rstrip(',\n')
        else:            
            keyfield_results = keyfield_results.rstrip('\n')
        return keyfield_results


    # short hand of default interface methods
    def generic_interface_method(self, result_strings, temp_strings, interface_method, api_method, bl_name, bl_dir_name, bl_interface_name, lowercase_interface):
        temp_strings += temp_strings.join(map(str,api_method))    
        temp_strings = self.replace_common_strings(temp_strings, bl_name, bl_dir_name, bl_interface_name, lowercase_interface)

        input_strings = temp_strings

        if interface_method == "V":
            input_isvalid_position = 0
            input_isvalid_position = self.get_isvalid_field_position(input_isvalid_position, input_strings)
            
            temp_strings = self.replace_input_output_isvalid(temp_strings, input_strings, input_isvalid_position, bl_name)
        
        elif interface_method == "G":
            input_example_position = 0
            input_example_position  = self.get_input_example_position(input_example_position, input_strings)
            temp_strings            = self.replace_output(temp_strings, input_example_position, input_strings)                          

            temp_strings = temp_strings.replace("[input_example]", "input")

        elif interface_method == "GL":
            index_position = 0
            index_position          = self.get_field_position(":GetList", '(', index_position, input_strings)            
            temp_strings            = self.replace_output(temp_strings, index_position, input_strings)                          

        self.implementation_methods(temp_strings, interface_method)
        result_strings += temp_strings               
        temp_strings = self.empty_strings(temp_strings) 

        return result_strings


    # short hand of default interface methods like fetch and initialize
    def generic_interface_method2(self, result_strings, temp_strings, interface_method, api_method, example_lists, be, keyfields, bl_name, bl_dir_name, bl_interface_name, lowercase_interface):
        # variables                
        lowercase_keyfield_list = []
        field_datatype          = []                
        param_results           = ""                
        example_results         = ""
        indent_results          = ""
        keyfield_results        = ""     
        input_example_position  = 0
        input_keyfield_position = 0

        temp_strings += temp_strings.join(map(str,api_method))    
        temp_strings = self.replace_common_strings(temp_strings, bl_name, bl_dir_name, bl_interface_name, lowercase_interface)        

        input_strings = temp_strings
        
        input_example_position  = self.get_input_example_position(input_example_position, input_strings)
        input_keyfield_position = self.get_input_keyfield_position(input_keyfield_position, input_strings)
        temp_strings            = self.replace_output(temp_strings, input_example_position, input_strings)                          
        
        lowercase_keyfield_list = self.keyfields_lowercase(lowercase_keyfield_list, keyfields)
        
        param_results    = self.get_param_result(param_results, keyfields)
        indent_results   = self.get_indent_result(" ", indent_results, input_example_position)
        example_results  = self.get_example_result(example_results, example_lists, indent_results, interface_method, keyfields)

        field_datatype   = self.get_field_datatype(field_datatype, be, keyfields)                
        keyfield_results = self.get_keyfield_result(keyfield_results, input_keyfield_position, lowercase_keyfield_list, field_datatype, interface_method, keyfields)
        
        temp_strings = temp_strings.replace("[param_kf]", param_results)
        temp_strings = temp_strings.replace("[input_example]", example_results)
        temp_strings = temp_strings.replace("[input_keyfield]", keyfield_results)
            
        self.implementation_methods(temp_strings, interface_method)
        result_strings += temp_strings               
        temp_strings = self.empty_strings(temp_strings) 

        return result_strings


# Uncomment to run this generator
# ig = Interface_Generator()
