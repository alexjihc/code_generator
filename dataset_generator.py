#!/usr/bin/env python
import os
import re
import sys
import pathlib
import fileinput

# dataset_path: C:\Users\a3q\Documents\generatorV1\dataset
base_path        = os.getcwd()
dataset_path     = os.getcwd() + '\dataset'
definition_path  = os.getcwd() + '\definition'

sys.path.append(dataset_path)
sys.path.append(definition_path)

from config import *
from dataset.be_dataset import *
from dataset.service_dataset import *
from dataset.field_to_generate import *


class Dataset_Generator():
    # lists for be or services fields used in implementation generator
    # ex. dataset_fields - all fields:   ['AmountPaid', SiteCode', 'ProjectNumber', 'PartNumber', 'PartStatus']    
    # ex. native_fields - only @source:  ['site_code', 'proj_no', 'part_no']
    # ex. logical_fields - match native: ['SiteCode', 'ProjectNumber', 'PartNumber']    
    dataset_fields = []
    native_fields  = []
    logical_fields = []

    # instantiate config, be, service, and field_generate dataset py file
    def __init__(self):        
        c  = Config()
        be = BE()
        se = Service()
        fi = Fields_Generate()

        # config variables
        is_be               = c.get('is_be')
        bl_name             = c.get('bl_name')
        be_source           = c.get('be_source')
        be_keyfields        = c.get('be_keyfields')

        # create default variables
        # replace from SiteCode,PartNumber,WorkOrderNumber -> SiteCode PartNumber WorkOrderNumber
        index_keyfields = be_keyfields.replace(',', ' ')
        field_results   = ""
        field_results2  = ""
        
        # set dataset_definition if be is true else false
        if is_be:
            dataset_definition = 'beDatasetDefinition.txt'
            ds = be.be_dataset
        else:
            dataset_definition = 'serviceDatasetDefinition.txt'
            ds = se.service_dataset

        pathlib.Path(bl_name).mkdir(parents=True, exist_ok=True) 
        os.chdir(os.getcwd() + "\\" + bl_name)  

        # create dataset
        dataset = "ds" + bl_name + ".i"
        f_dataset = open(dataset, 'w+')

        # copy datasetDefinition texts to new dataset
        with open(os.path.join(definition_path, dataset_definition), 'r') as f_datasetDefinition:
            readlines = f_datasetDefinition.read()
            dd_readlines = readlines
            f_datasetDefinition.close()
            
        f_dataset.write(dd_readlines)
        f_dataset.close()

        self.replace_inline(os.getcwd(), bl_name, be_source, be_keyfields, index_keyfields, dataset)

        # in case keyerror, make sure to check service_dataset or be_dataset file if that field exist. add the field if necessary
        for dataset_field in fi.generated_fields:
            field_results += ds[dataset_field]
            self.dataset_fields.append(dataset_field)
        
        # open implementation and replace main-content
        with open(os.path.join(os.getcwd(), dataset), 'r') as f_implementation:
            readFile = f_implementation.read()            
            mainContent = '[Main-Content]'
            content = readFile.replace(mainContent, field_results)

        # update dataset
        with open(os.path.join(os.getcwd(), dataset), 'w') as f_implementation:                
            f_implementation.write(content)  

        # checking be_dataset regardless in order to check if source exist in dataset field. add the field if necessary
        for dataset_field in fi.generated_fields:        
            try:    
                if "@source" in be.be_dataset[dataset_field]:
                    field_results2 += be.be_dataset[dataset_field]                    
            except KeyError:
                continue

        field_lines = field_results2.splitlines()
        for n, line in enumerate(field_lines, 1):
            if "@source" in line:
                isSource = True
                # splits into list. e.g. ['*', '@source', 'site_code']
                source_line = line.split()
                # append e.g. ['site_code', 'proj_no', 'part_no']
                self.native_fields.append(source_line[2])

            if "field " in line:
                if isSource:
                    # splits into list. e.g. ['field', 'SiteCode', 'as', 'character']
                    field_line = line.split()
                    # append e.g. ['SiteCode', 'ProjectNumber', 'PartNumber']
                    self.logical_fields.append(field_line[1])
                isSource = False


##### METHOD DEFINITIONS #####

    # replace inline
    def replace_inline(self, get_dir, bl_name, be_source, be_keyfields, index_keyfields, dataset):
        for line in fileinput.input(os.path.join(get_dir, dataset), inplace=True):
            print(line.replace('bl_name', bl_name), end='')

        for line in fileinput.input(os.path.join(get_dir, dataset), inplace=True):
            print(line.replace('be_source', be_source), end='')

        for line in fileinput.input(os.path.join(get_dir, dataset), inplace=True):
            print(line.replace('be_keyfields', be_keyfields), end='')

        for line in fileinput.input(os.path.join(get_dir, dataset), inplace=True):
            print(line.replace('index_keyfields', index_keyfields), end='')



# Uncomment to run this generator
# dg = Dataset_Generator()