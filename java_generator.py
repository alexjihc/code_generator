#!/usr/bin/env python
import os
import re
import sys
import json
import time
import pathlib
import linecache
import fileinput

# definition_path: C:\Users\a3q\Documents\generatorV1\definition
base_path = os.getcwd()

from config import *
from interface_generator import *


class Java_Generator():

    def __init__(self):
        c  = Config()
        ig = Interface_Generator()

        # config variables
        is_be               = c.get('is_be')
        bl_name             = c.get('bl_name')
        bl_dir_name         = c.get('bl_dir_name')
        bl_interface        = c.get('bl_interface')
        be_keyfields        = c.get('be_keyfields')
        url_prefix          = c.get('java_url_prefix')
        url_resource        = c.get('java_url_resource')
        keyfields_type      = c.get('java_keyfields_type')

        # variables
        lowercase_keyfield_list       = []
        service_result_strings        = ""
        entity_result_strings         = ""
        datacontroller_result_strings = ""

        java_interface = []
        keyfield_list = []
        keyfield_type_list = []

        # remove comma e.g. ['SiteCode PartNumber WorkOrderNumber']
        # remove comma e.g. "String,String"
        keyfields = be_keyfields.split(',')
        keyfield_types = keyfields_type.split(',')

        # capitalized keyfields as a list
        for keyfield in keyfields:
            keyfield_list.append(keyfield)

        # add keyfield type as a list e.g. ['String', 'String']
        for keyfield_type in keyfield_types:
            keyfield_type_list.append(keyfield_type)

        # lowercase keyfields
        lowercase_keyfield_list = self.keyfields_lowercase(lowercase_keyfield_list, keyfields)

        # lowercase bl name e.g. partsReceiveWorkOrder
        lowercase_bl_name = bl_name[0].lower() + bl_name[1:]


        # java folder name
        java_path = "java_" + bl_name

        java_service_definition = 'serviceimpl_definition.java'
        java_datacontroller_definition = 'datacontroller_definition.java'

        # change directory up to generatorv1 folder
        os.chdir('..')

        pathlib.Path(java_path).mkdir(parents=True, exist_ok=True)
        os.chdir(os.getcwd() + "\\" + java_path)
        pathlib.Path(bl_name).mkdir(parents=True, exist_ok=True)
        os.chdir(os.getcwd() + "\\" + bl_name)

        # C:\Users\a3q\Documents\generatorV1\java\definition
        definition_path  = base_path + '\java\definition'

        # Create service_impl java file
        service_implementation = bl_name + "ServiceImpl.java"
        s_implementation = open(service_implementation, 'w+')

        # Copy service implementation definition
        with open(os.path.join(definition_path, java_service_definition), 'r') as si_definition:
            readlines = si_definition.read()
            si_readlines = readlines
            si_definition.close()

        s_implementation.write(si_readlines)
        s_implementation.close()

        # Create datacontroller java file
        datacontroller_implementation = bl_name + "DataController.java"
        d_implementation = open(datacontroller_implementation, 'w+')

        # Copy datacontroller definition
        with open(os.path.join(definition_path, java_datacontroller_definition), 'r') as dc_definition:
            readlines = dc_definition.read()
            dc_readlines = readlines
            dc_definition.close()

        d_implementation.write(dc_readlines)
        d_implementation.close()


        # DATACONTROLLER #
        # Get updated interface e.g. java_interface ['GE', 'PO', 'D', 'V', 'E']
        java_interface = self.copy_datacontroller_interface(bl_interface)

        datacontroller_result_strings = self.add_crud_to_datacontroller(java_interface, bl_interface, datacontroller_result_strings)

        java_url_prefix_resource = self.url_prefix_and_resource(url_prefix, url_resource)
        java_request_param = self.request_param(lowercase_keyfield_list, True)
        java_request_param2 = self.request_param(lowercase_keyfield_list, False)
        java_keyfield_param = self.keyfield_param(lowercase_keyfield_list)
        java_null_check = self.create_if_null_check(lowercase_keyfield_list, True)
        java_null_check2 = self.create_if_null_check(lowercase_keyfield_list, False)
        java_ignorecase_check = self.ignore_case_check(lowercase_keyfield_list, keyfield_list)
        java_getlist_view_or_grid = self.getlist_view_or_grid(bl_interface)
        java_filter_container = self.getlist_filter_container(bl_interface, lowercase_keyfield_list, keyfield_list)

        datacontroller_result_strings = datacontroller_result_strings.replace("[url_prefix_resource]", java_url_prefix_resource)
        datacontroller_result_strings = datacontroller_result_strings.replace("[request_param]", java_request_param)
        datacontroller_result_strings = datacontroller_result_strings.replace("[request_param2]", java_request_param2)
        datacontroller_result_strings = datacontroller_result_strings.replace("[keyfield_param]", java_keyfield_param)
        datacontroller_result_strings = datacontroller_result_strings.replace("[is_null_check]", java_null_check)
        datacontroller_result_strings = datacontroller_result_strings.replace("[is__not_null_check]", java_null_check2)
        datacontroller_result_strings = datacontroller_result_strings.replace("[ignorecase_check]", java_ignorecase_check)
        datacontroller_result_strings = datacontroller_result_strings.replace("[get_entity_list_view_or_grid]", java_getlist_view_or_grid)
        datacontroller_result_strings = datacontroller_result_strings.replace("[filter_container_for_getlist_view_grid]", java_filter_container)

        datacontroller_result_strings = self.replace_common_strings(datacontroller_result_strings, bl_name, lowercase_bl_name)


        # SERVICE IMPLMENTATION #
        service_result_strings = self.add_crud_to_service_implementation(bl_interface)
        java_type_keyfield_param = self.type_and_keyfield_parameter(lowercase_keyfield_list, keyfield_type_list)

        service_result_strings = service_result_strings.replace("[type_and_field_param]", java_type_keyfield_param)
        service_result_strings = service_result_strings.replace("[keyfield_param]", java_keyfield_param)
        service_result_strings = self.replace_common_strings(service_result_strings, bl_name, lowercase_bl_name)

        entity_result_strings = self.add_crud_entity_to_service_implementation(bl_interface, keyfield_list)
        entity_result_strings = self.replace_common_strings(entity_result_strings, bl_name, lowercase_bl_name)



        # if O is found, then get all optional methods from config.json
        for optional_method in bl_interface:
            if 'O' in optional_method:
                bl_methods   = c.get('bl_method')
                datacontroller_result_strings += self.generate_datacontroller_impl_methods(bl_methods, bl_name, lowercase_bl_name)
                service_result_strings += self.generate_service_impl_methods(bl_methods, bl_name, lowercase_bl_name)

        # replace inline strings
        self.replace_inline(os.getcwd(), datacontroller_implementation, service_implementation, bl_name, bl_dir_name, lowercase_bl_name, datacontroller_result_strings, service_result_strings, entity_result_strings)



##### METHOD DEFINITIONS #####

    # empty string variables
    def empty_strings(self, string_to_empty):
        string_to_empty = ""
        return string_to_empty


    # lowercase first letter of each keyfield e.g. SiteCode -> siteCode
    def keyfields_lowercase(self, lowercase_keyfield_list, keyfields):
        for keyfield in keyfields:
            lowercase_keyfields = keyfield[0].lower() + keyfield[1:]
            lowercase_keyfield_list.append(lowercase_keyfields)

        return lowercase_keyfield_list


    # Return indented string
    def indent_space(self, number_to_indent):
        indent_string = ""
        indent        = " "
        indent_string = indent * number_to_indent

        return indent_string


    # replace inline for both datacontroller and service impl
    def replace_inline(self, get_dir, datacontroller_implementation, service_implementation, bl_name, bl_dir_name, lowercase_bl_name, datacontroller_result_strings, service_result_strings, entity_result_strings):

        for line in fileinput.input(os.path.join(get_dir, datacontroller_implementation), inplace=True):
            print(line.replace('bl_name', bl_name), end='')

        for line in fileinput.input(os.path.join(get_dir, datacontroller_implementation), inplace=True):
            print(line.replace('bl_dir_name', bl_dir_name), end='')

        for line in fileinput.input(os.path.join(get_dir, datacontroller_implementation), inplace=True):
            print(line.replace('bl_lowercase', lowercase_bl_name), end='')

        for line in fileinput.input(os.path.join(get_dir, datacontroller_implementation), inplace=True):
            print(line.replace('[DATACONTROLLER_API]', datacontroller_result_strings), end='')

        for line in fileinput.input(os.path.join(get_dir, service_implementation), inplace=True):
            print(line.replace('bl_name', bl_name), end='')

        for line in fileinput.input(os.path.join(get_dir, service_implementation), inplace=True):
            print(line.replace('bl_dir_name', bl_dir_name), end='')

        for line in fileinput.input(os.path.join(get_dir, service_implementation), inplace=True):
            print(line.replace('bl_lowercase', lowercase_bl_name), end='')

        for line in fileinput.input(os.path.join(get_dir, service_implementation), inplace=True):
            print(line.replace('[SERVICE_IMPLEMENTATION_METHODS]', service_result_strings), end='')

        for line in fileinput.input(os.path.join(get_dir, service_implementation), inplace=True):
            print(line.replace('[SERVICE_ENTITY_METHODS]', entity_result_strings), end='')


    # replace common strings
    def replace_common_strings(self, temp_strings, bl_name, lowercase_bl_name):
        temp_strings = temp_strings.replace("bl_name", bl_name)
        temp_strings = temp_strings.replace("lowercase_name", lowercase_bl_name)

        return temp_strings

    # replace [method_name] e.g. checkPartsForWorkOrderReturn
    def replace_method_name(self, temp_strings, method_name):
        temp_strings = temp_strings.replace('[method_name]', method_name)

        return temp_strings

    # replace [method_return] e.g. void
    def replace_method_return(self, temp_strings, method_return):
        temp_strings = temp_strings.replace('[method_return]', method_return)

        return temp_strings

    # replace input and output parameters
    def replace_method_input_output_and_parameter(self, temp_strings, method_input_output, method_parameter):
        method_result_strings   = ""
        factory_result_strings  = ""
        isHolder = False

        for i in range(len(method_input_output)):
            # if its 'input' or ''input-output'
            if not method_input_output[i] == 'output':

                # loop parameter item e.g. ('siteCode', 'character')
                for key, value in method_parameter[i].items():

                    # if its bl_name: dataset-handle
                    if key == 'bl_name':
                        method_result_strings = method_result_strings + 'bl_nameContainer container, '
                        if method_input_output[i] == 'input':
                            factory_result_strings = factory_result_strings + '((Qrabl_nameContainer)container).getProDataGraph(), '
                        else:
                            factory_result_strings = factory_result_strings + 'holder, '
                            isHolder = True
                    else:
                        factory_result_strings = factory_result_strings + key + ', '

                        if value == 'character':
                            method_result_strings = method_result_strings + 'String ' + key + ', '
                        elif value == 'integer':
                            method_result_strings = method_result_strings + 'Integer ' + key + ', '
                        elif value == 'decimal':
                            method_result_strings = method_result_strings + 'BigDecimal ' + key + ', '
                        elif value == 'logical':
                            method_result_strings = method_result_strings + 'Boolean ' + key + ', '

            # output
            else:
                # loop parameter item e.g. ('siteCode', 'character')
                for key, value in method_parameter[i].items():

                    # if its bl_name: dataset-handle
                    if key == 'bl_name':
                        factory_result_strings = factory_result_strings + 'holder, '
                        isHolder = True
                    else:
                        factory_result_strings = factory_result_strings + key + ', '

                        if value == 'character':
                            method_result_strings = method_result_strings + 'Holder <String> ' + key + ', '
                        elif value == 'integer':
                            method_result_strings = method_result_strings + 'Holder <Integer> ' + key + ', '
                        elif value == 'decimal':
                            method_result_strings = method_result_strings + 'Holder <BigDecimal> ' + key + ', '
                        elif value == 'logical':
                            method_result_strings = method_result_strings + 'Holder <Boolean> ' + key + ', '

        method_result_strings = method_result_strings.rstrip(', ')
        factory_result_strings = factory_result_strings.rstrip(', ')

        temp_strings = temp_strings.replace('[method_parameter]', method_result_strings)
        temp_strings = temp_strings.replace('[service_parameter]', factory_result_strings)

        return temp_strings, isHolder


    # Combine url prefix and url resources e.g. /api/erp/tools
    def url_prefix_and_resource(self, java_url_prefix, java_url_resource):
        java_url_prefix_resource = ""
        java_url_prefix_resource = java_url_prefix + java_url_resource

        return java_url_prefix_resource

    # Construct request param e.g. @RequestParam(value="siteCode", required=false) String siteCode,
    def request_param(self, lowercase_keyfield_list, hasInitialize):
        indent_space = self.indent_space(12)

        request_param_string1 = '@RequestParam(value="{}", required=false) String {},\n'
        request_param_string2 = indent_space + '@RequestParam(value="{}", required=false) String {},\n'
        request_param_result_string = ""

        for i in range(len(lowercase_keyfield_list)):
            if i < 1:
                request_param_result_string += request_param_string1.format(lowercase_keyfield_list[i], lowercase_keyfield_list[i])
            else:
                request_param_result_string += request_param_string2.format(lowercase_keyfield_list[i], lowercase_keyfield_list[i])

        if hasInitialize:
            request_param_result_string = request_param_result_string.rstrip('\n')
        else:
            request_param_result_string = request_param_result_string.rstrip(',\n')

        return request_param_result_string


    # Construct keyfields parameter e.g. service.initialize(siteCode,toolNumber);
    def keyfield_param(self, lowercase_keyfield_list):
        keyfield_string = ""
        for keyfield in lowercase_keyfield_list:
            keyfield_string += keyfield + ', '

        keyfield_string = keyfield_string.rstrip(', ')
        return keyfield_string


    # Construct if equal or not equal check statement e.g. if (siteCode == null && toolNumber == null)
    def create_if_null_check(self, lowercase_keyfield_list, isNull):
        null_check_string = ""
        equal_string     = " == null"
        not_equal_string = " != null"

        if isNull:
            for i in range(len(lowercase_keyfield_list)):
                if i < len(lowercase_keyfield_list)-1:
                    null_check_string += lowercase_keyfield_list[i] + equal_string + " && "
                else:
                    null_check_string += lowercase_keyfield_list[i] + equal_string
        else:
            for i in range(len(lowercase_keyfield_list)):
                if i < len(lowercase_keyfield_list)-1:
                    null_check_string += lowercase_keyfield_list[i] + not_equal_string + " && "
                else:
                    null_check_string += lowercase_keyfield_list[i] + not_equal_string

        return null_check_string


    # Construct ignorecase statement for createAndUpdate API e.g. !siteCode.equalsIgnoreCase(entity.getSitecode()) && !toolNumber.equalsIgnoreCase(entity.getPartnumber())
    def ignore_case_check(self, lowercase_keyfield_list, keyfield_list):
        ignore_case_check_string = ""

        for i in range(len(lowercase_keyfield_list)):
            if i < len(lowercase_keyfield_list)-1:
                ignore_case_check_string += "!" + lowercase_keyfield_list[i] + ".equalsIgnoreCase(entity.get" + keyfield_list[i] + "()) && "
            else:
                ignore_case_check_string += "!" + lowercase_keyfield_list[i] + ".equalsIgnoreCase(entity.get" + keyfield_list[i] + "())"

        return ignore_case_check_string


    # Construct entity.read statement for readentity e.g. entity.getSiteCode(), entity.getStoresRequisitionNumber(), entity.getLineNumber()
    def entity_read_keyfield_param(self, keyfield_list):
        entity_default_string = "entity.get{}()"
        entity_result_string = ""

        for keyfield in keyfield_list:
            entity_result_string += entity_default_string.format(keyfield) + ", "

        entity_result_string = entity_result_string.strip(', ')
        return entity_result_string



    ##### DATACONTROLLER DEFINITIONS #####

    # Update datacontroller result string
    def add_crud_to_datacontroller(self, java_interface, bl_interface, datacontroller_result_strings):
        for crud_method in java_interface:
            if 'GE' == crud_method:
                datacontroller_result_strings += self.add_datacontroller_template(crud_method)

            elif 'PO' == crud_method:
                datacontroller_result_strings += self.add_datacontroller_template(crud_method)

            elif 'D' == crud_method:
                datacontroller_result_strings += self.add_datacontroller_template(crud_method)

            elif 'V' == crud_method:
                datacontroller_result_strings += self.add_datacontroller_template(crud_method)

            elif 'G' == crud_method:
                datacontroller_result_strings += self.add_datacontroller_template(crud_method)

        for bl_method in bl_interface:
            if 'GL' == bl_method:
                datacontroller_result_strings += self.add_datacontroller_template(bl_method)

        return datacontroller_result_strings


    # Add and return datacontroller template
    def add_datacontroller_template(self, crud_method):
        datacontroller_strings = ""

        if crud_method == 'GE':
            datacontroller_strings += datacontroller_strings.join(map(str,self.get_mapping_template()))
            return datacontroller_strings

        elif crud_method == 'PO':
            datacontroller_strings += datacontroller_strings.join(map(str,self.post_mapping_template()))
            return datacontroller_strings

        elif crud_method == 'D':
            datacontroller_strings += datacontroller_strings.join(map(str,self.delete_mapping_template()))
            return datacontroller_strings

        elif crud_method == 'V':
            datacontroller_strings += datacontroller_strings.join(map(str,self.isvalid_mapping_template()))
            return datacontroller_strings

        elif crud_method == 'G':
            datacontroller_strings += datacontroller_strings.join(map(str,self.getcontrolstate_mapping_template()))
            return datacontroller_strings

        elif crud_method == 'GL':
            datacontroller_strings += datacontroller_strings.join(map(str,self.getlist_grid_mapping_template()))
            return datacontroller_strings


    # Return filtered list of interfaces used for datacontroller
    # e.g. bl_interface   ['I', 'C', 'R', 'U', 'D', 'V', 'E', 'GL']
    # e.g. java_interface ['GE', 'PO', 'D', 'V', 'E'] GE: GetMapping, PO: PostMapping, D: DeleteMapping, V: IsValid, G: GetControlState
    def copy_datacontroller_interface(self, bl_interface):
        has_get    = False
        has_post   = False
        java_interface = []
        java_interface = bl_interface[:]

        # print(java_interface)
        for crud_method in java_interface:
            if 'I' in crud_method or 'R' in crud_method or 'GL' in crud_method:
                has_get = True
                java_interface.remove(crud_method)

                # another check to remove interface if index has been shifted to 0
                for crud_method in java_interface:
                    if 'I' in crud_method or 'R' in crud_method or 'GL' in crud_method:
                        java_interface.remove(crud_method)

        for crud_method in java_interface:
            if 'C' in crud_method or 'U' in crud_method:
                has_post = True
                java_interface.remove(crud_method)

                # another check to remove interface if index has been shifted to 0
                for crud_method in java_interface:
                    if 'C' in crud_method or 'U' in crud_method:
                        java_interface.remove(crud_method)

        if has_get:
            java_interface.insert(0, 'GE')
        if has_post:
            java_interface.insert(1, 'PO')

        # print(java_interface)
        return java_interface


    # Return either standard getEntityListView or getEntityListViewGrid
    def getlist_view_or_grid(self, bl_interface):
        getentity_view_string = "return getEntityListView(model, queryParameters);"
        getentity_grid_string = "return getEntityListViewGrid(model, filterContainer);"
        getentity_result_string = ""

        # for getlist_method in bl_interface:
        if "GL" in bl_interface:
            return getentity_grid_string
        else:
            return getentity_view_string


    # Return Filter container if interface contains GetListViewGrid
    # ex. FilterContainer filterContainer = ((PartsUsageAnalysisServiceImpl) crudProviderService).getEntityFactory().createFilterContainer();
    #     String filteredPartNumber = queryParameters.getFilterValue("partNumber");
    #     Filter filter = filterContainer.addFilter();
    #     filter.setFieldName("partNumber");
    #     filter.setOperator("=");
    #     filter.setFieldValue(filteredPartNumber);
    def getlist_filter_container(self, bl_interface, lowercase_keyfield_list, keyfield_list):
        filtercontainer_string = "FilterContainer filterContainer = ((bl_nameServiceImpl) crudProviderService).getEntityFactory().createFilterContainer();\n        Filter filter = null;\n\n"

        filterkey_string1 = '{2}String filtered{0} = queryParameters.getFilterValue("{1}");\n{2}filter = filterContainer.addFilter();\n{2}filter.setFieldName("{1}");\n{2}filter.setOperator("=");\n{2}filter.setFieldValue(filtered{0});\n\n'
        filterkey_string2 = '{2}String filtered{0} = queryParameters.getFilterValue("{1}");\n{2}filter = filterContainer.addFilter();\n{2}filter.setFieldName("{1}");\n{2}filter.setOperator("=");\n{2}filter.setFieldValue(filtered{0});\n\n'

        filtercontainer_result_string = ""
        indent_space = self.indent_space(8)

        if "GL" in bl_interface:
            filtercontainer_result_string += filtercontainer_string
            for i in range(len(lowercase_keyfield_list)):
                if i < 1:
                    filtercontainer_result_string += filterkey_string1.format(keyfield_list[i], lowercase_keyfield_list[i], indent_space)
                else:
                    filtercontainer_result_string += filterkey_string2.format(keyfield_list[i], lowercase_keyfield_list[i], indent_space)

            return filtercontainer_result_string
        else:
            return ""


    # Create generic datacontroller method for optional method
    def generate_datacontroller_impl_methods(self, bl_methods, bl_name, lowercase_name):
        method_name                 = []
        method_return               = []
        method_parameter            = []
        method_input_output         = []
        method_name_result          = ""
        method_return_result        = ""
        method_parameter_result     = ""
        method_input_output_result  = ""

        temp_strings = ""

        for i in range(len(bl_methods)):
            for key, value in bl_methods[i].items():
                # 'method_name': 'CheckPartsForWorkOrderReturn'
                method_name.append(value['method_name'][:1].lower() + value['method_name'][1:])
                # 'method_return': 'void', 'logical'
                if value['method_return'] == 'logical':
                    value['method_return'] = 'boolean'
                method_return.append(value['method_return'])
                # method_parameter: {'siteCode': 'character'}, {'partNumber': 'character'}, {'bl_name': 'dataset-handle'}, {'returnMessage': 'character'}
                method_parameter.append(value['param'])
                # method_input_ouput: 'input', 'input', 'output', 'output'
                method_input_output.append(value['input_output'])

            temp_strings += temp_strings.join(map(str,self.generic_service_impl_template()))
            temp_strings = self.replace_method_name(temp_strings, method_name[i])
            temp_strings = self.replace_method_return(temp_strings, method_return[i])
            temp_strings,isHolder = self.replace_method_input_output_and_parameter(temp_strings, method_input_output[i], method_parameter[i])

            # Possibly no longer need isHolder
            # if isHolder:
            #     holder_string = 'Holder<DataGraph> holder = new Holder<>();'
            #     temp_strings = temp_strings.replace('[holder_datagraph]', holder_string)
            # else:
            #     temp_strings = temp_strings.replace('[holder_datagraph]\n            ', '')

            temp_strings = self.replace_common_strings(temp_strings, bl_name, lowercase_name)

        return temp_strings


    ##### SERVICE IMPLEMENTATION DEFINITIONS #####

    # Update service implmentation result string
    def add_crud_to_service_implementation(self, bl_interface):
        service_result_strings = ""
        for crud_method in bl_interface:
            if 'I' in crud_method:
                service_result_strings += service_result_strings.join(map(str,self.initialize_service_impl_template()))

            elif 'R' in crud_method:
                service_result_strings += service_result_strings.join(map(str,self.read_service_impl_template()))

            elif 'D' in crud_method:
                service_result_strings += service_result_strings.join(map(str,self.delete_service_impl_template()))

            elif 'V' in crud_method:
                service_result_strings += service_result_strings.join(map(str,self.isvalid_service_impl_template()))

            elif 'GL' in crud_method:
                service_result_strings += service_result_strings.join(map(str,self.getlist_service_impl_template()))

            elif 'G' in crud_method:
                service_result_strings += service_result_strings.join(map(str,self.getcontrolstate_service_impl_template()))

            # TODO: for get_fetchlist and arconfirmationrequired, etc.
            # elif 'GF' in crud_method:
            #     pass
            # elif 'AC' in crud_method:
            #     pass

        return service_result_strings


    # Add CRUD entity templates to strings
    # service_result_strings
    def add_crud_entity_to_service_implementation(self, bl_interface, keyfields):
        entity_result_strings           = ""
        entity_create_string            = ""
        entity_read_string              = ""
        entity_update_string            = ""
        entity_delete_string            = ""
        return_null_string              = "return null;"
        return_delete_message_string    = "/* Remove this delete entity method if delete crud is not being used. */"
        entity_keyfield_string = ""

        entity_result_strings = entity_result_strings.join(map(str,self.generic_service_impl_entity_template()))

        for crud_method in bl_interface:
            if 'C' in crud_method:
                entity_create_string = entity_create_string.join(map(str,self.create_entity_template()))
                entity_result_strings = entity_result_strings.replace('[create_entity_null]', entity_create_string)

            elif 'R' in crud_method:
                entity_read_string = entity_read_string.join(map(str,self.read_entity_template()))
                entity_result_strings = entity_result_strings.replace('[read_entity_null]', entity_read_string)
                entity_keyfield_string = self.entity_read_keyfield_param(keyfields)
                entity_result_strings = entity_result_strings.replace('[keyfield_param]', entity_keyfield_string)

            elif 'U' in crud_method:
                entity_update_string = entity_update_string.join(map(str,self.update_entity_template()))
                entity_result_strings = entity_result_strings.replace('[update_entity_null]', entity_update_string)

            elif 'D' in crud_method:
                entity_delete_string = entity_delete_string.join(map(str,self.delete_entity_template()))
                entity_result_strings = entity_result_strings.replace('[delete_entity_null]', entity_delete_string)

        # if method doesn't exist, then add 'return null' string
        for crud_method in bl_interface:
            if not 'C' in crud_method:
                entity_result_strings = entity_result_strings.replace('[create_entity_null]', return_null_string)
            if not 'R' in crud_method:
                entity_result_strings = entity_result_strings.replace('[read_entity_null]', return_null_string)
            if not 'U' in crud_method:
                entity_result_strings = entity_result_strings.replace('[update_entity_null]', return_null_string)
            if not 'D' in crud_method:
                entity_result_strings = entity_result_strings.replace('[delete_entity_null]', return_delete_message_string)


        return entity_result_strings


    # Construct type and keyfield parameter e.g. delete(String siteCode, String toolNumber)
    def type_and_keyfield_parameter(self, keyfields, keyfields_type):
        default_string = "{} {}"
        type_and_keyfield_result_string = ""

        for i in range(len(keyfields)):
            type_and_keyfield_result_string += default_string.format(keyfields_type[i], keyfields[i]) + ", "

        type_and_keyfield_result_string = type_and_keyfield_result_string.strip(', ')
        return type_and_keyfield_result_string

    # Create generic service implmentation method for optional method
    def generate_service_impl_methods(self, bl_methods, bl_name, lowercase_name):
        method_name                 = []
        method_return               = []
        method_parameter            = []
        method_input_output         = []
        method_name_result          = ""
        method_return_result        = ""
        method_parameter_result     = ""
        method_input_output_result  = ""

        temp_strings = ""

        for i in range(len(bl_methods)):
            for key, value in bl_methods[i].items():
                # 'method_name': 'CheckPartsForWorkOrderReturn'
                method_name.append(value['method_name'][:1].lower() + value['method_name'][1:])
                # 'method_return': 'void', 'logical'
                if value['method_return'] == 'logical':
                    value['method_return'] = 'boolean'
                method_return.append(value['method_return'])
                # method_parameter: {'siteCode': 'character'}, {'partNumber': 'character'}, {'bl_name': 'dataset-handle'}, {'returnMessage': 'character'}
                method_parameter.append(value['param'])
                # method_input_ouput: 'input', 'input', 'output', 'output'
                method_input_output.append(value['input_output'])

            temp_strings += temp_strings.join(map(str,self.generic_service_impl_template()))
            temp_strings = self.replace_method_name(temp_strings, method_name[i])
            temp_strings = self.replace_method_return(temp_strings, method_return[i])
            temp_strings,isHolder = self.replace_method_input_output_and_parameter(temp_strings, method_input_output[i], method_parameter[i])

            # Possibly no longer need isHolder
            # if isHolder:
            #     holder_string = 'Holder<DataGraph> holder = new Holder<>();'
            #     temp_strings = temp_strings.replace('[holder_datagraph]', holder_string)
            # else:
            #     temp_strings = temp_strings.replace('[holder_datagraph]\n            ', '')

            temp_strings = self.replace_common_strings(temp_strings, bl_name, lowercase_name)

        return temp_strings
        # print(isHolder)
        # print(temp_strings)
        # print(method_name)
        # print(method_return)
        # print(method_parameter)
        # print(method_input_output)






##### TEMPLATES #####
    ##### DATACONTROLLER TEMPLATE #####

    # GET MAPPING
    def get_mapping_template(self):
        get_mapping_template = [
    """
	/**
	 * Handle API request for List and Read actions.
	 */
	@GetMapping(value = "[url_prefix_resource]", produces = MediaType.APPLICATION_JSON_VALUE)
	public View lowercase_nameReadApi(Model model, @ModelAttribute("QueryParameters") QueryParameters queryParameters,
            [request_param]
            @RequestParam(value="initialize", required=false) boolean initialize)
	{
        bl_nameServiceImpl service = ((bl_nameServiceImpl)crudProviderService);
        [filter_container_for_getlist_view_grid]
		if (initialize)
		{
			bl_nameContainer entity = service.initialize([keyfield_param]);
			return getEntityReadView(model, entity);
		}
		else if ([is_null_check])
		{
            [get_entity_list_view_or_grid]
		}
		else if ([is__not_null_check])
		{
			bl_nameContainer entity = service.read([keyfield_param]);
			return getEntityReadView(model, entity);
		}
		else
			throw new IllegalArgumentException(StringCodeConstants.KEY_FIELD_PARAMETER_ALL_NULL_OR_ALL_NON_NULL);
	}
    """
        ]
        return get_mapping_template

    # POST MAPPING
    def post_mapping_template(self):
        post_mapping_template = [
    """
    /**
	 * Handle API submit request for Update and Create actions.
	 */
	@PostMapping(value = "[url_prefix_resource]", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody View lowercase_nameCreateAndUpdateApi(Model model, @RequestBody String json,
			[request_param2])
	{
		bl_nameContainer container = getEntityFromJson(json);

		if ([is_null_check])
		{
			return getEntityCreateView(model, container);
		}
		else if ([is__not_null_check])
		{
			bl_nameServiceImpl service = ((bl_nameServiceImpl)crudProviderService);
			List<bl_name> entityList = service.getEntityListFromContainer(container);
			if (entityList.isEmpty())
				throw new IllegalArgumentException(StringCodeConstants.NO_ENTITY_DATA_SUBMITTED_IN_UPDATE_REQUEST);

			bl_name entity = entityList.get(0);
			if ([ignorecase_check])
				throw new IllegalArgumentException(StringCodeConstants.KEY_FIELD_PARAMETER_MISMATCH);

			return getEntityUpdateView(model, container);
		}
		else
			throw new IllegalArgumentException(StringCodeConstants.KEY_FIELD_PARAMETER_ALL_NULL_OR_ALL_NON_NULL);
	}
    """
        ]
        return post_mapping_template

    # DELETE MAPPING
    def delete_mapping_template(self):
        delete_mapping_template = [
    """
	/**
	 * Handle API request for Delete action.
	 */
	@DeleteMapping(value = "[url_prefix_resource]", produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody View toolDeleteApi(Model model,
			[request_param2])
	{
        bl_nameServiceImpl service = ((bl_nameServiceImpl)crudProviderService);

		if ([is_null_check])
			throw new IllegalArgumentException(StringCodeConstants.KEY_FIELD_PARAMETER_ALL_NULL_OR_ALL_NON_NULL);

		SubmitResultAndData<bl_nameContainer> result = service.delete([keyfield_param]);
		return getEntityDeleteView(model, result);
	}
    """
        ]
        return delete_mapping_template

    # ISVALID MAPPING
    def isvalid_mapping_template(self):
        isvalid_mapping_template = [
    """
	/**
	 * Handle API request for IsValid action
	 */
	@PostMapping(value = "[url_prefix_resource]/isValid", consumes = MediaType.APPLICATION_JSON_VALUE)
	public View isValid(Model model, @RequestBody String json,
		    @RequestParam(value="fieldName", required=true) String fieldName)
	{
		SubmitResult submitResult = new SubmitResult();

		bl_nameContainer container = getEntityFromJson(json);
		bl_nameServiceImpl service = (bl_nameServiceImpl)crudProviderService;
		bl_nameContainer currentEntityData = null;

		SubmitResultAndData<bl_nameContainer> serviceResult = service.isValid(fieldName, container);

        if (serviceResult.getSuccess())
            currentEntityData = serviceResult.getData();
        else
            submitResult.setErrors(serviceResult.getErrors());

		model.addAttribute(MODEL_ATTRIBUTE_SUBMIT_RESULT, submitResult);
		model.addAttribute(MODEL_ATTRIBUTE_DATA, currentEntityData);
		View view = createJsonView(new HashSet<String>(Arrays.asList(MODEL_ATTRIBUTE_SUBMIT_RESULT, MODEL_ATTRIBUTE_DATA)));
		String jsonToReturn = getJsonFromEntity(currentEntityData);
		model.addAttribute(MODEL_ATTRIBUTE_DATA, new JsonDataHolder(jsonToReturn));

		return view;
    }
    """
        ]
        return isvalid_mapping_template

    # GETCONTROLSTATE MAPPING
    def getcontrolstate_mapping_template(self):
        getcontrolstate_mapping_template = [
    """
	/**
	 *  Handle API request for dynamic Enabling/Disabling of fields
	 */
	@PostMapping(value = "[url_prefix_resource]/getControlStates", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody View getControlStates(Model model, @RequestBody String json)
	{
		bl_nameContainer container = getEntityFromJson(json);
		Holder <String> fieldNames = new Holder<>();
		Holder <String> fieldStates = new Holder<>();

		bl_nameServiceImpl service = ((bl_nameServiceImpl)crudProviderService);
		service.getControlStates(container, fieldNames, fieldStates);

		HashSet<String> rendererAttribute = new HashSet<>();

		model.addAttribute("fieldNames", fieldNames.getValue());
		model.addAttribute("fieldStates", fieldStates.getValue());

		return createJsonView(rendererAttribute);
	}
    """
        ]
        return getcontrolstate_mapping_template

    # GETLIST VIEWGRID MAPPING
    def getlist_grid_mapping_template(self):
        getlist_grid_mapping_template = [
    """
    private View getEntityListViewGrid(Model model, FilterContainer filterContainer)
	{
		bl_nameContainer container = ((bl_nameServiceImpl)crudProviderService).getList(filterContainer);
		int count = 0;
		count = getEntityCount(container);

		String jsonToReturn = getJsonFromEntity(container);
		model.addAttribute(MODEL_ATTRIBUTE_DATA, new JsonDataHolder(jsonToReturn));
		model.addAttribute(MODEL_ATTRIBUTE_COUNT, count);

		return createJsonView(new HashSet<String>(Arrays.asList(MODEL_ATTRIBUTE_DATA, MODEL_ATTRIBUTE_COUNT)));
	}
    """
        ]
        return getlist_grid_mapping_template

    def generic_datacontroller_template(self):
        datacontroller_template = [
    """
    /**
	 *  Handle API request for
	 */
	[request_mapping_api]
	public View [method_name](Model model, @RequestBody String json,
			 [request_param])
	{
		SubmitResult submitResult = new SubmitResult();

		bl_nameServiceImpl service = ((bl_nameServiceImpl)crudProviderService);
        Holder <String> returnMessage = new Holder<>();
        Set<String> set = new HashSet<>();
        set.add(AssetmgmtConstants.RETURN_MESSAGE);
        set.add(MODEL_ATTRIBUTE_SUBMIT_RESULT);

        try {
            service.[method_service_name];
            model.addAttribute(AssetmgmtConstants.RETURN_MESSAGE, returnMessage.getValue());
        } catch (ApiException apie) {
            logger.error(apie.getMessage());
            ExceptionUtil.convertException(apie, submitResult);
        }

        model.addAttribute(MODEL_ATTRIBUTE_SUBMIT_RESULT, submitResult);
        return createJsonView(set);

    }
    """
        ]
        return datacontroller_template



# SERVICE IMPLEMENTATION TEMPLATE
    def initialize_service_impl_template(self):
        initialize_service_impl_template = [
    """
    public bl_nameContainer initialize([type_and_field_param])
    {
		bl_nameFactory factory = getEntityFactory();
		bl_nameContainer container = factory.createbl_nameContainer();
		Holder<DataGraph> holder = new Holder<>();
		factory.getbl_nameService().initialize([keyfield_param], holder);
		((Qrabl_nameContainer)container).setProDataGraph((ProDataGraph)holder.getValue());

		return container;
    }
    """
        ]
        return initialize_service_impl_template

    def read_service_impl_template(self):
        read_service_impl_template = [
    """
    public bl_nameContainer read([type_and_field_param])
	{
        bl_nameContainer entity = null;
        try
        {
			Holder<DataGraph> holder = new Holder<>();
			bl_nameFactory factory = getEntityFactory();
			entity = factory.createbl_nameContainer();
			factory.getbl_nameService().fetch([keyfield_param], holder);
			((Qrabl_nameContainer)entity).setProDataGraph((ProDataGraph) holder.getValue());
		}
        catch (ApiException apie)
        {
			if (ExceptionUtil.isMessageNumber(apie, 5))
				entity = null;
			else
				throw apie;
		}
		return entity;
    }
    """
        ]
        return read_service_impl_template

    def delete_service_impl_template(self):
        delete_service_impl_template = [
    """
    public SubmitResultAndData<bl_nameContainer> delete([type_and_field_param])
	{
		SubmitResultAndData<bl_nameContainer> submitResultAndData = new SubmitResultAndData<>();
		bl_nameContainer resultContainer = null;

		try
		{
			bl_nameFactory factory = getEntityFactory();
			bl_nameContainer container = factory.createbl_nameContainer();
			container.addbl_name([keyfield_param]);
			factory.getbl_nameService().delete(((Qrabl_nameContainer)container).getProDataGraph());
			resultContainer = factory.createbl_nameContainer();
		}
		catch (ApiException apie)
		{
			ExceptionUtil.convertException(apie, submitResultAndData);
		}

		submitResultAndData.setData(resultContainer);
		return submitResultAndData;
    }
    """
        ]
        return delete_service_impl_template

    def isvalid_service_impl_template(self):
        isvalid_service_impl_template = [
    """
    public SubmitResultAndData<bl_nameContainer> isValid(String fieldName, bl_nameContainer container)
	{
		SubmitResultAndData<bl_nameContainer> submitResult = new SubmitResultAndData<>();

		try
		{
			bl_nameFactory factory = getEntityFactory();
			Holder<DataGraph> holder = new Holder<>();
			holder.setValue(((Qrabl_nameContainer)container).getProDataGraph());
			factory.getbl_nameService().isValid(fieldName, holder);
			((Qrabl_nameContainer)container).setProDataGraph((ProDataGraph)holder.getValue());
			submitResult.setData(container);
			submitResult.getErrors().addAll(getWarningInfoMessages(container));
		}
		catch (ApiException apie)
		{
			ExceptionUtil.convertException(apie, submitResult);
		}
		catch (RuntimeException re)
		{
			if (re.getCause() instanceof ApiException)
			{
				ApiException apie = (ApiException)re.getCause();
				ExceptionUtil.convertException(apie, submitResult);
			}
			else
				throw re;
		}
		return submitResult;
    }
    """
        ]
        return isvalid_service_impl_template

    def getlist_service_impl_template(self):
        getlist_service_impl_template = [
    """
    public bl_nameContainer getList(FilterContainer filterContainer)
	{
		Qrabl_nameContainer container = null;
		try
		{
			Qrabl_nameFactory factory = (Qrabl_nameFactory)getEntityFactory();
			container = factory.createbl_nameContainer();
			Holder<DataGraph> holder = new Holder<>();
			factory.getbl_nameService().getList(((QraFilterContainer)filterContainer).getProDataGraph(), holder);
			container.setProDataGraph((ProDataGraph)holder.getValue());
		}
		catch (ApiException apie)
		{
			ExceptionUtil.reportException(apie);
		}

		return container;
	}
    """
        ]
        return getlist_service_impl_template

    def get_fetchlist_service_impl_template(self):
        get_fetchlist_service_impl_template = [
    """
    @Override
    public List<bl_nameContainer> getList(QueryParameters queryParameters) {

        TODO: create a method to generate these filter values
        String partNumber = queryParameters.getFilterValue("partNumber");
        String siteCode = queryParameters.getFilterValue("siteCode");

        List<bl_nameContainer> detailList = new ArrayList<>();

        try
        {
            Qrabl_nameFactory bl_nameFactory = (Qrabl_nameFactory)getEntityFactory();
            Qrabl_nameContainer container = bl_nameFactory.createbl_nameContainer();
            Holder<DataGraph> holder = new Holder<>();
            bl_nameFactory.getbl_nameService().fetchList([keyfield_param], holder);
            container.setProDataGraph((ProDataGraph)holder.getValue());
            detailList.add(container);
        } catch (ApiException apiException) {
            ExceptionUtil.reportException(apiException);
        }

        return detailList;
    }
    """
        ]
        return get_fetchlist_service_impl_template

    def getcontrolstate_service_impl_template(self):
        getcontrolstate_service_impl_template = [
    """
	public void getControlStates(bl_nameContainer container, Holder <String> fieldNames, Holder <String> fieldStates)
	{
		bl_nameFactory factory = getEntityFactory();
		factory.getbl_nameService().getControlStates(((Qrabl_nameContainer)container).getProDataGraph(), fieldNames, fieldStates);
    }
    """
        ]
        return getcontrolstate_service_impl_template

    def areconfirmationrequired_service_impl_template(self):
        areconfirmationrequired_service_impl_template = [
    """
	@Override
	protected boolean areConfirmationsRequired(bl_nameConfContainer confirmationContainer) {
		boolean confirmationsRequired = false;
		Collection<? extends bl_nameConf> confirmations = confirmationContainer.lowercase_nameConfs().values();
		for (bl_nameConf confirmation : confirmations) {
			if ((confirmation.getDeleteConfirmMsg() != null && !confirmation.getIsDeleteConfirmed()) ||
				(confirmation.getUpdateConfirmMsg() != null && !confirmation.getIsUpdateConfirmed()))
				confirmationsRequired = true;
		}
		return confirmationsRequired;
	}
    """
        ]
        return areconfirmationrequired_service_impl_template

    def create_entity_template(self):
        create_entity_template = [
    """bl_nameFactory factory = getEntityFactory();
        factory.getbl_nameService().create(((Qrabl_nameContainer)container).getProDataGraph());
	    return container;
    """
        ]
        return create_entity_template

    def read_entity_template(self):
        read_entity_template = [
    """bl_name entity = getEntityListFromContainer(container).get(0);
	    return read([keyfield_param]);
    """
        ]
        return read_entity_template

    def update_entity_template(self):
        update_entity_template = [
    """bl_nameFactory factory = getEntityFactory();
	    factory.getbl_nameService().update(((Qrabl_nameContainer)container).getProDataGraph());
	    return container;
    """
        ]
        return update_entity_template

    def delete_entity_template(self):
        delete_entity_template = [
    """bl_nameFactory factory = getEntityFactory();
        factory.getbl_nameService().delete(((Qrabl_nameContainer)container).getProDataGraph());
    """
        ]
        return delete_entity_template

    def generic_service_impl_entity_template(self):
        generic_service_impl_entity_template = [
    """
    @Override
    protected bl_nameContainer createEntity(bl_nameContainer container)
    {
        [create_entity_null]
    }

    @Override
    protected bl_nameContainer readEntity(bl_nameContainer container)
    {
        [read_entity_null]
    }

    @Override
    protected bl_nameContainer updateEntity(bl_nameContainer container)
    {
        [update_entity_null]
    }

    @Override
    protected void deleteEntity(bl_nameContainer container)
    {
        [delete_entity_null]
    }
    """
        ]
        return generic_service_impl_entity_template

    def generic_service_impl_template(self):
        service_impl_template = [
    """
    public [method_return] [method_name]([method_parameter])
    {
        Qrabl_nameFactory lowercase_nameFactory = (Qrabl_nameFactory) getEntityFactory();
        bl_nameService lowercase_nameService = lowercase_nameFactory.getbl_nameService();
        lowercase_nameService.[method_name]([service_parameter]);
    }
    """
        ]
        return service_impl_template


# Uncomment to run this generator
jg = Java_Generator()
