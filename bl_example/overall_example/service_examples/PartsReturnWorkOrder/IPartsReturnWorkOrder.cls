/*
 * com.qad.assetmgmt.inventory.parts.IPartsReturnWorkOrder - Interface for PartsReturnWorkOrder API 
 *
 * Copyright 1986 QAD Inc. All rights reserved.
 *
 * $Id: IPartsReturnWorkOrder.cls 286660 2020-08-03 18:09:06Z jpm $
 */ 

/** 
 * @wsnamespace urn:services-qad-com:assetmgmt:IPartsReturnWorkOrder:2020-07-21
 * @entityname RETURN_TO_WORK_ORDER
 * @parent com.qad.assetmgmt.inventory.parts.IParts
 */

interface com.qad.assetmgmt.inventory.parts.IPartsReturnWorkOrder: 

   {com/qad/assetmgmt/inventory/parts/dsPartsReturnWorkOrder.i}

   /**
    * Check to see if fabricated parts can return to workorder.
    *
    * @param siteCode
    * @param partNumber
    * @param dsPartsReturnWorkOrder
    * @param returnMessage
    *
    * @external
    *
    * @example
    *   
    *   using com.qad.assetmgmt.inventory.parts.IPartsReturnWorkOrder.
    *   using com.qad.assetmgmt.AssetMgmtServices.
    *
    *   {com/qad/assetmgmt/inventory/parts/dsPartsReturnWorkOrder.i}
    *
    *   define variable partsReturnWorkOrderService as IPartsReturnWorkOrder no-undo.
    *
    *   partsReturnWorkOrderService = AssetMgmtServices:GetPartsReturnWorkOrder().
    *
    *   partsReturnWorkOrderService:CheckPartsForWorkOrderReturn(input '10-100',
    *                                                            input 'HUB-010',
    *                                                            output dataset dsPartsReturnWorkOrder by-reference,
    *                                                            output 'Success').
    *   
    */  
   method public void CheckPartsForWorkOrderReturn(input siteCode as character,
                                                   input partNumber as character,
                                                   output dataset-handle dsPartsReturnWorkOrder,
                                                   output returnMessage as character).
    
   /**
    * Get default locations, quantities, and costs associated with fabricated work order for return.
    *
    * @param dsPartsReturnWorkOrder
    *
    * @external
    *
    * @example
    *   
    *   using com.qad.assetmgmt.inventory.parts.IPartsReturnWorkOrder.
    *   using com.qad.assetmgmt.AssetMgmtServices.
    *
    *   {com/qad/assetmgmt/inventory/parts/dsPartsReturnWorkOrder.i}
    *
    *   define variable partsReturnWorkOrderService as IPartsReturnWorkOrder no-undo.
    *
    *   partsReturnWorkOrderService = AssetMgmtServices:GetPartsReturnWorkOrder().
    *
    *   partsReturnWorkOrderService:GetDefaultWorkOrderForReturn(input-output dataset dsPartsReturnWorkOrder by-reference).
    *   
    */  
   method public void GetDefaultWorkOrderForReturn(input-output dataset-handle dsPartsReturnWorkOrder).
    
   /**
    * Get parts location.
    *
    * @param siteCode
    * @param partNumber
    * @param partsLocation
    *
    * @external
    *
    * @example
    *   
    *   using com.qad.assetmgmt.inventory.parts.IPartsReturnWorkOrder.
    *   using com.qad.assetmgmt.AssetMgmtServices.
    *
    *   define variable partsReturnWorkOrderService as IPartsReturnWorkOrder no-undo.
    *
    *   partsReturnWorkOrderService = AssetMgmtServices:GetPartsReturnWorkOrder().
    *
    *   partsReturnWorkOrderService:GetPartsLocation(input '10-100',
    *                                                input 'HUB-010',
    *                                                output '1A-D-B1').
    *   
    */  
   method public void GetPartsLocation(input siteCode as character,
                                       input partNumber as character,
                                       output partsLocation as character).
    
   /**
    * Get serial number for rotable parts.
    *
    * @param siteCode
    * @param partNumber
    * @param serializedParts
    *
    * @external
    *
    * @example
    *   
    *   using com.qad.assetmgmt.inventory.parts.IPartsReturnWorkOrder.
    *   using com.qad.assetmgmt.AssetMgmtServices.
    *
    *   define variable partsReturnWorkOrderService as IPartsReturnWorkOrder no-undo.
    *
    *   partsReturnWorkOrderService = AssetMgmtServices:GetPartsReturnWorkOrder().
    *
    *   partsReturnWorkOrderService:GetSerializedParts(input '10-100',
    *                                                  input 'HUB-010',
    *                                                  output 'B1A1QERA').
    *   
    */  
   method public void GetSerializedParts(input siteCode as character,
                                         input partNumber as character,
                                         output serializedParts as character).
    
   /**
    * Return fabricated parts to work order.
    *
    * @param dsPartsReturnWorkOrder
    *
    * @external
    *
    * @example
    *   
    *   using com.qad.assetmgmt.inventory.parts.IPartsReturnWorkOrder.
    *   using com.qad.assetmgmt.AssetMgmtServices.
    *
    *   {com/qad/assetmgmt/inventory/parts/dsPartsReturnWorkOrder.i}
    *
    *   define variable partsReturnWorkOrderService as IPartsReturnWorkOrder no-undo.
    *
    *   partsReturnWorkOrderService = AssetMgmtServices:GetPartsReturnWorkOrder().
    *
    *   partsReturnWorkOrderService:UpdateReturnToWorkOrder(input dataset dsPartsReturnWorkOrder by-reference).
    *   
    */  
   method public void UpdateReturnToWorkOrder(input dataset-handle dsPartsReturnWorkOrder).
    
   /**
    * Get on hand quantity from locations.
    *
    * @param siteCode
    * @param partNumber
    * @param location
    * @param onHandQuantity
    *
    * @external
    *
    * @example
    *   
    *   using com.qad.assetmgmt.inventory.parts.IPartsReturnWorkOrder.
    *   using com.qad.assetmgmt.AssetMgmtServices.
    *
    *   define variable partsReturnWorkOrderService as IPartsReturnWorkOrder no-undo.
    *
    *   partsReturnWorkOrderService = AssetMgmtServices:GetPartsReturnWorkOrder().
    *
    *   partsReturnWorkOrderService:GetOnHandQuantityFromLocation(input '10-100',
    *                                                             input 'AID-010',
    *                                                             input '1A-D-B1',
    *                                                             output 3).
    *   
    */  
   method public void GetOnHandQuantityFromLocation(input siteCode as character,
                                                    input partNumber as character,
                                                    input location as character,
                                                    output onHandQuantity as integer).
    
 
end interface.
