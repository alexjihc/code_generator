/*
 * com.qad.assetmgmt.inventory.parts.PartsReturnWorkOrder - API implementation of IPartsReturnWorkOrder interface 
 *
 * Copyright 1986 QAD Inc. All rights reserved.
 *
 * $Id: PartsReturnWorkOrder.cls 3000 2020-07-21 15:56:35Z a3q $
 */

using com.qad.assetmgmt.AssetMgmtConstants.
using com.qad.assetmgmt.AssetMgmtError.
using com.qad.assetmgmt.AssetMgmtMessageKey.
using com.qad.assetmgmt.AssetMgmtTranslatedString.
using com.qad.assetmgmt.AssetMgmtUtils.
using com.qad.assetmgmt.inventory.parts.IPartsReturnWorkOrder.
using com.qad.assetmgmt.utility.UserSession.

using com.qad.lang.IList.
using com.qad.lang.IMessage.
using com.qad.lang.List.
using com.qad.lang.Message.

using com.qad.qra.QraServices.
using com.qad.qra.base.ImplementationBase.

block-level on error undo, throw.

class com.qad.assetmgmt.inventory.parts.PartsReturnWorkOrder inherits ImplementationBase implements IPartsReturnWorkOrder: 
	         
   {com/qad/assetmgmt/common/dsMessage.i}
   {com/qad/assetmgmt/common/dsSubmitError.i}
   {com/qad/assetmgmt/common/dsParts.i &PREFIX=Native}
   {com/qad/assetmgmt/common/dsEAMWorkOrder.i}   
   {com/qad/assetmgmt/common/dsEAMPartsLoc.i}   
   {com/qad/assetmgmt/common/dsSerialPt.i}   
   {com/qad/assetmgmt/inventory/parts/dsPartsReturnWorkOrder.i}
   
   method protected void CheckPartsReturnWorkOrders (input dataset dsPartsReturnWorkOrder):
   end method. /* CheckPartsReturnWorkOrders */
   
   method public void CheckPartsForWorkOrderReturn(input siteCode as character,
                                                   input partNumber as character,
                                                   output dataset-handle dsPartsReturnWorkOrder,
                                                   output returnMessage as character):

      {com/qad/qra/base/OutDSPre.i &dataset = "dsPartsReturnWorkOrder"}      
      this-object:CheckPartsForWorkOrderReturnPrivate(input siteCode,
                                                      input partNumber,
                                                      output dataset-handle dsPartsReturnWorkOrderTemp by-reference,
                                                      output returnMessage).      
      {com/qad/qra/base/OutDSPost.i &dataset = "dsPartsReturnWorkOrder"}
      
      finally:
         delete object dsPartsReturnWorkOrder no-error.
      end finally.
      
   end method.

   method protected void CheckPartsForWorkOrderReturnPrivate(input siteCode as character,
                                                             input partNumber as character,
                                                             output dataset dsPartsReturnWorkOrder,
                                                             output returnMessage as character):
      
      define variable assetMgmtError     as AssetMgmtError no-undo.
      define variable userSessionService as UserSession    no-undo.
      define variable validateStatus     as integer        no-undo.
      define variable counter            as integer        no-undo.  

      userSessionService = new UserSession().      
      
      if userSessionService:LoggedIn then do on error undo, throw:      
         
         returnMessage = "".

         dataset dsNativeParts:empty-dataset().
         run inventory/parts/si/getByID.p (input partNumber,
                                           input siteCode,
                                           output dataset dsNativeParts by-reference,
                                           input userSessionService:SessionID).

         find ttNativeParts no-error.     
         if not available(ttNativeParts) then do:
            assetMgmtError = new AssetMgmtError(new Message(input AssetMgmtMessageKey:RECORD_DOES_NOT_EXIST, AssetMgmtTranslatedString:PARTS)).
            undo, throw assetMgmtError.    
         end.

         run inventory/parts/si/CheckCanReturnToWorkOrder.p (input dataset dsNativeParts by-reference,
                                                             input-output validateStatus,
                                                             output dataset dsMessage by-reference,
                                                             input userSessionService:SessionID).
         
         counter = 0.
         for each ttMessage:
            assetMgmtError = new AssetMgmtError(new Message(counter, ttMessage.MsgText)).
            undo, throw assetMgmtError.
         end. 

         create ttPartsReturnWorkOrder.
         assign
            ttPartsReturnWorkOrder.SiteCode              = siteCode
            ttPartsReturnWorkOrder.PartNumber            = partNumber
            ttPartsReturnWorkOrder.DateReturned          = today
            ttPartsReturnWorkOrder.TotalOnHandQuantity   = ttNativeParts.curr_qty
            ttPartsReturnWorkOrder.IsRotable             = ttNativeParts.serialized.

         AssetMgmtUtils:ReplaceUnknownValues(buffer ttPartsReturnWorkOrder:handle).

      end.
   
   end method.
    
   method public void GetDefaultWorkOrderForReturn(input-output dataset-handle dsPartsReturnWorkOrder):

      {com/qad/qra/base/InAndInOutDSPre.i &dataset = "dsPartsReturnWorkOrder"}      
      this-object:GetDefaultWorkOrderForReturnPrivate(input-output dataset-handle dsPartsReturnWorkOrderTemp by-reference).      
      {com/qad/qra/base/InOutDSPost.i &dataset = "dsPartsReturnWorkOrder"}
      
      finally:
         delete object dsPartsReturnWorkOrder no-error.
      end finally.
      
   end method.

   method protected void GetDefaultWorkOrderForReturnPrivate(input-output dataset dsPartsReturnWorkOrder):
      
      define variable assetMgmtError     as AssetMgmtError no-undo.
      define variable userSessionService as UserSession    no-undo.
      define variable unitOfMeasureCost  as decimal        no-undo.
      define variable onHandQuantity     as integer        no-undo.
      define variable partsLocation      as character      no-undo.
      define variable serializedParts    as character      no-undo.      

      userSessionService = new UserSession().      
      if userSessionService:LoggedIn then do on error undo, throw:               
         for each ttPartsReturnWorkOrder:
        
            dataset dsEAMWorkOrder:empty-dataset().

            run maintenance/workorder/si/getByID.p (input ttPartsReturnWorkOrder.SiteCode,
                                                    input ttPartsReturnWorkOrder.WorkOrderNumber,
                                                    output dataset dsEAMWorkOrder by-reference,
                                                    input userSessionService:SessionID).

            find first ttEAMWorkOrder no-error.     
            if not available ttEAMWorkOrder then do:
               assetMgmtError = new AssetMgmtError(new Message(input AssetMgmtMessageKey:RECORD_DOES_NOT_EXIST, AssetMgmtTranslatedString:ASSET_WORK_ORDERS)).
               undo, throw assetMgmtError. 
            end.

            if not ttEAMWorkOrder.fabricated then do:               
               assetMgmtError = new AssetMgmtError(new Message(input AssetMgmtMessageKey:WORK_ORDER_DOES_NOT_HAVE_INTERNAL_PARTS)).
               undo, throw assetMgmtError.    
            end.          

            else if ttEAMWorkOrder.fab_part_no <> ttPartsReturnWorkOrder.PartNumber then do:
               assetMgmtError = new AssetMgmtError(new Message(input AssetMgmtMessageKey:WORK_ORDER_HAS_INVALID_PART_NUMBER)).
               undo, throw assetMgmtError.    
            end.   

            else do:
               if ttPartsReturnWorkOrder.IsRotable then do:                  
                  this-object:GetSerializedParts(input ttPartsReturnWorkOrder.SiteCode,
                                                 input ttPartsReturnWorkOrder.PartNumber,
                                                 output serializedParts).

               end.
               
               this-object:GetPartsLocation(input ttPartsReturnWorkOrder.SiteCode,
                                            input ttPartsReturnWorkOrder.PartNumber,
                                            output partsLocation).

               this-object:GetOnHandQuantityFromLocation(input ttPartsReturnWorkOrder.SiteCode,
                                                         input ttPartsReturnWorkOrder.PartNumber,
                                                         input ttPartsReturnWorkOrder.Location,
                                                         output onHandQuantity).

               run inventory/inventorytransaction/si/GetWorkOrderReceiptUOMCost.p (input ttPartsReturnWorkOrder.WorkOrderNumber,
                                                                                   input ttPartsReturnWorkOrder.SiteCode,
                                                                                   output unitOfMeasureCost,
                                                                                   input userSessionService:SessionID).               
            
               assign
                  ttPartsReturnWorkOrder.SourceSite         = ttEAMWorkOrder.source_site
                  ttPartsReturnWorkOrder.WorkOrderStatus    = ttEAMWorkOrder.status_code
                  ttPartsReturnWorkOrder.ReceivedQuantity   = ttEAMWorkOrder.qty_rec
                  ttPartsReturnWorkOrder.OnHandQuantity     = onHandQuantity
                  ttPartsReturnWorkOrder.SerialNumber       = serializedParts
                  ttPartsReturnWorkOrder.CostPerItem        = unitOfMeasureCost
                  ttPartsReturnWorkOrder.Location           = partsLocation
                  ttPartsReturnWorkOrder.DateReturned       = today.
            end.
         end.
      end.
   
   end method.

   method public void GetPartsLocation(input siteCode as character,
                                       input partNumber as character,
                                       output partsLocation as character):
      define variable userSessionService as UserSession no-undo.

      userSessionService = new UserSession().      
      do on error undo, throw:                    
         run inventory/partsloc/si/GetLocationList.p (input siteCode,                                                       
                                                      input partNumber,
                                                      output partsLocation,
                                                      input userSessionService:SessionID).
            
      end.
         
   end method.

   method public void GetSerializedParts(input siteCode as character,
                                         input partNumber as character,
                                         output serializedParts as character):
      define variable userSessionService as UserSession no-undo.

      userSessionService = new UserSession().      
      do on error undo, throw:            
         run inventory/serialpt/si/GetSerializedPartsList.p (input siteCode,
                                                             input partNumber,
                                                             output serializedParts,
                                                             input userSessionService:SessionID).
            
      end.
         
   end method.

   method public void GetOnHandQuantityFromLocation(input siteCode as character,
                                                    input partNumber as character,
                                                    input location as character,
                                                    output onHandQuantity as integer):
      define variable userSessionService as UserSession no-undo.

      userSessionService = new UserSession().      
      do on error undo, throw:       
         run inventory/partsloc/si/GetOnHandQuantity.p (input siteCode,
                                                        input partNumber,
                                                        input location,
                                                        output onHandQuantity,
                                                        input userSessionService:SessionID).
      end.

   end method.
    
   method public void UpdateReturnToWorkOrder(input dataset-handle dsPartsReturnWorkOrder):

      {com/qad/qra/base/InAndInOutDSPre.i &dataset = "dsPartsReturnWorkOrder"}      
      this-object:UpdateReturnToWorkOrderPrivate(input dataset-handle dsPartsReturnWorkOrderTemp by-reference).
      
      finally:
         delete object dsPartsReturnWorkOrder no-error.
      end finally.
      
   end method.

   method protected void UpdateReturnToWorkOrderPrivate(input dataset dsPartsReturnWorkOrder):
      
      define variable assetMgmtError              as AssetMgmtError no-undo.
      define variable msg                         as IMessage       no-undo.
      define variable valMsgs                     as IList          no-undo.
      define variable validateStatus              as integer        no-undo.
      define variable counter                     as integer        no-undo.  
      define variable userSessionService          as UserSession    no-undo.
      define variable effectiveDate               as date           no-undo.   
      define variable isReopenWorkOrder           as logical        no-undo.           
      define variable isWorkOrderStatusAvailable  as logical        no-undo.           

      valMsgs = new List().
      userSessionService = new UserSession().
      
      if userSessionService:LoggedIn then do on error undo, throw:               
         for each ttPartsReturnWorkOrder:
            run inventory/partsloc/si/ValidateOnReturnToWorkOrder.p (input ttPartsReturnWorkOrder.QuantityToReturn,
                                                                     input ttPartsReturnWorkOrder.PartNumber,
                                                                     input ttPartsReturnWorkOrder.SiteCode,
                                                                     input ttPartsReturnWorkOrder.Location,
                                                                     input ttPartsReturnWorkOrder.WorkOrderNumber,
                                                                     input ttPartsReturnWorkOrder.SourceSite,
                                                                     input ttPartsReturnWorkOrder.DateReturned,
                                                                     output effectiveDate,
                                                                     output validateStatus,
                                                                     output dataset dsMessage by-reference,
                                                                     input userSessionService:SessionID).    

            counter = 0.                                                                     
            for each ttMessage:
               counter = counter + 1.
               msg = new Message(counter, ttMessage.MsgText).
               valMsgs:Add(msg).
            end.         
            if not valMsgs:IsEmpty()
            then do:
               undo, throw new AssetMgmtError(valMsgs).
            end.

            if ttPartsReturnWorkOrder.WorkOrderStatus = AssetMgmtConstants:DOCUMENT_CLOSED and ttPartsReturnWorkOrder.IsCancelQuantity <> true then do:

               if ttPartsReturnWorkOrder.NewWorkOrderStatus = AssetMgmtConstants:EMPTY_STATUS then do:
                     assetMgmtError = new AssetmgmtError(new Message(input AssetMgmtMessageKey:WORK_ORDER_STATUS_REQUIRED)).
                     undo, throw assetMgmtError.
               end.

               run maintenance/wostatus/si/checkWOStatus.p (input ttPartsReturnWorkOrder.NewWorkOrderStatus,
                                                            output isWorkOrderStatusAvailable,
                                                            input userSessionService:SessionID).

               if not isWorkOrderStatusAvailable then do:
                     assetMgmtError = new AssetmgmtError(new Message(input AssetMgmtMessageKey:WORK_ORDER_STATUS_NOT_AVAILABLE)).
                     undo, throw assetMgmtError.
      
               end.                        
            end.
            
            run maintenance/workorder/si/CheckIfNeedToReopen.p (input ttPartsReturnWorkOrder.SiteCode,
                                                                input ttPartsReturnWorkOrder.WorkOrderNumber,
                                                                input ttPartsReturnWorkOrder.QuantityToReturn, 
                                                                input ttPartsReturnWorkOrder.IsCancelQuantity, 
                                                                output isReopenWorkOrder,
                                                                input userSessionService:SessionID).

            run inventory/inventorytransaction/si/ReturnToWorkorder.p (input ttPartsReturnWorkOrder.SiteCode,
                                                                       input ttPartsReturnWorkOrder.WorkOrderNumber,
                                                                       input ttPartsReturnWorkOrder.Location,
                                                                       input ttPartsReturnWorkOrder.DateReturned,
                                                                       input ttPartsReturnWorkOrder.QuantityToReturn,
                                                                       input ttPartsReturnWorkOrder.CostPerItem,
                                                                       input ttPartsReturnWorkOrder.SerialNumber,
                                                                       input ttPartsReturnWorkOrder.IsCancelQuantity,
                                                                       input ttPartsReturnWorkOrder.NewWorkOrderStatus,
                                                                       input effectiveDate,
                                                                       input time,
                                                                       input-output validateStatus,
                                                                       input-output dataset dsSubmitError by-reference,
                                                                       input userSessionService:SessionID).               
            
            for each ttSubmitError:
               counter = counter + 1.
               msg = new Message(counter, ttSubmitError.MessageText).
               
               if MessageType = AssetMgmtConstants:MESSAGE_TYPE_WARNING then do:
                  QraServices:GetSupplementaryMessageLogger():Warn(ttSubmitError.MessageText).
               end.                    
               else do: 
                  msg = new Message(counter, ttSubmitError.MessageText).                                                     
                  valMsgs:Add(msg).
               end.                                                                                        
            end.                                   
            
            if not valMsgs:IsEmpty() then do:                               
               undo, throw new AssetMgmtError(valMsgs).
            end. 
         end.
      end.
   
   end method.
    
end class.