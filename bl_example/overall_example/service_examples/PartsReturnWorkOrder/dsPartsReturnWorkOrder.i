/*
 * dsPartsReturnWorkOrder.i - Dataset Definition for PartsReturnWorkOrder
 *
 * Copyright 1986 QAD Inc. All rights reserved.
 *
 * $Id: dsPartsReturnWorkOrder.i 286660 2020-08-03 18:09:06Z jpm $
 */

&IF DEFINED (ds{&PREFIX}PartsReturnWorkOrder_i) = 0 &THEN
&GLOBAL-DEFINE ds{&PREFIX}PartsReturnWorkOrder_i

define temp-table tt{&PREFIX}PartsReturnWorkOrder no-undo serialize-name "PartsReturnWorkOrder" {&REFERENCE-ONLY}

  /**
   * Site
   *
   * @label mfg-SITE
   * @format X(8)
   */
   field SiteCode as character initial ?

  /**
   * Part Number
   *
   * @label mfg-ITEM
   * @format x(20)
   */
   field PartNumber as character initial ?

  /**
   * Work Order
   *
   * @label WORK_ORDER   
   * @format >>>>>>>>9
   */
   field WorkOrderNumber as integer initial ?

  /**
   * Location
   *
   * @label eam-5391acc7-88d8-ee4a-981a-ddd56ea3b5f9
   * @datalist list:assetmgmt:inventory:parts:Location
   * @format x(20)
   */
   field Location as character initial ?

  /**
   * On Hand
   *
   * @label mfg-ON_HAND
   * @format >,>>>,>>>9
   * @readonly
   */
   field OnHandQuantity as integer initial ?

  /**
   * Status
   *
   * @label mfg-STATUS
   * @format x(20)
   */
   field NewWorkOrderStatus as character initial ?  

  /**
   * Date Returned
   *
   * @label eam-ba5c81e2-933a-cf86-da11-6bea6490ebe9
   * @format 99/99/9999
   */
   field DateReturned as date initial ?

  /**
   * Quantity To Return
   *
   * @label mfg-QUANTITY_TO_RETURN
   * @format >,>>>,>>>9
   * @required
   */
   field QuantityToReturn as integer initial ?

  /**
   * Source Site
   *
   * @label eam-e96be5ae-01d5-63b3-dd11-547051e43e84
   * @format X(8)
   */
   field SourceSite as character initial ?

  /**
   * Cost Per Item
   *
   * @label COST_PER_ITEM
   * @format >>>,>>>,>>>.99
   */
   field CostPerItem as decimal initial ?

  /**
   * Cancel
   *
   * @label CANCEL_QUANTITY
   * @format mfg-YES/mfg-NO
   */
   field IsCancelQuantity as logical initial ?

  /**
   * Received Quantity
   *
   * @label mfg-QUANTITY_RECEIVED
   * @format >,>>>,>>>.99
   * @readonly
   */
   field ReceivedQuantity as decimal initial ?   

  /**
   * Rotable
   *
   * @label eam-bf7346b4-07d6-6694-e411-2e1e9338af97
   * @format mfg-YES/mfg-NO
   */
   field IsRotable as logical initial ?

  /**
   *  Serial Number
   *
   * @label mfg-SERIAL_NUMBER
   * @datalist list:assetmgmt:inventory:parts:SerialNumber   
   * @format x(30)
   */
   field SerialNumber as character initial ?

  /**
   * Total On Hand Quantity
   *
   * @label mfg-ON_HAND
   * @format >,>>>,>>9.99
   */
   field TotalOnHandQuantity as decimal initial ?   

  /**
   * Status
   *
   * @label mfg-STATUS
   * @format x(20)
   */
   field WorkOrderStatus as character initial ?  

index PartsReturnWorkOrderPrim is primary unique SiteCode PartNumber.

  /**
   * @define datalist
     <DataList>
       <DataListCode>list:assetmgmt:inventory:parts:Location</DataListCode>
       <ServiceCall>com.qad.assetmgmt.inventory.parts.IPartsReturnWorkOrderDataList:Location</ServiceCall>    
     </DataList>
     <DataList>
       <DataListCode>list:assetmgmt:inventory:parts:SerialNumber</DataListCode>
       <ServiceCall>com.qad.assetmgmt.inventory.parts.IPartsReturnWorkOrderDataList:SerialNumber</ServiceCall>    
     </DataList>

   * @generatemetadata
   */
  define dataset ds{&PREFIX}PartsReturnWorkOrder serialize-name "PartsReturnWorkOrders"
      for tt{&PREFIX}PartsReturnWorkOrder.
      
&ENDIF