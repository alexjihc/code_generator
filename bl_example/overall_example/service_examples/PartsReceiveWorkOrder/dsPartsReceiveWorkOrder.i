/*
 * dsPartsReceiveWorkOrder.i - Dataset Definition for PartsReceiveWorkOrder
 *
 * Copyright 1986 QAD Inc. All rights reserved.
 *
 * $Id: dsPartsReceiveWorkOrder.i 286660 2020-08-03 18:09:06Z jpm $
 */

&IF DEFINED (ds{&PREFIX}PartsReceiveWorkOrder_i) = 0 &THEN
&GLOBAL-DEFINE ds{&PREFIX}PartsReceiveWorkOrder_i

define temp-table tt{&PREFIX}PartsReceiveWorkOrder no-undo serialize-name "PartsReceiveWorkOrder" {&REFERENCE-ONLY}

  /**
   * Site
   *
   * @label mfg-SITE
   * @format x(8)
   */
   field SiteCode as character initial ?

  /**
   * Item
   *
   * @label mfg-ITEM
   * @format x(20)
   * @readonly
   */
   field PartNumber as character initial ?
   
  /**
   * Work Order
   *
   * @label WORK_ORDER   
   * @format >>>>>>>>9
   */
   field WorkOrderNumber as integer initial ?

  /**
   * Planned Quantity
   *
   * @label PLANNED_QUANTITY
   * @format >,>>>,>>>.99
   * @readonly
   */
   field PlannedQuantity as decimal initial ?

  /**
   * Received Quantity
   *
   * @label mfg-RECEIVED_QUANTITY
   * @format >,>>>,>>>.99
   * @readonly
   */
   field ReceivedQuantity as decimal initial ?   

  /**
   * Received Cost
   *
   * @label mfg-RECEIVED_COST
   * @format >,>>>,>>9.99
   * @readonly
   */
   field ReceivedCost as decimal initial ?

  /**
   * Location
   *
   * @label eam-5391acc7-88d8-ee4a-981a-ddd56ea3b5f9
   * @datalist list:assetmgmt:inventory:parts:Location
   * @format x(25)
   */
   field Location as character initial ?

  /**
   * On Hand
   *
   * @label mfg-ON_HAND
   * @format >,>>>,>>9.99
   */
   field OnHand as decimal initial ?

  /**
   * Date Received
   *
   * @label mfg-DATE_RECEIVED
   * @format 99/99/9999
   */
   field DateReceived as date initial ?

  /**
   * Quantity To Receive
   *
   * @label mfg-QUANTITY_TO_RECEIVE
   * @format >>>,>>>,>>>.99
   * @required
   */
   field QuantityToReceive as decimal initial ?

  /**
   * Cost Per Item
   *
   * @label COST_PER_ITEM
   * @format >>>,>>>,>>>.99
   * @readonly
   */
   field CostPerItem as decimal initial ?
   
  /**
   * Labor
   *
   * @label mfg-LABOR
   * @format >>>,>>>,>>>.99
   * @readonly
   */
   field LaborCost as decimal initial ?

  /**
   * Material
   *
   * @label mfg-MATERIAL
   * @format >>>,>>>,>>>.99
   * @readonly
   */
   field MaterialCost as decimal initial ?
   
  /**
   * Contract
   *
   * @label mfg-CONTRACT
   * @format >>>,>>>,>>>.99
   * @readonly
   */
   field ContractCost as decimal initial ?

  /**
   * Total
   *
   * @label mfg-TOTAL
   * @format >>>,>>>,>>>.99
   * @readonly
   */
   field TotalCost as decimal initial ?   

  /**
   * Inventory Cost
   *
   * @label eam-82fa1a43-be0d-0c84-db11-d8a7c6573a92
   * @format >>>,>>>,>>>.99
   * @readonly
   */
   field InventoryCost as decimal initial ?   
   
  /**
   * Close Date
   *
   * @label mfg-CLOSE_DATE
   * @format 99/99/9999
   */
   field CloseDate as date initial ?
   
  /**
   * Close Time
   *
   * @label mfg-CLOSE_TIME	
   * @format >,>>>,>>>.99
   */
   field CloseTime as integer initial ?
   
  /**
   * Close Date Time
   *
   * @label mfg-LAST_MODIFIED_DATE_TIME-short
   * @format DATETIME
   */
   field CloseDateAndTime as datetime initial ?
  
  /**
   * Rotable
   *
   * @label eam-bf7346b4-07d6-6694-e411-2e1e9338af97
   * @format mfg-YES/mfg-NO
   */
   field IsRotable as logical initial ?
   
  /**
   * Manufacturer
   *
   * @label mfg-MANUFACTURER
   * @format x(30)
   */
   field Manufacturer as character initial ?
   
  /**
   *  Serial
   *
   * @label mfg-SERIAL_NUMBER   
   * @format x(30)
   */
   field SerialNumber as character initial ?

  /**
   * Asset Number
   *
   * @label mfg-ASSET
   * @format x(30)
   */
   field AssetNumber as character initial ?
   
  /**
   * Model
   *
   * @label EAM-MODEL
   * @format x(30)
   */
   field ModelNumber as character initial ?
   
  /**
   * Warranty Date
   *
   * @label mfg-WARRANTY_DATE
   * @format 99/99/9999
   */
   field WarrantyDate as date initial ?

  /**
   * Life Expectancy
   *
   * @label LIFE_EXPECTANCY
   * @format >>>,>>>9
   */
   field LifeExpectancy as integer initial ?


index PartsReceiveWorkOrderPrim is primary unique SiteCode PartNumber.

  /**
   * @define datalist
     <DataList>
       <DataListCode>list:assetmgmt:inventory:parts:Location</DataListCode>
       <ServiceCall>com.qad.assetmgmt.inventory.parts.IPartsReceiveWorkOrderDataList:Location</ServiceCall>    
     </DataList>

   * @generatemetadata
   */
  define dataset ds{&PREFIX}PartsReceiveWorkOrder serialize-name "PartsReceiveWorkOrders"
      for tt{&PREFIX}PartsReceiveWorkOrder.
      
&ENDIF