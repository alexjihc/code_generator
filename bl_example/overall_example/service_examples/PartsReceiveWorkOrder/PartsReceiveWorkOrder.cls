/*
 * com.qad.assetmgmt.inventory.parts.PartsReceiveWorkOrder - API implementation of IPartsReceiveWorkOrder interface 
 *
 * Copyright 1986 QAD Inc. All rights reserved.
 *
 * $Id: PartsReceiveWorkOrder.cls 286648 2020-08-03 17:10:59Z jpm $ 
 */

using com.qad.assetmgmt.AssetMgmtConstants.
using com.qad.assetmgmt.AssetMgmtError.
using com.qad.assetmgmt.AssetMgmtMessageKey.
using com.qad.assetmgmt.AssetMgmtTranslatedString.
using com.qad.assetmgmt.AssetMgmtUtils.
using com.qad.assetmgmt.inventory.parts.IPartsReceiveWorkOrder.
using com.qad.assetmgmt.utility.UserSession.

using com.qad.lang.IList.
using com.qad.lang.IMessage.
using com.qad.lang.List.
using com.qad.lang.Message.

using com.qad.qra.QraServices.
using com.qad.qra.base.ImplementationBase.

block-level on error undo, throw.

class com.qad.assetmgmt.inventory.parts.PartsReceiveWorkOrder inherits ImplementationBase implements IPartsReceiveWorkOrder: 
	
   {com/qad/assetmgmt/common/dsFieldState.i}
          
   {com/qad/assetmgmt/common/dsMessage.i}
   {com/qad/assetmgmt/common/dsSubmitError.i}
   {com/qad/assetmgmt/common/dsEAMControlState.i}   
   {com/qad/assetmgmt/common/dsParts.i &PREFIX=Native}
   {com/qad/assetmgmt/common/dsEAMWorkOrder.i}   
   {com/qad/assetmgmt/common/dsEAMPartsLoc.i}   
   {com/qad/assetmgmt/maintenance/workordercostanalysis/dsWorkOrderCostAnalysis.i}
   {com/qad/assetmgmt/inventory/parts/dsPartsReceiveWorkOrder.i}

   method protected void CheckPartsReceiveWorkOrders (input dataset dsPartsReceiveWorkOrder):
   end method. /* CheckPartsReceiveWorkOrders */
   
   method public void CheckPartsForWorkOrderReceive(input siteCode as character,
                                                    input partNumber as character,
                                                    output dataset-handle dsPartsReceiveWorkOrder,
                                                    output returnMessage as character):

      {com/qad/qra/base/OutDSPre.i &dataset = "dsPartsReceiveWorkOrder"}
      this-object:CheckPartsForWorkOrderReceivePrivate(input siteCode,
                                                       input partNumber,
                                                       output dataset-handle dsPartsReceiveWorkOrderTemp by-reference,
                                                       output returnMessage).
      {com/qad/qra/base/OutDSPost.i &dataset = "dsPartsReceiveWorkOrder"}
      
      finally:
         delete object dsPartsReceiveWorkOrder no-error.
      end finally.
      
   end method.

   method protected void CheckPartsForWorkOrderReceivePrivate(input siteCode as character,
                                                              input partNumber as character,
                                                              output dataset dsPartsReceiveWorkOrder,
                                                              output returnMessage as character):
      
      define variable assetMgmtError     as AssetMgmtError no-undo.
      define variable validateStatus     as integer        no-undo.
      define variable counter            as integer        no-undo.  
      define variable userSessionService as UserSession    no-undo.
      
      userSessionService = new UserSession().      

      if userSessionService:LoggedIn then do on error undo, throw:                                                            

         returnMessage = "".
         
         dataset dsNativeParts:empty-dataset().
         run inventory/parts/si/getByID.p (input partNumber,
                                           input siteCode,
                                           output dataset dsNativeParts by-reference,
                                           input userSessionService:SessionID).

         find ttNativeParts no-error.     
         if not available(ttNativeParts) then do:
            assetMgmtError = new AssetMgmtError(new Message(input AssetMgmtMessageKey:RECORD_DOES_NOT_EXIST, AssetMgmtTranslatedString:PARTS)).
            undo, throw assetMgmtError.    
         end.

         if ttNativeParts.source <> "internal" then do:
            returnMessage = AssetMgmtTranslatedString:INTERNAL_PARTS.           
         end.
         else do:
            run inventory/parts/si/CheckCanReceiveFromWorkOrder.p (input dataset dsNativeParts by-reference,
                                                                   input-output validateStatus,
                                                                   output dataset dsMessage by-reference,
                                                                   input userSessionService:SessionID).
            
            for each ttMessage:
               assetMgmtError = new AssetMgmtError(new Message(counter, ttMessage.MsgText)).
               undo, throw assetMgmtError.
            end.            
            
            run maintenance/workorder/si/CheckForOpenFabricatedWorkOrder.p (input partNumber,
                                                                            input siteCode,
                                                                            output validateStatus,
                                                                            output dataset dsMessage by-reference,
                                                                            input userSessionService:SessionID).
            
            for each ttMessage:
               assetMgmtError = new AssetMgmtError(new Message(counter, ttMessage.MsgText)).
               undo, throw assetMgmtError.
            end.             
            
            create ttPartsReceiveWorkOrder.
            assign
               ttPartsReceiveWorkOrder.SiteCode       = siteCode
               ttPartsReceiveWorkOrder.PartNumber     = partNumber
               ttPartsReceiveWorkOrder.DateReceived   = today
               ttPartsReceiveWorkOrder.WarrantyDate   = today
               ttPartsReceiveWorkOrder.OnHand         = ttNativeParts.curr_qty
               ttPartsReceiveWorkOrder.IsRotable      = ttNativeParts.serialized.

            AssetMgmtUtils:ReplaceUnknownValues(buffer ttPartsReceiveWorkOrder:handle).

         end.      
      end.
   
   end method.
    
   method public void GetDefaultQtyAndCostsForWorkOrder(input-output dataset-handle dsPartsReceiveWorkOrder):

      {com/qad/qra/base/InAndInOutDSPre.i &dataset = "dsPartsReceiveWorkOrder"}
      this-object:GetDefaultQtyAndCostsForWorkOrderPrivate(input-output dataset-handle dsPartsReceiveWorkOrderTemp by-reference).
      {com/qad/qra/base/InOutDSPost.i &dataset = "dsPartsReceiveWorkOrder"}
      
      finally:
         delete object dsPartsReceiveWorkOrder no-error.
      end finally.
      
   end method.

   method protected void GetDefaultQtyAndCostsForWorkOrderPrivate(input-output dataset dsPartsReceiveWorkOrder):
      
      define variable assetMgmtError     as AssetMgmtError no-undo.
      define variable userSessionService as UserSession    no-undo.
      define variable quantityToReceive  as decimal        no-undo.
      define variable receivedCost       as decimal        no-undo.
      define variable inventoryCost      as decimal        no-undo.
      define variable costPerItem        as decimal        no-undo.
      define variable partsLocation      as character      no-undo.
      
      userSessionService = new UserSession().      

      if userSessionService:LoggedIn then do on error undo, throw:
         
         for each ttPartsReceiveWorkOrder:
        
            dataset dsWorkOrderCostAnalysis:empty-dataset().
            dataset dsEAMWorkOrder:empty-dataset().

            run maintenance/workorder/si/getByID.p (input ttPartsReceiveWorkOrder.SiteCode,
                                                    input ttPartsReceiveWorkOrder.WorkOrderNumber,
                                                    output dataset dsEAMWorkOrder by-reference,
                                                    input userSessionService:SessionID).

            find first ttEAMWorkOrder no-error.     
            if not available ttEAMWorkOrder then do:
               assetMgmtError = new AssetMgmtError(new Message(input AssetMgmtMessageKey:RECORD_DOES_NOT_EXIST, AssetMgmtTranslatedString:ASSET_WORK_ORDERS)).
               undo, throw assetMgmtError. 
            end.

            if not ttEAMWorkOrder.fabricated then do:               
               assetMgmtError = new AssetMgmtError(new Message(input AssetMgmtMessageKey:WORK_ORDER_DOES_NOT_HAVE_INTERNAL_PARTS)).
               undo, throw assetMgmtError.    
            end.          

            else if ttEAMWorkOrder.fab_part_no <> ttPartsReceiveWorkOrder.partNumber then do:
               assetMgmtError = new AssetMgmtError(new Message(input AssetMgmtMessageKey:WORK_ORDER_HAS_INVALID_PART_NUMBER)).
               undo, throw assetMgmtError.    
            end.   

            else do:
               
               run maintenance/workorder/si/GetDefaultQtyToReceive.p (input ttPartsReceiveWorkOrder.WorkOrderNumber,
                                                                      input ttPartsReceiveWorkOrder.SiteCode,
                                                                      output quantityToReceive,
                                                                      input userSessionService:SessionID).

               run inventory/inventorytransaction/si/GetWorkOrderReceivedCost.p (input ttPartsReceiveWorkOrder.WorkOrderNumber,
                                                                                 input ttPartsReceiveWorkOrder.SiteCode,
                                                                                 output receivedCost,
                                                                                 input userSessionService:SessionID).

               this-object:GetPartsLocation(input ttPartsReceiveWorkOrder.SiteCode,
                                            input ttPartsReceiveWorkOrder.PartNumber,
                                            output partsLocation).

               run maintenance/workordercostanalysis/si/getByID.p (input ttPartsReceiveWorkOrder.SiteCode,
                                                                  input ttPartsReceiveWorkOrder.WorkOrderNumber,
                                                                  output dataset dsWorkOrderCostAnalysis by-reference, 
                                                                  input userSessionService:SessionID).
               
               find first ttWorkOrderCostAnalysis no-error.
               if available ttWorkOrderCostAnalysis then do:
                  run maintenance/workordercostanalysis/si/GetInventoryCost.p (input dataset dsWorkOrderCostAnalysis by-reference,
                                                                               output inventoryCost,
                                                                               input userSessionService:SessionID).

               end.     

               run maintenance/workordercostanalysis/si/CalculateUnitCostforWOReceipt.p (input dataset dsWorkOrderCostAnalysis by-reference,
                                                                                         input quantityToReceive,
                                                                                         output costPerItem,
                                                                                         input userSessionService:SessionID).

               if costPerItem < 0 then
                  costPerItem = 0.

               assign
                  ttPartsReceiveWorkOrder.PlannedQuantity   = ttEAMWorkOrder.qty_to_build
                  ttPartsReceiveWorkOrder.ReceivedQuantity  = ttEAMWorkOrder.qty_rec
                  ttPartsReceiveWorkOrder.LaborCost         = ttWorkOrderCostAnalysis.ActLabor
                  ttPartsReceiveWorkOrder.MaterialCost      = ttWorkOrderCostAnalysis.ActMaterial
                  ttPartsReceiveWorkOrder.ContractCost      = ttWorkOrderCostAnalysis.ActContract
                  ttPartsReceiveWorkOrder.TotalCost         = ttWorkOrderCostAnalysis.ActTotal
                  ttPartsReceiveWorkOrder.QuantityToReceive = quantityToReceive
                  ttPartsReceiveWorkOrder.ReceivedCost      = receivedCost
                  ttPartsReceiveWorkOrder.Location          = partsLocation
                  ttPartsReceiveWorkOrder.CostPerItem       = costPerItem
                  ttPartsReceiveWorkOrder.InventoryCost     = inventoryCost
                  ttPartsReceiveWorkOrder.CloseDateAndTime  = now
                  ttPartsReceiveWorkOrder.CloseDate         = today.
            end.
         
         end.              
      
      end.
   
   end method.

   method public void GetPartsLocation(input siteCode as character,
                                       input partNumber as character,
                                       output partsLocation as character):
      
      define variable userSessionService as UserSession    no-undo.

      userSessionService = new UserSession().      
      
      do on error undo, throw:      
         dataset dsEAMPartsLoc:empty-dataset().         
         run inventory/partsloc/si/GetLocationList.p (input siteCode,                                                       
                                                      input partNumber,
                                                      output partsLocation,
                                                      input userSessionService:SessionID).
            
      end.
         
   end method.
    
   method public void UpdateReceiveFromWorkOrder(input dataset-handle dsPartsReceiveWorkOrder,
                                                 output returnMessage as character):

      {com/qad/qra/base/InAndInOutDSPre.i &dataset = "dsPartsReceiveWorkOrder"}
      this-object:UpdateReceiveFromWorkOrderPrivate(input dataset-handle dsPartsReceiveWorkOrderTemp by-reference,
                                                    output returnMessage).
      
      finally:
         delete object dsPartsReceiveWorkOrder no-error.
      end finally.
      
   end method.

   method protected void UpdateReceiveFromWorkOrderPrivate(input dataset dsPartsReceiveWorkOrder,
                                                           output returnMessage as character):
      
      define variable msg                as IMessage       no-undo.
      define variable valMsgs            as IList          no-undo.
      define variable validateStatus     as integer        no-undo.
      define variable counter            as integer        no-undo.  
      define variable userSessionService as UserSession    no-undo.                                              
      define variable validatedDate      as date           no-undo.           
      define variable closeWorkOrder     as logical        no-undo.   
                                                   
      valMsgs = new List().
      userSessionService = new UserSession().                                                        

      if userSessionService:LoggedIn then do on error undo, throw:
         
         for each ttPartsReceiveWorkOrder:         
         
            run inventory/inventorytransaction/si/ValidateOnReceiveFromWorkOrder.p (input ttPartsReceiveWorkOrder.QuantityToReceive,
                                                                                    input ttPartsReceiveWorkOrder.PartNumber,
                                                                                    input ttPartsReceiveWorkOrder.SiteCode,
                                                                                    input ttPartsReceiveWorkOrder.SerialNumber,
                                                                                    input ttPartsReceiveWorkOrder.Manufacturer,
                                                                                    input ttPartsReceiveWorkOrder.DateReceived,
                                                                                    output validatedDate,
                                                                                    output validateStatus,
                                                                                    output dataset dsMessage by-reference,
                                                                                    input userSessionService:SessionID).
            
            for each ttMessage:
               counter = counter + 1.
               msg = new Message(counter, ttMessage.MsgText).
               valMsgs:Add(msg).
            end.         
            if not valMsgs:IsEmpty()
            then do:
               undo, throw new AssetMgmtError(valMsgs).
            end.
            
            run inventory/inventorytransaction/si/ReceiveFromWorkOrder.p (input ttPartsReceiveWorkOrder.SiteCode,
                                                                          input ttPartsReceiveWorkOrder.WorkOrderNumber,
                                                                          input ttPartsReceiveWorkOrder.Location,
                                                                          input ttPartsReceiveWorkOrder.DateReceived,
                                                                          input ttPartsReceiveWorkOrder.QuantityToReceive,
                                                                          input ttPartsReceiveWorkOrder.CostPerItem,                                                                                 
                                                                          input ttPartsReceiveWorkOrder.Manufacturer,
                                                                          input ttPartsReceiveWorkOrder.SerialNumber,
                                                                          input ttPartsReceiveWorkOrder.AssetNumber,
                                                                          input ttPartsReceiveWorkOrder.ModelNumber,
                                                                          input ttPartsReceiveWorkOrder.WarrantyDate,
                                                                          input ttPartsReceiveWorkOrder.LifeExpectancy,
                                                                          output closeWorkOrder,
                                                                          input-output validateStatus,
                                                                          input-output dataset dsSubmitError by-reference,
                                                                          input userSessionService:SessionID).
            
            counter = 0.
            for each ttSubmitError:
               counter = counter + 1.
               msg = new Message(counter, ttSubmitError.MessageText).
               
               if MessageType = AssetMgmtConstants:MESSAGE_TYPE_WARNING then do:
                  QraServices:GetSupplementaryMessageLogger():Warn(ttSubmitError.MessageText).
               end.                    
               else do: 
                  msg = new Message(counter, ttSubmitError.MessageText).                                                     
                  valMsgs:Add(msg).
               end.                                                                                        
            end.                                   
            if not valMsgs:IsEmpty() then do:                               
               undo, throw new AssetMgmtError(valMsgs).
            end. 

            if closeWorkOrder then do:
      
               dataset dsEAMWorkOrder:empty-dataset().

               run maintenance/workorder/si/getByID.p (input ttPartsReceiveWorkOrder.SiteCode,
                                                       input ttPartsReceiveWorkOrder.WorkOrderNumber,
                                                       output dataset dsEAMWorkOrder by-reference,
                                                       input userSessionService:SessionID).

               run maintenance/workorder/si/ChangeStatus.p (input dataset dsEAMWorkOrder by-reference,
                                                            input AssetMgmtConstants:DOCUMENT_CLOSED,
                                                            input ttPartsReceiveWorkOrder.CloseDate,
                                                            input ttPartsReceiveWorkOrder.CloseTime,
                                                            input-output validateStatus,
                                                            output dataset dsMessage by-reference,
                                                            input userSessionService:SessionID).

               for each ttMessage:
                  counter = counter + 1.
                  returnMessage = returnMessage + ttMessage.MsgType + AssetMgmtConstants:DELIMITER_CHARACTER + ttMessage.MsgText + AssetMgmtConstants:DELIMITER_CHARACTER.
               end.
      
            end.
      
         end.
      
      end.
      
   end method.

   method public void GetCalculatedCostPerItem (input siteCode as character,
                                                input workOrderNumber as integer,
                                                input quantityToReceive as integer,
                                                output costPerItem as decimal):
      define variable userSessionService as UserSession    no-undo.

      userSessionService = new UserSession().      
      do on error undo, throw:      
         dataset dsWorkOrderCostAnalysis:empty-dataset().        
         run maintenance/workordercostanalysis/si/getByID.p (input siteCode,
                                                             input workOrderNumber,
                                                             output dataset dsWorkOrderCostAnalysis by-reference, 
                                                             input userSessionService:SessionID).

         find first ttWorkOrderCostAnalysis no-error.
         if available ttWorkOrderCostAnalysis then do:
            run maintenance/workordercostanalysis/si/CalculateUnitCostforWOReceipt.p (input dataset dsWorkOrderCostAnalysis by-reference,
                                                                                      input quantityToReceive,
                                                                                      output costPerItem,
                                                                                      input userSessionService:SessionID).
         end.

         if costPerItem < 0 then
            costPerItem = 0.
      end.

   end method.
    
end class.