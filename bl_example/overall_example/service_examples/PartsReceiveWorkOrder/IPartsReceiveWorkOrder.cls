/*
 * com.qad.assetmgmt.inventory.parts.IPartsReceiveWorkOrder - Interface for PartsReceiveWorkOrder API 
 *
 * Copyright 1986 QAD Inc. All rights reserved.
 *
 * $Id: IPartsReceiveWorkOrder.cls 286660 2020-08-03 18:09:06Z jpm $ 
 */ 
 
 /** 
 * @wsnamespace urn:services-qad-com:assetmgmt:IPartsReceiveWorkOrder:2020-06-03
 * @entityname RECEIVE_FROM_WORK_ORDER
 * @parent com.qad.assetmgmt.inventory.parts.IParts
 */

interface com.qad.assetmgmt.inventory.parts.IPartsReceiveWorkOrder: 

    {com/qad/assetmgmt/inventory/parts/dsPartsReceiveWorkOrder.i}

   /**
    * Check to see if parts can receive against the workorder, and if there open work order associated with fabricated parts.
    *
    * @param siteCode
    * @param partNumber
    * @param dsPartsReceiveWorkOrder
    * @param returnMessage
    *
    * @external
    *
    * @example
    *   
    *   using com.qad.assetmgmt.inventory.parts.IPartsReceiveWorkOrder.
    *   using com.qad.assetmgmt.AssetMgmtServices.
    *
    *   {com/qad/assetmgmt/inventory/parts/dsPartsReceiveWorkOrder.i}
     *
    *   define variable partsReceiveWorkOrderService as IPartsReceiveWorkOrder no-undo.
    *
    *   partsReceiveWorkOrderService = AssetMgmtServices:GetPartsReceiveWorkOrder().
    *
    *   partsReceiveWorkOrderService:CheckPartsForWorkOrderReceive(input '10-100',
    *                                                              input 'HUB-010',
    *                                                              output dataset dsPartsReceiveWorkOrder by-reference,
    *                                                              output returnMessage).
    *   
    */  
   method public void CheckPartsForWorkOrderReceive(input siteCode as character,
                                                    input partNumber as character,
                                                    output dataset-handle dsPartsReceiveWorkOrder,
                                                    output returnMessage as character).
    
   /**
    * Get default quantities and costs associated with fabricated work order.
    *
    * @param dsPartsReceiveWorkOrder
    *
    * @external
    *
    * @example
    *   
    *   using com.qad.assetmgmt.inventory.parts.IPartsReceiveWorkOrder.
    *   using com.qad.assetmgmt.AssetMgmtServices.
    *
    *   {com/qad/assetmgmt/inventory/parts/dsPartsReceiveWorkOrder.i}
     *
    *   define variable partsReceiveWorkOrderService as IPartsReceiveWorkOrder no-undo.
    *
    *   partsReceiveWorkOrderService = AssetMgmtServices:GetPartsReceiveWorkOrder().
    *
    *   partsReceiveWorkOrderService:GetDefaultQtyAndCostsForWorkOrder(input-output dataset dsPartsReceiveWorkOrder by-reference).
    *   
    */  
   method public void GetDefaultQtyAndCostsForWorkOrder(input-output dataset-handle dsPartsReceiveWorkOrder).
    
   /**
    * Get parts location.
    *
    * @param siteCode
    * @param partNumber
    * @param partsLocation
    *
    * @external
    *
    * @example
    *   
    *   using com.qad.assetmgmt.inventory.parts.IPartsReceiveWorkOrder.
    *   using com.qad.assetmgmt.AssetMgmtServices.
     *
    *   define variable partsReceiveWorkOrderService as IPartsReceiveWorkOrder no-undo.
    *
    *   partsReceiveWorkOrderService = AssetMgmtServices:GetPartsReceiveWorkOrder().
    *
    *   partsReceiveWorkOrderService:GetPartsLocation(input '10-100',
    *                                                 input 'AID-010',
    *                                                 output partsLocation).
    *   
    */  
   method public void GetPartsLocation(input siteCode as character,
                                       input partNumber as character,
                                       output partsLocation as character).
       
   /**
    * Receive fabricated work order.
    *
    * @param dsPartsReceiveWorkOrder
    * @param returnMessage
    *
    * @external
    *
    * @example
    *   
    *   using com.qad.assetmgmt.inventory.parts.IPartsReceiveWorkOrder.
    *   using com.qad.assetmgmt.AssetMgmtServices.
    *
    *   {com/qad/assetmgmt/inventory/parts/dsPartsReceiveWorkOrder.i}
     *
    *   define variable partsReceiveWorkOrderService as IPartsReceiveWorkOrder no-undo.
    *
    *   partsReceiveWorkOrderService = AssetMgmtServices:GetPartsReceiveWorkOrder().
    *
    *   partsReceiveWorkOrderService:UpdateReceiveFromWorkOrder(input dataset dsPartsReceiveWorkOrder by-reference,
    *                                                           output returnMessage).
    *   
    */  
   method public void UpdateReceiveFromWorkOrder(input dataset-handle dsPartsReceiveWorkOrder,
                                                 output returnMessage as character).

   /**
    * Get calculated cost per Item.
    *
    * @param siteCode
    * @param workOrderNumber
    * @param quantityToReceive
    * @param costPerItem
    *
    * @external
    *
    * @example
    *   
    *   using com.qad.assetmgmt.inventory.parts.IPartsReceiveWorkOrder.
    *   using com.qad.assetmgmt.AssetMgmtServices.
     *
    *   define variable partsReceiveWorkOrderService as IPartsReceiveWorkOrder no-undo.
    *
    *   partsReceiveWorkOrderService = AssetMgmtServices:GetPartsReceiveWorkOrder().
    *
    *   partsReceiveWorkOrderService:GetCalculatedCostPerItem(input '10-100',
    *                                                         input 312,
    *                                                         input 4,
    *                                                         output costPerItem).
    *   
    */  
   method public void GetCalculatedCostPerItem(input siteCode as character,
                                               input workOrderNumber as integer,
                                               input quantityToReceive as integer,
                                               output costPerItem as decimal).
    
 
end interface.
