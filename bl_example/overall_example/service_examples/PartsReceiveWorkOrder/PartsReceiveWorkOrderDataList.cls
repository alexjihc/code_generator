/*
 * com.qad.assetmgmt.inventory.parts.PartsReceiveWorkOrderDataList - Implementation class for retrieving datalist info for PartsReceiveWorkOrder fields
 *
 * Copyright 1986 QAD Inc. All rights reserved.
 *
 * $Id: PartsReceiveWorkOrderDataList.cls 283776 2020-07-22 03:09:54Z jpm $
 */

using com.qad.assetmgmt.AssetMgmtError.
using com.qad.assetmgmt.AssetMgmtMessageKey.

using com.qad.lang.Message.

using com.qad.qra.metadata.IDataList.
 
routine-level on error undo, throw.

class com.qad.assetmgmt.inventory.parts.PartsReceiveWorkOrderDataList implements IDataList:
 
   {com/qad/qra/metadata/dsRefDataListValue.i}  
   
   method public void GetDataList (input dataListIdentifier as character, 
                                   output dataset for dsRefDataListValue):
                                             
      dataset dsRefDataListValue:empty-dataset().
 
      case dataListIdentifier:
         when "Location" then
            this-object:GetPartsReceiveWorkOrderLocationList().
         otherwise
            undo, throw new AssetMgmtError(new Message(AssetMgmtMessageKey:INVALID_DATALIST_IDENTIFIER, dataListIdentifier)).
      end case.
     
   end method.
        
   method private void GetPartsReceiveWorkOrderLocationList():
      this-object:CreateRefDataListValue("", "").
   end method.
        
   method private void CreateRefDataListValue(input dataValue as character,
                                              input labelStringCode as character):
    
      create ttRefDataListValue.
      assign
         ttRefDataListValue.DataValue = dataValue
         ttRefDataListValue.DataLabel = labelStringCode.
    
   end method.
     
end class.