# Dynamic Datalist Examples
PartsReceiveWorkOrder
ProjectReopen
MaintenanceOrder
ProjectChangeStatus
EquipmentDowntime
PreventativeMaint

# Static Datalist Examples
Project



# Dynamic Datalist
# 1. Specify on logical field with annotation @datalist
/**
 * Status
 *
 * @label eam-label
 * @datalist list:assetmgmt:maintenance:workorder:status_code
 * @source status_code
 * @format x(2)
 */
 field status_code as character initial ?

# 2. Define datalist at the bottom of dataset
/**
 *
 * @define datalist
 * <DataList>
 *   <DataListCode>list:assetmgmt:maintenance:workorder:status_code</DataListCode>
 *   <ServiceCall>com.qad.assetmgmt.maintenance.workorder.IMaintenanceOrderDataList:status_code</ServiceCall>
 * </DataList>
 */

 # 3. Create Datalist.cls in implementation folder
 Ex. MaintenanceOrderDataList.cls

 /*
 * com.qad.assetmgmt.maintenance.equipmentdowntime.MaintenanceOrderDataList
 *
 * $Id: MaintenanceOrderDataList.cls 218950 2019-10-02 19:15:17Z a3q $
 */

using com.qad.assetmgmt.AssetMgmtError.
using com.qad.assetmgmt.AssetMgmtMessageKey.
using com.qad.assetmgmt.AssetMgmtServices.
using com.qad.assetmgmt.maintenance.wostatus.IMaintOrderStatus.

using com.qad.lang.Message.

using com.qad.qra.metadata.IDataList.

routine-level on error undo, throw.

class com.qad.assetmgmt.maintenance.workorder.MaintenanceOrderDataList implements IDataList:

  {com/qad/qra/metadata/dsRefDataListValue.i}
  {com/qad/assetmgmt/common/dsEAMWOStatus.i}
  {com/qad/assetmgmt/maintenance/wostatus/dsMaintOrderStatus.i}

  define private property MaintOrderStatusService as class IMaintOrderStatus no-undo
     private get:
       if not valid-object(MaintOrderStatusService) then
          this-object:MaintOrderStatusService = AssetMgmtServices:GetMaintOrderStatus().
       return MaintOrderStatusService.
     end get.
     private set.

   method public void GetDataList (input dataListIdentifier as character,
                                   output dataset for dsRefDataListValue):

      /* First, clear the dsRefDataListValue dataset */
      dataset dsRefDataListValue:empty-dataset().

      case dataListIdentifier:
         when "status_code" then
            getWOStatusList().
         otherwise
            undo, throw new AssetMgmtError(new Message(AssetMgmtMessageKey:INVALID_DATALIST_IDENTIFIER, dataListIdentifier)).
      end case.

   end method.

   method private void GetWOStatusList():

      this-object:MaintOrderStatusService:GetWOStatus (output dataset dsMaintOrderStatus).

      for each ttMaintOrderStatus no-lock:

         this-object:CreateRefDataListValue(ttMaintOrderStatus.status_code,
                                            ttMaintOrderStatus.description).
      end.

   end method.

   method private void CreateRefDataListValue(input dataValue as character,
                                              input labelStringCode as character):

      create ttRefDataListValue.
      assign
         ttRefDataListValue.DataValue = dataValue
         ttRefDataListValue.DataLabel = labelStringCode.

   end method.

end class.

 # 4. Define on module-config.xml
 <Service
    ServiceClass="com.qad.qra.metadata.IDataList"
    ServiceKey="com.qad.assetmgmt.maintenance.workorder.IMaintenanceOrderDataList"
    ImplClass="com.qad.assetmgmt.maintenance.workorder.MaintenanceOrderDataList"
    LifetimePolicy="factory" />