# Various error handling returns

# For dsMessage
# 1. For returning a message
counter = 0.
for each ttMessage:
    counter = counter + 1.
    returnMessage = returnMessage + ttMessage.MsgType + AssetMgmtConstants:DELIMITER_CHARACTER + ttMessage.MsgText + AssetMgmtConstants:DELIMITER_CHARACTER.
end.



# For dsSubmitError
# 1.
for each ttSubmitError:
    counter = counter + 1.
    msg = new Message(counter, ttSubmitError.MessageText).
    
    if MessageType = AssetMgmtConstants:MESSAGE_TYPE_WARNING then do:
        QraServices:GetSupplementaryMessageLogger():Warn(ttSubmitError.MessageText).
    end.                    
    else do: 
        msg = new Message(counter, ttSubmitError.MessageText).                                                     
        valMsgs:Add(msg).
    end.                                                                                        
end.                                   
if not valMsgs:IsEmpty() then do:                               
    undo, throw new AssetMgmtError(valMsgs).                                                                       
end.  



# Undo and throw assetmgmt error
# 1. For dsMessage
counter = 0.                                                                     
for each ttMessage:
    counter = counter + 1.
    msg = new Message(counter, ttMessage.MsgText).
    valMsgs:Add(msg).
end.         
if not valMsgs:IsEmpty()
then do:
    undo, throw new AssetMgmtError(valMsgs).
end.


# 2.
if not ttEAMWorkOrder.fabricated then do:               
    assetMgmtError = new AssetMgmtError(new Message(input AssetMgmtMessageKey:WORK_ORDER_DOES_NOT_HAVE_INTERNAL_PARTS)).
    undo, throw assetMgmtError.    
end.      