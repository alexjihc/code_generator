

# Snippets
if userSessionService:LoggedIn then do on error undo, throw:      
    # empty return message
    returnMessage = "".

    # empty native dataset
    dataset dsNativeParts:empty-dataset().

    # run getById
    run inventory/parts/si/getByID.p (input partNumber,
                                    input siteCode,
                                    output dataset dsNativeParts by-reference,
                                    input userSessionService:SessionID).

    # find and if not available
    find ttNativeParts no-error.     
    if not available(ttNativeParts) then do:
        assetMgmtError = new AssetMgmtError(new Message(input AssetMgmtMessageKey:RECORD_DOES_NOT_EXIST, AssetMgmtTranslatedString:PARTS)).
        undo, throw assetMgmtError.    
    end.

    # run legacy procedure
    run inventory/parts/si/CheckCanReturnToWorkOrder.p (input dataset dsNativeParts by-reference,
                                                        input-output validateStatus,
                                                        output dataset dsMessage by-reference,
                                                        input userSessionService:SessionID).
        
    # for loop and undo throw assetmgmt error
    counter = 0.
    for each ttMessage:
        assetMgmtError = new AssetMgmtError(new Message(counter, ttMessage.MsgText)).
        undo, throw assetMgmtError.
    end. 

    # create temp-table and assign
    create ttPartsReturnWorkOrder.
    assign
        ttPartsReturnWorkOrder.SiteCode              = siteCode
        ttPartsReturnWorkOrder.PartNumber            = partNumber
        ttPartsReturnWorkOrder.DateReturned          = today
        ttPartsReturnWorkOrder.TotalOnHandQuantity   = ttNativeParts.curr_qty
        ttPartsReturnWorkOrder.IsRotable             = ttNativeParts.serialized.

    # replace unknownvalue
    AssetMgmtUtils:ReplaceUnknownValues(buffer ttPartsReturnWorkOrder:handle).

end.


# Snippets
# for each
for each ttPartsReturnWorkOrder:

    dataset dsEAMWorkOrder:empty-dataset().

    run maintenance/workorder/si/getByID.p (input ttPartsReturnWorkOrder.SiteCode,
                                            input ttPartsReturnWorkOrder.WorkOrderNumber,
                                            output dataset dsEAMWorkOrder by-reference,
                                            input userSessionService:SessionID).

    # find first native temp-table from getbyId and if not available undo throw assetmgmt error
    find first ttEAMWorkOrder no-error.     
    if not available ttEAMWorkOrder then do:
        assetMgmtError = new AssetMgmtError(new Message(input AssetMgmtMessageKey:RECORD_DOES_NOT_EXIST, AssetMgmtTranslatedString:ASSET_WORK_ORDERS)).
        undo, throw assetMgmtError. 
    end.

    # if and else statement
    if not ttEAMWorkOrder.fabricated then do:               
        assetMgmtError = new AssetMgmtError(new Message(input AssetMgmtMessageKey:WORK_ORDER_DOES_NOT_HAVE_INTERNAL_PARTS)).
        undo, throw assetMgmtError.    
    end.          

    else if ttEAMWorkOrder.fab_part_no <> ttPartsReturnWorkOrder.PartNumber then do:
        assetMgmtError = new AssetMgmtError(new Message(input AssetMgmtMessageKey:WORK_ORDER_HAS_INVALID_PART_NUMBER)).
        undo, throw assetMgmtError.    
    end.   

    else do:
        if ttPartsReturnWorkOrder.IsRotable then do:       
        # run this object
            this-object:GetSerializedParts(input ttPartsReturnWorkOrder.SiteCode,
                                            input ttPartsReturnWorkOrder.PartNumber,
                                            output serializedParts).

        end.

        assign
            ttPartsReturnWorkOrder.SourceSite         = ttEAMWorkOrder.source_site
            ttPartsReturnWorkOrder.WorkOrderStatus    = ttEAMWorkOrder.status_code
            ttPartsReturnWorkOrder.ReceivedQuantity   = ttEAMWorkOrder.qty_rec
            ttPartsReturnWorkOrder.OnHandQuantity     = onHandQuantity
            ttPartsReturnWorkOrder.SerialNumber       = serializedParts
            ttPartsReturnWorkOrder.CostPerItem        = unitOfMeasureCost
            ttPartsReturnWorkOrder.Location           = partsLocation
            ttPartsReturnWorkOrder.DateReturned       = today.
    end.
end.


# Snippets
method public void GetPartsLocation(input siteCode as character,
                                    input partNumber as character,
                                    output partsLocation as character):
    define variable userSessionService as UserSession no-undo.

    userSessionService = new UserSession().      
    # do on error undo and throw
    do on error undo, throw:                    
        run inventory/partsloc/si/GetLocationList.p (input siteCode,                                                       
                                                    input partNumber,
                                                    output partsLocation,
                                                    input userSessionService:SessionID).
        
    end.
        
end method.


# Snippets
if userSessionService:LoggedIn then do on error undo, throw:               
    for each ttPartsReturnWorkOrder:
        run inventory/partsloc/si/ValidateOnReturnToWorkOrder.p (input ttPartsReturnWorkOrder.QuantityToReturn,                                                                    
                                                                 input ttPartsReturnWorkOrder.DateReturned,
                                                                 output effectiveDate,
                                                                 output validateStatus,
                                                                 output dataset dsMessage by-reference,
                                                                 input userSessionService:SessionID).    

        # loop dsMessage and added to returnMessage string
        for each ttMessage:
            counter = counter + 1.
            returnMessage = returnMessage + ttMessage.MsgType + AssetMgmtConstants:DELIMITER_CHARACTER + ttMessage.MsgText + AssetMgmtConstants:DELIMITER_CHARACTER.
        end.

        # loop dsMessage and add errors
        counter = 0.                                                                     
        for each ttMessage:
            counter = counter + 1.
            msg = new Message(counter, ttMessage.MsgText).
            valMsgs:Add(msg).
        end.         
        if not valMsgs:IsEmpty()
        then do:
            undo, throw new AssetMgmtError(valMsgs).
        end.

        if ttPartsReturnWorkOrder.WorkOrderStatus = AssetMgmtConstants:DOCUMENT_CLOSED and ttPartsReturnWorkOrder.IsCancelQuantity <> true then do:

            if ttPartsReturnWorkOrder.NewWorkOrderStatus = AssetMgmtConstants:EMPTY_STATUS then do:
                assetMgmtError = new AssetmgmtError(new Message(input AssetMgmtMessageKey:WORK_ORDER_STATUS_REQUIRED)).
                undo, throw assetMgmtError.
            end.

            run maintenance/wostatus/si/checkWOStatus.p (input ttPartsReturnWorkOrder.NewWorkOrderStatus,
                                                        output isWorkOrderStatusAvailable,
                                                        input userSessionService:SessionID).

            if not isWorkOrderStatusAvailable then do:
                assetMgmtError = new AssetmgmtError(new Message(input AssetMgmtMessageKey:WORK_ORDER_STATUS_NOT_AVAILABLE)).
                undo, throw assetMgmtError.

            end.                        
        end.
        
        run inventory/inventorytransaction/si/ReturnToWorkorder.p (input ttPartsReturnWorkOrder.SiteCode,                                                                    
                                                                   input ttPartsReturnWorkOrder.NewWorkOrderStatus,
                                                                   input effectiveDate,
                                                                   input time,
                                                                   input-output validateStatus,
                                                                   input-output dataset dsSubmitError by-reference,
                                                                   input userSessionService:SessionID).               
        
        # loop dsSubmitError and add errors
        for each ttSubmitError:
            counter = counter + 1.
            msg = new Message(counter, ttSubmitError.MessageText).
            
            if MessageType = AssetMgmtConstants:MESSAGE_TYPE_WARNING then do:
                QraServices:GetSupplementaryMessageLogger():Warn(ttSubmitError.MessageText).
            end.                    
            else do: 
                msg = new Message(counter, ttSubmitError.MessageText).                                                     
                valMsgs:Add(msg).
            end.                                                                                        
        end.                                   
        
        if not valMsgs:IsEmpty() then do:                               
            undo, throw new AssetMgmtError(valMsgs).
        end. 
    end.
end.