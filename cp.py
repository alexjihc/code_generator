#!/usr/bin/env python
import os
import re
import sys
import argparse

parser = argparse.ArgumentParser()

# parser.add_argument("echo", help="echo the string you use here")
parser.add_argument("square", help="display a square of given number", type=int)
parser.add_argument("-v", "--verb", help="increase output verbosity", type=int, choices=[0,1,2])

args = parser.parse_args()
answer = args.square**2

if args.verb == 2:
    print("the square of {} equals {}".format(args.square, answer))
elif args.verb == 1:
    print("{}^2 == {}".format(args.square, answer))
else:
    print(answer)