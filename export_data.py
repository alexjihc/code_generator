#!/usr/bin/env python

import os
import re
import json


class Export_data():
    

    def be_data(self):
        be_export_data = [
        # isbe = true
        # module-config.xml, secureresource.xml, assetmgmtServices.cls, role.xml
    """
# module-config.xml
<Service
    ServiceClass="com.qad.assetmgmt.bl_dir_name.Ibl_name"
    ServiceKey=""
    ImplClass="com.qad.assetmgmt.bl_dir_name.bl_name"
    LifetimePolicy="factory" /> 

# secureresource.xml
<SecureResource>
    <ResourceURI>urn:browse:mfg:be_browseuri</ResourceURI>
    <StringCode>STRING</StringCode>
    <IsMenuEligible>true</IsMenuEligible>
    <ParentResourceURI>urn:be:com.qad.assetmgmt.bl_dir_name.Ibl_name</ParentResourceURI>
</SecureResource>

# assetgmtmServices
{com/qad/qra/base/ServiceMethodV2.i &METHOD-NAME = "Getbl_name"    &SERVICEINTERFACE = "com.qad.assetmgmt.bl_dir_name.Ibl_name"}

# role.xml
<ACESystemData>
  <ResourceURI>urn:be:com.qad.assetmgmt.bl_dir_name.Ibl_name</ResourceURI>
  <Allow>Create,Delete,Read,Write</Allow>
  <Deny/>
</ACESystemData>
    """     
        ]
        return be_export_data

    
    def service_data(self):
        service_export_data = [
        # isbe = false
        # module-config.xml, secureresource.xml, assetmgmtServices.cls java.properties, role.xml
    """
# module-config.xml
<Service
    ServiceClass="com.qad.assetmgmt.bl_dir_name.Ibl_name"
    ServiceKey=""
    ImplClass="com.qad.assetmgmt.bl_dir_name.bl_name"
    LifetimePolicy="factory" /> 

# secureresource.xml
<SecureResource> 		
    <ResourceURI>urn:service:com.qad.assetmgmt.bl_dir_name.Ibl_name-bl_names</ResourceURI>
    <StringCode>STRING</StringCode>
    <IsMenuEligible>false</IsMenuEligible>
    <ParentResourceURI>urn:be:com.qad.assetmgmt.bl_dir_parent.bl_parent</ParentResourceURI>
</SecureResource>   

# assetmgmtServices
{com/qad/qra/base/ServiceMethodV2.i &METHOD-NAME = "Getbl_name"    &SERVICEINTERFACE = "com.qad.assetmgmt.bl_dir_name.Ibl_name"}

# java.properties
com.qad.assetmgmt.bl_dir_name.Ibl_name, \\

# role.xml
<ACESystemData>
  <ResourceURI>urn:service:com.qad.assetmgmt.bl_dir_name.Ibl_name-bl_names</ResourceURI>
  <Allow>Create,Delete,Read,Write</Allow>
  <Deny/>
</ACESystemData>

# secureresources **Secureresources not required for service components per Olek"
<SecureResource>
    <ResourceURI>urn:service:com.qad.assetmgmt.bl_dir_name.Ibl_name-bl_names</ResourceURI>
    <StringCode>STRING</StringCode>
    <IsMenuEligible>false</IsMenuEligible>
    <ParentResourceURI>urn:be:com.qad.assetmgmt.bl_dir_parent.bl_parent</ParentResourceURI>
</SecureResource>   
    """     
        ]
        return service_export_data
