#!/usr/bin/env python
import os
import re

# Script to convert SOAP UI body
# copy and paste the body of soap tags below

# converts from:
# <ABCCode/>
# <AddDate xsi:nil="true"/>
# <AddTime>0</AddTime>
# <AvailableQuantity>1.0</AvailableQuantity>

# to:
# <urn:ABCCode></urn:ABCCode>
# <urn:AddDate></urn:AddDate>
# <urn:AddTime>0</urn:AddTime>
# <urn:AvailableQuantity>1</urn:AvailableQuantity>


# base path: C:\Users\a3q\Documents\bitbucket/code/generator/soap
base_path = os.getcwd()

soap_result = "soap_result.txt"

# copy and paste the xml fields below
soap_list = [
    """
<AddDate>2020-10-19</AddDate>
<AddUserID>mfg</AddUserID>
<AddTime>35979</AddTime>
<IsCertified>true</IsCertified>
<CertifiedDate>2020-10-01</CertifiedDate>
<CertifiedDueDate xsi:nil="true"/>
<ChangeDate>2020-10-19</ChangeDate>
<ChangeUser>mfg</ChangeUser>
<ChangeTime>35979</ChangeTime>
<LocationCode>T20</LocationCode>
<LocationStatus>Shelf</LocationStatus>
<SerialNumber>S10</SerialNumber>
<SiteCode>10-100</SiteCode>
<ToolNumber>T1002</ToolNumber>
<DisplayAddTime xsi:nil="true"/>
<DisplayChangeTime xsi:nil="true"/>
<LastIssued xsi:nil="true"/>
<DataOperation/>
<ConcurrencyHash>020010QoLdqYLEIW14hvkT8YrXxA==</ConcurrencyHash>

    """
]

soap_strings = ''.join(soap_list)
soap_url_lists = soap_strings.strip().splitlines()

modified_soap_string = ""
end_string = ""
urn_end_string = ""

for soap_url_list in soap_url_lists:

    prefix_string = soap_url_list[:1] + "urn:" + soap_url_list[1:]
    if "</" in prefix_string:
        prefix_string = prefix_string.replace("</", "</urn:")

        prefix_string = prefix_string + "\n"
        modified_soap_string += prefix_string

    elif "/>" in prefix_string:
        end_string = soap_url_list[1:-2]
        end_string = end_string.replace('xsi:nil="true"', "")
        end_string= end_string.rstrip()
        prefix_string = "<urn:" + end_string + "></urn:" + end_string + ">"

        prefix_string = prefix_string + "\n"
        modified_soap_string += prefix_string

with open(os.path.join(base_path, soap_result), 'w+') as result:
    result.write(modified_soap_string)
