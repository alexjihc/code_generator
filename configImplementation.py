#!/usr/bin/env python

import json
import os

class ConfigImplementation():
    def __init__(self):
        if os.path.exists('config.json'):
            with open(os.path.abspath('config.json'), 'r') as f:
                self.configImplementation = json.load(f)

    def get(self, name):
        if name == 'bl_native':
            return self.configImplementation['bl_native']
        elif name == 'bl_discarded_lists':
            return self.configImplementation['bl_discarded_lists']
        elif name == 'is_eamcontext':
            return self.configImplementation['is_eamcontext']
        elif name == 'bl_legacy_name':
            return self.configImplementation['bl_legacy_name']


##### IMPLEMENTATION #####

    def using_import_lists(self):
        using_import_list = [
            # general assetmgmt service
            "using com.qad.assetmgmt.general.site.ISiteAssetMgmt.",
            "using com.qad.assetmgmt.general.domain.IDomain.",
            # qra (both qraError and qraMessageKey are used with approval service)
            "using com.qad.qra.QraError.",
            "using com.qad.qra.QraMessageKey.",
            # business document
            "using com.qad.qra.businessdocument.IBusinessDocumentComponent.",
            # base
            "using com.qad.base.BaseError.",
            "using com.qad.base.BaseMessageKey.",
            "using com.qad.base.BaseUtils.",
            # approval
            "using com.qad.qra.config.QraConfig.",
            "using com.qad.qra.resource.IResourceResolver.",
            "using com.qad.qra.resource.ResourceIdentityUtils.",
            "using com.qad.approvals.IGenericApproval.",
            "using com.qad.approvals.GenericApprovalServices."

        ]
        return using_import_list

    # import datasets
    def import_lists(self):
        import_list = [
            # business document
            "{com/qad/qra/base/dsKeyField.i}",
            # datalist
            "{com/qad/qra/metadata/dsRefDataListValue.i}",
            # globaledit
            "{com/qad/assetmgmt/common/dsGlobalEditKeys.i &PREFIX=NATIVE}",

        ]
        return import_list

##### SERVICE #####

    # service
    def service_lists(self):
        service_list = [
    """
   define private property ValidationService as IValidate no-undo
      private get:
	     if not valid-object(ValidationService) then
		    this-object:ValidationService = AssetMgmtServices:GetValidate().
	     return ValidationService.
      end get.
      private set.
    """,

    """
   define public property GenericApprovalService as IGenericApproval no-undo
      get:
         if not valid-object(GenericApprovalService) then do:
            if QraConfig:IsServiceAvailable ("com.qad.approvals.IGenericApproval") then
               this-object:GenericApprovalService = GenericApprovalServices:GetGenericApproval().
         end.
         return GenericApprovalService.
      end get.
      set.
    """
        ]
        return service_list

    # initialize e.g. ci.initialize()[i] 0:standard, 1:service(simpler)
    def initialize(self):
        initialize = [
    """
   [bl_method]:

      {com/qad/qra/base/OutDSPre.i &dataset = "dsbl_name"}
      this-object:InitializePrivate([input_keyfield]
                                    output dataset-handle dsbl_nameTemp by-reference).
      {com/qad/qra/base/OutDSPost.i &dataset = "dsbl_name"}

      finally:
         delete object dsbl_name no-error.
      end finally.

   end method.

   [bl_protected_method]:
      define variable dsHandle as handle no-undo.
      define variable userSessionService as UserSession no-undo.

      create ttbl_name.
      assign [bl_logical_table].

      userSessionService = new UserSession().

      dataset dsbl_nativebl_name:empty-dataset().

      AssetMgmtUtils:ReplaceUnknownValues(buffer ttbl_name:handle).

      find first ttbl_name no-error.
      if available ttbl_name then do:
         create ttbl_nativebl_name.
         assign [bl_temp_table].
      end.
      else
         undo, throw new AssetMgmtError(new Message(AssetMgmtMessageKey:RECORD_DOES_NOT_EXIST, AssetMgmtTranslatedString:[bl_message_error])).

      this-object:CopyLogicalToNative().

      if userSessionService:LoggedIn then do on error undo:
         run bl_interface_name/si/getByID.p ([gbi_param]).

         assign dsHandle = dataset dsbl_nativebl_name:handle.

         if valid-handle(dsHandle) then
            run bl_interface_name/si/populateNewRec.p ([populate_new_rec], input userSessionService:SessionID).

         find first ttbl_nativebl_name no-error.

         dataset dsbl_name:empty-dataset().

         this-object:CopyNativeToLogical().
      end.

   end method.
    """,
    """
   [bl_method_service]:

      {com/qad/qra/base/OutDSPre.i &dataset = "dsbl_name"}
      this-object:InitializePrivate([input_keyfield]
                                    output dataset-handle dsbl_nameTemp by-reference).
      {com/qad/qra/base/OutDSPost.i &dataset = "dsbl_name"}

      finally:
         delete object dsbl_name no-error.
      end finally.

   end method.

   [bl_private_method_service]:
      define variable userSessionService as UserSession no-undo.

      dataset dsbl_nativebl_name:empty-dataset().
      userSessionService = new UserSession().

      if userSessionService:LoggedIn then do on error undo:
         run bl_interface_name/si/getByID.p ([gbi_param]).

         find first ttbl_nativebl_name no-error.
         if available ttbl_nativebl_name then do:
            create ttbl_name.
            assign [bl_logical_table].
         end.
         else
            undo, throw new AssetMgmtError(new Message(AssetMgmtMessageKey:RECORD_DOES_NOT_EXIST,
                                                       AssetMgmtTranslatedString:[bl_message_error])).

      end.

   end method.
    """
        ]
        return initialize

    # create
    def create(self):
        create = [
    """
   method public void Create(input dataset-handle dsbl_name):

      {com/qad/qra/base/InAndInOutDSPre.i &dataset = "dsbl_name"}
      this-object:CreatePrivate(input dataset-handle dsbl_nameTemp by-reference).

      finally:
         delete object dsbl_name no-error.
      end finally.

   end method.

   method protected void CreatePrivate(input dataset dsbl_name):
      define variable assetMgmtError     as AssetMgmtError no-undo.
      define variable msg                as IMessage       no-undo.
      define variable valMsgs            as IList          no-undo.
      define variable validationStatus   as integer        no-undo.
      define variable counter            as integer        no-undo.
      define variable userSessionService as UserSession    no-undo.

      userSessionService = new UserSession().
      valMsgs = new List().

      do on error undo, throw:
         dataset dsbl_nativebl_name:empty-dataset().
         temp-table ttbl_nativebl_name:tracking-changes = true.

         for each ttbl_name:
            create ttbl_nativebl_name.
            this-object:CopyLogicalToNative().
         end.

         run bl_interface_name/si/submit.p ([s_param]input userSessionService:SessionID).

         temp-table ttbl_nativebl_name:tracking-changes = false.

         for each ttSubmitError:
            counter = counter + 1.
            msg = new Message(counter, ttSubmitError.MessageText).
            valMsgs:Add(msg).
         end.
         if not valMsgs:IsEmpty() then do:
            undo, throw new AssetMgmtError(valMsgs).
         end.
      end.
      catch e as Progress.Lang.Error :
         assetMgmtError = new AssetMgmtError(new Message(AssetMgmtMessageKey:FAILED_TO_CREATE_RECORD, AssetMgmtTranslatedString:[bl_message_error])).
         assetMgmtError:RootCause = e.
         undo, throw assetMgmtError.
      end catch.

   end method.
    """
        ]
        return create

    # fetch
    def fetch(self):
        fetch = [
    """
   [bl_method]:

      {com/qad/qra/base/OutDSPre.i &dataset = "dsbl_name"}
      this-object:FetchPrivate([input_keyfield]
                               output dataset-handle dsbl_nameTemp by-reference).
      {com/qad/qra/base/OutDSPost.i &dataset = "dsbl_name"}

      finally:
         delete object dsbl_name no-error.
      end finally.

   end method.

   [bl_protected_method]:
      define variable userSessionService as UserSession no-undo.

      userSessionService = new UserSession().

      if userSessionService:LoggedIn then do on error undo:
         run bl_interface_name/si/getByID.p ([gbi_param]).

         this-object:CopyNativeToLogical().

      end.

   end method.
    """
        ]
        return fetch

    # update
    def update(self):
        update = [
    """
   method public void Update(input dataset-handle dsbl_name):

      {com/qad/qra/base/InAndInOutDSPre.i &dataset = "dsbl_name"}
      this-object:UpdatePrivate(input dataset-handle dsbl_nameTemp by-reference).

      finally:
         delete object dsbl_name no-error.
      end finally.

   end method.

   method protected void UpdatePrivate(input dataset dsbl_name):
      define variable assetMgmtError     as AssetMgmtError    no-undo.
      define variable msg                as IMessage          no-undo.
      define variable valMsgs            as IList             no-undo.
      define variable validationStatus   as integer           no-undo.
      define variable counter            as integer           no-undo.
      define variable userSessionService as UserSession       no-undo.

      valMsgs = new List().
      userSessionService = new UserSession().

      do on error undo, throw:
         for each ttbl_name:
            dataset dsbl_nativebl_name:empty-dataset().

            run bl_interface_name/si/getByID.p ([gbi_param]).

            find first ttbl_nativebl_name where [bl_temp_table] no-error.

            if not available(ttbl_nativebl_name) then do:
               assetMgmtError = new AssetMgmtError(new Message(input AssetMgmtMessageKey:OPTIMISTIC_LOCK_DELETE_ERROR_NODOMAIN)).
               undo, throw assetMgmtError.
            end.
            else if this-object:ConcurrencyControlHandler:CheckConcurrencyHash (buffer ttbl_name:handle,
                                                                                buffer ttbl_nativebl_name:handle)
            then do:
               assetMgmtError = new AssetMgmtError(new Message(input AssetMgmtMessageKey:OPTIMISTIC_LOCK_UPDATE_ERROR_NODOMAIN)).
               undo, throw assetMgmtError.
            end.

            temp-table ttbl_nativebl_name:tracking-changes = true.
            this-object:CopyLogicalToNative().

            run bl_interface_name/si/submit.p ([s_param]input userSessionService:SessionID).

            temp-table ttbl_nativebl_name:tracking-changes = false.

            for each ttSubmitError:
               counter = counter + 1.
               msg = new Message(counter, ttSubmitError.MessageText).
               valMsgs:Add(msg).
             end.

             if not valMsgs:IsEmpty() then do:
                undo, throw new AssetMgmtError(valMsgs).
             end.
         end.
      end.

   end method.
    """
        ]
        return update

    # delete
    def delete(self):
        delete = [
    """
   method public void Delete(input dataset-handle dsbl_name):

      {com/qad/qra/base/InAndInOutDSPre.i &dataset = "dsbl_name"}
      this-object:DeletePrivate(input dataset-handle dsbl_nameTemp by-reference).

      finally:
         delete object dsbl_name no-error.
      end finally.

   end method.

   method protected void DeletePrivate(input dataset dsbl_name):
      define variable appError           as AppError         no-undo.
      define variable validationStatus   as integer          no-undo.
      define variable counter            as integer          no-undo.
      define variable userSessionService as UserSession      no-undo.

      appError = new AppError().
      userSessionService = new UserSession().

      do on error undo, throw:

         for each ttbl_name:
            dataset dsbl_nativebl_name:empty-dataset().
            run bl_interface_name/si/getByID.p ([gbi_param]input userSessionService:SessionID).

            temp-table ttbl_nativebl_name:tracking-changes = true.

            for first ttbl_nativebl_name where [bl_temp_table]:
               delete ttbl_nativebl_name.
            end.

            run bl_interface_name/si/submit.p ([s_param]input userSessionService:SessionID).

            temp-table ttbl_nativebl_name:tracking-changes = false.

            for each ttSubmitError:
               if ttSubmitError.MessageType = AssetMgmtConstants:MESSAGE_TYPE_QUESTION then do:
                  validationStatus = AssetMgmtConstants:RESPONSE-RETURNED-YES.

                  run bl_interface_name/si/submit.p ([s_param2]input userSessionService:SessionID).

               end.
               else do:
                  counter = counter + 1.
                  if counter = 1 then
                     appError:AddMessage(input ttSubmitError.MessageText, input counter).

                  if counter > 0 then
                     undo, throw appError.
               end.
            end.
         end.
      end.

   end method.
    """
        ]
        return delete

    # isvalid
    def isvalid(self):
        isvalid = [
    """
   method public logical IsValid(input fieldName as character,
                                 input-output dataset-handle dsbl_name):

      define variable isValid as logical initial false no-undo.

      {com/qad/qra/base/InAndInOutDSPre.i &dataset = "dsbl_name"}
      isValid = this-object:IsValidPrivate(input fieldName,
                                           input-output dataset-handle dsbl_nameTemp by-reference).
      {com/qad/qra/base/InOutDSPost.i &dataset = "dsbl_name"}

      return isValid.

      finally:
         delete object dsbl_name no-error.
      end finally.

   end method.

   method protected logical IsValidPrivate(input fieldName as character,
                                           input-output dataset dsbl_name):
      define variable msg           as IMessage                 no-undo.
      define variable valMsgs       as IList                    no-undo.
      define variable counter       as integer                  no-undo.
      define variable datasetHandle as handle                   no-undo.
      define variable isValid       as logical initial false    no-undo.

      valMsgs = new List().
      dataset dsbl_nativebl_name:empty-dataset().

      for first ttbl_name no-lock:
         create ttdsbl_nativebl_name.
         this-object:CopyLogicalToNative().
      end.

      if not available ttbl_name then
           undo, throw new AssetMgmtError(new Message(AssetMgmtMessageKey:RECORD_NOT_FOUND, AssetMgmtTranslatedString:[bl_message_error])).

      assign datasetHandle = dataset dsbl_nativebl_name:handle.
      isValid = this-object:ValidationService:IsValid(input "legacy_be_name",
                                                      input fieldName,
                                                      input-output dataset-handle datasetHandle by-reference,
                                                      input-output dataset dsMessage by-reference).

      for each ttMessage:
          counter = counter + 1.
          msg = new Message(counter, ttMessage.MsgText).
          valMsgs:Add(msg).
      end.
      if not valMsgs:IsEmpty() then do:
          undo, throw new AssetmgmtError(valMsgs).
      end.
      else do:
         dataset dsbl_name:empty-dataset().
         this-object:CopyNativeToLogical().
      end.
      return isValid.

   end method.
    """
        ]
        return isvalid

    # exists
    def exists(self):
        exists = [
    """
   [bl_method]:
      define variable hasRecord as logical no-undo initial false.

      this-object:Fetch([input_keyfield]
                        output dataset dsbl_name by-reference).

      find first ttbl_name
           where [bl_logical_table]
      no-error.

      if available ttbl_name then
         hasRecord = true.
      else
         hasRecord = false.

      return hasRecord.

   end method.
    """
        ]
        return exists

    # getcontrolstates
    def getcontrolstates(self):
        getcontrolstates = [
    """
   method public void GetControlStates(input dataset-handle dsbl_name,
                                       output fieldNames as character,
                                       output fieldStates as character):

      {com/qad/qra/base/InAndInOutDSPre.i &dataset = "dsbl_name"}
      this-object:GetControlStatesPrivate(input dataset-handle dsbl_nameTemp by-reference,
                                          output fieldNames,
                                          output fieldStates).

      finally:
         delete object dsbl_name no-error.
      end finally.

   end method.

   method protected void GetControlStatesPrivate(input dataset dsbl_name,
                                                 output fieldNames as character,
                                                 output fieldStates as character):
      define variable userSessionService as UserSession no-undo.

      userSessionService = new UserSession().
      dataset dsbl_nativebl_name:empty-dataset().

      find first ttbl_name no-error.

      if not available ttbl_name then
         return.

      /* For a FETCH, CREATE, or UPDATE, the record has been saved and
         we want to disable the "on-save" fields, so ROW-STATE must NOT equal 3 */
      if ttbl_name.DataOperation = AssetMgmtConstants:EAM_OPERATION_UPDATE or
         ttbl_name.DataOperation = AssetMgmtConstants:EAM_OPERATION_FETCH or
         ttbl_name.DataOperation = AssetMgmtConstants:EAM_OPERATION_CREATE
      then do:
         run bl_interface_name/si/getByID.p ([gbi_param]).
      end.
      else do:
         /* For INITIALIZE, and ISVALID, the record is not yet saved and we want to keep the "on-save" fields enabled.
            Capture creation of a dummy record so ROW-STATE = 3 when fieldEnable.p is called */
         temp-table ttbl_nativebl_name:tracking-changes = true.

         create ttbl_nativebl_name.
         if ttbl_name.DataOperation = AssetMgmtConstants:EAM_OPERATION_ISVALID then do:
            assign [bl_temp_table].
         end.

         /* Populate the dummy record with the correct data so field enabling is accurately reflected */
         for first ttbl_nativebl_name:
            this-object:CopyLogicalToNative().
         end.
      end.

       if userSessionService:LoggedIn then do on error undo:
          run bl_interface_name/si/FieldEnable.p ([fieldEnable_param]).
       end.

       this-object:ValidationService:ControlState (input "legacy_be_name",
                                                   input dataset dsControlState,
                                                   output fieldNames,
                                                   output fieldStates).

   end method.
    """
        ]
        return getcontrolstates

    # replaceunknowns
    def replaceunknowns(self):
        replaceunknowns = [
    """
   method public void ReplaceUnknowns (input-output dataset dsbl_name):
      define variable dsHandle    as handle  no-undo.
      define variable fieldCount  as integer no-undo.
      define variable counter     as integer no-undo.

      dsHandle = buffer ttbl_name:handle.
      fieldCount = dsHandle:num-fields.

      for each ttbl_name:
         do counter = 1 to fieldCount:
            if (dsHandle:buffer-field(counter):buffer-value = "?"  or
               dsHandle:buffer-field(counter):buffer-value = ?)
            then do:
               if dsHandle:buffer-field(counter):data-type = "character" then
                  dsHandle:buffer-field(counter):buffer-value = "".
               if dsHandle:buffer-field(counter):data-type = "decimal" then
                  dsHandle:buffer-field(counter):buffer-value = 0.0.
               if dsHandle:buffer-field(counter):data-type = "integer" then
                  dsHandle:buffer-field(counter):buffer-value = 0.
               if dsHandle:buffer-field(counter):data-type = "logical" then
                  dsHandle:buffer-field(counter):buffer-value = false.
            end.
         end.
      end.
   end method.
    """
        ]
        return replaceunknowns

    # copylogicaltonative
    def copylogicaltonative(self):
        copylogicaltonative = [
    """
   method private void CopyLogicalToNative():
      assign
         [copy_logical_native].

   end method.
    """
        ]
        return copylogicaltonative

    # copynativetological
    def copynativetological(self):
        copynativetological = [
    """
   method private void CopyNativeToLogical():
      for each ttbl_nativebl_name:
         create ttbl_name.

         assign
            [copy_native_logical].

      end.

   end method.
    """
        ]
        return copynativetological

    # convertlogicaltonativefieldname
    def convertlogicaltonativefieldname(self):
        convertlogicaltonativefieldname = [
    """
   method protected void ConvertLogicalToNativeFieldName(input-output fieldName as character):

      [copy_logical_native_field]

   end method.
    """
        ]
        return convertlogicaltonativefieldname

    # convertnativetologicalfieldname
    def convertnativetologicalfieldname(self):
        convertnativetologicalfieldname = [
    """
   method protected void ConvertNativeToLogicalFieldName(input-output tempFieldNames as character,
                                                         input-output tempFieldStates as character,
                                                         input fieldNames as character,
                                                         input fieldStates as character,
                                                         input fieldCount as integer):
      define variable counter    as integer   no-undo.
      define variable fieldName  as character no-undo.
      define variable fieldState as character no-undo.

      counter = 0.
      do counter = 1 to fieldCount:
         fieldName = entry(counter, fieldNames, AssetMgmtConstants:FIELD_DELIMITER).
         fieldState = entry(counter, fieldStates, AssetMgmtConstants:FIELD_DELIMITER).

         [copy_native_logical_field]

      end.

   end method.
    """
        ]
        return convertnativetologicalfieldname

    # getlist
    def getlist(self):
        getlist = [
    """
   method public void GetList(input dataset-handle dsFilter,
                              output dataset-handle dsbl_name):

      {com/qad/qra/base/OutDSPre.i &dataset = "dsbl_name"}
      {com/qad/qra/base/InAndInOutDSPre.i &dataset = "dsFilter"}
      this-object:GetListPrivate(input dataset-handle dsFilterTemp by-reference,
                                 output dataset-handle dsbl_nameTemp by-reference).
      {com/qad/qra/base/OutDSPost.i &dataset = "dsbl_name"}

      finally:
         delete object dsbl_name no-error.
      end finally.

   end method.

   method protected void GetListPrivate(input dataset dsFilter,
                                        output dataset dsbl_name):
      define variable userSessionService as UserSession no-undo.
      define variable isHaveLast as logical no-undo initial false.

      dataset dsbl_name:empty-dataset().
      dataset dsPageInitialization:empty-dataset().

      userSessionService = new UserSession().
      do on error undo, throw:
         for each ttFilter no-lock:
            create ttColumn.
            assign ttColumn.ColumnName = ttFilter.FieldName.

            create ttSort.
            assign ttSort.ColumnName = ttFilter.FieldName
                   ttSort.SortOrder = 1
                   ttSort.SortDescending = true.

            create ttAdditionalFilter.
            assign ttAdditionalFilter.Order = 1
                   ttAdditionalFilter.Conjunction = ""
                   ttAdditionalFilter.OpenParen = false
                   ttAdditionalFilter.FieldName = ttFilter.FieldName
                   ttAdditionalFilter.Comparator = ttFilter.Operator
                   ttAdditionalFilter.CloseParen = false
                   ttAdditionalFilter.FieldValue = ttFilter.FieldValue
                   ttAdditionalFilter.DateValue = ?
                   ttAdditionalFilter.FieldName2 = ?
                   ttAdditionalFilter.ComparatorID = ?.

            create ttClientValue.
            assign ttClientValue.FieldName = ttFilter.FieldName
                   ttClientValue.FieldValue = ttFilter.FieldValue.
         end.

         run bl_interface_name/si/InitPaging.p ([initpaging_param]).

         this-object:CopyNativeToLogical().

      end.

   end method.
    """
        ]
        return getlist

    # fetchlist
    def fetchlist(self):
        fetchlist = [
    """
   method public void FetchList(input dataset dsKeyField,
                                input includeConcurrencyHash as logical,
                                output dataset-handle dsbl_name):

      {com/qad/qra/base/OutDSPre.i &dataset = "dsbl_name"}
      this-object:FetchListPrivate(input dataset dsKeyField by-reference,
                                   input includeConcurrencyHash,
                                   output dataset-handle dsbl_nameTemp by-reference).
      {com/qad/qra/base/OutDSPost.i &dataset = "dsbl_name"}

      finally:
         delete object dsbl_name no-error.
      end finally.

   end method.

   method protected void FetchListPrivate(input dataset dsKeyField,
                                          input includeConcurrencyHash as logical,
                                          output dataset dsbl_name):
      define variable userSessionService as UserSession no-undo.
      define variable hasLastRecord      as logical initial false.

      dataset dsbl_name:empty-dataset().
      userSessionService = new UserSession().

      do on error undo, throw:
         this-object:CreateQueryWithKeyFields(input dataset dsKeyField).

         run bl_interface_name/si/InitPaging.p ([initpaging_param]).

          this-object:CopyNativeToLogical(input true).

       end.

   end method.
    """
        ]
        return fetchlist

    # createquerywithkeyfield
    def createquerywithkeyfield(self):
        createquerywithkeyfield = [
    """
   method private void CreateQueryForKeyFields(input dataset-handle dsKeyField):
      define variable counter as integer no-undo.
      dataset dsPageInitialization:empty-dataset().

      for each ttKeyField no-lock:
         if ttKeyField.KeyFieldName = AssetMgmtConstants:SITECODE or
            ttKeyField.KeyFieldName = AssetMgmtConstants:SITE_CODE then do:
            create ttClientValue.
            assign ttClientValue.FieldName  = AssetMgmtConstants:SITE_CODE
                   ttClientValue.FieldValue = ttKeyField.KeyFieldValue.
         end.
         else do:
            counter = counter + 1.
            create ttAdditionalFilter.
            assign ttAdditionalFilter.Order = 1
                  ttAdditionalFilter.Conjunction = if counter > 1 then "and" else ""
                  ttAdditionalFilter.FieldName   = ttKeyField.KeyFieldName
                  ttAdditionalFilter.Comparator  = "="
                  ttAdditionalFilter.FieldValue  = ttKeyField.KeyFieldValue.
         end.
      end.

   end method.
    """
        ]
        return createquerywithkeyfield

    # getcomponentschema
    def getcomponentschema(self):
        getcomponentschema = [
    """
   method public void GetComponentSchema (output dataset-handle dsProject):
      dataset dsProject:empty-dataset().

   end method.
    """
        ]
        return getcomponentschema

    # createconfirmation
    def createconfirmation(self):
        createconfirmation = [
    """
   method public void CreateWithConfirmation(input dataset-handle dsbl_name,
                                             input-output dataset dsbl_nameConf):

      {com/qad/qra/base/InAndInOutDSPre.i &dataset = "dsbl_name"}
      this-object:CreateWithConfirmationPrivate(input dataset-handle dsbl_nameTemp by-reference,
                                                input-output dataset dsbl_nameConf by-reference).

      finally:
         delete object dsbl_name no-error.
      end finally.

   end method.

   method protected void CreateWithConfirmationPrivate(input dataset dsbl_name,
                                                       input-output dataset dsbl_nameConf):
		define variable confirmationsHandled as logical                    no-undo.
      define variable crudAction           as character initial "create" no-undo.

		this-object:HandleConfirmation(input dataset dsbl_name by-reference,
                                     input dataset dsSubmitError by-reference,
                                     input crudAction,
                                     input-output dataset dsbl_nameConf by-reference,
                                     output confirmationsHandled).

		if confirmationsHandled then
			this-object:Create(input dataset dsbl_name by-reference).

	end method.
    """
        ]
        return createconfirmation

    # updateconfirmation
    def updateconfirmation(self):
        updateconfirmation = [
    """
   method public void UpdateWithConfirmation(input dataset-handle dsbl_name,
                                             input-output dataset dsbl_nameConf):

      {com/qad/qra/base/InAndInOutDSPre.i &dataset = "dsbl_name"}
      this-object:UpdateWithConfirmationPrivate(input dataset-handle dsbl_nameTemp by-reference,
                                                input-output dataset dsbl_nameConf by-reference).

      finally:
         delete object dsbl_name no-error.
      end finally.

   end method.

	method protected void UpdateWithConfirmationPrivate(input dataset dsbl_name,
													                input-output dataset dsbl_nameConf):
       define variable confirmationsHandled   as logical                    no-undo.
       define variable crudAction             as character initial "update" no-undo.
       define variable validationStatus       as integer                    no-undo.
       define variable counter                as integer                    no-undo.
       define variable appError               as AppError                   no-undo.
       define variable assetMgmtError         as AssetMgmtError             no-undo.
       define variable msg                    as IMessage                   no-undo.
       define variable valMsgs                as IList                      no-undo.
       define variable userSessionService     as UserSession                no-undo.

	   userSessionService = new UserSession().
	   valMsgs = new List().

       do on error undo, throw:

          find first ttbl_name no-error.

          dataset dsbl_nativebl_name:empty-dataset().
          dataset dsMessage:empty-dataset().
          run bl_interface_name/si/getByID.p ([gbi_ds_param]).

          find first ttbl_nativebl_name no-error.

          if not available(ttbl_nativebl_name) then do:
             assetMgmtError = new AssetMgmtError(new Message(input AssetMgmtMessageKey:OPTIMISTIC_LOCK_DELETE_ERROR_NODOMAIN)).
             undo, throw assetMgmtError.
          end.
          else if ConcurrencyControlHandler:CheckConcurrencyHash (buffer ttbl_name:handle,
                                                                  buffer ttbl_nativebl_name:handle)
          then do:
             assetMgmtError = new AssetMgmtError(new Message(input AssetMgmtMessageKey:OPTIMISTIC_LOCK_UPDATE_ERROR_NODOMAIN)).
             undo, throw assetMgmtError.
          end.

          temp-table ttbl_nativebl_name:tracking-changes = true.

          this-object:CopyLogicalToNative().

          run bl_interface_name/si/checkRecordLevelSecurity.p ([crs_param]).
          for each ttMessage on error undo, throw:
            counter = counter + 1.
            msg = new Message(counter, ttMessage.MsgText).
            valMsgs:Add(msg).
          end.

          if not valMsgs:IsEmpty() then do:
            undo, throw new AssetMgmtError(valMsgs).
          end.

	      do on error undo, throw:
	         this-object:HandleConfirmation(input dataset dsbl_name by-reference,
                                           input dataset dsSubmitError by-reference,
                                           input crudAction,
                                           input-output dataset dsbl_nameConf by-reference,
                                           output confirmationsHandled).

             temp-table ttbl_nativebl_name:tracking-changes = false.

		  	 if confirmationsHandled then
		  		 this-object:Update(input dataset dsbl_name by-reference).

		  	 catch e as Progress.Lang.Error:
		  		 undo, throw e.
		     end catch.
	      end.
       end.

	end method.
    """
        ]
        return updateconfirmation

    # deleteconfirmation
    def deleteconfirmation(self):
        deleteconfirmation = [
    """
   method public void DeleteWithConfirmation(input dataset-handle dsbl_name,
                                             input-output dataset dsbl_nameConf):

      {com/qad/qra/base/InAndInOutDSPre.i &dataset = "dsbl_name"}
      this-object:DeleteWithConfirmationPrivate(input dataset-handle dsbl_nameTemp by-reference,
                                                input-output dataset dsbl_nameConf by-reference).

      finally:
         delete object dsbl_name no-error.
      end finally.

   end method.

   method protected void DeleteWithConfirmationPrivate(input dataset dsbl_name,
													                input-output dataset dsbl_nameConf):
      define variable confirmationsHandled as logical                    no-undo.
      define variable crudAction           as character initial "delete" no-undo.
      define variable validationStatus     as integer                    no-undo.
      define variable counter 			    as integer                    no-undo.
      define variable msg 			          as IMessage                   no-undo.
      define variable valMsgs 		       as IList                      no-undo.
      define variable userSessionService   as UserSession                no-undo.

      userSessionService = new UserSession().
      valMsgs = new List().

      dataset dsbl_nativebl_name:empty-dataset().
      dataset dsMessage:empty-dataset().

      find first ttbl_name no-error.
      run bl_interface_name/si/getByID.p ([gbi_ds_param]).

      temp-table ttbl_nativebl_name:tracking-changes = true.

      if not confirmationsHandled then do:
         for first ttbl_nativebl_name:
            delete ttbl_nativebl_name.
         end.

      run bl_interface_name/si/submit.p ([s_param]input userSessionService:SessionID).

      end.

      temp-table ttbl_nativebl_name:tracking-changes = false.

      for each ttSubmitError on error undo, throw:
         if ttSubmitError.MessageType =  AssetMgmtConstants:MESSAGE_TYPE_ERROR or
            ttSubmitError.MessageType = AssetMgmtConstants:MESSAGE_TYPE_FATAL then do:
               counter = counter + 1.
               msg = new Message(counter, ttSubmitError.MessageText).
               valMsgs:Add(msg).
         end.
      end.

      if not valMsgs:IsEmpty() then do:
          undo, throw new AssetMgmtError(valMsgs).
      end.

      do on error undo, throw:
         this-object:HandleConfirmation(input dataset dsbl_name by-reference,
                                        input dataset dsSubmitError by-reference,
                                        input crudAction,
                                        input-output dataset dsbl_nameConf by-reference,
                                        output confirmationsHandled).

         if confirmationsHandled then do:
             this-object:Delete(input dataset dsbl_name by-reference).
         end.
         catch e as Progress.Lang.Error:
            undo, throw e.
         end catch.
      end.

   end method.
    """
        ]
        return deleteconfirmation

    # HandleConfirmation
    def handleconfirmation(self):
        handleconfirmation = [
    """
   method protected void HandleConfirmationPrivate(input dataset dsbl_name,
                                                   input dataset dsSubmitError,
                                                   input crudAction as character,
                                                   input-output dataset dsbl_nameConf,
                                                   output confirmationsHandled as logical):
      define variable userSessionService as UserSession no-undo.
	   userSessionService = new UserSession().

      for each ttbl_name on error undo, throw:
         find first ttbl_nameConf of ttbl_name no-error.

         if not available(ttbl_nameConf) then do:
            create ttbl_nameConf.
            assign
               [bl_conf_table].

            case crudAction:
               when "delete" then do:

                  find first ttSubmitError no-error.

                  if available ttSubmitError then do:
                     assign ttbl_nameConf.IsDeleteConfirmed = false
                            ttbl_name.DeleteConfirmMsg = ttSubmitError.MessageText.
                     confirmationsHandled = not can-find(first ttbl_nameConf where ttbl_nameConf.IsDeleteConfirmed = false).
                  end.
                  else do:
                     ttbl_nameConf.IsDeleteConfirmed = true.
                     confirmationsHandled = true.
                  end.
               end.
            end.
         end.
         else do:
            confirmationsHandled = not can-find(first ttbl_nameConf where ttbl_nameConf.IsDeleteConfirmed = false).
         end.
      end.

   end method.
    """
        ]
        return handleconfirmation

    # Custom Method e.g. ci.optionalmethod()[i] 0:dataset 1:get 2:returnMessage
    def optionalmethod(self):
        optionalmethod = [
    """
   [bl_optional_method_dataset]:

      [bl_optional_private_input_method]

      finally:
         delete object dsbl_name no-error.
      end finally.

   end method.

   [bl_optional_private_method]:
      define variable assetMgmtError     as AssetMgmtError no-undo.
      define variable msg                as IMessage       no-undo.
      define variable valMsgs            as IList          no-undo.
      define variable validationStatus   as integer        no-undo.
      define variable counter            as integer        no-undo.
      define variable userSessionService as UserSession    no-undo.

      valMsgs = new List().
      userSessionService = new UserSession().

      if userSessionService:LoggedIn then do on error undo, throw:

      end.

   end method.
    """,

    """
   [bl_optional_method_get]:
      define variable userSessionService as UserSession no-undo.

      userSessionService = new UserSession().
      do on error undo, throw:
         run [**LEGACY_PROCEDURE**], input userSessionService:SessionID).

      end.

   end method.
    """,

    """
   [bl_optional_method_returnmessage]:
      define variable assetMgmtError     as AssetMgmtError  no-undo.
      define variable validationStatus   as integer         no-undo.
      define variable userSessionService as UserSession     no-undo.

      dataset dsbl_nativebl_name:empty-dataset().
      userSessionService = new UserSession().

      if userSessionService:LoggedIn then do on error undo:
         run bl_interface_name/si/getByID.p ([gbi_param]).

         find ttbl_name no-error.
         if not available(ttbl_name) then do:
            assetMgmtError = new AssetMgmtError(new Message(input AssetMgmtMessageKey:RECORD_DOES_NOT_EXIST, AssetMgmtTranslatedString:[bl_message_error])).
            undo, throw assetMgmtError.
         end.

         run bl_interface_name/si/[legacy_program].p ([**Legacy_Program**], input userSessionService:SessionID).

         for each ttMessage:
            returnMessage = returnMessage + ttMessage.MsgType + AssetMgmtConstants:DELIMITER_CHARACTER_HASH + ttMessage.MsgText + AssetMgmtConstants:DELIMITER_CHARACTER_HASH.
         end.
      end.

   end method.
    """
        ]
        return optionalmethod
