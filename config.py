#!/usr/bin/env python

import json
import os

class Config():
    def __init__(self):
        if os.path.exists('config.json'):
            with open(os.path.abspath('config.json'), 'r') as f:
                self.config = json.load(f)

    def get(self, name):
        if name == 'is_be':
            return self.config['is_be']
        elif name == 'bl_name':
            return self.config['bl_name']
        elif name == 'bl_dir_name':
            return self.config['bl_dir_name']
        elif name == 'bl_wsnamespace':
            return self.config['bl_wsnamespace']
        elif name == 'bl_entityname':
            return self.config['bl_entityname']
        elif name == 'be_source':
            return self.config['be_source']
        elif name == 'be_keyfields':
            return self.config['be_keyfields']
        elif name == 'be_native_keyfields':
            return self.config['be_native_keyfields']
        elif name == 'be_browseuri':
            return self.config['be_browseuri']
        elif name == 'be_securitycontext':
            return self.config['be_securitycontext']
        elif name == 'bl_parent':
            return self.config['bl_parent']
        elif name == 'bl_interface':
            return self.config['bl_interface']
        elif name == 'bl_implementation':
            return self.config['bl_implementation']
        elif name == 'bl_method':
            return self.config['optional_method']
        elif name == 'bl_parent':
            return self.config['bl_parent']
        elif name == 'bl_dir_parent':
            return self.config['bl_dir_parent']
        elif name == 'is_datalist':
            return self.config['is_datalist']
        elif name == 'java_url_prefix':
            return self.config['java_url_prefix']
        elif name == 'java_url_resource':
            return self.config['java_url_resource']
        elif name == 'java_keyfields_type':
            return self.config['java_keyfields_type']

##### INTERFACE #####

    # imports
    def import_lists(self):
        import_lists = [
            "{com/qad/assetmgmt/bl_interface_name/dsbl_name.i}",
            "{com/qad/assetmgmt/bl_interface_name/dsbl_nameConf.i}",
            "{com/qad/assetmgmt/common/dsMessage.i}",
            "{com/qad/qra/base/dsFilter.i}",
            "{com/qad/qra/base/dsKeyField.i}",
        ]
        return import_lists

    # create
    def create(self):
        create = [
    """
   /**
    * Create a new bl_name records.
    *
    * @param dsbl_name
    *
    * @external
    *
    * @example
    *
    *   using com.qad.assetmgmt.bl_dir_name.Ibl_name.
    *   using com.qad.assetmgmt.AssetMgmtServices.
    *
    *   {com/qad/assetmgmt/bl_interface_name/dsbl_name.i}
    *
    *   define variable lowercase_interfaceService as Ibl_name no-undo.
    *
    *   lowercase_interfaceService = AssetMgmtServices:Getbl_name().
    *
    *   lowercase_interfaceService:Create(input dataset dsbl_name by-reference).
    *
    */
   method public void Create(input dataset-handle dsbl_name).
    """
        ]
        return create

    # fetch
    def fetch(self):
        fetch = [
    """
   /**
    * Fetch a collection of Project records.
    *
[param_kf]
    * @param dsbl_name
    *
    * @external
    *
    * @example
    *
    *   using com.qad.assetmgmt.bl_dir_name.Ibl_name.
    *   using com.qad.assetmgmt.AssetMgmtServices.
    *
    *   {com/qad/assetmgmt/bl_interface_name/dsbl_name.i}
    *
    *   define variable lowercase_interfaceService as Ibl_name no-undo.
    *
    *   lowercase_interfaceService = AssetMgmtServices:Getbl_name().
    *
    *   lowercase_interfaceService:Fetch([input_example]
    *   [output] dataset dsbl_name by-reference).
    *
    */
   method public void Fetch([input_keyfield]
                            output dataset-handle dsbl_name).
    """
        ]
        return fetch

    # update
    def update(self):
        update = [
    """
   /**
    * Update one or more bl_name records.
    *
    * @param dsbl_name
    *
    * @external
    *
    * @example
    *
    *   using com.qad.assetmgmt.bl_dir_name.Ibl_name.
    *   using com.qad.assetmgmt.AssetMgmtServices.
    *
    *   {com/qad/assetmgmt/bl_interface_name/dsbl_name.i}
    *
    *   define variable lowercase_interfaceService as Ibl_name no-undo.
    *
    *   lowercase_interfaceService = AssetMgmtServices:Getbl_name().
    *
    *   lowercase_interfaceService:Update(input dataset dsbl_name by-reference).
    *
    */
   method public void Update(input dataset-handle dsbl_name).
    """
        ]
        return update

    # delete
    def delete(self):
        delete = [
    """
   /**
    * Delete a bl_name record.
    *
    * @param dsbl_name
    *
    * @external
    *
    * @example
    *
    *   using com.qad.assetmgmt.bl_dir_name.Ibl_name.
    *   using com.qad.assetmgmt.AssetMgmtServices.
    *
    *   {com/qad/assetmgmt/bl_interface_name/dsbl_name.i}
    *
    *   define variable lowercase_interfaceService as Ibl_name no-undo.
    *
    *   lowercase_interfaceService = AssetMgmtServices:Getbl_name().
    *
    *   lowercase_interfaceService:Update(input dataset dsbl_name by-reference).
    *
    */
   method public void Delete(input dataset-handle dsbl_name).
    """
        ]
        return delete

    # initialize
    def initialize(self):
        initialize = [
    """
   /**
    * Initialize the bl_name record.
    *
[param_kf]
    * @param dsbl_name
    *
    * @external
    *
    * @example
    *
    *   using com.qad.assetmgmt.bl_dir_name.Ibl_name.
    *   using com.qad.assetmgmt.AssetMgmtServices.
    *
    *   {com/qad/assetmgmt/bl_interface_name/dsbl_name.i}
    *
    *   define variable lowercase_interfaceService as Ibl_name no-undo.
    *
    *   lowercase_interfaceService = AssetMgmtServices:Getbl_name().
    *
    *   lowercase_interfaceService:Initialize([input_example]
    *   [output] dataset dsbl_name by-reference).
    *
    */
   method public void Initialize([input_keyfield]
                                 output dataset-handle dsbl_name).
    """
        ]
        return initialize

    # isvalid
    def isvalid(self):
        isvalid = [
    """
   /**
    * Validate bl_name field.
    *
    * @param fieldName
    * @param dsbl_name
    *
    * @return true if field passes all validations for input dataset, otherwise false
    *
    * @external
    *
    * @example
    *
    *   using com.qad.assetmgmt.bl_dir_name.Ibl_name.
    *   using com.qad.assetmgmt.AssetMgmtServices.
    *
    *   {com/qad/assetmgmt/bl_interface_name/dsbl_name.i}
    *
    *   define variable lowercase_interfaceService as Ibl_name no-undo.
    *
    *   lowercase_interfaceService = AssetMgmtServices:Getbl_name().
    *
    *   lowercase_interfaceService:IsValid(input fieldName,
[isvalid-line]
    *
    */
   method public logical IsValid(input fieldName as character,
                                 input-output dataset-handle dsbl_name).
    """
        ]
        return isvalid

    # exists
    def exists(self):
        exists = [
    """
   /**
    * Returns whether a record exists for the specified key fields.
    *
[param_kf]
    *
    * @external
    *
    * @example
    *
    *   using com.qad.assetmgmt.bl_dir_name.Ibl_name.
    *   using com.qad.assetmgmt.AssetMgmtServices.
    *
    *   define variable lowercase_interfaceService as Ibl_name no-undo.
    *   define variable hasRecord as logical no-undo.
    *
    *   lowercase_interfaceService = AssetMgmtServices:Getbl_name().
    *
    *   hasRecord = lowercase_interfaceService:Exists([input_example]).
    *
    */
   method public logical Exists([input_keyfield]).
    """
        ]
        return exists

    # getcontrolstates
    def getcontrolstates(self):
        getcontrolstates = [
    """
   /**
    * Enable or Disable fields based on data or data operations.
    *
    * @param dsbl_name
    * @param fieldNames
    * @param fieldStates
    *
    * @external
    *
    * @example
    *
    *   using com.qad.assetmgmt.bl_dir_name.Ibl_name.
    *   using com.qad.assetmgmt.AssetMgmtServices.
    *
    *   {com/qad/assetmgmt/bl_interface_name/dsbl_name.i}
    *
    *   define variable lowercase_interfaceService as Ibl_name no-undo.
    *
    *   lowercase_interfaceService = AssetMgmtServices:Getbl_name().
    *
    *   lowercase_interfaceService:GetControlStates([input_example] dataset dsbl_name by-reference,
    *   [output] fieldNames as character,
    *   [output] fieldStates as character).
    *
    */
   method public void GetControlStates(input dataset-handle dsbl_name,
                                       output fieldNames as character,
                                       output fieldStates as character).
    """
        ]
        return getcontrolstates

    # getlist
    def getlist(self):
        getlist = [
    """
   /**
    * Get a list of bl_name records matching the passed key criteria.
    *
    * @param dsFilter
    * @param dsbl_name
    *
    * @external
    *
    * @example
    *
    *   using com.qad.assetmgmt.bl_dir_name.Ibl_name.
    *   using com.qad.assetmgmt.AssetMgmtServices.
    *
    *   {com/qad/qra/base/dsFilter.i}
    *   {com/qad/assetmgmt/bl_interface_name/dsbl_name.i}
    *
    *   define variable lowercase_interfaceService as Ibl_name no-undo.
    *
    *   lowercase_interfaceService = AssetMgmtServices:Getbl_name().
    *
    *   lowercase_interfaceService:GetList(input dataset dsFilter by-reference,
    *   [output] dataset dsbl_name by-reference).
    *
    */
   method public void GetList(input dataset-handle dsFilter,
                              output dataset-handle dsbl_name).
    """
        ]
        return getlist

    # fetchlist
    def fetchlist(self):
        fetchlist = [
    """
   /**
    * Fetch a list of records from a business component
    *
    * @param dsKeyField A dataset containing key field names and values
    * @param includeConcurrencyHash Include the concurrency hash in the result set
    * @param dsComponent Dataset containing the business component data
    *
    */
   /** @securitycontext SYSTEM */
   method public void FetchList(input dataset dsKeyField,
                                input includeConcurrencyHash as logical,
                                output dataset-handle dsbl_name).
    """
        ]
        return fetchlist

    # createconfirmation
    def createconfirmation(self):
        createconfirmation = [
    """
   /**
    * Create one or more bl_name records returning any necessary confirmation messages.
    *
    * @param dsbl_name
    * @param dsbl_nameConf
    *
    * @external
    *
    */
   method public void CreateWithConfirmation(input dataset-handle dsbl_name,
                                             input-output dataset-handle dsdsbl_nameConf).
    """
        ]
        return createconfirmation

    # updateconfirmation
    def updateconfirmation(self):
        updateconfirmation = [
    """
   /**
    * Update one or more bl_name records returning any necessary confirmation messages.
    *
    * @param dsbl_name
    * @param dsbl_nameConf
    *
    * @external
    *
    */
   method public void UpdateWithConfirmation(input dataset-handle dsbl_name,
                                             input-output dataset-handle dsdsbl_nameConf).
    """
        ]
        return updateconfirmation

    # deleteconfirmation
    def deleteconfirmation(self):
        deleteconfirmation = [
    """
   /**
    * Delete one or more bl_name records returning any necessary confirmation messages.
    *
    * @param dsbl_name
    * @param dsbl_nameConf
    *
    * @external
    *
    */
   method public void DeleteWithConfirmation(input dataset-handle dsbl_name,
                                             input-output dataset-handle dsdsbl_nameConf).
    """
        ]
        return deleteconfirmation

    # Custom Method
    def optionalmethod(self):
        optionalmethod = [
    """
   /**
    * method_description
    *
[param]
    *
    * @external
    *
    * @example
    *
    *   using com.qad.assetmgmt.bl_dir_name.Ibl_name.
    *   using com.qad.assetmgmt.AssetMgmtServices.
[dataset_import]
    *   define variable lowercase_interfaceService as Ibl_name no-undo.
    *
    *   lowercase_interfaceService = AssetMgmtServices:Getbl_name().
    *
    *   lowercase_interfaceService:method_name([input_example]).
    *
    */
   method public method_return method_name([input_output_param]).
    """
        ]
        return optionalmethod

##### INPUT EXAMPLES #####
    def example_lists(self):
        example_lists = [
            '10-100',
            '131',
            '2',
            'AID-010',
            'AC0001',
            'Success',
            'P1001',
            '010202',
            '02-01-20',
            'Create',
            'false',
            'dataset dsbl_name by-reference',
            'returnMessage as character'

        ]
        return example_lists
