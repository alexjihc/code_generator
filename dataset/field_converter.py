#!/usr/bin/env python
import os
import re

field_converter_result = "field_converter_result.txt"
isvalid_field_converter_result = "isvalid_field_converter_result.txt"
logical_fields_lists   = []
native_fields_lists    = []

# script outputs 2 results
# 1. script to convert native temp-table fields to logical fields.
# to run, copy fields from common dataset.i file and paste the fields within comment string, cd into location of this dir and then run python field_converter.py. it generates field_converter_result.txt

# 2. script to output isvalid fieldname statement: ex. if fieldName = "short_qty" then fieldName = "short".
#                                                  ex. if fieldName = "manuf_code_desc" then fieldName = "manuf_code-desc".

# change this option for ReplaceFieldName e.g. 0, 1
replace_field_option = 1

# Option 0
# if fieldName = "aBCCode" then fieldName = "abc_code".
# if fieldName = "addDate" then fieldName = "add_date".

# Option 1
# if fieldName = "aBCCode" then fieldName = "abc_code".
# else if fieldName = "addDate" then fieldName = "add_date".

class Field_Converter():
    def __init__(self):

        # copy and paste the fields
        # ex. field abc_code as character
        temptable_field_lists = [
            """

field add_date         as date
field add_id           as character
field add_time         as integer
field certified        as logical
field certif_date      as date
field certif_due_date  as date
field chg_date         as date
field chg_id           as character
field chg_time         as integer
field loc_code         as character
field loc_status       as character
field serial_no        as character
field site_code        as character
field tool_no          as character
field display_add_time as character
field display_chg_time as character
field last_iss         as date

            """
        ]

        # convert to strings, strip and split
        temptable_strings = ''.join(temptable_field_lists)
        temptable_lists = temptable_strings.strip().splitlines()

        for temptable_list in temptable_lists:
            fields = temptable_list.strip().split()

            native_fields_lists.append(fields[1])
            field = self.check_fields(fields[1])
            if field != None:
                logical_fields_lists.append(field)
            else:
                print(fields[1], "is not in the check_field dictionary")

        # field_converter_result
        string_result = ""
        for i in range(len(logical_fields_lists)):
            if i < len(logical_fields_lists)-1:
                string_result += "'" + logical_fields_lists[i] + "',\n"
            else:
                string_result += "'" + logical_fields_lists[i] + "'"

        string_result = string_result.strip()
        with open(os.path.join(os.getcwd(), field_converter_result), 'w+') as result:
            result.write(string_result)


        # isvalid_field_converter_result
        fieldname_result = ""
        for i in range(len(logical_fields_lists)):

            fieldname = logical_fields_lists[i][:1].lower() + logical_fields_lists[i][1:]

            if replace_field_option == 0:
                fieldname_result += 'if fieldName = "' + fieldname + '" then fieldName = "' + native_fields_lists[i] + '".\n'
            elif replace_field_option == 1:
                if i < 1:
                    fieldname_result += 'if fieldName = "' + fieldname + '" then fieldName = "' + native_fields_lists[i] + '".\n'
                else:
                    fieldname_result += 'else if fieldName = "' + fieldname + '" then fieldName = "' + native_fields_lists[i] + '".\n'


        fieldname_result = fieldname_result.strip()
        with open(os.path.join(os.getcwd(), isvalid_field_converter_result), 'w+') as result:
            result.write(fieldname_result)

    # Field definition: add, modify, update if necessary
    def check_fields(self, field):
        fields = {
            'add_date': 'AddDate',
            'add_id': 'AddUserID',
            'add_time': 'AddTime',
            'certified': 'IsCertified',
            'certif_date': 'CertifiedDate',
            'certif_due_date': 'CertifiedDueDate',
            'chg_date': 'ChangeDate',
            'chg_id': 'ChangeUserID',
            'chg_time': 'ChangeTime',
            'loc_code': 'LocationCode',
            'loc_status': 'LocationStatus',
            'serial_no': 'SerialNumber',
            'site_code': 'SiteCode',
            'tool_no': 'ToolNumber',
            'display_add_time': 'DisplayAddTime',
            'display_chg_time': 'DisplayChangeTime',
            'last_iss': 'LastIssued'

        }
        return fields.get(field)


# Uncomment to run this generator
fc = Field_Converter()