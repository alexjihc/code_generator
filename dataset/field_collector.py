#!/usr/bin/env python
import os
import re

be_ds                  = "be_dataset.py"
service_ds             = "service_dataset.py"
be_dataset_result      = "be_dataset_result.txt"
service_dataset_result = "service_dataset_result.txt"
string_result          = ""

# script to look at what fields we have in be and service dataset.py file
# to run, CD in to dataset directory, and run python field_collector.py. it generates result.txt
# ex. Service Dataset: ReceivedCost, Field: ReceivedCost, Type: decimal

# Service
with open(os.path.join(os.getcwd(), service_ds), 'r') as service:
    readFile = service.readlines()                    

with open(os.path.join(os.getcwd(), service_dataset_result), 'w+') as result:
    for line in readFile:                        
        if "service_dataset['" in line:
            service_dataset_variable = line.split("'")            
            string_result += "Service Dataset: " + service_dataset_variable[1] + ", "

        if "   field " in line:
            field_variable = line.split()            
            string_result += "Field: " + field_variable[1] + ", Type: " + field_variable[3] + "\n"

    result.write(string_result)

string_result = ""

# BE
with open(os.path.join(os.getcwd(), be_ds), 'r') as be:
    readFile = be.readlines()                    

with open(os.path.join(os.getcwd(), be_dataset_result), 'w+') as result:
    for line in readFile:                        
        if "be_dataset['" in line:
            be_dataset_variable = line.split("'")            
            string_result += "BE Dataset: " + be_dataset_variable[1] + ", "

        if "   field " in line:
            field_variable = line.split()                        
            string_result += "Field: " + field_variable[1] + ", Type: " + field_variable[3] + "\n"

    result.write(string_result)


