#!/usr/bin/env python

##### BE DATASET FIELDS #####
be_dataset = {}

class BE():
  def __init__(self):
    self.be_dataset = be_dataset

### A ###
be_dataset['ABCCode'] = \
"""
  /**
   * ABC Code
   *
   * @label eam-aa492f96-87b5-88ba-da11-0fd6e9edd9ad
   * @source abc_code
   * @format x(12)
   */
   field ABCCode as character initial ?
"""

be_dataset['ActualContract'] = \
"""
  /**
   * Act Contract
   *
   * @label eam-8469f75b-62c9-10bf-db11-1748730ed4de
   * @readonly
   * @format >>>,>>>,>>>.99
   */
   field ActualContract as decimal initial ?
"""

be_dataset['ActualLabor'] = \
"""
  /**
   * Act Labor
   *
   * @label eam-8469f75b-62c9-10bf-db11-1748b70b1ad6
   * @readonly
   * @format >>>,>>>,>>>.99
   */
   field ActualLabor as decimal initial ?
"""

be_dataset['ActualManual'] = \
"""
  /**
   * Act Manual
   *
   * @label eam-8469f75b-62c9-10bf-db11-1748176496e3
   * @readonly
   * @format >>>,>>>,>>>.99
   */
   field ActualManual as decimal initial ?
"""

be_dataset['ActualMaterial'] = \
"""
  /**
   * Act Material
   *
   * @label eam-8469f75b-62c9-10bf-db11-1748ff6741da
   * @readonly
   * @format >>>,>>>,>>>.99
   */
   field ActualMaterial as decimal initial ?
"""

be_dataset['ActualTax'] = \
"""
  /**
   * Act Tax
   *
   * @label mfg-TAX
   * @readonly
   * @format >>>,>>>,>>>.99
   */
   field ActualTax as decimal initial ?
"""

be_dataset['ActualTotal'] = \
"""
  /**
   * Act Total
   *
   * @label eam-8469f75b-62c9-10bf-db11-17483f6a69e8
   * @readonly
   * @format >>>,>>>,>>>.99
   */
   field ActualTotal as decimal initial ?
"""

be_dataset['AddDate'] = \
"""
  /**
   * Add Date
   *
   * @label eam-00793df6-a9f7-d043-9d95-9d2e10275e67
   * @readonly
   * @source add_date
   * @format 99/99/9999
   */
   field AddDate as date initial ?
"""

be_dataset['AddUserID'] = \
"""
  /**
   * Add User ID
   *
   * @label eam-0bbd9e58-7a00-d545-955c-871e9bc77f25
   * @readonly
   * @source add_id
   * @format x(12)
   */
   field AddUserID as character initial ?
"""

be_dataset['AddTime'] = \
"""
  /**
   * Add Time
   *
   * @label eam-012f51bf-32de-5845-b37b-45c20a48740e
   * @readonly
   * @source add_time
   * @format >>>>9
   */
   field AddTime as integer initial ?
"""

be_dataset['ActualCost'] = \
"""
  /**
   * Actual Cost (Partial Issue)
   *
   * @label eam-d3a950e0-370f-5fbc-e211-01d963b38ab9
   * @format >,>>>,>>9.99
   */
   field ActualCost as decimal initial ?
"""

be_dataset['AdjustLocation'] = \
"""
  /**
   * Location
   *
   * @label eam-5391acc7-88d8-ee4a-981a-ddd56ea3b5f9
   * @format x(10)
   * @required
   */
   field AdjustLocation as character initial ?
"""

be_dataset['AdjustmentCostCenter'] = \
"""
  /**
   * Adj Cost Center
   *
   * @label eam-a86cbdee-fe9d-8dab-dd11-b5f3c0ab36bb
   * @format x(12)
   */
   field AdjustmentCostCenter as character initial ?
"""

be_dataset['AddToBOM'] = \
"""
  /**
   * Add to BOM?
   *
   * @label eam-aa492f96-87b5-88ba-da11-0fd6d27502ae
   * @format mfg-YES/mfg-NO
   */
   field AddToBOM as logical initial ?
"""

be_dataset['AdjustmentAccountCode'] = \
"""
  /**
   * Adj Acct No
   *
   * @label eam-a86cbdee-fe9d-8dab-dd11-b5f349cab77c
   * @format x(12)
   */
   field AdjustmentAccountCode as character initial ?
"""

be_dataset['Account'] = \
"""
  /**
   * Account
   *
   * @label eam-181
   * @readonly
   * @source acct_no
   * @format x(20)
   */
   field Account as character initial ?
"""

be_dataset['AccountNumber'] = \
"""
  /**
   * Account
   *
   * @label eam-2660a9c1-d85c-4048-944e-26b04f421d80
   * @source acct_no
   * @format x(20)
   */
   field AccountNumber as character initial ?
"""

be_dataset['AccountNumberDescription'] = \
"""
  /**
   * Account Description
   *
   * @label mfg-ACCOUNT_DESCRIPTION
   * @readonly
   * @source acct_no-desc
   * @format x(30)
   */
   field AccountNumberDescription as character initial ?
"""

be_dataset['AccountNumberDescription1'] = \
"""
  /**
   * Account Description
   *
   * @label mfg-ACCOUNT_DESCRIPTION
   * @readonly
   * @format x(30)
   */
   field AccountNumberDescription as character initial ?
"""

be_dataset['AccountDescription'] = \
"""
  /**
   * Account Description
   *
   * @label mfg-ACCOUNT_DESCRIPTION
   * @readonly
   * @format x(30)
   */
   field AccountNumberDescription as character initial ?
"""

be_dataset['AccountManager'] = \
"""
  /**
   * Account Manager
   *
   * @label eam-c8e7cf62-5f32-59ab-de11-41a1c9fa9068
   * @source acct_mgr
   * @format x(20)
   */
   field AccountManager as character initial ?
"""

be_dataset['AccountManagerDescription'] = \
"""
  /**
   * Account Manager Description
   *
   * @label eam-ACCOUNT_TYPE_DESCRIPTION
   * @readonly
   * @format x(30)
   */
   field AccountManagerDescription as character initial ?
"""

be_dataset['ActualLaborHours'] = \
"""
  /**
   * Actual Labor Hours
   *
   * @label ACTUAL_LABOUR_HOURS
   * @readonly
   * @source act_labor_hrs
   * @format ->>,>>9.99
   */
   field ActualLaborHours as decimal initial ?
"""

be_dataset['ActualCost'] = \
"""
  /**
   * Actual Cost
   *
   * @label eam-a3a0db23-a6a5-12a3-db11-485971623d82
   * @source actcost
   * @format >>>,>>>,>>>,>>9.99
   */
   field ActualCost as decimal initial ?
"""

be_dataset['ActualCompletionDate'] = \
"""
  /**
   * Actual Completion Date
   *
   * @label mfg-ACTUAL_COMPLETION_DATE
   * @readonly
   * @source cdate
   * @format 99/99/9999
   */
   field ActualCompletionDate as date initial ?
"""

be_dataset['AllocatedToJobs'] = \
"""
  /**
   * Allocated to Jobs
   *
   * @label eam-b928136c-0233-8fa8-e111-e10a009fe5f2
   * @readonly
   * @source alloc_to_jobs
   * @format >>>,>>>,>>>,>>9.99
   */
   field AllocatedToJobs as decimal initial ?
"""

be_dataset['Allocated'] = \
"""
  /**
   * Reserved Qty
   *
   * @label RESERVED_QTY
   * @readonly
   * @format >,>>>,>>9.99
   */
   field Allocated as decimal initial ?
"""

be_dataset['AllocatedQuantity'] = \
"""
  /**
   * Reserved Quantity
   *
   * @label RESERVED_QTY
   * @readonly
   * @source allocated
   * @format >,>>>,>>9.99
   */
   field AllocatedQuantity as decimal initial ?
"""

be_dataset['AmountPaid'] = \
"""
  /**
   * Amount Paid
   *
   * @label eam-8bbd16c7-aa0e-0e9d-db11-714a9ce9b589
   * @format ->,>>>,>>9.99
   */
   field AmountPaid as decimal initial ?
"""

be_dataset['AvailableQuantity'] = \
"""
  /**
   * Available
   *
   * @label mfg-AVAILABLE
   * @readonly
   * @source avail_qty
   * @format >,>>>,>>9.99
   */
   field AvailableQuantity as decimal initial ?
"""

be_dataset['AvailableQuantity2'] = \
"""
  /**
   * Available
   *
   * @label mfg-AVAILABLE
   * @readonly
   * @source pt_available
   * @format >,>>>,>>9.99
   */
   field AvailableQuantity as decimal initial ?
"""

be_dataset['Area'] = \
"""
   /**
   * Rebuild
   *
   * @label eam-ac50b67f-cc67-5885-dd11-2feaf91f7233
   * @source area
   * @format X(20)
   */
   field Area as character initial ?
"""

be_dataset['AreaCode'] = \
"""
  /**
   * Area
   *
   * @label eam-aa492f96-87b5-88ba-da11-0fd68f610eae
   * @source area_code
   * @format x(20)
   */
   field AreaCode as character initial ?
"""

be_dataset['AreaCodeDescription'] = \
"""
  /**
   * Area Description
   *
   * @label AREA_DESCRIPTION
   * @readonly
   * @source area_code-desc
   * @format x(30)
   */
   field AreaCodeDescription as character initial ?
"""

be_dataset['AreaCodeDescription1'] = \
"""
  /**
   * Area Description
   *
   * @label AREA_DESCRIPTION
   * @readonly
   * @format x(30)
   */
   field AreaCodeDescription as character initial ?
"""

be_dataset['APvariance'] = \
"""
  /**
   * AP Variance
   *
   * @label eam-8c3cc92c-f8ba-e8be-e011-4d2dc849adfa
   * @readonly
   * @source ap_variance
   * @format ->>>,>>>,>>>,>>9.99
   */
   field APvariance as decimal initial ?
"""

be_dataset['Assigned'] = \
"""
  /**
   * Assigned
   *
   * @label eam-fbaef324-e999-4db8-de11-356b5841175b
   * @source assign_to
   * @format x(11)
   * @length 22
   */
   field Assigned as character initial ?
"""

be_dataset['AssignedDescription'] = \
"""
  /**
   * Assigned Description
   *
   * @label ASSIGNED_DESCRIPTION
   * @readonly
   * @format x(30)
   */
   field AssignedDescription as character initial ?
"""

be_dataset['AssetLife'] = \
"""
  /**
   * Asset Life
   *
   * @label eam-c8e7cf62-5f32-59ab-de11-41a19395cc68
   * @source asst_life
   * @format 999
   */
   field AssetLife as integer initial ?
"""

be_dataset['AssemblyCode'] = \
"""
  /**
   * Assembly
   *
   * @label eam-1cef6a0d-39ad-8440-981e-6b642abe1d98
   * @source assy_code
   * @format X(5)
   */
   field AssemblyCode as character initial ?
"""

be_dataset['Attachments'] = \
"""
  /**
   * Links
   *
   * @label eam-915757b4-fe96-0185-df11-f29fefc7588f
   * @readonly
   * @format mfg-YES/mfg-NO
   */
   field Attachments as logical initial ?
"""

be_dataset['Authorized'] = \
"""
  /**
   * Authorized
   *
   * @label mfg-AUTHORIZED
   * @readonly
   * @source auth
   * @format mfg-YES/mfg-NO
   */
   field Authorized as logical initial ?
"""

be_dataset['AuthorizedByDescription'] = \
"""
  /**
   * Authorized By Description
   *
   * @label AUTHORIZED_BY_DESCRIPTION
   * @readonly
   * @format x(30)
   */
   field AuthorizedByDescription as character initial ?
"""

be_dataset['AuthorizedFunding'] = \
"""
  /**
   * Authorized Funding
   *
   * @label eam-97822ccb-2f56-4d81-e111-f10ecfdf88d7
   * @readonly
   * @source auth_cost
   * @format >>>,>>>,>>>,>>9.99
   */
   field AuthorizedFunding as decimal initial ?
"""

be_dataset['AuthorizedMargin'] = \
"""
  /**
   * Authorized Margin %
   *
   * @label EAM-AUTHORIZED_MARGIN_PERCENT
   * @readonly
   * @source auth_margin
   * @format >>,>>9.99
   */
   field AuthorizedMargin as decimal initial ?
"""

be_dataset['AuthorizedPurchases'] = \
"""
  /**
   * Authorized Purchases
   *
   * @label EAM-AUTHORIZED_PURCHASES
   * @readonly
   * @source auth_purch
   * @format >>>,>>>,>>>,>>9.99
   */
   field AuthorizedPurchases as decimal initial ?
"""

be_dataset['AuthorizationStatus'] = \
"""
  /**
   * Authorization Status
   *
   * @label EAM-AUTHORIZATION_STATUS
   * @readonly
   * @source auth_status
   * @format x(12)
   */
   field AuthorizationStatus as character initial ?
"""

be_dataset['AuthorizedSpending'] = \
"""
  /**
   * Authorized Spending
   *
   * @label EAM-AUTHORIZED_SPENDING
   * @readonly
   * @format ->>>,>>>,>>>,>>>,>>9.99
   */
   field AuthorizedSpending as decimal initial ?
"""

be_dataset['AuthorizedBy'] = \
"""
  /**
   * Authorized By
   *
   * @label EAM-AUTHORIZEDBY
   * @readonly
   * @source auth_by
   * @format x(12)
   */
   field AuthorizedBy as character initial ?
"""

### B ###
be_dataset['BillToCustomer'] = \
"""
  /**
   * Bill To Customer
   *
   * @label eam-c8e7cf62-5f32-59ab-de11-41a19b44fc68
   * @source bill_to_cust
   * @format x(30)
   */
   field BillToCustomer as character initial ?
"""

be_dataset['Buyer'] = \
"""
  /**
   * Buyer
   *
   * @label mfg-BUYER
   * @source buyer
   * @format x(12)
   */
   field Buyer as character initial ?
"""

be_dataset['BuyerDescription'] = \
"""
  /**
   * Buyer Description
   *
   * @label BUYER_DESCRIPTION
   * @readonly
   * @source buyer-desc
   * @format x(30)
   */
   field BuyerDescription as character initial ?
"""

be_dataset['BuyerDescription1'] = \
"""
  /**
   * Buyer Description
   *
   * @label BUYER_DESCRIPTION
   * @readonly
   * @format x(30)
   */
   field BuyerDescription as character initial ?
"""

be_dataset['BuyerCode'] = \
"""
  /**
   * Buyer
   *
   * @label eam-84126798-c5bd-63be-dc11-7134c2563966
   * @format x(12)
   */
   field BuyerCode as character initial ?
"""

### C ###
be_dataset['CapitalizedAmount'] = \
"""
  /**
   * Capitalized Amount
   *
   * @label eam-f81fe08f-199b-c5b4-e011-21771d60abef
   * @source cap_amount
   * @format >>>,>>>,>>>,>>9.99
   */
   field CapitalizedAmount as decimal initial ?
"""

be_dataset['Catalog'] = \
"""
  /**
   * Catalog
   *
   * @label eam-80531bc1-b0b7-5fb1-db11-791679f8194c
   * @source catalog
   * @format x(12)
   */
   field Catalog as character initial ?
"""

be_dataset['CatalogDescription'] = \
"""
  /**
   * Catalog Description
   *
   * @label eam-f77b9a98-0dcb-534e-9665-2c68ff17c419
   * @readonly
   * @source catalog-desc
   * @format x(30)
   */
   field CatalogDescription as character initial ?
"""

be_dataset['CatalogDescription1'] = \
"""
  /**
   * Catalog Description
   *
   * @label eam-f77b9a98-0dcb-534e-9665-2c68ff17c419
   * @readonly
   * @format x(30)
   */
   field CatalogDescription as character initial ?
"""

be_dataset['CertifiedDate'] = \
"""
  /**
   * Certified Date
   *
   * @label CERTIFIED_DATE
   * @source certif_date
   * @format 99/99/99
   */
   field CertifiedDate as date initial ?
"""

be_dataset['CertifiedDueDate'] = \
"""
  /**
   * Certified Due Date
   *
   * @label CERTIFIED_ DUE_DATE
   * @source certif_due_date
   * @format 99/99/99
   */
   field CertifiedDueDate as date initial ?
"""

be_dataset['ChangeDate'] = \
"""
  /**
   * Change Date
   *
   * @label eam-prototype00924
   * @source chg_date
   * @format 99/99/9999
   */
   field ChangeDate as date initial ?
"""

be_dataset['ChangeUserID'] = \
"""
  /**
   * Change User
   *
   * @label CHANGE_USER
   * @source chg_id
   * @format x(12)
   */
   field ChangeUser as character initial ?
"""

be_dataset['ChangeTime'] = \
"""
  /**
   * Change Time
   *
   * @label eam-003e5cb0-1175-0c4f-9045-da313e407994
   * @readonly
   * @source chg_time
   * @format >>>>>9
   */
   field ChangeTime as integer initial ?
"""

be_dataset['ClassCode'] = \
"""
  /**
   * Class
   *
   * @label eam-1a94b819-018a-e14d-b341-33fe071f9fd6
   * @format x(12)
   */
   field ClassCode as character initial ?
"""

be_dataset['Comment'] = \
"""
  /**
   * Comment
   *
   * @label COMMENT
   * @format X(60)
   */
   field Comment as character initial ?
"""

be_dataset['ConsignCostCenter'] = \
"""
  /**
   * Cons Asset CC
   *
   * @label eam-9614cccb-59e3-7397-da11-daf7916362ba
   * @format x(12)
   */
   field ConsignCostCenter as character initial ?
"""

be_dataset['ConsignAdjustCostCenter'] = \
"""
  /**
   * Cons Adj CC
   *
   * @label eam-9614cccb-59e3-7397-da11-daf7e9c564ba
   * @format x(12)
   */
   field ConsignAdjustCostCenter as character initial ?
"""

be_dataset['ConsignAccount'] = \
"""
  /**
   * Cons Asset Acct
   *
   * @label eam-9614cccb-59e3-7397-da11-daf7eaef2dba
   * @format x(12)
   */
   field ConsignAccount as character initial ?
"""

be_dataset['ConsignAdjustAccount'] = \
"""
  /**
   * Cons Adj Acct
   *
   * @label eam-9614cccb-59e3-7397-da11-daf7435230ba
   * @format x(12)
   */
   field ConsignAdjustAccount as character initial ?
"""

be_dataset['CostPerItem'] = \
"""
  /**
   * Cost Per Item
   *
   * @label COST_PER_ITEM
   * @format >>>,>>>,>>>.99
   */
   field CostPerItem as decimal initial ?
"""

be_dataset['Closed'] = \
"""
  /**
   * Closed
   *
   * @label eam-83fbc6ef-e011-6ab3-db11-fcf75c8843b6
   * @source closed
   * @format 99/99/9999
   */
   field Closed as date initial ?
"""

be_dataset['ClosedTime'] = \
"""
  /**
   * Time
   *
   * @label eam-87958a18-58c1-8d85-dc11-54cfcc0244ae
   * @source closed_time
   * @format x(8)
   */
   field ClosedTime as character initial ?
"""

be_dataset['CommitBalance'] = \
"""
  /**
   * Balance to Commit
   *
   * @label EAM-BALANCE_TO_COMMIT
   * @readonly
   * @source commit_balance
   * @format >>>,>>>,>>>,>>9.99
   */
   field CommitBalance as decimal initial ?
"""

be_dataset['Committed'] = \
"""
  /**
   * Committed
   *
   * @label eam-949bb89c-be0a-1baf-de11-932e1abffcb9
   * @readonly
   * @source committed
   * @format >>>,>>>,>>>,>>9.99
   */
   field Committed as decimal initial ?
"""

be_dataset['CostCenter'] = \
"""
  /**
   * Cost Center
   *
   * @label COST_CENTER
   * @source cc
   * @format x(12)
   */
   field CostCenter as character initial ?
"""

be_dataset['CostCenterDescription'] = \
"""
  /**
   * Cost Center Description
   *
   * @label mfg-COST_CENTER_DESCRIPTION
   * @readonly
   * @source cc-desc
   * @format x(30)
   */
   field CostCenterDescription as character initial ?
"""

be_dataset['CostCenterDescription1'] = \
"""
  /**
   * Cost Center Description
   *
   * @label mfg-COST_CENTER_DESCRIPTION
   * @readonly
   * @format x(30)
   */
   field CostCenterDescription as character initial ?
"""

be_dataset['CostPerUnitOfMeasure'] = \
"""
  /**
   * Cost
   *
   * @label eam-aa492f96-87b5-88ba-da11-0fd690e439b0
   * @readonly
   * @format mfg-YES/mfg-NO
   */
   field CostPerUnitOfMeasure as decimal initial ?
"""

be_dataset['CostPlannedTax'] = \
"""
  /**
   * Planned Tax - Cost Panel
   *
   * @label PLANNED_TAX
   * @readonly
   * @nomapping
   * @format >>>,>>>,>>>,>>9.99
   */
   field CostPlannedTax as decimal initial ?
"""

be_dataset['CostIncluded'] = \
"""
  /**
   * Cost Included
   *
   * @label COSTS_INCLUDED
   * @readonly
   * @format x(12)
   */
   field CostIncluded as character initial ?
"""

be_dataset['ContractAccount'] = \
"""
  /**
   * Contract Account
   *
   * @label CONTRACT_ACCOUNT
   * @source acct_noc
   * @format x(20)
   */
   field ContractAccount as character initial ?
"""

be_dataset['ContractAccountDescription'] = \
"""
  /**
   * Contract Account Description
   *
   * @label CONTRACT_ACCOUNT_DESCRIPTION
   * @readonly
   * @source acct_noc-desc
   * @format x(30)
   */
   field ContractAccountDescription as character initial ?
"""

be_dataset['ContractAccountDescription1'] = \
"""
  /**
   * Contract Account Description
   *
   * @label CONTRACT_ACCOUNT_DESCRIPTION
   * @readonly
   * @format x(30)
   */
   field ContractAccountDescription as character initial ?
"""

be_dataset['ContractSubAccount'] = \
"""
  /**
   * Contract Sub Account
   *
   * @label CONTRACT_SUB_ACCOUNT
   * @source sub_acct_noc
   * @format x(20)
   */
   field ContractSubAccount as character initial ?
"""

be_dataset['ContractSubAccountDescription'] = \
"""
  /**
   * Contract Sub Account Description
   *
   * @label CONTRACT_SUB_ACCOUNT_DESCRIPTION
   * @readonly
   * @source sub_acct_noc-desc
   * @format x(30)
   */
   field ContractSubAccountDescription as character initial ?
"""

be_dataset['ContractSubAccountDescription1'] = \
"""
  /**
   * Contract Sub Account Description
   *
   * @label CONTRACT_SUB_ACCOUNT_DESCRIPTION
   * @readonly
   * @format x(30)
   */
   field ContractSubAccountDescription as character initial ?
"""

be_dataset['ContractInvoiceCost'] = \
"""
  /**
   * Contract Invoice Cost
   *
   * @label eam-87549459-5a21-59ae-de11-0efb38efcb89
   * @readonly
   * @format >>>,>>>,>>>.99
   */
   field ContractInvoiceCost as decimal initial ?
"""

be_dataset['ContractCost'] = \
"""
  /**
   * Cont Cost
   *
   * @label eam-8b72bfb1-2686-3da1-da11-3cc09bd0825d
   * @readonly
   * @source exp_cont
   * @format >>>,>>>,>>>,>>9.99
   */
   field ContractCost as decimal initial ?
"""

be_dataset['ContractBudget'] = \
"""
  /**
   * Contract Budget
   *
   * @label eam-ae49559c-92bd-3b87-de11-a5e5d8f79b27
   * @source cont_budg
   * @format ->>,>>9.99
   */
   field ContractBudget as decimal initial ?
"""

be_dataset['ContractForecast'] = \
"""
  /**
   * Contract Forecast
   *
   * @label eam-ae49559c-92bd-3b87-de11-a5e5111c019a
   * @source cont_forecast
   * @format ->>,>>9.99
   */
   field ContractForecast as decimal initial ?
"""

be_dataset['ContractPOVariance'] = \
"""
  /**
   * Contract PO Variance
   *
   * @label eam-87549459-5a21-59ae-de11-0efb457f89ce
   * @readonly
   * @format >>>,>>>,>>>.99
   */
   field ContractPOVariance as decimal initial ?
"""

be_dataset['Character1'] = \
"""
  /**
   * Character 1
   *
   * @label eam-9e128334-2108-0090-de11-ef2c0553e0e5
   * @userdefinedfield
   * @source char_1
   * @format x(20)
   */
   field Character1 as character initial ?
"""

be_dataset['Character1Description'] = \
"""
  /**
   * Character 1 Description
   *
   * @label eam-e3cd0b3b-a750-1b83-df11-5925356ce1ae
   * @readonly
   * @source char_1-desc
   * @format x(30)
   */
   field Character1Description as character initial ?
"""

be_dataset['Character1Description1'] = \
"""
  /**
   * Character 1 Description
   *
   * @label eam-e3cd0b3b-a750-1b83-df11-5925356ce1ae
   * @readonly
   * @format x(30)
   */
   field Character1Description as character initial ?
"""

be_dataset['Character2'] = \
"""
  /**
   * Character 2
   *
   * @label eam-9e128334-2108-0090-de11-ef2c6864e5ea
   * @userdefinedfield
   * @source char_2
   * @format x(20)
   */
   field Character2 as character initial ?
"""

be_dataset['Character2Description'] = \
"""
  /**
   * Character 2 Description
   *
   * @label eam-965c3bf9-5ad1-1188-dc11-e9442301928e
   * @readonly
   * @source char_2-desc
   * @format x(30)
   */
   field Character2Description as character initial ?
"""

be_dataset['Character2Description1'] = \
"""
  /**
   * Character 2 Description
   *
   * @label eam-965c3bf9-5ad1-1188-dc11-e9442301928e
   * @readonly
   * @format x(30)
   */
   field Character2Description as character initial ?
"""

be_dataset['Character3'] = \
"""
  /**
   * Character 3
   *
   * @label eam-ce8623a2-44db-0c83-dd11-43c5f0a007b1
   * @userdefinedfield
   * @source char_3
   * @format x(20)
   */
   field Character3 as character initial ?
"""

be_dataset['Character4'] = \
"""
  /**
   * Character 4
   *
   * @label eam-ce8623a2-44db-0c83-dd11-43c5a8b4b2b7
   * @userdefinedfield
   * @source char_4
   * @format x(20)
   */
   field Character4 as character initial ?
"""

be_dataset['CustomChar01'] = \
"""
  /**
   * @label mfg-WARNING
   * @readonly
   * @source cust_chr01
   * @format x(8)
   */
   field CustomChar01 as character initial ?
"""

be_dataset['CustomChar02'] = \
"""
  /**
   * @label
   * @readonly
   * @source cust_chr02
   * @format x(8)
   */
   field CustomChar02 as character initial ?
"""

be_dataset['CustomChar03'] = \
"""
  /**
   * @label eam-bb95fe9d-f41e-7b94-dc11-24831ac3d380
   * @readonly
   * @source cust_chr03
   * @format x(8)
   */
   field CustomChar03 as character initial ?
"""

be_dataset['CustomChar04'] = \
"""
  /**
   * @label eam-bb95fe9d-f41e-7b94-dc11-24839a011b84
   * @readonly
   * @source cust_chr04
   * @format x(8)
   */
   field CustomChar04 as character initial ?
"""

be_dataset['CustomDecimal01'] = \
"""
  /**
   * @label eam-bb95fe9d-f41e-7b94-dc11-24833a2d4f87
   * @readonly
   * @source cust_dec01
   * @format >>>,>>>,>>9.99
   */
   field CustomDecimal01 as decimal initial ?
"""

be_dataset['CustomDecimal02'] = \
"""
  /**
   * @label eam-bb95fe9d-f41e-7b94-dc11-2483762cf88a
   * @readonly
   * @source cust_dec02
   * @format >>>,>>>,>>9.99
   */
   field CustomDecimal02 as decimal initial ?
"""

be_dataset['CustomDecimal03'] = \
"""
  /**
   * @label eam-bb95fe9d-f41e-7b94-dc11-2483e243388e
   * @readonly
   * @source cust_dec03
   * @format >>>,>>>,>>9.99
   */
   field CustomDecimal03 as decimal initial ?
"""

be_dataset['CustomDecimal04'] = \
"""
  /**
   * @label eam-bb95fe9d-f41e-7b94-dc11-2483d20b8991
   * @readonly
   * @source cust_dec04
   * @format >>>,>>>,>>9.99
   */
   field CustomDecimal04 as decimal initial ?
"""

be_dataset['CustomDate01'] = \
"""
  /**
   * @label eam-bb95fe9d-f41e-7b94-dc11-248326f35f97
   * @readonly
   * @source cust_dte01
   * @format 99/99/99
   */
   field CustomDate01 as date initial ?
"""

be_dataset['CustomLogical01'] = \
"""
  /**
   * @label eam-bb95fe9d-f41e-7b94-dc11-2483ba58ae9a
   * @readonly
   * @source cust_log01
   * @format mfg-YES/mfg-NO
   */
   field CustomLogical01 as logical initial ?
"""

be_dataset['CreatedDate'] = \
"""
  /**
   * Original Date
   *
   * @label EAM-ORIGINALDATE
   * @readonly
   * @source add_date
   * @format 99/99/9999
   */
   field CreatedDate as date initial ?
"""

be_dataset['CreatedByUserID'] = \
"""
  /**
   * Originator
   *
   * @label eam-a3a0db23-a6a5-12a3-db11-485940f2bc86
   * @source add_id
   * @format x(12)
   */
   field CreatedByUserID as character initial ?
"""

be_dataset['CreatedTime'] = \
"""
  /**
   * Add Time
   *
   * @label eam-012f51bf-32de-5845-b37b-45c20a48740e
   * @readonly
   * @source add_time
   * @format >>>>9
   */
   field CreatedTime as integer initial ?
"""

be_dataset['Critical'] = \
"""
  /**
   * Critical
   *
   * @label mfg-CRITICAL
   * @source critical
   * @format mfg-YES/mfg-NO
   */
   field Critical as logical initial ?
"""

be_dataset['CurrentEstimate'] = \
"""
  /**
   * Current Estimate
   *
   * @label CURRENT_ESTIMATE
   * @source curr_est
   * @format >>>,>>>,>>>,>>9.99
   */
   field CurrentEstimate as decimal initial ?
"""

be_dataset['CustomerBusinessGroup'] = \
"""
  /**
   * Customer Business Group
   *
   * @label EAM-CUSTOMER_BUSINESS_GROUP
   * @source cust_bus_no
   * @format x(30)
   * @length 60
   */
   field CustomerBusinessGroup as character initial ?
"""

be_dataset['CurrentQuantity'] = \
"""
  /**
   * On Hand
   *
   * @label eam-8bbd16c7-aa0e-0e9d-db11-714a6dbfec89
   * @readonly
   * @source curr_qty
   * @format ->,>>>,>>9.99
   */
   field CurrentQuantity as decimal initial ?
"""

be_dataset['CurrentQuantity2'] = \
"""
  /**
   * Current On Hand
   *
   * @label eam-cb2944f0-b5ac-4bb8-db11-5968cee217c0
   * @readonly
   * @source curr_qty
   * @format ->,>>>,>>9.99
   */
   field CurrentQuantity as decimal initial ?
"""

be_dataset['CurrentQuantity3'] = \
"""
  /**
   * On Hand Quantity
   *
   * @label ON_HAND_QTY
   * @readonly
   * @format >,>>>,>>9.99
   */
   field CurrentQuantity as decimal initial ?
"""

### D ###
be_dataset['DateDue'] = \
"""
  /**
   * Date Due
   *
   * @label eam-b9fcaed1-5c01-0fae-dc11-dc2f96e24002
   * @readonly
   * @format 99/99/9999
   */
   field DateDue as date initial ?
"""

be_dataset['DateReceived'] = \
"""
  /**
   * Date Received
   *
   * @label mfg-DATE_RECEIVED
   * @format 99/99/9999
   */
   field DateReceived as date initial ?
"""

be_dataset['DateReturned'] = \
"""
  /**
   * Date Returned
   *
   * @label eam-ba5c81e2-933a-cf86-da11-6bea6490ebe9
   * @format 99/99/9999
   */
   field DateReturned as date initial ?
"""

be_dataset['Date1'] = \
"""
  /**
   * Date 1
   *
   * @label eam-ce8623a2-44db-0c83-dd11-43c5b869b8ce
   * @userdefinedfield
   * @source date_1
   * @format 99/99/9999
   */
   field Date1 as date initial ?
"""

be_dataset['Decimal1'] = \
"""
  /**
   * Decimal 1
   *
   * @label eam-ce8623a2-44db-0c83-dd11-43c5ea41c2d3
   * @userdefinedfield
   * @source dec_1
   * @format ->>>,>>>,>>9.99
   */
   field Decimal1 as decimal initial ?
"""

be_dataset['Decimal2'] = \
"""
  /**
   * Decimal 2
   *
   * @label eam-ce8623a2-44db-0c83-dd11-43c58e895dd9
   * @userdefinedfield
   * @source dec_2
   * @format ->>>,>>>,>>9.99
   */
   field Decimal2 as decimal initial ?
"""

be_dataset['Decimal3'] = \
"""
  /**
   * Decimal 3
   *
   * @label eam-9520c19c-4616-c9ac-de11-45a2a215b0d9
   * @userdefinedfield
   * @source dec_3
   * @format ->>>,>>>,>>9.99
   */
   field Decimal3 as decimal initial ?
"""

be_dataset['Decimal4'] = \
"""
  /**
   * Decimal 4
   *
   * @label eam-9520c19c-4616-c9ac-de11-45a23a9acde0
   * @userdefinedfield
   * @source dec_4
   * @format ->>>,>>>,>>9.99
   */
   field Decimal4 as decimal initial ?
"""

be_dataset['DefaultLocation'] = \
"""
  /**
   * Default Location
   *
   * @label eam-8867945b-8da5-2ea1-e511-49832173dcf4
   * @source default_loc
   * @format x(8)
   */
   field DefaultLocation as character initial ?
"""

be_dataset['Description'] = \
"""
  /**
   * Description
   *
   * @label DESCRIPTION
   * @source description
   * @format x(30)
   */
   field Description as character initial ?
"""

be_dataset['DepartmentCode'] = \
"""
  /**
   * Department
   *
   * @label eam-940ad953-9fd0-6d9e-dd11-af9bc81039dc
   * @source dept_code
   * @format X(10)
   */
   field DepartmentCode as character initial ?
"""

be_dataset['DepartmentCodeDescription'] = \
"""
  /**
   * Department Description
   *
   * @label DEPARTMENT_DESCRIPTION
   * @readonly
   * @source dept_code-desc
   * @format x(30)
   */
   field DepartmentCodeDescription as character initial ?
"""

be_dataset['DepartmentCodeDescription1'] = \
"""
  /**
   * Department Description
   *
   * @label DEPARTMENT_DESCRIPTION
   * @readonly
   * @format x(30)
   */
   field DepartmentCodeDescription as character initial ?
"""

be_dataset['DisplayAddTime'] = \
"""
  /**
   * Add Time(c)
   *
   * @label eam-90e73b6e-e0c0-1da8-e311-27a6c79adbc7
   * @readonly
   * @nomapping
   * @format x(12)
   */
   field DisplayAddTime as character initial ?
"""

be_dataset['DisplayChangeTime'] = \
"""
  /**
   * Change Time(c)
   *
   * @label eam-90e73b6e-e0c0-1da8-e311-27a6278adddf
   * @readonly
   * @nomapping
   * @format x(12)
   */
   field DisplayChangeTime as character initial ?
"""

be_dataset['DisallowedActions'] = \
"""
  /**
   * Based on some conditions this field tells the consumer what are
   * the actions that cannot be performed on this record.
   * e.g. "EDIT", "DELETE", "EDITDELETE" or ""
   *
   */
   field DisallowedActions as character initial ?
"""

be_dataset['DisallowedActionsMessage'] = \
"""
  /**
   * Message for disallowed actions.
   *
   */
   field DisallowedActionsMessage as character initial ?
"""

### E ###
be_dataset['EndItemPart'] = \
"""
  /**
   * End Item Part
   *
   * @label END_ITEM_PART
   * @source end_item_part
   * @format x(30)
   */
   field EndItemPart as character initial ?
"""

be_dataset['EstimatedCompletionDate'] = \
"""
  /**
   * Estimated Completion Date
   *
   * @label EAM-ESTIMATED_COMPLETION_DATE
   * @source edate
   * @format 99/99/9999
   */
   field EstimatedCompletionDate as date initial ?
"""

be_dataset['Estimates'] = \
"""
  /**
   * Estimates
   *
   * @label eam-0e58ab96-8bdc-4bfe-bc4c-eb1bf82d5024
   * @readonly
   * @nomapping
   * @format ->>>,>>>,>>>,>>>,>>9.99
   */
   field Estimates as decimal initial ?
"""

be_dataset['EstimatedContract'] = \
"""
  /**
   * Estimated Contract
   *
   * @label eam-8469f75b-62c9-10bf-db11-17489f49bcb3
   * @readonly
   * @format >>>,>>>,>>>.99
   */
   field EstimatedContract as decimal initial ?
"""

be_dataset['EstimatedAvailableFunding'] = \
"""
  /**
   * Estimated Net Available Funding
   *
   * @label EAM-ESTIMATED_NET_AVAILABLE_FUNDING
   * @readonly
   * @source est_avail_fund
   * @format >>>,>>>,>>>.99
   */
   field EstimatedAvailableFunding as decimal initial ?
"""

be_dataset['EstimatedInvoiceDate'] = \
"""
  /**
   * Estimated Invoice Date
   *
   * @label ESTIMATED_INVOICE_DATE
   * @source est_inv_date
   * @format 99/99/9999
   */
   field EstimatedInvoiceDate as date initial ?
"""

be_dataset['EstimatedInvoice'] = \
"""
  /**
   * Estimated Invoice Date Comment
   *
   * @label ESTIMATED_INVOICE_DATE_COMMENT
   * @source est_inv_date_comment
   * @format x(150)
   * @length 2000
   */
   field EstimatedInvoice DateComment as character initial ?
"""

be_dataset['EstimatedLabor'] = \
"""
  /**
   * Estimated Labor
   *
   * @label eam-8469f75b-62c9-10bf-db11-174843b266a8
   * @readonly
   * @format >>>,>>>,>>>.99
   */
   field EstimatedLabor as decimal initial ?
"""

be_dataset['EstimatedtMaterial'] = \
"""
  /**
   * Estimated Material
   *
   * @label eam-8469f75b-62c9-10bf-db11-1748876e93ae
   * @readonly
   * @format >>>,>>>,>>>.99
   */
   field EstimatedtMaterial as decimal initial ?
"""

be_dataset['EstimatedPPAPDate'] = \
"""
  /**
   * Estimated PPAP Date
   *
   * @label ESTIMATED_PPAP_DATE
   * @source est_ppap_date
   * @format 99/99/9999
   */
   field EstimatedPPAPDate as date initial ?
"""

be_dataset['EstimateToComplete'] = \
"""
  /**
   * Estimate To Complete
   *
   * @label eam-d7161e15-56f7-61ac-df11-4447c4f86a77
   * @readonly
   * @source est_to_comp
   * @format >>>,>>>,>>>,>>9.99
   */
   field EstimateToComplete as decimal initial ?
"""

be_dataset['EstimatedTax'] = \
"""
  /**
   * Est Tax
   *
   * @label mfg-TAX
   * @readonly
   * @format >>>,>>>,>>>.99
   */
   field EstimatedTax as decimal initial ?
"""

be_dataset['EstimatedTotal'] = \
"""
  /**
   * Est Total
   *
   * @label eam-8469f75b-62c9-10bf-db11-1748a762a2b8
   * @readonly
   * @format >>>,>>>,>>>.99
   */
   field EstimatedTotal as decimal initial ?
"""

be_dataset['EquipmentNumber'] = \
"""
  /**
   * Equipment
   *
   * @label eam-prototype00792
   * @source equip_no
   * @format x(20)
   */
   field EquipmentNumber as character initial ?
"""

be_dataset['ExpenseAccount'] = \
"""
  /**
   * Expense Account
   *
   * @label mfg-EXPENSE_ACCOUNT
   * @format x(20)
   */
   field ExpenseAccount as character initial ?
"""

be_dataset['ExpenseAccountDescription'] = \
"""
  /**
   * Expense Account Description
   *
   * @label mfg-EXPENSE_ACCOUNT
   * @format x(30)
   */
   field ExpenseAccountDescription as character initial ?
"""

be_dataset['ExpenseCostCenter'] = \
"""
  /**
   * Exp Cost Center
   *
   * @label eam-961276bf-3a65-819e-dc11-050a9ea8e8e6
   * @format x(12)
   */
   field ExpenseCostCenter as character initial ?
"""

be_dataset['ExpenseSite'] = \
"""
  /**
   * Expense Site
   *
   * @label eam-80531bc1-b0b7-5fb1-db11-791681593b4c
   * @source exp_site
   * @format X(8)
   */
   field ExpenseSite as character initial ?
"""

be_dataset['ExpenseSiteDescription'] = \
"""
  /**
   * Expense Site Description
   *
   * @label EXPENSE_SITE_DESCRIPTION
   * @readonly
   * @source exp_site-desc
   * @format x(30)
   */
   field ExpenseSiteDescription as character initial ?
"""

be_dataset['ExpenseSiteDescription1'] = \
"""
  /**
   * Expense Site Description
   *
   * @label EXPENSE_SITE_DESCRIPTION
   * @readonly
   * @format x(30)
   */
   field ExpenseSiteDescription as character initial ?
"""

be_dataset['ExpenseType'] = \
"""
  /**
   * Expense Type
   *
   * @label eam-9e128334-2108-0090-de11-092da1106638
   * @source exp_type
   * @format mfg-YES/mfg-NO
   */
   field ExpenseType as logical initial ?
"""

be_dataset['ExpenseTypeCharacter'] = \
"""
  /**
   * Expense Type Character
   *
   * @label eam-9e128334-2108-0090-de11-092da1106638
   * @datalist list:assetmgmt:finance:proj_hdr:exp_type_char
   * @format x(12)
   */
   field ExpenseTypeCharacter as char initial ?
"""

be_dataset['ExpenseTypeDescription'] = \
"""
  /**
   * Expense Type Description
   *
   * @label EXPENSE_TYPE_DESCRIPTION
   * @readonly
   * @format x(30)
   */
   field ExpenseTypeDescription as character initial ?
"""

be_dataset['ExternalMaterialCost'] = \
"""
  /**
   * External Material Cost
   *
   * @label EAM-EXTERNAL_MATERIAL_COST
   * @readonly
   * @source ext_exp_mtrl
   * @format >>>,>>>,>>>,>>9.99
   */
   field ExternalMaterialCost as decimal initial ?
"""

### F ###
be_dataset['FailureType'] = \
"""
  /**
   * Failure Type
   *
   * @label eam-80531bc1-b0b7-5fb1-db11-791682593b4c
   * @source failure_type
   * @format x(8)
   */
   field FailureType as character initial ?
"""

be_dataset['FormPreparer'] = \
"""
  /**
   * Form Preparer
   *
   * @label eam-c8e7cf62-5f32-59ab-de11-41a14346836a
   * @source form_preparer
   * @format x(12)
   */
   field FormPreparer as character initial ?
"""

be_dataset['FormPreparerDescription'] = \
"""
  /**
   * Form Preparer Description
   *
   * @label FORM_PREPARER_DESCRIPTION
   * @readonly
   * @format x(30)
   */
   field FormPreparerDescription as character initial ?
"""

be_dataset['FundedAmount'] = \
"""
  /**
   * Funded Amount
   *
   * @label FUNDED_AMOUNT
   * @readonly
   * @source fund_amt
   * @format ->>>,>>>,>>>,>>>,>>9.99
   */
   field FundedAmount as decimal initial ?
"""

be_dataset['FundedStatus'] = \
"""
  /**
   * Funded Status
   *
   * @label eam-c8e7cf62-5f32-59ab-de11-41a1f941e06a
   * @readonly
   * @source fund_status
   * @format x(1)
   */
   field FundedStatus as character initial ?
"""

be_dataset['FundingType'] = \
"""
  /**
   * Funding Type
   *
   * @label eam-c8e7cf62-5f32-59ab-de11-41a1a78e0d6b
   * @source fund_type
   * @format x(1)
   */
   field FundingType as character initial ?
"""

be_dataset['FundedStatus'] = \
"""
  /**
   * Funded Status
   *
   * @label eam-c8e7cf62-5f32-59ab-de11-41a1f941e06a
   * @readonly
   * @format x(12)
   */
   field FundedStatus as character initial ?
"""

be_dataset['FullyIssued'] = \
"""
  /**
   * Fully Issued
   *
   * @label eam-eb821ce7-b73a-3c97-e111-36eda95c4ed1
   * @source fully_issued
   * @format mfg-YES/mfg-NO
   */
   field FullyIssued as logical initial ?

"""

### G ###
### H ###
be_dataset['HighPriority'] = \
"""
  /**
   * High Priority
   *
   * @label HIGH_PRIORITY
   * @format mfg-YES/mfg-NO
   */
   field HighPriority as logical initial ?
"""

be_dataset['HistoricalRequisitionNumber'] = \
"""
  /**
   * Historical Req No
   *
   * @label eam-8b52eec8-4444-d780-de11-d008a97c413b
   * @readonly
   * @source poi_no
   * @format >>>>>>>>9
   */
   field HistoricalRequisitionNumber as integer initial ?
"""

### I ###
be_dataset['InitialLocation'] = \
"""
  /**
   * Initial Location
   *
   * @label eam-fe2bc93f-be51-a6a8-db11-97fdb870666d
   * @readonly
   * @format x(12)
   */
   field InitialLocation as character initial ?
"""

be_dataset['IsAutoIssue'] = \
"""
  /**
   * Auto Issue?
   *
   * @label eam-aa492f96-87b5-88ba-da11-0fd6412613ae
   * @format mfg-YES/mfg-NO
   */
   field IsAutoIssue as logical initial ?
"""

be_dataset['IsActive'] = \
"""
  /**
   * Active?
   *
   * @label eam-3ed88c60-dbb8-274d-9784-c40c8a1095f4
   * @format mfg-YES/mfg-NO
   */
   field IsActive as logical initial ?
"""

be_dataset['IsAuthorized'] = \
"""
  /**
   * Authorized
   *
   * @label eam-8c15796c-12e7-1e85-dc11-1f2bf30eadcf
   * @readonly
   * @source auth
   * @format mfg-YES/mfg-NO
   */
   field IsAuthorized as logical initial ?
"""

be_dataset['IsCancelQuantity'] = \
"""
/**
   * Cancel
   *
   * @label CANCEL_QUANTITY
   * @format mfg-YES/mfg-NO
   */
   field IsCancelQuantity as logical initial ?
"""

be_dataset['IsCertified'] = \
"""
  /**
   * Certified
   *
   * @label CERTIFIED
   * @source certified
   * @format mfg-YES/mfg-NO
   */
   field IsCertified as logical initial ?
"""


be_dataset['IsConsigned'] = \
"""
  /**
   * Consignment
   *
   * @label eam-eb974414-a239-7cb7-de11-d3c323373502
   * @readonly
   * @format mfg-YES/mfg-NO
   */
   field IsConsigned as logical initial ?
"""

be_dataset['IsFullyIssued'] = \
"""
  /**
   * Fully Issued
   *
   * @label eam-eb821ce7-b73a-3c97-e111-36eda95c4ed1
   * @source fully_issued
   * @format mfg-YES/mfg-NO
   */
   field IsFullyIssued as logical initial ?
"""

be_dataset['IsNetBudget'] = \
"""
  /**
   * Net Budget
   *
   * @label eam-99f1ea26-d7da-c74f-b2ac-eae376e92a2a
   * @readonly
   * @format mfg-YES/mfg-NO
   */
   field IsNetBudget as decimal initial ?
"""

be_dataset['IsPrimary'] = \
"""
  /**
   * Primary
   *
   * @label mfg-PRIMARY
   * @format mfg-YES/mfg-NO
   */
   field IsPrimaryAccount as logical initial ?
"""

be_dataset['IssueCost'] = \
"""
  /**
   * Issue Cost
   *
   * @label mfg-ISSUE_COST
   * @format ->,>>>,>>9.99
   */
   field IssueCost as decimal initial ?
"""

be_dataset['IsRotable'] = \
"""
  /**
   * Rotable
   *
   * @label eam-bf7346b4-07d6-6694-e411-2e1e9338af97
   * @readonly
   * @source rotable
   * @format mfg-YES/mfg-NO
   */
   field IsRotable as logical initial ?
"""

be_dataset['IsSerialized'] = \
"""
  /**
   * Rotable
   *
   * @label eam-bf7346b4-07d6-6694-e411-2e1e9338af97
   * @readonly
   * @source serialized
   * @format mfg-YES/mfg-NO
   */
   field IsSerialized as logical initial ?
"""

be_dataset['IsSelected'] = \
"""
  /**
    * Indicate if the current record is selected for action
    *
    * @format mfg-YES/mfg-NO
    */
   field IsSelected as logical initial ?
"""

be_dataset['IsSource'] = \
"""
  /**
   * Source
   *
   * @label eam-aa492f96-87b5-88ba-da11-0fd65a4bdfaf
   * @source source
   * @format mfg-YES/mfg-NO
   */
   field IsSource as logical initial ?
"""

be_dataset['IsReserved'] = \
"""
  /**
   * Reserved
   *
   * @label eam-bb7a00b4-edbc-a996-db11-0b06a4491970
   * @readonly
   * @source reserved
   * @format mfg-YES/mfg-NO
   */
   field IsReserved as logical initial ?
"""

be_dataset['IsReserved2'] = \
"""
  /**
   * Reserved
   *
   * @label eam-bb7a00b4-edbc-a996-db11-0b06a4491970
   * @readonly
   * @format mfg-YES/mfg-NO
   */
   field IsReserved as logical initial ?
"""

be_dataset['IsRotable'] = \
"""
  /**
   * Rotable
   *
   * @label eam-bf7346b4-07d6-6694-e411-2e1e9338af97
   * @format mfg-YES/mfg-NO
   */
   field IsRotable as logical initial ?
"""

be_dataset['Issued'] = \
"""
  /**
   * Issued
   *
   * @label eam-f6348607-c873-978c-e111-b9eb09071737
   * @readonly
   * @format >,>>>,>>9.99
   */
   field Issued as decimal initial ?
"""

be_dataset['IssuedQuantity'] = \
"""
  /**
   * Issued Quantity
   *
   * @label mfg-ISSUED_QTY
   * @source iss_qty
   * @format >,>>>,>>9.99
   */
   field IssuedQuantity as decimal initial ?
"""

be_dataset['IssueQuantity'] = \
"""
  /**
   * Quantity To Issue
   *
   * @label QTY_TO_ISSUE
   * @format >,>>>,>>9.99
   */
   field IssueQuantity as decimal initial ?
"""

be_dataset['IssueUnitofMeasure'] = \
"""
  /**
   * Issue UOM
   *
   * @label eam-aa492f96-87b5-88ba-da11-0fd685bd32b0
   * @readonly
   * @format x(12)
   */
   field IssueUnitofMeasure as character initial ?
"""

be_dataset['IsSuccessful'] = \
"""
  /**
   * @label mfg-RESULT
   * @format mfg-YES/mfg-NO
   */
   field IsSuccessful as logical initial ?
"""

be_dataset['IsTaxable'] = \
"""
  /**
   * Taxable?
   *
   * @label eam-eb974414-a239-7cb7-de11-d3c38dc7a915
   * @readonly
   * @format mfg-YES/mfg-NO
   */
   field IsTaxable as logical initial ?
"""

be_dataset['IsTemplate'] = \
"""
  /**
   * Template
   *
   * @label TEMPLATE
   * @source IsTemplate
   * @format mfg-YES/mfg-NO
   */
   field IsTemplate as logical initial ?
"""

be_dataset['InternalMaterialCost'] = \
"""
  /**
   * Internal Material Cost
   *
   * @label EAM-INTERNAL_MATERIAL_COST
   * @readonly
   * @source exp_mtrl
   * @format >>>,>>>,>>>,>>9.99
   */
   field InternalMaterialCost as decimal initial ?
"""

be_dataset['Integer1'] = \
"""
  /**
   * Integer 1
   *
   * @label eam-ce8623a2-44db-0c83-dd11-43c53078daf8
   * @source int_1
   * @format ->>>,>>>,>>9
   */
   field Integer1 as integer initial ?
"""

be_dataset['Integer2'] = \
"""
  /**
   * Integer 2
   *
   * @label eam-ce8623a2-44db-0c83-dd11-43c5faf30bff
   * @source int_2
   * @format ->>>,>>>,>>9
   */
   field Integer2 as integer initial ?
"""

be_dataset['InvoiceAmount'] = \
"""
  /**
   * Invoice Amount
   *
   * @label INVOICE_AMOUNT
   * @readonly
   * @source invoice_amt
   * @format ->>>,>>>,>>>,>>>,>>9.99
   */
   field InvoiceAmount as decimal initial ?
"""

be_dataset['InvoiceCostTax'] = \
"""
  /**
   * Invoice Cost Tax
   *
   * @label mfg-TAX
   * @readonly
   * @format >>>,>>>,>>>.99
   */
   field InvoiceCostTax as decimal initial ?
"""

### J ###
be_dataset['JobNumber'] = \
"""
  /**
   * Job
   *
   * @label eam-ecfbaa03-7abb-754c-a4d1-cdffe11379cf
   * @source job_no
   * @format x(20)
   */
   field JobNumber as character initial ?
"""

### K ###
### L ###
be_dataset['LaborBudget'] = \
"""
  /**
   * Labor Budget
   *
   * @label eam-ae49559c-92bd-3b87-de11-a4e558f48114
   * @source lab_budg
   * @format ->>,>>9.99
   */
   field LaborBudget as decimal initial ?
"""

be_dataset['LaborBurden'] = \
"""
  /**
   * Labor Burden
   *
   * @label eam-aa947156-2b0a-cf81-df11-bd413eb35e40
   * @source lab_burden
   * @readonly
   * @format ->>,>>9.99
   */
   field LaborBurden as decimal initial ?
"""

be_dataset['LaborCost'] = \
"""
  /**
   * Labor Cost
   *
   * @label eam-238b41be-4fa8-234e-aaa2-8ec2219eb8e8
   * @readonly
   * @source exp_labor
   * @format >>>,>>>,>>>,>>9.99
   */
   field LaborCost as decimal initial ?
"""

be_dataset['LaborForecast'] = \
"""
  /**
   * Labor Forecast
   *
   * @label eam-ae49559c-92bd-3b87-de11-a5e586e9f687
   * @source lab_forecast
   * @format ->>,>>9.99
   */
   field LaborForecast as decimal initial ?
"""

be_dataset['LastIssued'] = \
"""
/**
   * Last Issued
   *
   * @label LAST_ISSUED
   * @readonly
   * @source last_iss
   * @format 99/99/9999
   */
   field LastIssued as date initial ?
"""

be_dataset['LastPhysical'] = \
"""
  /**
   * Last Physical
   *
   * @label eam-aa492f96-87b5-88ba-da11-0fd6f81568af
   * @readonly
   * @source phy_last
   * @format 99/99/9999
   */
   field LastPhysical as date initial ?
"""

be_dataset['LastReceived'] = \
"""
  /**
   * Last Received
   *
   * @label Last_Received
   * @readonly
   * @source last_rec
   * @format 99/99/9999
   */
   field LastReceived as date initial ?
"""

be_dataset['LastRefresh'] = \
"""
  /**
   * Date/Time
   *
   * @label eam-2485e61e-cc6e-5d49-8694-5ae3e531156a
   * @readonly
   * @source last_refresh
   * @format x(16)
   * @length 32
   */
   field LastRefresh as character initial ?
"""

be_dataset['LeadDays'] = \
"""
  /**
   * Lead Days
   *
   * @label eam-aa492f96-87b5-88ba-da11-0fd6cda314af
   * @source lead_days
   * @format >>9
   */
   field LeadDays as integer initial ?
"""

be_dataset['Locked'] = \
"""
  /**
   * Locked
   *
   * @label mfg-LOCKED
   * @readonly
   * @source locked_proj
   * @format mfg-YES/mfg-NO
   */
   field Locked as logical initial ?
"""

be_dataset['Logical1'] = \
"""
  /**
   * Logical 1
   *
   * @label eam-ce8623a2-44db-0c83-dd11-44c5fe9c1408
   * @userdefinedfield
   * @source log_1
   * @format mfg-YES/mfg-NO
   */
   field Logical1 as logical initial ?
"""

be_dataset['LastSalesOrderInvoice'] = \
"""
  /**
   * Last Sales Order Invoice
   *
   * @label LAST_SALES_ORDER_INVOICE
   * @readonly
   * @format x(12)
   */
   field LastSalesOrderInvoice as character initial ?
"""

be_dataset['LastSalesOrderLineReturnQuantity'] = \
"""
  /**
   * Last Issue/Return Qty
   *
   * @label LAST_ISSUE_RETURN_QTY
   * @format >,>>>,>>9.99
   */
   field LastSalesOrderLineReturnQuantity as decimal initial ?
"""

be_dataset['LastSalesOrder'] = \
"""
  /**
   * Last Sales Order
   *
   * @label LAST_SALES_ORDER
   * @readonly
   * @format x(12)
   */
   field LastSalesOrder as character initial ?
"""

be_dataset['LastSalesOrderLine'] = \
"""
  /**
   * Last Sales Order Line
   *
   * @label LAST_SALES_ORDER_LINE
   * @readonly
   * @format ->,>>>,>>9
   */
   field LastSalesOrderLine as integer initial ?
"""

be_dataset['LastSOLineQuantityOrdered'] = \
"""
  /**
   * Last SO Line Qty Ordered
   *
   * @label eam-a56ce30e-ce0a-5a8e-e211-330f408f7427
   * @readonly
   * @format >,>>>,>>9.99
   */
   field LastSOLineQuantityOrdered as decimal initial ?
"""

be_dataset['Location'] = \
"""
  /**
   * Location
   *
   * @label eam-5391acc7-88d8-ee4a-981a-ddd56ea3b5f9
   * @source location
   * @format x(10)
   */
   field Location as character initial ?
"""

be_dataset['LocationCode'] = \
"""
  /**
   * Location
   *
   * @label eam-5391acc7-88d8-ee4a-981a-ddd56ea3b5f9
   * @source loc_code
   * @format x(20)
   */
   field LocationCode as character initial ?
"""

be_dataset['LocationStatus'] = \
"""
  /**
   * Location Status
   *
   * @label mfg-LOCATION_STATUS
   * @readonly
   * @source loc_status
   * @format x(12)
   */
   field LocationStatus as character initial ?
"""

be_dataset['LocationType'] = \
"""
   /**
   * Location Type
   *
   * @label eam-5391acc7-88d8-ee4a-981a-ddd56ea3b5f9
   * @source location
   * @format x(10)
   */
   field LocationType as character initial ?
"""

be_dataset['LineNumber'] = \
"""
  /**
   * Line
   *
   * @label eam-8f1aaaae-24ec-4b9e-dd11-
   * @source line_no
   * @format >9
   */
   field LineNumber as integer initial ?
"""

be_dataset['LinesSelected'] = \
"""
   /**
   * Lines selected
   *
   * @label LINES_SELECTED
   * @readonly
   * @format >>>>>>>9
   */
   field LinesSelected as integer initial ?
"""

be_dataset['LineShort'] = \
"""
  /**
   * Short
   *
   * @label LINE_SHORT
   * @format >,>>>,>>9.99
   */
   field ShortQuantity as decimal initial ?
"""

be_dataset['ListRequiredDate'] = \
"""
  /**
   * List Required Date
   *
   * @label LIST_REQUIRED_DATE
   * @format 99/99/9999
   */
   field ListRequiredDate as date initial ?
"""

### M ###
be_dataset['ManagementGoal'] = \
"""
  /**
   * Management Order Qty
   *
   * @label MANAGEMENT_ORDER_QTY
   * @readonly
   * @source mgt_goal
   * @format >,>>>,>>>.99
   */
   field ManagementGoal as decimal initial ?
"""

be_dataset['Margin'] = \
"""
  /**
   * Margin
   *
   * @label eam-c8e7cf62-5f32-59ab-de11-41a15d8a6a6b
   * @readonly
   * @source margin
   * @format ->>>,>>>,>>>,>>>,>>9.99
   */
   field Margin as decimal initial ?
"""

be_dataset['ManualCost'] = \
"""
  /**
   * Manual Cost
   *
   * @label eam-949bb89c-be0a-1baf-de11-932ec21819de
   * @readonly
   * @source exp_manual
   * @format >>>,>>>,>>>,>>9.99
   */
   field ManualCost as decimal initial ?
"""

be_dataset['ManufacturerCode'] = \
"""
  /**
   * Manufacturer
   *
   * @label mfg-MANUFACTURER
   * @source manuf_code
   * @format x(15)
   */
   field ManufacturerCode as character initial ?
"""

be_dataset['ManufacturerCodeDescription'] = \
"""
  /**
   * Manufacturer Description
   *
   * @label MANUFACTURER_DESCRIPTION
   * @readonly
   * @source manuf_code-desc
   * @format x(30)
   */
   field ManufacturerCodeDescription as character initial ?
"""

be_dataset['ManufacturerCodeDescription1'] = \
"""
  /**
   * Manufacturer Description
   *
   * @label MANUFACTURER_DESCRIPTION
   * @readonly
   * @format x(30)
   */
   field ManufacturerCodeDescription as character initial ?
"""

be_dataset['ManufacturerPartNumber'] = \
"""
  /**
   * Manufacturer Part Number
   *
   * @label MANUFACTURER_PART_NUMBER
   * @source manuf_part_no
   * @format x(50)
   */
   field ManufacturerPartNumber as character initial ?
"""

be_dataset['MasterPartListNumber'] = \
"""
  /**
   * Mstr Prts No
   *
   * @label MASTER_PARTS_LISTS
   * @source pph_no
   * @format >>>>>>>>9
   */
   field MasterPartListNumber as integer initial ?
"""

be_dataset['MaterialAccount'] = \
"""
  /**
   * Material Account
   *
   * @label mfg-MATERIAL_ACCOUNT
   * @source acct_nop
   * @format x(20)
   */
   field MaterialAccount as character initial ?
"""

be_dataset['MaterialAccountDescription'] = \
"""
  /**
   * Material Account Description
   *
   * @label MATERIAL_ACCOUNT_DESCRIPTION
   * @readonly
   * @source acct_nop-desc
   * @format x(30)
   */
   field MaterialAccountDescription as character initial ?
"""

be_dataset['MaterialAccountDescription1'] = \
"""
  /**
   * Material Account Description
   *
   * @label MATERIAL_ACCOUNT_DESCRIPTION
   * @readonly
   * @format x(30)
   */
   field MaterialAccountDescription as character initial ?
"""

be_dataset['MaterialSubAccount'] = \
"""
  /**
   * Matl Sub Acct
   *
   * @label MATERIAL_SUB_ACCOUNT
   * @source sub_acct_nop
   * @format x(20)
   */
   field MaterialSubAccount as character initial ?
"""

be_dataset['MaterialSubAccountDescription'] = \
"""
  /**
   * Material Sub Account Description
   *
   * @label MATERIAL_SUB_ACCOUNT_DESCRIPTION
   * @readonly
   * @source sub_acct_nop-desc
   * @format x(30)
   */
   field MaterialSubAccountDescription as character initial ?
"""

be_dataset['MaterialSubAccountDescription1'] = \
"""
  /**
   * Material Sub Account Description
   *
   * @label MATERIAL_SUB_ACCOUNT_DESCRIPTION
   * @readonly
   * @format x(30)
   */
   field MaterialSubAccountDescription as character initial ?
"""

be_dataset['MaterialBudget'] = \
"""
  /**
   * Material Budget
   *
   * @label eam-ae49559c-92bd-3b87-de11-a5e56bea141e
   * @source mtrl_budg
   * @format ->>,>>9.99
   */
   field MaterialBudget as decimal initial ?
"""

be_dataset['MaterialForecast'] = \
"""
  /**
   * Material Forecast
   *
   * @label eam-ae49559c-92bd-3b87-de11-a5e5bda2e88f
   * @source mtrl_forecast
   * @format ->>,>>9.99
   */
   field MaterialForecast as decimal initial ?
"""

be_dataset['MaterialInvoiceCost'] = \
"""
  /**
   * Material Invoice Cost
   *
   * @label EAM-MATERIAL_INVOICE_COST
   * @readonly
   * @format >>>,>>>,>>>.99
   */
   field MaterialInvoiceCost as decimal initial ?
"""

be_dataset['ModelYear'] = \
"""
  /**
   * Model Year
   *
   * @label eam-c8e7cf62-5f32-59ab-de11-41a165399a6b
   * @source model_year
   * @format 9999
   */
   field ModelYear as integer initial ?
"""

be_dataset['MoneyAuthorized'] = \
"""
  /**
   * Money Authorized
   *
   * @label MONEY_AUTHORIZED
   * @readonly
   * @source auth_money
   * @format mfg-YES/mfg-NO
   */
   field MoneyAuthorized as logical initial ?
"""

be_dataset['MSDSCode'] = \
"""
  /**
   * MSDS Code
   *
   * @label mfg-MSDS
   * @source msds_code
   * @format x(10)
   */
   field MSDSCode as character initial ?
"""

be_dataset['MSDSCodeDescription'] = \
"""
  /**
   * MSDS Description
   *
   * @label MSDS_DESCRIPTION
   * @readonly
   * @source msds_code-desc
   * @format x(30)
   */
   field MSDSCodeDescription as character initial ?
"""

be_dataset['MSDSCodeDescription1'] = \
"""
  /**
   * MSDS Description
   *
   * @label MSDS_DESCRIPTION
   * @readonly
   * @format x(30)
   */
   field MSDSCodeDescription as character initial ?
"""

### N ###
be_dataset['NewOnHand'] = \
"""
  /**
   * New On Hand
   *
   * @label eam-cb2944f0-b5ac-4bb8-db11-59685e7457d1
   * @format ->,>>>,>>9.99
   */
   field NewOnHand as decimal initial ?
"""

be_dataset['NetBudget'] = \
"""
  /**
   * Net Budget
   *
   * @label eam-99f1ea26-d7da-c74f-b2ac-eae376e92a2a
   * @readonly
   * @source net_budg
   * @format ->>>,>>>,>>>,>>>,>>9.99
   */
   field NetBudget as decimal initial ?
"""

be_dataset['NetCostToDate'] = \
"""
  /**
   * Net Cost To-Date
   *
   * @label eam-9520c19c-4616-c9ac-de11-23a28adae064
   * @readonly
   * @format ->>>,>>>,>>>,>>>,>>9.99
   */
   field NetCostToDate as decimal initial ?
"""

be_dataset['NextApprover'] = \
"""
  /**
   * Next Approver
   *
   * @label eam-9de13805-5e14-a09c-e111-09d53c3b4165
   * @readonly
   * @source next_approver
   * @format x(12)
   */
   field NextApprover as character initial ?
"""

be_dataset['NextApproverName'] = \
"""
  /**
   * Next Approver Name
   *
   * @label eam-cee9a96b-8635-9d8e-e411-a054cdd658bd
   * @readonly
   * @format x(30)
   */
   field NextApproverName as character initial ?
"""

be_dataset['Notes'] = \
"""
  /**
   * Notes
   *
   * @label eam-b59bf61d-d161-9d81-de11-0f97f3c280f0
   * @source notes
   * @format x(60)
   */
   field Notes as character initial ?
"""

be_dataset['NormalPriority'] = \
"""
  /**
   * Normal
   *
   * @label eam-b09d843b-382b-dcb9-db11-3b01d06ac306
   * @format mfg-YES/mfg-NO
   */
   field NormalPriority as logical initial ?
"""

be_dataset['NoLableField1'] = \
"""
  /**
   *
   * @label eam-809ec028-ada6-d78c-de11-50621898f29a
   * @readonly
   * @format x(12)
   */
   field NoLableField1 as character initial ?
"""

be_dataset['NoLableField2'] = \
"""
  /**
   *
   * @label eam-809ec028-ada6-d78c-de11-50621898f29a
   * @readonly
   * @format x(12)
   */
   field NoLableField2 as character initial ?
"""

be_dataset['NoLabelField3'] = \
"""
  /**
   *
   * @label eam-809ec028-ada6-d78c-de11-50621898f29a
   * @readonly
   * @format x(12)
   */
   field NoLabelField3 as character initial ?
"""

be_dataset['NoLabelField4'] = \
"""
  /**
   *
   * @label eam-809ec028-ada6-d78c-de11-50621898f29a
   * @readonly
   * @format x(12)
   */
   field NoLabelField4 as character initial ?
"""

be_dataset['NoLabelField5'] = \
"""
  /**
   *
   * @label eam-809ec028-ada6-d78c-de11-50621898f29a
   * @readonly
   * @format x(12)
   */
   field NoLabelField5 as character initial ?
"""

be_dataset['NoLabelField6'] = \
"""
  /**
   *
   * @label eam-809ec028-ada6-d78c-de11-50621898f29a
   * @readonly
   * @format x(12)
   */
   field NoLabelField6 as character initial ?
"""

be_dataset['Notify'] = \
"""
  /**
   * Notify
   *
   * @label mfg-NOTIFY
   * @source notify
   * @format x(12)
   */
   field Notify as character initial ?
"""

be_dataset['NotifyDescription'] = \
"""
  /**
   * Notify Description
   *
   * @label NOTIFY_DESCRIPTION
   * @readonly
   * @source notify-desc
   * @format x(30)
   */
   field NotifyDescription as character initial ?
"""

be_dataset['NotifyDescription1'] = \
"""
  /**
   * Notify Description
   *
   * @label NOTIFY_DESCRIPTION
   * @readonly
   * @format x(30)
   */
   field NotifyDescription as character initial ?
"""

### O ###
be_dataset['OnHand'] = \
"""
  /**
   * On Hand
   *
   * @label mfg-ON_HAND
   * @source on_hand
   * @format >,>>>,>>9.99
   */
   field OnHand as decimal initial ?
"""

be_dataset['OnHandQuantity'] = \
"""
  /**
   * On Hand
   *
   * @label mfg-ON_HAND
   * @source curr_qty
   * @format >>>,>>>,>>9.99
   */
   field OnHandQuantity as decimal initial ?
"""

be_dataset['OnOrderQuantity'] = \
"""
  /**
   * On Order
   *
   * @label eam-ab78dd14-673e-e792-dc11-770ff96c4657
   * @readonly
   * @source on_order
   * @format >>>,>>>,>>9.99
   */
   field OnOrderQuantity as decimal initial ?
"""

be_dataset['OrderQuantity'] = \
"""
  /**
   * Ordered Qty
   *
   * @label ORDERED_QTY
   * @readonly
   * @source on_order
   * @format >,>>>,>>9.99
   */
   field OrderQuantity as decimal initial ?
"""

be_dataset['OrderUM'] = \
"""
  /**
   * Order UM
   *
   * @label mfg-ORDER_UNIT_OF_MEASURE-medium
   * @source ord_uom_code
   * @format x(8)
   */
   field OrderUM as character initial ?
"""

be_dataset['Oem'] = \
"""
  /**
   * OEM
   *
   * @label eam-c8e7cf62-5f32-59ab-de11-41a16de8c96b
   * @source oem
   * @format x(30)
   * @length 60
   */
   field Oem as character initial ?
"""

be_dataset['OnHold'] = \
"""
  /**
   * On Hold
   *
   * @label eam-812886ef-cf3a-bcbe-e311-a4b527828332
   * @readonly
   * @source on_hold
   * @format mfg-YES/mfg-NO
   */
   field OnHold as logical initial ?
"""

be_dataset['OriginalBudget'] = \
"""
  /**
   * Original Budget
   *
   * @label eam-9520c19c-4616-c9ac-de11-02a3d0aae8d9
   * @readonly
   * @source orig_budg
   * @format ->>>,>>>,>>>,>>>,>>9.99
   */
   field OriginalBudget as decimal initial ?
"""

be_dataset['OriginalBudget2'] = \
"""
  /**
   * Original Budget
   *
   * @label eam-9520c19c-4616-c9ac-de11-02a3d0aae8d9
   * @readonly
   * @format ->>>,>>>,>>>,>>>,>>9.99
   */
   field OriginalBudget2 as decimal initial ?
"""

be_dataset['OriginalEstimate'] = \
"""
  /**
   * Original Estimate
   *
   * @label EAM-ORIGINAL_ESTIMATE
   * @source orig_est
   * @format ->>>,>>>,>>>,>>>,>>9.99
   */
   field OriginalEstimate as decimal initial ?
"""

be_dataset['Owner'] = \
"""
  /**
   * Owner
   *
   * @label eam-9e128334-2108-0090-de11-f02c7796702a
   * @source owner
   * @format x(12)
   */
   field Owner as character initial ?
"""

be_dataset['OwnerDescription'] = \
"""
  /**
   * Owner Description
   *
   * @label OWNER_DESCRIPTION
   * @readonly
   * @format x(30)
   */
   field OwnerDescription as character initial ?
"""

be_dataset['OverheadGroup'] = \
"""
  /**
   * Overhead Group
   *
   * @label OVERHEAD_GROUP
   * @source ovhd_code
   * @format X(5)
   */
   field OverheadGroup as character initial ?
"""

be_dataset['OverSpendEstimate'] = \
"""
  /**
   * Over Spend Estimate
   *
   * @label OVER_SPEND_ESTIMATE
   * @readonly
   * @source pr_over
   * @format >>>,>>>,>>>,>>9.9%
   */
   field OverSpendEstimate as decimal initial ?
"""

### P ###
be_dataset['PartNumber'] = \
"""
  /**
   * Part
   *
   * @label mfg-PART
   * @source part_no
   * @format x(20)
   */
   field PartNumber as character initial ?
"""

be_dataset['PartDescription'] = \
"""
  /**
   * Part Description
   *
   * @label PART_DESCRIPTION
   * @readonly
   * @source pt_desc
   * @format X(30)
   */
   field PartDescription as character initial ?
"""

be_dataset['PartDescription2'] = \
"""
  /**
   * Description
   *
   * @label eam-02918711-612a-5d4e-b616-19a5a044e12a
   * @readonly
   * @source pt_desc
   * @format x(30)
   */
   field PartDescription as character initial ?
"""

be_dataset['PartDescription3'] = \
"""
  /**
   * Description
   *
   * @label eam-02918711-612a-5d4e-b616-19a5a044e12a
   * @source ep_description
   * @format x(30)
   */
   field PartDescription as character initial ?
"""

be_dataset['PartStatus'] = \
"""
  /**
   * Status
   *
   * @label eam-26719618-5811-9b47-b1fb-d0858912bcdb
   * @readonly
   * @format x(12)
   */
   field PartStatus as character initial ?
"""

be_dataset['PartRequiredDate'] = \
"""
  /**
   * Part Required Date
   *
   * @label mfg-REQUIRED_DATE
   * @format 99/99/9999
   */
   field PartRequiredDate as date initial ?
"""

be_dataset['PhysicalDue'] = \
"""
  /**
   * Physical Due
   *
   * @label PHYSICAL_DUE
   * @readonly
   * @source phy_due
   * @format 99/99/9999
   */
   field PhysicalDue as date initial ?
"""

be_dataset['PlanCode'] = \
"""
  /**
   * Planner
   *
   * @label mfg-PLANNER
   * @source plan_code
   * @format x(12)
   */
   field PlanCode as character initial ?
"""

be_dataset['PlanCodeDescription'] = \
"""
  /**
   * Planner Description
   *
   * @label PLANNER_DESCRIPTION
   * @readonly
   * @source plan_code-desc
   * @format x(30)
   */
   field PlanCodeDescription as character initial ?
"""

be_dataset['PlanCodeDescription1'] = \
"""
  /**
   * Planner Description
   *
   * @label PLANNER_DESCRIPTION
   * @readonly
   * @format x(30)
   */
   field PlanCodeDescription as character initial ?
"""

be_dataset['Planned'] = \
"""
  /**
   * Planned
   *
   * @label mfg-PLANNED
   * @format >,>>>,>>>.99
   */
   field PlannedQuantity as decimal initial ?
"""

be_dataset['PlannedCost'] = \
"""
  /**
   * Planned Cost
   *
   * @label eam-6615de88-580f-5a43-b183-915949cda65f
   * @source plncost
   * @format >>>,>>>,>>>,>>9.99
   */
   field PlannedCost as decimal initial ?
"""

be_dataset['PlannedCost2'] = \
"""
  /**
   * Planned Cost
   *
   * @label eam-f876688b-2365-d5af-db11-c123f308413a
   * @format >,>>>,>>9.99
   */
   field PlannedCost as decimal initial ?
"""

be_dataset['PlannedContract'] = \
"""
  /**
   * Planned Contract
   *
   * @label eam-8469f75b-62c9-10bf-db11-17487b54accd
   * @readonly
   * @format >>>,>>>,>>>.99
   */
   field PlannedContract as decimal initial ?
"""

be_dataset['PlannedFunding'] = \
"""
  /**
   * Planned Funding
   *
   * @label eam-ce8d59d2-4960-1d84-e211-b35540828c9b
   * @readonly
   * @source plan_fund
   * @format >>>,>>>,>>>,>>9.99
   */
   field PlannedFunding as decimal initial ?
"""

be_dataset['PlannedInternal'] = \
"""
  /**
   * Planned Internal
   *
   * @label eam-c8e7cf62-5f32-59ab-de11-41a12f01076d
   * @readonly
   * @source plan_internal
   * @format >>>,>>>,>>>,>>9.99
   */
   field PlannedInternal as decimal initial ?
"""

be_dataset['PlannedLabor'] = \
"""
  /**
   * Planned Labor
   *
   * @label eam-8469f75b-62c9-10bf-db11-174847a2fdc2
   * @readonly
   * @format >>>,>>>,>>>.99
   */
   field PlannedLabor as decimal initial ?
"""

be_dataset['PlannedLaborCost'] = \
"""
  /**
   * Planned Labor Cost
   *
   * @label eam-8fc6dd59-f8d6-a589-e111-701014ee1dff
   * @readonly
   * @source plan_lab_cost
   * @format >>>,>>>,>>>,>>>.99
   */
   field PlannedLaborCost as decimal initial ?
"""

be_dataset['PlannedLaborHours'] = \
"""
  /**
   * Planned Labor Hrs
   *
   * @label eam-aa947156-2b0a-cf81-df11-a7417684868e
   * @source plan_lab_hrs
   * @format ->>,>>9.99
   */
   field PlannedLaborHours as decimal initial ?
"""

be_dataset['PlannedMargin'] = \
"""
  /**
   * Planned Margin %
   *
   * @label eam-ce8d59d2-4960-1d84-e211-f555b151034b
   * @readonly
   * @source plan_margin
   * @format >>>,>>>,>>>,>>9.9%
   */
   field PlannedMargin as decimal initial ?
"""

be_dataset['PlannedMaterial'] = \
"""
  /**
   * Planned Material
   *
   * @label eam-8469f75b-62c9-10bf-db11-17487f6a13c8
   * @readonly
   * @format >>>,>>>,>>>.99
   */
   field PlannedMaterial as decimal initial ?
"""

be_dataset['PlannedOrder'] = \
"""
  /**
   * Planned Order
   *
   * @label eam-aa492f96-87b5-88ba-da11-0fd6b50174af
   * @readonly
   * @source plan_order
   * @format >,>>>,>>9.99
   */
   field PlannedOrder as decimal initial ?
"""

be_dataset['PlannedTax'] = \
"""
  /**
   * Planned Tax
   *
   * @label PLANNED_TAX
   * @readonly
   * @format >>>,>>>,>>>.99
   */
   field PlannedTax as decimal initial ?
"""

be_dataset['PlannedTotal'] = \
"""
  /**
   * Planned Total
   *
   * @label eam-8469f75b-62c9-10bf-db11-1748e7991dd2
   * @readonly
   * @format >>>,>>>,>>>.99
   */
   field PlannedTotal as decimal initial ?
"""

be_dataset['PlannedQuantity'] = \
"""
  /**
   * Planned Quantity
   *
   * @label mfg-PLANNED_QTY
   * @source qty
   * @format >>>,>>9.99
   */
   field PlannedQuantity as decimal initial ?
"""

be_dataset['PlannedOrderQuantity'] = \
"""
  /**
   * Planned Order
   *
   * @label eam-aa492f96-87b5-88ba-da11-0fd6b50174af
   * @readonly
   * @source plan_order
   * @format >,>>>,>>9.99
   */
   field PlannedOrderQuantity as decimal initial ?
"""

be_dataset['POVarianceTax'] = \
"""
  /**
   * PO Variance Tax
   *
   * @label mfg-TAX
   * @readonly
   * @format >>>,>>>,>>>.99
   */
   field POVarianceTax as decimal initial ?
"""

be_dataset['POVarianceMaterial'] = \
"""
  /**
   * PO Variance Material
   *
   * @label eam-fafb388d-c870-7db0-df11-5c0006379b54
   * @readonly
   * @format >>>,>>>,>>>.99
   */
   field POVarianceMaterial as decimal initial ?
"""

be_dataset['POText'] = \
"""
  /**
   * PO Text
   *
   * @label eam-aa492f96-87b5-88ba-da11-0fd6c0287baf
   * @source po_text
   * @format x(150)
   */
   field POText as character initial ?
"""

be_dataset['PrimarySupplier'] = \
"""
  /**
   * Primary Supplier
   *
   * @label PRIMARY_SUPPLIER
   * @readonly
   * @source pri_vend
   * @format x(20)
   */
   field PrimarySupplier as character initial ?
"""

be_dataset['PrimarySupplierDescription'] = \
"""
  /**
   * Primary Supplier Description
   *
   * @label PRIMARY_SUPPLIER_DESCRIPTION
   * @readonly
   * @source pri_vend-desc
   * @format x(30)
   */
   field PrimarySupplierDescription as character initial ?
"""

be_dataset['PrimarySupplierDescription1'] = \
"""
  /**
   * Primary Supplier Description
   *
   * @label PRIMARY_SUPPLIER_DESCRIPTION
   * @readonly
   * @format x(30)
   */
   field PrimarySupplierDescription as character initial ?
"""

be_dataset['ProjectNumber'] = \
"""
  /**
   * Project
   *
   * @label mfg-PROJECT
   * @source proj_no
   * @format x(20)
   */
   field ProjectNumber as character initial ?
"""

be_dataset['ProjectDescription'] = \
"""
  /**
   * Description
   *
   * @label eam-1879484b-3f56-4e43-898c-c6feb7a03609
   * @source pr_desc
   * @format x(30)
   * @length 60
   */
   field ProjectDescription as character initial ?
"""

be_dataset['ProjectApprovalGroup'] = \
"""
  /**
   * Project Approval Group
   *
   * @label eam-bc487382-c62d-1d8d-dc11-7d7e1cbcf34f
   * @source pr_grp_no
   * @format x(20)
   */
   field ProjectApprovalGroup as character initial ?
"""

be_dataset['ProjectApprovalGroupDescription'] = \
"""
  /**
   * Project Approver Group Description
   *
   * @label PROJECT_APPROVER_GROUP_DESCRIPTION
   * @readonly
   * @format x(30)
   */
   field ProjectApprovalGroupDescription as character initial ?
"""

be_dataset['ProjectString'] = \
"""
  /**
   * Project
   *
   * @label mfg-PROJECT
   * @readonly
   * @source proj_no
   * @format x(20)
   */
   field ProjectString as character initial ?
"""

be_dataset['ProjectStatus'] = \
"""
  /**
   * Status
   *
   * @label eam-9b22676b-5f59-bd8d-de11-353e829c1c0d
   * @source pr_status
   * @format x(2)
   */
   field ProjectStatus as character initial ?
"""

be_dataset['ProjectStatusDescription'] = \
"""
  /**
   * Status Description
   *
   * @label eam-ebc2c0d5-ffe0-fd89-db11-0ded826c0647
   * @readonly
   * @format x(30)
   */
   field ProjectStatusDescription as character initial ?
"""

be_dataset['ProjectExpensed'] = \
"""
  /**
   * Total Spent At This Level
   *
   * @label eam-937d5b3c-1a24-2e8d-e111-9a25d0d191dd
   * @readonly
   * @source proj_expensed
   * @format >>>,>>>,>>>,>>9.99
   */
   field ProjectExpensed as decimal initial ?
"""

be_dataset['ProjectTitle'] = \
"""
  /**
   * Project Title
   *
   * @label eam-c8e7cf62-5f32-59ab-de11-41a105b9226e
   * @source proj_title
   * @format x(30)
   */
   field ProjectTitle as character initial ?
"""

be_dataset['ProjectUtilized'] = \
"""
  /**
   * % Utilized
   *
   * @label eam-c8e7cf62-5f32-59ab-de11-41a19112396d
   * @readonly
   * @source pr_commit
   * @format >>>,>>>,>>>,>>9.9%
   */
   field ProjectUtilized as decimal initial ?
"""

be_dataset['ProgramController'] = \
"""
  /**
   * Program Controller
   *
   * @label PROGRAM_CONTROLLER
   * @source pg_control
   * @format x(12)
   */
   field ProgramController as character initial ?
"""

be_dataset['ProgramControllerDescription'] = \
"""
  /**
   * Program Controller Description
   *
   * @label PROGRAM_CONTROLLER_DESCRIPTION
   * @readonly
   * @format x(30)
   */
   field ProgramControllerDescription as character initial ?
"""

be_dataset['ProgramBuyer'] = \
"""
  /**
   * Program Buyer
   *
   * @label eam-c8e7cf62-5f32-59ab-de11-41a12752d76c
   * @source pgm_buyer
   * @format x(12)
   */
   field ProgramBuyer as character initial ?
"""

be_dataset['ProgramBuyerDescription'] = \
"""
  /**
   * Program Buyer Description
   *
   * @label PROGRAM_BUYER_DESCRIPTION
   * @readonly
   * @format x(30)
   */
   field ProgramBuyerDescription as character initial ?
"""

be_dataset['ProgramManager'] = \
"""
  /**
   * Program Manager
   *
   * @label eam-c8e7cf62-5f32-59ab-de11-41a199c1686d
   * @source prog_mgr
   * @format x(20)
   */
   field ProgramManager as character initial ?
"""

be_dataset['ProgramManagerDescription'] = \
"""
  /**
   * Program Manager Desc
   *
   * @label eam-9520c19c-4616-c9ac-de11-22a274410c94
   * @readonly
   * @format x(30)
   */
   field ProgramManagerDescription as character initial ?
"""

be_dataset['ProgramName'] = \
"""
  /**
   * Program Name
   *
   * @label eam-c8e7cf62-5f32-59ab-de11-41a1470e966d
   * @source prog_name
   * @format x(30)
   */
   field ProgramName as character initial ?
"""

be_dataset['ProgramNumber'] = \
"""
  /**
   * Program No
   *
   * @label eam-b5e335fb-ea40-a982-de11-10a258e45247
   * @source prog_no
   * @format x(30)
   */
   field ProgramNumber as character initial ?
"""

be_dataset['ProgramType'] = \
"""
  /**
   * Program Type
   *
   * @label eam-c8e7cf62-5f32-59ab-de11-41a1fd09f36d
   * @source prog_type
   * @format x(16)
   */
   field ProgramType as character initial ?
"""

be_dataset['PurchaseOrderComments'] = \
"""
  /**
   * PO Text
   *
   * @label eam-b9fcaed1-5c01-0fae-dc11-dc2f7cd6d03d
   * @source po_text
   * @format X(60)
   */
   field PurchaseOrderComments as character initial ?
"""

be_dataset['PurchaseOrderDueDate'] = \
"""
  /**
   * Due Date
   *
   * @label mfg-DUE_DATE
   * @readonly
   * @format 99/99/9999
   */
   field PurchaseOrderDueDate as date initial ?
"""

be_dataset['PurchaseOrderLineNumber'] = \
"""
  /**
   * Requisition Line
   *
   * @label eam-8659e75d-8c59-2787-da11-83ead8cf8307
   * @format >>,>>>,>>9
   */
   field PurchaseOrderLineNumber as integer initial ?
"""

be_dataset['PurchaseOrderNumber'] = \
"""
  /**
   * Order
   *
   * @label mfg-PURCHASE_ORDER
   * @readonly
   * @format >>,>>>,>>9
   */
   field PurchaseOrderNumber as integer initial ?
"""

be_dataset['WeightUnitOfMeasure'] = \
"""
  /**
   * @label mfg-STATUS
   * @readonly
   * @format x(40)
   */
   field ProcessResult as character initial ?
"""

be_dataset['WeightUnitOfMeasure'] = \
"""
  /**
   * @label mfg-RESULT
   * @readonly
   * @format x(40)
   */
   field ProcessMessage as character initial ?
"""

### Q ###
be_dataset['QuantityToOrder'] = \
"""
  /**
   * Qty to Order
   *
   * @label eam-d3ed0b98-82be-e996-db11-2b59f17e8d5d
   * @source to_order
   * @format >,>>>,>>9.99
   */
   field QuantityToOrder as decimal initial ?
"""

be_dataset['QtyOrdered'] = \
"""
  /**
   * Qty Ordered
   *
   * @label eam-d755d7b5-1ccc-be88-dc11-b9301d10ee99
   * @readonly
   * @format >,>>>,>>9.99
   */
   field QtyOrdered as decimal initial ?
"""

be_dataset['QuantityToReceive'] = \
"""
  /**
   * Quantity To Receive
   *
   * @label mfg-QUANTITY_TO_RECEIVE
   * @format >>>,>>>,>>>.99
   * @required
   */
   field QuantityToReceive as decimal initial ?
"""

be_dataset['QuantityToReserve'] = \
"""
  /**
   * Quantity To Reserve
   *
   * @label QTY_TO_RESERVE
   * @format ->,>>>,>>9.99
   */
   field ReserveQuantity as decimal initial ?
"""

be_dataset['QuantityToReturn'] = \
"""
  /**
   * Quantity To Return
   *
   * @label mfg-QUANTITY_TO_RETURN
   * @format >,>>>,>>>9
   * @required
   */
   field QuantityToReturn as integer initial ?
"""

### R ###
be_dataset['ReallocatedMinus'] = \
"""
  /**
   * Reallocated -
   *
   * @label eam-949bb89c-be0a-1baf-de11-922e3d3ed6b3
   * @readonly
   * @source reallocated_minus
   * @format ->>>,>>>,>>>,>>>,>>9.99
   */
   field ReallocatedMinus as decimal initial ?
"""

be_dataset['ReallocatedPlus'] = \
"""
  /**
   * Reallocated +
   *
   * @label eam-949bb89c-be0a-1baf-de11-922ee69745bd
   * @readonly
   * @source reallocated_plus
   * @format ->>>,>>>,>>>,>>>,>>9.99
   */
   field ReallocatedPlus as decimal initial ?
"""

be_dataset['Received'] = \
"""
  /**
   * Received
   *
   * @label eam-7c39014f-f250-1746-ba1a-66e622685383
   * @readonly
   * @source received
   * @format >>>,>>>,>>>,>>9.99
   */
   field Received as decimal initial ?
"""

be_dataset['ReferenceNumber'] = \
"""
  /**
   * Customer Reference
   *
   * @label mfg-CUSTOMER_REFERENCE
   * @source ref_no
   * @format x(30)
   * @length 60
   */
   field ReferenceNumber as character initial ?
"""

be_dataset['ReimbursedAmount'] = \
"""
  /**
   * Reimbursed Amount
   *
   * @label REIMBURSED_AMOUNT
   * @readonly
   * @source reimb_amt
   * @format ->>>,>>>,>>>,>>>,>>9.99
   */
   field ReimbursedAmount as decimal initial ?
"""

be_dataset['ReorderPointQuantity'] = \
"""
  /**
   * Reorder Point
   *
   * @label eam-aa492f96-87b5-88ba-da11-0fd6669cafaf
   * @readonly
   * @format >,>>>,>>9.99
   */
   field ReorderPointQuantity as decimal initial ?
"""

be_dataset['ReorderQuantity'] = \
"""
  /**
   * Min Order Qty
   *
   * @label eam-aa492f96-87b5-88ba-da11-0fd6ca25b9af
   * @readonly
   * @format >,>>>,>>9.99
   */
   field ReorderQuantity as decimal initial ?
"""

be_dataset['ReportID'] = \
"""
  /**
   * Reporting ID
   *
   * @label eam-c8e7cf62-5f32-59ab-de11-41a17728b46e
   * @source report_id
   * @format x(30)
   */
   field ReportID as character initial ?
"""

be_dataset['RequestedFunding'] = \
"""
  /**
   * Requested Funding
   *
   * @label eam-b928136c-0233-8fa8-e111-e10ac65204f6
   * @source req_fund
   * @format ->>>,>>>,>>>,>>>,>>9.99
   */
   field RequestedFunding as decimal initial ?
"""

be_dataset['RequisitionGroupNumber'] = \
"""
  /**
   * Requisition Group Number
   *
   * @label eam-9164493a-635c-ab84-dc11-a98039355fca
   * @source req_grp_no
   * @format x(10)
   */
   field RequisitionGroupNumber as character initial ?
"""

be_dataset['RequisitionApproverGroupDescription'] = \
"""
  /**
   * Requisition Approver Group Description
   *
   * @label REQUISITION_APPROVER_GROUP_DESCRIPTION
   * @readonly
   * @format x(30)
   */
   field RequisitionApproverGroupDescription as character initial ?
"""

be_dataset['RequestorCode'] = \
"""
  /**
   * Requestor
   *
   * @label eam-e96be5ae-01d5-63b3-dd11-5470a1e57163
   * @source req_code
   * @format x(20)
   */
   field RequestorCode as character initial ?
"""

be_dataset['RequestorName'] = \
"""
  /**
   * Requestor Name
   *
   * @label eam-8be7b4b8-5fd3-fd8e-e211-5f1ccc976dc5
   * @readonly
   * @format x(20)
   * @associated RequestorCode
   */
   field RequestorName as character initial ?
"""

be_dataset['RequisitionDate'] = \
"""
  /**
   * Required Date
   *
   * @label eam-f876688b-2365-d5af-db11-c123dfe1393a
   * @source req_date
   * @format 99/99/9999
   */
   field RequisitionDate as date initial ?
"""

be_dataset['RequiredDate'] = \
"""
  /**
   * Required Date
   *
   * @label eam-f876688b-2365-d5af-db11-c123dfe1393a
   * @readonly
   * @source req_date
   * @format 99/99/9999
   */
   field RequiredDate as date initial ?
"""

be_dataset['RequisitionNumber'] = \
"""
  /**
   * Requisition
   *
   * @label eam-812dc5fe-1592-d596-e111-33aff09cb385
   * @readonly
   * @source req_grp_no
   * @format >>>>>>>>9
   */
   field RequisitionNumber as integer initial ?
"""

be_dataset['ReserveQuantity'] = \
"""
  /**
   * Quantity To Reserve
   *
   * @label QTY_TO_RESERVE
   * @format ->,>>>,>>9.99
   */
   field ReserveQuantity as decimal initial ?
"""

be_dataset['ReservedQuantity'] = \
"""
  /**
   * Reserved
   *
   * @label mfg-RESERVED
   * @readonly
   * @source reserved
   * @format >,>>>,>>9.99
   */
   field ReservedQuantity as decimal initial ?
"""

be_dataset['ReservedQuantity2'] = \
"""
  /**
   * Quantity To Reserve
   *
   * @label mfg-RESERVED
   * @source allocated
   * @format >>>,>>>,>>9.99
   */
   field ReservedQuantity as decimal initial ?
"""

be_dataset['ReservedOrderQuantity'] = \
"""
  /**
   * Reserved Orders
   *
   * @label eam-a3f0f44a-8af2-9daa-db11-ee8abe730e22
   * @readonly
   * @source res_order
   * @format >,>>>,>>9.99
   */
   field ReservedOrderQuantity as decimal initial ?
"""

be_dataset['ReturnQuantity'] = \
"""
  /**
   * Quantity to Return
   *
   * @label mfg-QUANTITY_TO_RETURN-medium
   * @format >,>>>,>>9.99
   */
   field ReturnQuantity as decimal initial ?
"""

be_dataset['ReturnLocation'] = \
"""
  /**
   * Return Location
   *
   * @label LOCATION_TO_RETURN
   * @format x(10)
   */
   field ReturnLocation as character initial ?
"""

be_dataset['RequisitionQuantity'] = \
"""
  /**
   * Quantity to Order
   *
   * @label mfg-QUANTITY_TO_ORDER-medium
   * @format >,>>>,>>9.99
   */
   field RequisitionQuantity as decimal initial ?
"""

be_dataset['RevisedCompletionDate'] = \
"""
  /**
   * Revised Completion Date
   *
   * @label mfg-REVISED_COMPLETION_DATE
   * @source rdate
   * @format 99/99/9999
   */
   field RevisedCompletionDate as date initial ?
"""

be_dataset['RevisionLevel'] = \
"""
  /**
   * Revision Level
   *
   * @label MFG-REVISION_LEVEL
   * @source rev_level
   * @format x(10)
   */
   field RevisionLevel as character initial ?
"""

### S ###
be_dataset['SafetyStockQuantity'] = \
"""
  /**
   * Safety Stock
   *
   * @label eam-aa492f96-87b5-88ba-da11-0fd67ceabdaf
   * @readonly
   * @format >,>>>,>>9.99
   */
   field SafetyStockQuantity as decimal initial ?
"""

be_dataset['SegmentCode'] = \
"""
  /**
   * Business Segment
   *
   * @label eam-07cab6e1-34e6-6f49-8acb-76c100ec223d
   * @source segm_code
   * @format x(15)
   * @length 30
   */
   field SegmentCode as character initial ?
"""

be_dataset['SegmentCodeDescription'] = \
"""
  /**
   * Business Segment Description
   *
   * @label BUSINESS_SEGMENT_DESCRIPTION
   * @readonly
   * @format x(30)
   */
   field SegmentCodeDescription as character initial ?
"""

be_dataset['SerialNumber'] = \
"""
  /**
   * Serial Number
   *
   * @label eam-dbf38483-dd74-c79f-1014-790e9825b9a6
   * @source serial_no
   * @format x(20)
   */
   field SerialNumber as character initial ?
"""

be_dataset['SalesOrderNumber'] = \
"""
  /**
   * Sales Order
   *
   * @label eam-cabb2116-e24c-ce9b-de11-19bf88d94d42
   * @source so_nbr
   * @format x(20)
   */
   field SalesOrderNumber as character initial ?
"""

be_dataset['SalesOrderNotify'] = \
"""
  /**
   * Sale Order Notify
   *
   * @label eam-a0f94370-cfdc-25ac-de11-88c55eba568b
   * @source so_notify
   * @format x(12)
   */
   field SalesOrderNotify as character initial ?
"""

be_dataset['ShortQuantity'] = \
"""
  /**
   * Short
   *
   * @label mfg-SHORT
   * @readonly
   * @source short
   * @format >>>,>>>,>>9.99
   */
   field ShortQuantity as decimal initial ?
"""

be_dataset['ShortQuantity2'] = \
"""
  /**
   * Short
   *
   * @label mfg-SHORT
   * @readonly
   * @source short_qty
   * @format >>>,>>>,>>9.99
   */
   field ShortQuantity as decimal initial ?
"""

be_dataset['SiteCode'] = \
"""
  /**
   * Site
   *
   * @label mfg-SITE
   * @source site_code
   * @format X(8)
   */
   field SiteCode as character initial ?
"""

be_dataset['SizeCode'] = \
"""
  /**
   * Size
   *
   * @label eam-aa492f96-87b5-88ba-da11-0fd69d5fd3af
   * @source size_code
   * @format x(20)
   */
   field SizeCode as character initial ?
"""

be_dataset['SizeCodeDescription'] = \
"""
  /**
   * Size Description
   *
   * @label SIZE_DESCRIPTION
   * @readonly
   * @source size_code-desc
   * @format x(30)
   */
   field SizeCodeDescription as character initial ?
"""

be_dataset['SizeCodeDescription1'] = \
"""
  /**
   * Size Description
   *
   * @label SIZE_DESCRIPTION
   * @readonly
   * @format x(30)
   */
   field SizeCodeDescription as character initial ?
"""

be_dataset['SoleSource'] = \
"""
  /**
   * Sole Source
   *
   * @label SOLE_SOURCE
   * @readonly
   * @format mfg-YES/mfg-NO
   */
   field SoleSource as logical initial ?
"""

be_dataset['Source'] = \
"""
  /**
   * Source
   *
   * @label eam-aa492f96-87b5-88ba-da11-0fd65a4bdfaf
   * @readonly
   * @source source
   * @format X(9)
   */
   field Source as character initial ?
"""

be_dataset['SourceSite'] = \
"""
  /**
   * Source Site
   *
   * @label eam-e96be5ae-01d5-63b3-dd11-547051e43e84
   * @source source_site
   * @format X(8)
   */
   field SourceSite as character initial ?
"""

be_dataset['SpendAtThisLevel'] = \
"""
  /**
   * Spend at this Level
   *
   * @label SPEND_AT_THIS_LEVEL
   * @source level_spend
   * @format mfg-YES/mfg-NO
   */
   field SpendAtThisLevel as logical initial ?
"""

be_dataset['SpendingEstimate'] = \
"""
  /**
   * Spending Estimate
   *
   * @label SPENDING_EST
   * @source spend_est
   * @format >>>,>>>,>>>,>>9.99
   */
   field SpendingEstimate as decimal initial ?
"""

be_dataset['SpendingEstimate2'] = \
"""
  /**
   * % Spending Estimate
   *
   * @label EAM-PERCENT_SPENDING_ESTIMATE
   * @readonly
   * @source pr_complete
   * @format >>>,>>>,>>>,>>9.9%
   */
   field SpendingEstimate as decimal initial ?
"""

be_dataset['SpendingLimit'] = \
"""
  /**
   * Spending Limit
   *
   * @label eam-112b672b-452e-b840-a8ef-d32f8df767a0
   * @readonly
   * @format ->>>,>>>,>>>,>>>,>>9.99
   */
   field SpendingLimit as decimal initial ?
"""

be_dataset['StartDate'] = \
"""
  /**
   * Start Date
   *
   * @label eam-9584de26-8958-9144-9754-e19509a2d9ac
   * @source sdate
   * @format 99/99/9999
   */
   field StartDate as date initial ?
"""

be_dataset['StoresApprovalGroup'] = \
"""
  /**
   * Stores Approval Group
   *
   * @label EAM-STORES_APPROVAL_GROUP
   * @source wph_appr_grp_no
   * @format x(12)
   */
   field StoresApprovalGroup as character initial ?
"""

be_dataset['StoresApprovalGroupDescription'] = \
"""
  /**
   * Stores Approver Group Description
   *
   * @label STORES_APPROVER_GROUP_DESCRIPTION
   * @readonly
   * @format x(30)
   */
   field StoresApprovalGroupDescription as character initial ?
"""

be_dataset['StoresRequisitionNumber'] = \
"""
  /**
   * Stores Requisition
   *
   * @label mfg-STORES_REQUISITION
   * @readonly
   * @source wph_no
   * @format >>>>>>>>9
   */
   field StoresRequisitionNumber as integer initial ?
"""

be_dataset['StoresRequisitionDescription'] = \
"""
  /**
   * Description
   *
   * @label eam-02918711-612a-5d4e-b616-19a5a044e12a
   * @source wph_desc
   * @format x(30)
   */
   field StoresRequisitionDescription as character initial ?
"""

be_dataset['StoresRequisitionStatus'] = \
"""
  /**
   * Status
   *
   * @label eam-b9fcaed1-5c01-0fae-dc11-db2f8435f558
   * @readonly
   * @source wp_status
   * @format X(2)
   */
   field StoresRequisitionStatus as character initial ?
"""

be_dataset['StoresRequisitionLineStatus'] = \
"""
  /** Line status: warning, error, OK(blank)
   *
   * @label mfg-Status
   * @readonly
   * @format x(8)
   */
   field StoresRequisitionLineStatus as character initial ?
"""

be_dataset['StockOutQuantity'] = \
"""
  /**
   * Stock Out Qty
   *
   * @label eam-aa492f96-87b5-88ba-da11-0fd66572e6af
   * @readonly
   * @format mfg-YES/mfg-NO
   */
   field StockOutQuantity as decimal initial ?
"""

be_dataset['StatusCode'] = \
"""
  /**
   * Status
   *
   * @label mfg-STATUS
   * @source status_code
   * @format x(20)
   */
   field StatusCode as character initial ?
"""

be_dataset['StatusDescription'] = \
"""
  /**
   * Status
   *
   * @label mfg-STATUS
   * @readonly
   * @nomapping
   * @format X(18)
   */
   field StatusDescription as character initial ?
"""

be_dataset['SubAccountNumber'] = \
"""
  /**
   * Sub Account
   *
   * @label SUB_ACCOUNT
   * @source sub_acct_no
   * @format x(20)
   */
   field SubAccountNumber as character initial ?
"""

be_dataset['SubAccountNumberDescription'] = \
"""
  /**
   * Sub Account Description
   *
   * @label fin-726901579
   * @readonly
   * @source sub_acct_no-desc
   * @format x(30)
   */
   field SubAccountNumberDescription as character initial ?
"""

be_dataset['SubAccountNumberDescription1'] = \
"""
  /**
   * Sub Account Description
   *
   * @label fin-726901579
   * @readonly
   * @format x(30)
   */
   field SubAccountNumberDescription as character initial ?
"""

be_dataset['StockReplenishNumber'] = \
"""
  /**
   * Stock Replenishment Run
   *
   * @label EAM-STOCKRUN
   * @readonly
   * @source stock_no
   * @format >>>>>>9
   */
   field StockReplenishNumber as integer initial ?
"""

be_dataset['SubCatalog'] = \
"""
  /**
   * Sub Catalog
   *
   * @label SUBCATALOG
   * @readonly
   * @source sub_catalog
   * @format x(12)
   */
   field SubCatalog as character initial ?
"""

be_dataset['SubCatalogDescription'] = \
"""
  /**
   * Sub Catalog Description
   *
   * @label SUB_CATALOG_DESCRIPTION
   * @readonly
   * @source sub_catalog-desc
   * @format x(30)
   */
   field SubCatalogDescription as character initial ?
"""

be_dataset['SubCatalogDescription1'] = \
"""
  /**
   * Sub Catalog Description
   *
   * @label SUB_CATALOG_DESCRIPTION
   * @readonly
   * @format x(30)
   */
   field SubCatalogDescription as character initial ?
"""

be_dataset['Supplier'] = \
"""
  /**
   * Supplier
   *
   * @label eam-d8611446-e922-9b9d-11e0-9e618d34bc5e
   * @source pri_vend
   * @format x(10)
   */
   field Supplier as character initial ?
"""

be_dataset['SupplierDescription'] = \
"""
  /**
   * Supplier Description
   *
   * @label mfg-SUPPLIER_DESCRIPTION
   * @readonly
   * @format x(12)
   */
   field SupplierDescription as character initial ?
"""

be_dataset['SupplierPart'] = \
"""
  /**
   * Supplier Part
   *
   * @label mfg-SUPPLIER_PART
   * @readonly
   * @source pri_vpart_no
   * @format x(30)
   */
   field SupplierPart as character initial ?
"""

be_dataset['SupplierPartNumber'] = \
"""
  /**
   * Supplier Part Number
   *
   * @label SUPPLIER_PART_NUMBER
   * @readonly
   * @source pri_vpart_no
   * @format x(30)
   */
   field SupplierPartNumber as character initial ?
"""

be_dataset['SupplierCode'] = \
"""
  /**
   * Supplier
   *
   * @label mfg-SUPPLIER
   * @source vendor_no
   * @format x(10)
   */
   field SupplierCode as character initial ?
"""

be_dataset['SupplierDescription'] = \
"""
  /**
   * Supplier Name
   *
   * @label mfg-SUPPLIER_DESCRIPTION
   * @readonly
   * @format x(30)
   * @associated SupplierCode
   */
   field SupplierDescription as character initial ?
"""

be_dataset['SystemCode'] = \
"""
  /**
   * System
   *
   * @label eam-4f5a5486-6478-194f-aa2c-5e6fa9b2fe7d
   * @source sys_code
   * @format X(5)
   */
   field SystemCode as character initial ?
"""

### T ###
be_dataset['Taxable'] = \
"""
  /**
   * Taxable
   *
   * @label mfg-TAXABLE
   * @source taxable
   * @format mfg-YES/mfg-NO
   */
   field Taxable as logical initial ?
"""

be_dataset['TaxCode'] = \
"""
  /**
   * Tax Code
   *
   * @label eam-990cdf2e-06d8-3880-dd11-92a770b75ad2
   * @readonly
   * @source tax_code
   * @format x(12)
   */
   field TaxCode as character initial ?
"""

be_dataset['TaxCodeDescription'] = \
"""
  /**
   * Tax Code Description
   *
   * @label mfg-TAX_CODE_DESCRIPTION
   * @readonly
   * @format x(30)
   */
   field TaxCodeDescription as character initial ?
"""

be_dataset['TaxCost'] = \
"""
  /**
   * Tax Cost
   *
   * @label ACTUAL_TAX
   * @readonly
   * @source tax_cost
   * @format >>>,>>>,>>>,>>9.99
   */
   field TaxCost as decimal initial ?
"""

be_dataset['TaxClass'] = \
"""
  /**
   * Tax Class
   *
   * @label eam-9e128334-2108-0090-de11-ef2c4afe7ad9
   * @readonly
   * @source class_code
   * @format x(12)
   */
   field TaxClass as character initial ?
"""

be_dataset['TaxClassDescription'] = \
"""
  /**
   * Tax Class Description
   *
   * @label mfg-TAX_CLASS_DESCRIPTION
   * @readonly
   * @format x(30)
   */
   field TaxClassDescription as character initial ?
"""

be_dataset['ToolClass'] = \
"""
  /**
   * Tool Class
   *
   * @label eam-93692734-ff41-d3b2-e511-167c54795584
   * @source class_code
   * @format x(12)
   */
   field ToolClass as character initial ?
"""

be_dataset['ToolClassDescription'] = \
"""
  /**
   * Tool Class Description
   *
   * @label TOOL_CLASS_DESCRIPTION
   * @readonly
   * @source class_code-desc
   * @format x(30)
   */
   field ToolClassDescription as character initial ?
"""

be_dataset['ToolClassDescription1'] = \
"""
  /**
   * Tool Class Description
   *
   * @label TOOL_CLASS_DESCRIPTION
   * @readonly
   * @format x(30)
   */
   field ToolClassDescription as character initial ?
"""

be_dataset['ToolNumber'] = \
"""
  /**
   * Tool Number
   *
   * @label mfg-TOOL_NUMBER
   * @source tool_no
   * @format x(20)
   */
   field ToolNumber as character initial ?
"""

be_dataset['ToolStatusCode'] = \
"""
  /**
   * Tool Status
   *
   * @label eam-92ec9c98-6947-5287-e511-117ce7a62428
   * @source status_code
   * @format x(12)
   */
   field ToolStatusCode as character initial ?
"""

be_dataset['ToolStatusCodeDescription'] = \
"""
  /**
   * Tool Status Desc
   *
   * @label eam-93692734-ff41-d3b2-e511-177c44ebbddb
   * @readonly
   * @source status_code-desc
   * @format x(30)
   */
   field ToolStatusCodeDescription as character initial ?
"""

be_dataset['ToolStatusCodeDescription1'] = \
"""
  /**
   * Tool Status Desc
   *
   * @label eam-93692734-ff41-d3b2-e511-177c44ebbddb
   * @readonly
   * @format x(30)
   */
   field ToolStatusCodeDescription as character initial ?
"""

be_dataset['ToolType'] = \
"""
  /**
   * Tool Type
   *
   * @label TOOL_TYPE
   * @source type_code
   * @format x(12)
   */
   field ToolType as character initial ?
"""

be_dataset['ToolTypeDescription'] = \
"""
  /**
   * Tool Type Description
   *
   * @label TOOL_TYPE
   * @readonly
   * @source type_code-desc
   * @format x(30)
   */
   field ToolTypeDescription as character initial ?
"""

be_dataset['ToolTypeDescription1'] = \
"""
  /**
   * Tool Type Description
   *
   * @label TOOL_TYPE
   * @readonly
   * @format x(30)
   */
   field ToolTypeDescription as character initial ?
"""

be_dataset['TotalCost2'] = \
"""
  /**
   * Total Cost
   *
   * @label eam-87958a18-58c1-8d85-dc11-54cfc0e9a42c
   * @readonly
   * @format >,>>>,>>9.99
   */
   field TotalCost as decimal initial ?
"""

be_dataset['TotalCost'] = \
"""
  /**
   * Planned Cost
   *
   * @label eam-f876688b-2365-d5af-db11-c123f308413a
   * @source tot_cost
   * @format >,>>>,>>9.99
   */
   field TotalCost as decimal initial ?
"""

be_dataset['TotalInvoiceCost'] = \
"""
  /**
   * Total Invoice Cost
   *
   * @label eam-a0450b3f-6173-c684-de11-93e50e7fba3a
   * @readonly
   * @format >>>,>>>,>>>.99
   */
   field TotalInvoiceCost as decimal initial ?
"""

be_dataset['TotalLaborBurden'] = \
"""
  /**
   * Total Labor Burden
   *
   * @label eam-a0450b3f-6173-c684-de11-93e5da30bfad
   * @readonly
   * @format >>>,>>>,>>>.99
   */
   field TotalLaborBurden as decimal initial ?
"""

be_dataset['TotalOnHandQuantity'] = \
"""
  /**
   * Total On Hand Quantity
   *
   * @label mfg-ON_HAND
   * @format >,>>>,>>9.99
   */
   field TotalOnHandQuantity as decimal initial ?
"""

be_dataset['TotalPOVariance'] = \
"""
  /**
   * Total PO Variance
   *
   * @label eam-a0450b3f-6173-c684-de11-93e50a2a9c5c
   * @readonly
   * @format >>>,>>>,>>>.99
   */
   field TotalPOVariance as decimal initial ?
"""

be_dataset['TotalSpent'] = \
"""
  /**
   * Total Spent
   *
   * @label eam-6291f659-9966-c044-a07f-2836412f631d
   * @readonly
   * @source expensed
   * @format >>>,>>>,>>>,>>9.99
   */
   field TotalSpent as decimal initial ?
"""

be_dataset['TotalTax'] = \
"""
  /**
   * Total Tax
   *
   * @label mfg-TOTAL_TAX
   * @readonly
   * @format >>>,>>>,>>>.99
   */
   field TotalTax as decimal initial ?
"""

be_dataset['TypeCode'] = \
"""
  /**
   * Type
   *
   * @label eam-80531bc1-b0b7-5fb1-db11-791656a6684c
   * @source type_code
   * @format x(5)
   */
   field TypeCode as character initial ?
"""

be_dataset['TypeCode2'] = \
"""
  /**
   * Type
   *
   * @label eam-80531bc1-b0b7-5fb1-db11-791656a6684c
   * @readonly
   * @format x(12)
   */
   field TypeCode as character initial ?
"""

be_dataset['TypeCodeDescription'] = \
"""
  /**
   * Type Description
   *
   * @label eam-823c8344-6881-8381-df11-e49844f44005
   * @readonly
   * @format x(30)
   */
   field TypeCodeDescription as character initial ?
"""

### U ###
be_dataset['UseAllocations'] = \
"""
  /**
   * Use Allocations
   *
   * @label USE_ALLOCATIONS
   * @source use_alloc
   * @format mfg-YES/mfg-NO
   */
   field UseAllocations as logical initial ?
"""

be_dataset['UnallocatedBalance'] = \
"""
  /**
   * Unallocated Balance
   *
   * @label eam-b928136c-0233-8fa8-e111-e10a960bb5f3
   * @readonly
   * @source unalloc_bal
   * @format >>>,>>>,>>>,>>9.99
   */
   field UnallocatedBalance as decimal initial ?
"""

be_dataset['UnauthorizedPurchases'] = \
"""
  /**
   * Unauthorized Purchases
   *
   * @label EAM-UNAUTHORIZED_PURCHASES
   * @readonly
   * @source plan_purch
   * @format >>>,>>>,>>>,>>9.99
   */
   field UnauthorizedPurchases as decimal initial ?
"""

be_dataset['UncapitalizedAmount'] = \
"""
  /**
   * Uncapitalized Amount
   *
   * @label eam-c960a771-bfbc-8986-e111-14c5fa9b3284
   * @readonly
   * @format mfg-YES/mfg-NO
   */
   field UncapitalizedAmount as decimal initial ?
"""

be_dataset['UnfundedAmount'] = \
"""
  /**
   * Unfunded Amt
   *
   * @label eam-9520c19c-4616-c9ac-de11-23a286dded30
   * @readonly
   * @source unfunded_amt
   * @format ->>>,>>>,>>>,>>>,>>9.99
   */
   field UnfundedAmount as decimal initial ?
"""

be_dataset['UnitOfMeasure'] = \
"""
  /**
   * UM
   *
   * @label mfg-UM
   * @source uom_code
   * @format x(12)
   */
   field UnitOfMeasure as character initial ?
"""

be_dataset['UnitOfMeasureCode'] = \
"""
  /**
   * UM
   *
   * @label mfg-UM
   * @source uom_code
   * @format x(12)
   */
   field UnitOfMeasureCode as character initial ?
"""

be_dataset['UnreserveQuantity'] = \
"""
 /**
   * Unreserve Quantity
   *
   * @label UNRESERVE_QTY
   * @format >,>>>,>>9.99
   */
   field UnreserveQuantity as decimal initial ?
"""

be_dataset['UsageCode'] = \
"""
  /**
   * Tax Usage
   *
   * @label eam-8cd98a05-26db-8046-ae2c-d05894224a3f
   * @readonly
   * @source usage_code
   * @format x(12)
   */
   field UsageCode as character initial ?
"""

be_dataset['UsageCodeDescription'] = \
"""
  /**
   * Tax Usage Description
   *
   * @label eam-a4fec391-2df7-be83-dd11-581c3cdb8736
   * @readonly
   * @format x(30)
   */
   field UsageCodeDescription as character initial ?
"""

### V ###
be_dataset['VarianceContract'] = \
"""
  /**
   * Variance Contract
   *
   * @label EAM-VARIANCE_CONTRACT
   * @readonly
   * @format >>>,>>>,>>>.99
   */
   field VarianceContract as decimal initial ?
"""

be_dataset['VarianceLabor'] = \
"""
  /**
   * Variance Labor
   *
   * @label eam-8469f75b-62c9-10bf-db11-1748c33ba6ec
   * @readonly
   * @format >>>,>>>,>>>.99
   */
   field VarianceLabor as decimal initial ?
"""

be_dataset['VarianceMaterial'] = \
"""
  /**
   * Variance Material
   *
   * @label eam-8469f75b-62c9-10bf-db11-17489ba55cf1
   * @readonly
   * @format >>>,>>>,>>>.99
   */
   field VarianceMaterial as decimal initial ?
"""

be_dataset['VarianceTax'] = \
"""
  /**
   * Var Tax
   *
   * @label mfg-TAX
   * @readonly
   * @format >>>,>>>,>>>.99
   */
   field VarianceTax as decimal initial ?
"""

be_dataset['VarianceTotal'] = \
"""
  /**
   * Var Total
   *
   * @label eam-b29dedc4-9e76-1388-db11-28f86333e118
   * @readonly
   * @format >>>,>>>,>>>.99
   */
   field VarianceTotal as decimal initial ?
"""

be_dataset['Vouchered'] = \
"""
  /**
   * AP Invoices
   *
   * @label eam-97822ccb-2f56-4d81-e111-a10f0f13c128
   * @readonly
   * @source vouchered
   * @format ->>>,>>>,>>>,>>9.99
   */
   field Vouchered as decimal initial ?
"""

### W ###
be_dataset['WaitHours'] = \
"""
  /**
   * Wait Hours
   *
   * @label WAIT_HOURS
   * @format >>>9
   */
   field WaitHours as integer initial ?
"""

be_dataset['WaitMinutes'] = \
"""
  /**
   * Wait Minutes
   *
   * @label WAIT_MINUTES
   * @format >>>9
   */
   field WaitMinutes as integer initial ?
"""

be_dataset['WarningLimit'] = \
"""
  /**
   * Warning Limit
   *
   * @label eam-ac7ad135-cd21-b8af-e411-6a3e61cc0e17
   * @source warn_amt
   * @format ->>>,>>>,>>>,>>>,>>9.99
   */
   field WarningLimit as decimal initial ?
"""

be_dataset['WarehouseSite'] = \
"""
  /**
   * Warehouse Site
   *
   * @label eam-aa492f96-87b5-88ba-da11-0fd6631e54b0
   * @readonly
   * @format x(12)
   */
   field WarehouseSite as character initial ?
"""

be_dataset['Weight'] = \
"""
  /**
   * Weight
   *
   * @label eam-80531bc1-b0b7-5fb1-db11-79160d6b6d4c
   * @source weight
   * @format >,>>>,>>9.99
   */
   field Weight as decimal initial ?
"""

be_dataset['WeightUnitOfMeasure'] = \
"""
  /**
   * Weight UM
   *
   * @label mfg-WEIGHT_UM
   * @source wt_uom
   * @format x(12)
   */
   field WeightUnitOfMeasure as character initial ?
"""

be_dataset['WorkOrderNumber2'] = \
"""
  /**
   * Asset Work Order
   *
   * @label ASSET_WORK_ORDER
   * @source wo_no
   * @format >>>>>>>>9
   */
   field WorkOrderNumber as integer initial ?
"""

be_dataset['WorkOrderNumber'] = \
"""
  /**
   * Work Order
   *
   * @label eam-a0757faf-48de-d840-9cc6-5d320fe01a3a
   * @readonly
   * @source wo_no
   * @format >>>>>>>>9
   */
   field WorkOrderNumber as integer initial ?
"""

be_dataset['WorkOrderStatus'] = \
"""
  /**
   * Status
   *
   * @label mfg-STATUS
   * @format x(20)
   */
   field WorkOrderStatus as character initial ?
"""

be_dataset['WriteOffAmount'] = \
"""
  /**
   * Write Off Amount
   *
   * @label EAM-WRITE_OFF_AMOUNT
   * @readonly
   * @source writeoff_amt
   * @format ->>>,>>>,>>>,>>>,>>9.99
   */
   field WriteOffAmount as decimal initial ?
"""

### X ###
### Y ###
### Z ###



























