#!/usr/bin/env python

##### SERVICE DATASET FIELDS #####
service_dataset = {}

class Service():
  def __init__(self):
    self.service_dataset = service_dataset

### A ###
service_dataset['AccountNumber'] = \
"""
  /**
   * Account Number
   *
   * @label eam-2660a9c1-d85c-4048-944e-26b04f421d80
   * @format x(20)
   */
   field AccountNumber as character initial ?
"""

service_dataset['AccountDescription'] = \
"""
  /**
   * Account Description
   *
   * @label eam-d8ae100c-9558-99b8-e311-019aad851c7d
   * @readonly
   * @format x(20)
   */
   field AccountDescription as character initial ?
"""

service_dataset['AmountPaid'] = \
"""
  /**
   * Amount Paid
   *
   * @label eam-8bbd16c7-aa0e-0e9d-db11-714a9ce9b589
   * @format ->,>>>,>>9.99
   */
   field AmountPaid as decimal initial ?
"""

service_dataset['Allocated'] = \
"""
  /**
   * Reserved Qty
   *
   * @label RESERVED_QTY
   * @format >>>,>>>,>>9.99
   */
   field Allocated as decimal initial ?
"""

service_dataset['Area'] = \
"""
  /**
   * Rebuild
   *
   * @label eam-8bbd16c7-aa0e-0e9d-db11-714af6a7368a
   * @format Blank
   */
   field Area as character initial ?

"""
service_dataset['AssemblyCode'] = \
"""
  /**
   * Assembly
   *
   * @label eam-85f0935a-f8ed-aa97-dc11-645546b46f97
   * @format X(20)
   */
   field AssemblyCode as character initial ?   
"""

service_dataset['AssetNumber'] = \
"""
  /**
   * Asset Number
   *
   * @label mfg-ASSET
   * @format x(30)
   */
   field AssetNumber as character initial ?
"""

service_dataset['AdjustLocation'] = \
"""
  /**
   * Location
   *
   * @label eam-5391acc7-88d8-ee4a-981a-ddd56ea3b5f9
   * @format x(10)
   */
   field AdjustLocation as character initial ?
"""

### B ###
### C ###
service_dataset['CancelOrderedParts'] = \
"""
  /**
   * Cancel?
   *
   * @label eam-aee617ec-a504-0e8c-dc11-a213d87d55ca
   * @format mfg-YES/mfg-NO
   */
   field CancelOrderedParts as logical initial ?
"""

service_dataset['CloseDate'] = \
"""
  /**
   * Close Date
   *
   * @label mfg-CLOSE_DATE
   * @format 99/99/9999
   */
   field CloseDate as date initial ?
"""

service_dataset['CloseTime'] = \
"""   
  /**
   * Close Time
   *
   * @label mfg-CLOSE_TIME	
   * @format >,>>>,>>>.99
   */
   field CloseTime as integer initial ?
"""

service_dataset['CloseDateAndTime'] = \
"""   
  /**
   * Close Date Time
   *
   * @label mfg-LAST_MODIFIED_DATE_TIME-short
   * @format DATETIME
   */
   field CloseDateAndTime as datetime initial ?
"""

service_dataset['Comment'] = \
"""
  /**
   * Comment
   *
   * @label mfg-COMMENT
   * @format x(40)
   */
   field Comment as character initial ?
"""

service_dataset['CommodityCode'] = \
"""
  /**
   * Commodity
   *
   * @label eam-3a973fc5-09c1-6d43-9ca9-09a64d2f7474
   * @format X(20)
   */
   field CommodityCode as character initial ?   
"""

service_dataset['CostCenter'] = \
"""
  /**
   * Cost Center
   *
   * @label eam-1f4a7c86-96c0-d241-bd66-5927595580b8
   * @format x(20)
   */
   field CostCenter as character initial ?
"""

service_dataset['CostCenterDescription'] = \
"""
  /**
   * Cost Center Description
   *
   * @label eam-3c3c9e91-02ad-034b-bca2-5c2d465fd32a
   * @readonly
   * @format x(20)
   */
   field CostCenterDescription as character initial ?
"""

service_dataset['Code'] = \
"""
  /**
   * Code
   *
   * @label eam-8bbd16c7-aa0e-0e9d-db11-714a4aaeba89
   * @format x(1)
   */
   field Code as character initial ?
"""

service_dataset['CostPerItem'] = \
"""
  /**
   * Cost Per Item
   *
   * @label COST_PER_ITEM
   * @format >>>,>>>,>>>.99
   */
   field CostPerItem as decimal initial ?
"""

service_dataset['ContractCost'] = \
"""
  /**
   * Contract
   *
   * @label mfg-CONTRACT
   * @format >>>,>>>,>>>.99
   */
   field ContractCost as decimal initial ?
"""

service_dataset['Consign'] = \
"""
  /**
   * Consignment
   *
   * @label eam-95c294ef-13a1-e497-db11-36472f7ce3f6
   * @format mfg-YES/mfg-NO
   */
   field Consign as logical initial ?
"""

service_dataset['Contractor'] = \
"""
  /**
   * Contractor
   *
   * @label eam-8bbd16c7-aa0e-0e9d-db11-714a4ed5c189
   * @format mfg-YES/mfg-NO
   */
   field Contractor as logical initial ?
"""

service_dataset['Cost'] = \
"""
  /**
   * UOM Base Cost
   *
   * @label UOM_BASE_COST   
   * @format ->,>>>,>>9.99999
   */
   field Cost as decimal initial ?
"""

service_dataset['Craft'] = \
"""
  /**
   * Craft
   *
   * @label EAM-PRICRAFT
   * @format x(8)
   */
   field Craft as character initial ?
"""

service_dataset['Correction'] = \
"""
  /**
   * Correction
   *
   * @label eam-8bbd16c7-aa0e-0e9d-db11-714a1036e389
   * @format mfg-YES/mfg-NO
   */
   field Correction as logical initial ?   
"""

service_dataset['CurrencyCode'] = \
"""
  /**
   * Currency
   *
   * @label eam-80531bc1-b0b7-5fb1-db11-791645e4254c
   * @format X(5)
   */
   field CurrencyCode as character initial ?
"""

service_dataset['CurrentQty'] = \
"""
  /**
   * Current On Hand
   *
   * @label eam-cb2944f0-b5ac-4bb8-db11-5968cee217c0
   * @readonly
   * @source curr_qty
   * @format ->,>>>,>>9.99
   */
   field CurrentQty as decimal initial ?
"""

### D ###
service_dataset['DateComplete'] = \
"""
  /**
   * Date
   *
   * @label eam-91363955-1347-b4a3-de11-9b91ff37730d
   * @format 99/99/9999
   */
   field DateComplete as date initial ?
"""

service_dataset['DateShipped'] = \
"""
  /**
   * Date Shipped
   *
   * @label eam-8bbd16c7-aa0e-0e9d-db11-714a53fcc889
   * @format 99/99/9999
   */
   field DateShipped as date initial ?
"""

service_dataset['DateStamp'] = \
"""
  /**
   * Date Stamp
   *
   * @label eam-8bbd16c7-aa0e-0e9d-db11-714aaa5ecb89
   * @format X(15)
   */
   field DateStamp as character initial ?
"""

service_dataset['DateDue'] = \
"""
  /**
   * Date Due
   *
   * @label eam-b9fcaed1-5c01-0fae-dc11-dc2f96e24002
   * @format 99/99/9999
   */
   field DateDue as date initial ?
"""

service_dataset['DateReceived'] = \
"""
  /**
   * Date Received
   *
   * @label mfg-DATE_RECEIVED
   * @format 99/99/9999
   */
   field DateReceived as date initial ?
"""

service_dataset['DateReturned'] = \
"""
  /**
   * Date Returned
   *
   * @label eam-ba5c81e2-933a-cf86-da11-6bea6490ebe9
   * @format 99/99/9999
   */
   field DateReturned as date initial ?
"""

service_dataset['DepartmentCode'] = \
"""
  /**
   * Department
   *
   * @label eam-940ad953-9fd0-6d9e-dd11-af9bc81039dc
   * @format x(10)
   */
   field DepartmentCode as character initial ?
"""

service_dataset['DisallowedActions'] = \
"""
  /**
   * Based on some conditions this field tells the consumer what are
   * the actions that cannot be performed on this record.
   * e.g. "EDIT", "DELETE", "EDITDELETE" or ""
   *
   */   
   field DisallowedActions as character initial ?
"""

service_dataset['DisallowedActionsMessage'] = \
"""    
  /**
   * Message for disallowed actions.
   *
   */
   field DisallowedActionsMessage as character initial ?  
"""

### E ###
service_dataset['Employee'] = \
"""
  /**
   * Employee
   *
   * @label mfg-EMPLOYEE
   * @format x(20)
   */
   field Employee as character initial ?
"""

service_dataset['EmployeeName'] = \
"""
  /**
   * Name
   *
   * @label eam-5e7826de-02e7-9c45-96d0-bc436ccb0ea5
   * @format x(30)
   */
   field EmployeeName as character initial ?   
"""

service_dataset['EquipmentCode'] = \
"""
  /**
   * Equipment
   *
   * @label mfg-EQUIPMENT
   * @format x(20)
   */
   field EquipmentCode as character initial ?
"""

service_dataset['EquipmentNumber'] = \
"""
  /**
   * Equipment
   *
   * @label EAM-EQUIPMENT
   * @format x(20)
   */
   field EquipmentNumber as character initial ?   
"""

service_dataset['EquipmentDescription'] = \
"""   
  /**
   * Equipment Description
   *
   * @label mfg-DESCRIPTION
   * @format x(30)
   */
   field EquipmentDescription as character initial ?
"""

service_dataset['ExchangeRate'] = \
"""
  /**
   * Exchange Rate
   *
   * @label eam-8bbd16c7-aa0e-0e9d-db11-714a06e8d489
   * @format >>,>>9.999999
   */
   field ExchangeRate as decimal initial ?
"""

service_dataset['ExpenseSite'] = \
"""
  /**
   * Expense Site
   *
   * @label eam-cbd171ba-657d-3d8e-e311-89821d39cb9e
   * @format X(8)
   */
   field ExpenseSite as character initial ?
"""

### F ###
service_dataset['FailureType'] = \
"""
  /**
   * Failure Type
   *
   * @label eam-80531bc1-b0b7-5fb1-db11-791682593b4c
   * @format X(5)
   */
   field FailureType as character initial ?
"""

service_dataset['FromBusinessEntityID'] = \
"""
  /**
   * From BE ID
   *
   * @label eam-d3000bfb-7f8c-58b9-dc11-5bcf3764a8e5
   * @format x(12)
   */
   field FromBusinessEntityID as character initial ?
"""

service_dataset['FullyIssued'] = \
"""
  /**
   * Fully Issued
   *
   * @label eam-eb821ce7-b73a-3c97-e111-36eda95c4ed1
   * @format mfg-YES/mfg-NO
   */
   field FullyIssued as logical initial ?
"""

### G ###
### H ###
service_dataset['HasParent'] = \
"""
  /**
   * Has Parent Equipment
   *
   * @label HAS_PARENT
   * @format mfg-YES/mfg-NO
   */
   field HasParent as logical initial ?
"""

### I ###
service_dataset['IsCancelQuantity'] = \
"""
/**
   * Cancel
   *
   * @label CANCEL_QUANTITY
   * @format mfg-YES/mfg-NO
   */
   field IsCancelQuantity as logical initial ?
"""

service_dataset['IsLocked'] = \
"""
  /**
   * Locked
   *
   * @label mfg-LOCKED
   * @format mfg-YES/mfg-NO
   */
   field IsLocked as logical initial ?
"""

service_dataset['IsReserved'] = \
"""
  /**
   * Reserved
   *
   * @label eam-bb7a00b4-edbc-a996-db11-0b06a4491970
   * @format mfg-YES/mfg-NO
   */
   field IsReserved as logical initial ?   
"""

service_dataset['IsRotable'] = \
"""
  /**
   * Rotable
   *
   * @label eam-bf7346b4-07d6-6694-e411-2e1e9338af97
   * @format mfg-YES/mfg-NO
   */
   field IsRotable as logical initial ?
"""

service_dataset['IssueCost'] = \
"""
  /**
   * Issue Cost
   *
   * @label mfg-ISSUE_COST
   * @format ->,>>>,>>9.99
   */
   field IssueCost as decimal initial ?
"""

service_dataset['Issued'] = \
"""
  /**
   * Issued
   *
   * @label eam-f6348607-c873-978c-e111-b9eb09071737
   * @format >>>,>>>,>>9.99
   */
   field IssuedQuantity as decimal initial ?
"""

service_dataset['InventoryCost'] = \
"""
  /**
   * Inventory Cost
   *
   * @label eam-82fa1a43-be0d-0c84-db11-d8a7c6573a92
   * @format >>>,>>>,>>>.99
   */
   field InventoryCost as decimal initial ?   
"""

### J ###
service_dataset['JobNumber'] = \
"""
  /**
   * Job
   *
   * @label eam-ecfbaa03-7abb-754c-a4d1-cdffe11379cf
   * @format x(20)
   */
   field JobNumber as character initial ?
"""

### K ###
### L ###
service_dataset['LaborCost'] = \
"""
  /**
   * Labor
   *
   * @label mfg-LABOR
   * @format >>>,>>>,>>>.99   
   */
   field LaborCost as decimal initial ?
"""

service_dataset['LineNumber'] = \
"""
  /**
   * Line
   *
   * @label mfg-LINE
   * @format >>>>>>>>9
   */
   field LineNumber as integer initial ?
"""

service_dataset['LineShort'] = \
"""
  /**
   * Short
   *
   * @label LINE_SHORT
   * @format >,>>>,>>9.99
   */
   field ShortQuantity as decimal initial ?
"""

service_dataset['LifeExpectancy'] = \
"""
  /**
   * Life Expectancy
   *
   * @label LIFE_EXPECTANCY
   * @format >>>,>>>9
   */
   field LifeExpectancy as integer initial ?  
"""

service_dataset['Location'] = \
"""
  /**
   * Location
   *
   * @label eam-5391acc7-88d8-ee4a-981a-ddd56ea3b5f9
   * @format x(20)
   */
   field Location as character initial ?
"""

### M ###
service_dataset['Manufacturer'] = \
"""
  /**
   * Manufacturer
   *
   * @label mfg-MANUFACTURER
   * @format x(30)
   */
   field Manufacturer as character initial ?
"""

service_dataset['MaterialCost'] = \
"""
  /**
   * Material
   *
   * @label mfg-MATERIAL
   * @format >>>,>>>,>>>.99
   */
   field MaterialCost as decimal initial ?
"""

service_dataset['ModelNumber'] = \
"""
  /**
   * Model
   *
   * @label EAM-MODEL
   * @format x(30)
   */
   field ModelNumber as character initial ?
"""

### N ###
service_dataset['NetQuantity'] = \
"""
  /**
   * Net Qty Issued
   *
   * @label eam-ff76839f-e8ca-87a0-db11-7efeda1b08ff
   * @format ->,>>>,>>9.99
   */
   field NetQuantity as decimal initial ?
"""

service_dataset['NewCurrentQty'] = \
"""
  /**
   * New On Hand
   *
   * @label eam-cb2944f0-b5ac-4bb8-db11-59685e7457d1
   * @format ->,>>>,>>9.99
   */
   field NewCurrentQty as decimal initial ?
"""

### O ###
service_dataset['OnHand'] = \
"""
  /**
   * On Hand
   *
   * @label mfg-ON_HAND
   * @format >,>>>,>>9.99
   */
   field OnHand as decimal initial ?
"""

service_dataset['OnHandQuantity'] = \
"""
  /**
   * On Hand
   *
   * @label mfg-ON_HAND
   * @format >,>>>,>>>9
   */
   field OnHandQuantity as integer initial ?
"""

service_dataset['OverheadGroup'] = \
"""
  /**
   * Overhead Group
   *
   * @label OVERHEAD_GROUP
   * @format X(12)
   */
   field OverheadGroup as character initial ?
"""

### P ###
service_dataset['PackingSlip'] = \
"""
  /**
   * Packing Slip
   *
   * @label eam-8bbd16c7-aa0e-0e9d-db11-714aa46c3b8a
   * @source packing_slip
   * @format Blank
   */
   field PackingSlip as character initial ?
"""

service_dataset['ParentEquipmentCode'] = \
"""
  /**
   * Parent Code
   *
   * @label mfg-PARENT
   * @format x(20)
   */
   field ParentEquipmentCode as character initial ?
"""

service_dataset['PartNumber'] = \
"""
  /**
   * Part Number
   *
   * @label mfg-PART
   * @format x(20)
   */
   field PartNumber as character initial ?
"""

service_dataset['PartDescription'] = \
"""
  /**
   * Part Description
   *
   * @label PART_DESCRIPTION
   * @readonly
   * @format X(30)
   */
   field PartDescription as character initial ?
"""

service_dataset['PartStatus'] = \
"""
  /**
   * Status
   *
   * @label eam-26719618-5811-9b47-b1fb-d0858912bcdb
   * @readonly
   * @format x(12)
   */
   field PartStatus as character initial ?  
"""

service_dataset['PayAdditiveCode'] = \
"""
  /**
   * Pay Additive
   *
   * @label eam-c29618fc-ba1d-2281-dc11-8bb9425b70fa
   * @format x(8)
   */
   field PayAdditiveCode as character initial ?
"""

service_dataset['PayMultiplierCode'] = \
"""
  /**
   * Pay Multiplier
   *
   * @label eam-8f910888-acc2-4bb7-dc11-0f718dc5d32a
   * @format x(8)
   */
   field PayMultiplierCode as character initial ?
"""

service_dataset['Period'] = \
"""
  /**
   * Period
   *
   * @label eam-8bbd16c7-aa0e-0e9d-db11-714a386e1c8a
   * @format >>9
   */
   field Period as integer initial ?
"""

service_dataset['Planned'] = \
"""
  /**
   * Planned
   *
   * @label mfg-PLANNED
   * @format >>>,>>>,>>>.99
   */
   field PlannedQuantity as decimal initial ?
"""

service_dataset['PlannedCost'] = \
"""
  /**
   * Planned Cost
   *
   * @label eam-f876688b-2365-d5af-db11-c123f308413a
   * @format >,>>>,>>9.99
   */
   field PlannedCost as decimal initial ?
"""

service_dataset['PlannedQuantity'] = \
"""
  /**
   * Planned Quantity
   *
   * @label PLANNED_QUANTITY
   * @format >,>>>,>>>.99
   */
   field PlannedQuantity as decimal initial ?
"""

service_dataset['ProjectNumber'] = \
"""
  /**
   * Project
   *
   * @label eam-615c5f74-d4bd-6c43-b039-bb2a566027ce
   * @format x(10)
   */
   field ProjectNumber as character initial ?   
"""

### Q ###
service_dataset['Quantity'] = \
"""
  /**
   * Quantity
   *
   * @label eam-c8e7cf62-5f32-59ab-de11-45a101fe3872
   * @format ->,>>>,>>9.99
   */
   field Quantity as decimal initial ?
"""

service_dataset['Quarter'] = \
"""
  /**
   * Quarter
   *
   * @label eam-8bbd16c7-aa0e-0e9d-db11-714af5a7368a
   * @format 9
   */
   field Quarter as integer initial ?
"""

service_dataset['QtyOrdered'] = \
"""
  /**
   * Qty Ordered
   *
   * @label eam-d755d7b5-1ccc-be88-dc11-b9301d10ee99
   * @format >,>>>,>>9.99
   */
   field QtyOrdered as decimal initial ?
"""

service_dataset['QuantityToReceive'] = \
"""
  /**
   * Quantity To Receive
   *
   * @label mfg-QUANTITY_TO_RECEIVE
   * @format >>>,>>>,>>>.99
   * @required
   */
   field QuantityToReceive as decimal initial ?
"""

service_dataset['QuantityToReturn'] = \
"""
  /**
   * Quantity To Return
   *
   * @label mfg-QUANTITY_TO_RETURN
   * @format >,>>>,>>>9
   * @required
   */
   field QuantityToReturn as integer initial ?
"""

### R ###
service_dataset['Rebuild'] = \
"""
  /**
   * Rebuild
   *
   * @label eam-87958a18-58c1-8d85-dc11-54cf60dc2154
   * @format x(20)
   */
   field Rebuild as character initial ?
"""

service_dataset['ReceivedCost'] = \
"""
  /**
   * Received Cost
   *
   * @label mfg-RECEIVED_COST
   * @format >,>>>,>>9.99
   */
   field ReceivedCost as decimal initial ?
"""

service_dataset['Received'] = \
"""
  /**
   * Received
   *
   * @label eam-7c39014f-f250-1746-ba1a-66e622685383
   * @format >,>>>,>>9.99
   */
   field Received as decimal initial ?
"""

service_dataset['Receiver'] = \
"""
  /**
   * Receiver
   *
   * @label eam-8bbd16c7-aa0e-0e9d-db11-714aa36c3b8a
   * @format X(20)
   */
   field Receiver as character initial ?
"""

service_dataset['RecordCount'] = \
"""
  /**
   * Record Count
   *
   * @label mfg-RECORD_COUNT
   * @format >9
   */
   field RecordCount as integer initial ?
"""

service_dataset['RequestorCode'] = \
"""
  /**
   * Requestor
   *
   * @label eam-8bbd16c7-aa0e-0e9d-db11-714a5131408a
   * @format x(20)
   */
   field RequestorCode as character initial ?
"""

service_dataset['RequiredDate'] = \
"""
  /**
   * Required Date
   *
   * @label eam-f876688b-2365-d5af-db11-c123dfe1393a
   * @format 99/99/9999
   */
   field RequiredDate as date initial ?
"""

service_dataset['RequisitionNumber'] = \
"""  
  /**
   * Requisition
   *
   * @label eam-812dc5fe-1592-d596-e111-33aff09cb385
   * @format ->,>>>,>>9
   */
   field RequisitionNumber as integer initial ?   
"""

service_dataset['Reserved'] = \
"""
  /**
   * Reserved
   *
   * @label eam-aa492f96-87b5-88ba-da11-0fd6843a07ae
   * @format >,>>9.99
   */
   field ReservedQuantity as decimal initial ?   
"""

service_dataset['ReservedQuantity'] = \
"""
  /**
   * Reserved Quantity
   *
   * @label RESERVED_QTY
   * @format >>>,>>>,>>9.99
   */
   field ReservedQuantity as decimal initial ?
"""

service_dataset['ReturnUOM'] = \
"""
  /**
   * Return UOM
   *
   * @label eam-8bbd16c7-aa0e-0e9d-db11-714aa993428a
   * @format X(6)
   */
   field ReturnUOM as character initial ?
"""

service_dataset['ReturnCode'] = \
"""
  /**
   * Return
   *
   * @label eam-8bbd16c7-aa0e-0e9d-db11-714afff5448a
   * @format X(5)
   */
   field ReturnCode as character initial ?
"""

service_dataset['RotablePart'] = \
"""
  /**
   * Rotable Part
   *
   * @label ROTABLE_ITEM
   * @format x(20)
   */
   field RotablePart as character initial ?   
"""

### S ###
service_dataset['SendMFGPro'] = \
"""
  /**
   * Send to ERP?
   *
   * @label eam-8bbd16c7-aa0e-0e9d-db11-714a5758478a
   * @format mfg-YES/mfg-NO
   */
   field SendMFGPro as logical initial ?
"""

service_dataset['SerialNumber'] = \
"""
  /**
   *  Serial
   *
   * @label mfg-SERIAL_NUMBER   
   * @format x(30)
   */
   field SerialNumber as character initial ?
"""

service_dataset['SiteCode'] = \
"""
  /**
   * Site
   *
   * @label mfg-SITE
   * @format X(8)
   */
   field SiteCode as character initial ?
"""

service_dataset['ShortQuantity'] = \
"""
  /**
   * Short
   *
   * @label eam-d51a7c3b-e9dc-a0b4-dc11-5c1f344e5dda
   * @format >,>>>,>>9.99
   */
   field ShortQuantity as decimal initial ?
"""

service_dataset['SourceSite'] = \
"""
  /**
   * Source Site
   *
   * @label eam-af09e099-92ed-a6a2-de11-396cac581b9e
   * @format X(8)
   */
   field SourceSite as character initial ?
"""

service_dataset['StatusCode'] = \
"""
  /**
   * Status
   *
   * @label mfg-STATUS
   * @format X(2)
   */
   field StatusCode as character initial ?
"""

service_dataset['StatusDescription'] = \
"""
  /**
   * Status
   *
   * @label mfg-STATUS
   * @readonly
   * @nomapping
   * @format X(18)   
   */
   field StatusDescription as character initial ?   
"""

service_dataset['StartDate'] = \
"""
  /**
   * Start Date
   *
   * @label eam-8bbd16c7-aa0e-0e9d-db11-714a051d4c8a
   * @format 99/99/9999
   */
   field StartDate as date initial ?
"""

service_dataset['StartTime'] = \
"""
  /**
   * Start Time
   *
   * @label eam-8bbd16c7-aa0e-0e9d-db11-714ab2e1508a
   * @format >,>>>,>>9
   */
   field StartTime as integer initial ?
"""

service_dataset['SubAccount'] = \
"""
  /**
   * Sub Account
   *
   * @label eam-522c91fe-7f25-5a45-bc67-b13d2a1b8b19
   * @format X(10)
   */
   field SubAccount as character initial ?
"""

service_dataset['SubAccountDescription'] = \
"""
  /**
   * Sub Account Description
   *
   * @label fin-726901579
   * @readonly
   * @format x(20)
   */
   field SubAccountDescription as character initial ?
"""

service_dataset['SystemCode'] = \
"""
  /**
   * System
   *
   * @label eam-4f5a5486-6478-194f-aa2c-5e6fa9b2fe7d
   * @format X(10)
   */
   field SystemCode as character initial ?
"""

service_dataset['StoresRequisitionNumber'] = \
"""
  /**
   * Stores Requisition
   *
   * @label mfg-STORES_REQUISITION
   * @format >>>>>>>>9
   */
   field StoresRequisitionNumber as integer initial ?
"""

### T ###
service_dataset['TimeSpent'] = \
"""
  /**
   * Time Spent
   *
   * @label EAM-TIME_SPENT
   * @format HH:MM
   */
   field TimeSpent as character initial ?
"""

service_dataset['TotalOnHandQuantity'] = \
"""
  /**
   * Total On Hand Quantity
   *
   * @label mfg-ON_HAND
   * @format >,>>>,>>9.99
   */
   field TotalOnHandQuantity as decimal initial ?   
"""

service_dataset['TotalTimeSeconds'] = \
"""
  /**
   * Time Spent
   *
   * @label EAM-TIME_SPENT
   * @format >>>>>>>9
   */
   field TotalTimeSeconds as integer initial ?  
"""

service_dataset['ToSite'] = \
"""
  /**
   * To Site
   *
   * @label eam-8bbd16c7-aa0e-0e9d-db11-714a60a6558a
   * @format X(8)
   */
   field ToSite as character initial ?
"""

service_dataset['TransCurrency'] = \
"""
  /**
   * Trans Currency
   *
   * @label eam-8bbd16c7-aa0e-0e9d-db11-714a61a6558a
   * @format X(5)
   */
   field TransCurrency as character initial ?
"""

service_dataset['TransCurrencyMaterial'] = \
"""
  /**
   * Total Cost
   *
   * @label eam-87958a18-58c1-8d85-dc11-54cfc0e9a42c
   * @format >>,>>>,>>9.99
   */
   field TransCurrencyMaterial as decimal initial ?
"""

service_dataset['TotalCost'] = \
"""
  /**
   * Total
   *
   * @label mfg-TOTAL
   * @format >>>,>>>,>>>.99
   */
   field TotalCost as decimal initial ?   
"""

### U ###
service_dataset['UserID'] = \
"""
  /**
   * User ID
   *
   * @label eam-8bbd16c7-aa0e-0e9d-db11-714a991e2d8a
   * @format x(12)
   */
   field UserID as character initial ?
"""

service_dataset['UpdatedDate'] = \
"""
  /**
   * Update Date
   *
   * @label mfg-LAST_UPDATED
   * @format 99/99/9999
   */
   field UpdatedDate as date initial ?
"""

service_dataset['UpdatedTime'] = \
"""
  /**
   * Updated Time
   *
   * @label eam-92a898a9-414f-a1a7-dc11-117cbcb3f731
   * @format >>>>>9
   */
   field UpdatedTime as integer initial ?
"""

service_dataset['UpdatedByUserID'] = \
"""
  /**
   * Updated By User ID
   *
   * @label mfg-UPDATED_BY
   * @format x(12)
   */
   field UpdatedByUserID as character initial ?
"""

service_dataset['UnitOfMeasure'] = \
"""
  /**
   * UM
   *
   * @label mfg-UM
   * @format x(12)
   */
   field UnitOfMeasure as character initial ?
"""

### V ###
### W ###
service_dataset['WarrantyDate'] = \
"""
  /**
   * Warranty Date
   *
   * @label mfg-WARRANTY_DATE
   * @format 99/99/9999
   */
   field WarrantyDate as date initial ?
"""

service_dataset['WOPartsLineNumber'] = \
"""
  /**
   * Item
   *
   * @label eam-9cb812b7-10a0-1ca8-df11-9641b05e8501
   * @format >9
   */
   field WOPartsLineNumber as integer initial ?   
"""

service_dataset['WorkOrderNumber'] = \
"""
  /**
   * Work Order
   *
   * @label WORK_ORDER   
   * @format >>>>>>>>9
   */
   field WorkOrderNumber as integer initial ?
"""

service_dataset['WorkOrderStatus'] = \
"""
  /**
   * Status
   *
   * @label mfg-STATUS
   * @format x(20)
   */
   field WorkOrderStatus as character initial ?  
"""

### X ###
### Y ###
service_dataset['Year'] = \
"""
  /**
   * Year
   *
   * @label eam-8bbd16c7-aa0e-0e9d-db11-714ac156668a
   * @format 9999
   */
   field Year as integer initial ?   
"""

### Z ###

