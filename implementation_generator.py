#!/usr/bin/env python
import os
import re
import sys
import json
import time
import pathlib
import linecache
import fileinput

# definition_path: C:\Users\a3q\Documents\generatorV1\definition
base_path        = os.getcwd()
definition_path  = os.getcwd() + '\definition'

# sys.path.append(definition_path)

from config import *
from configImplementation import *
from interface_generator import *
from export_data import *


class Implementation_Generator():

    def __init__(self):
        c  = Config()
        ci = ConfigImplementation()
        ig = Interface_Generator()
        ed = Export_data()

        # coming from dataset generator originate from field_to_generate e.g. ['site_code', 'proj_no', 'part_no']
        bl_nativefield  = ig.native_fields

        # e.g.['SiteCode', 'ProjectNumber', 'PartNumber']
        bl_logicalfield = ig.logical_fields

        # config variables
        is_be               = c.get('is_be')
        bl_name             = c.get('bl_name')
        be_source           = c.get('be_source')
        be_keyfields        = c.get('be_keyfields')
        be_native_keyfields = c.get('be_native_keyfields')
        be_browseuri        = c.get('be_browseuri')
        bl_dir_name         = c.get('bl_dir_name')
        bl_parent           = c.get('bl_parent')
        bl_dir_parent       = c.get('bl_dir_parent')
        bl_interface        = c.get('bl_interface')
        bl_implementation   = c.get('bl_implementation')
        is_datalist         = c.get('is_datalist')
        bl_native           = ci.get('bl_native')
        be_discarded_lists  = ci.get('bl_discarded_lists')
        is_eamcontext       = ci.get('is_eamcontext')
        bl_legacy_name      = ci.get('bl_legacy_name')

        # variables
        bl_using_import            = ""
        bl_dataset_import          = ""
        service_strings            = ""
        bl_method                  = []
        bl_method_value            = []
        bl_optional_method         = []
        bl_optional_method_lists   = []
        bl_method_option           = []

        # if O is found, then get all optional methods from config.json
        for optional_method in bl_interface:
            if 'O' in optional_method:
                bl_methods   = c.get('bl_method')

                # add method option from config file e.g. inputds, outputds, inputoutputds, get, returnMessage
                for i in range(len(bl_methods)):
                    for key, value in bl_methods[i].items():
                        bl_method_option.append(value['method_option'])


        # get lists from configImplementation.py
        using_import_lists         = ci.using_import_lists()
        import_lists               = ci.import_lists()
        service_lists              = ci.service_lists()


        # create default variables
        # add slash to business entity e.g. inventory.parts -> inventory/parts
        bl_interface_name = bl_dir_name.replace('.', '/')

        # place holder for translated message used in error handling
        bl_message_error = "TRANSLATED_STRING_REQUIRED".upper()

        # keyfields from config.json
        # e.g. "SiteCode,PartNumber,WorkOrderNumber"
        be_keyfield = be_keyfields.split(',')

        # e.g. "site_code,part_no,wo_no"
        be_native_keyfield = be_native_keyfields.split(',')

        # create export data text
        export_data_name = bl_name + '_data.txt'


        if is_be:
            implementation_definition = 'beImplementationDefinition.txt'
        else:
            implementation_definition = 'serviceImplementationDefinition.txt'

        # change directory up to generatorv1 folder
        os.chdir('..')

        pathlib.Path(bl_name).mkdir(parents=True, exist_ok=True)
        os.chdir(os.getcwd() + "\\" + bl_name)

        # Create implementation file
        implementation = bl_name + ".cls"
        f_implementation = open(implementation, 'w+')

        # Copy implementation definition
        with open(os.path.join(definition_path, implementation_definition), 'r') as f_implementationDefinition:
            readlines = f_implementationDefinition.read()
            im_readlines = readlines
            f_implementationDefinition.close()

        f_implementation.write(im_readlines)
        f_implementation.close()

        # method implementation from interface generator
        for k,v in ig.method_implementation.items():

            # interface initial from config.json e.g. ['C', 'R', 'I', 'O']
            bl_method.append(k)

            # e.g. ['C', 'R', 'I', 'O_CheckPartsForWorkOrderReceive', 'O_UpdateReceiveFromWorkOrder']
            # e.g. 'method public void CheckPartsForWorkOrderReceive(input siteCode as character,\n
            #                                                        input partNumber as character,\n
            #                                                        output dataset-handle dsPartsReceiveWorkOrder,\n
            bl_method_value.append(v)

            # add validation service for isvalid or getcontrolstate
            if k == 'V'  or k == 'G':
                service_strings = service_lists[0]

            if 'O_' in k:
                # if its optional method create a list e.g. O_PostLabor
                method_name = k[2:]
                bl_optional_method.append(method_name)
                #  e. g. ['method public void CheckPartsForWorkOrderReceive(input siteCode as character,\n
                #                                                           input partNumber as character,\n
                #                                                           output dataset-handle dsPartsReceiveWorkOrder,\n
                bl_optional_method_lists.append(v)

        # Replace names in implementation
        self.replace_inline(base_path, bl_name, implementation, bl_using_import, bl_dataset_import, service_strings, be_discarded_lists, bl_dir_name, bl_interface_name, bl_native)

        tempFieldStrings            = ""
        tempTableStrings            = ""
        submitStrings               = ""
        inputKeyFieldStrings        = ""
        assigntoLogicalStrings      = ""
        methodNameStrings           = ""
        methodNamePrivateStrings    = ""
        methodPrivateStrings        = ""
        gbiStrings                  = ""
        crsStrings                  = ""
        fieldEnableStrings          = ""
        initPagingStrings           = ""
        assigntoConfStrings         = ""
        logicalNativeStrings        = ""
        nativeLogicalStrings        = ""
        convertLogicalFieldStrings  = ""
        convertNativeFieldlStrings  = ""

        resultFieldStrings          = ""


        for interface_method in bl_interface:
            if interface_method == 'C':

                tempFieldStrings += tempFieldStrings.join(map(str,ci.create()))
                tempFieldStrings = self.replace_value(tempFieldStrings, bl_name, bl_dir_name, bl_interface_name, bl_native, bl_message_error)
                submitStrings    = self.submit_procedure(submitStrings, tempFieldStrings, be_keyfield, bl_native, bl_name, is_eamcontext)

                tempFieldStrings = tempFieldStrings.replace("[s_param]", submitStrings)
                resultFieldStrings += tempFieldStrings

                tempFieldStrings = self.empty_strings(tempFieldStrings)
                tempTableStrings = self.empty_strings(tempTableStrings)
                submitStrings    = self.empty_strings(submitStrings)

            if interface_method == 'R':

                tempFieldStrings            += tempFieldStrings.join(map(str,ci.fetch()))
                tempFieldStrings            = self.replace_value(tempFieldStrings, bl_name, bl_dir_name, bl_interface_name, bl_native, bl_message_error)
                inputKeyFieldStrings        = self.replace_input_keyfield(inputKeyFieldStrings, tempFieldStrings, be_keyfield)
                methodNameStrings           = self.replace_method_name(methodNameStrings, bl_method, bl_method_value, interface_method)
                methodNamePrivateStrings    = self.replace_public_to_protected(methodNamePrivateStrings, methodNameStrings, interface_method)
                gbiStrings                  = self.replace_gbi(gbiStrings, tempFieldStrings, be_keyfield)
                crsStrings                  = self.replace_crs(crsStrings, tempFieldStrings, be_keyfield, bl_native, bl_name)

                tempFieldStrings = tempFieldStrings.replace("[input_keyfield]", inputKeyFieldStrings)
                tempFieldStrings = tempFieldStrings.replace("[bl_method]", methodNameStrings)
                tempFieldStrings = tempFieldStrings.replace("[bl_protected_method]", methodNamePrivateStrings)
                tempFieldStrings = tempFieldStrings.replace("[gbi_param]", gbiStrings)
                tempFieldStrings = tempFieldStrings.replace("[crs_param]", crsStrings)
                tempFieldStrings = tempFieldStrings.replace("bl_name", bl_name)
                tempFieldStrings = tempFieldStrings.replace("bl_native", bl_native)
                resultFieldStrings += tempFieldStrings

                tempFieldStrings            = self.empty_strings(tempFieldStrings)
                inputKeyFieldStrings        = self.empty_strings(inputKeyFieldStrings)
                methodNameStrings           = self.empty_strings(methodNameStrings)
                methodNamePrivateStrings    = self.empty_strings(methodNamePrivateStrings)
                gbiStrings                  = self.empty_strings(gbiStrings)
                crsStrings                  = self.empty_strings(crsStrings)

            if interface_method == 'U':

                tempFieldStrings += tempFieldStrings.join(map(str,ci.update()))
                tempFieldStrings = self.replace_value(tempFieldStrings, bl_name, bl_dir_name, bl_interface_name, bl_native, bl_message_error)
                tempTableStrings = self.assign_logical_to_native(tempTableStrings, tempFieldStrings, be_keyfield, be_native_keyfield, bl_native, bl_name, is_find=True)
                gbiStrings       = self.replace_gbi(gbiStrings, tempFieldStrings, be_keyfield)
                submitStrings    = self.submit_procedure(submitStrings, tempFieldStrings, be_keyfield, bl_native, bl_name, is_eamcontext)

                tempFieldStrings = tempFieldStrings.replace("[bl_temp_table]", tempTableStrings)
                tempFieldStrings = tempFieldStrings.replace("[gbi_param]", gbiStrings)
                tempFieldStrings = tempFieldStrings.replace("[s_param]", submitStrings)
                tempFieldStrings = tempFieldStrings.replace("bl_name", bl_name)
                tempFieldStrings = tempFieldStrings.replace("bl_native", bl_native)
                resultFieldStrings += tempFieldStrings

                tempFieldStrings            = self.empty_strings(tempFieldStrings)
                tempTableStrings            = self.empty_strings(tempTableStrings)
                gbiStrings                  = self.empty_strings(gbiStrings)
                submitStrings               = self.empty_strings(submitStrings)

            if interface_method == 'D':

                tempFieldStrings += tempFieldStrings.join(map(str,ci.delete()))
                tempFieldStrings = self.replace_value(tempFieldStrings, bl_name, bl_dir_name, bl_interface_name, bl_native, bl_message_error)
                tempTableStrings = self.assign_logical_to_native(tempTableStrings, tempFieldStrings, be_keyfield, be_native_keyfield, bl_native, bl_name, is_find=True)
                gbiStrings       = self.replace_gbi(gbiStrings, tempFieldStrings, be_keyfield)

                submitStrings    = self.submit_procedure(submitStrings, tempFieldStrings, be_keyfield, bl_native, bl_name, is_eamcontext)
                tempFieldStrings = tempFieldStrings.replace("[s_param]", submitStrings)
                submitStrings    = self.empty_strings(submitStrings)
                submitStrings    = self.submit_procedure(submitStrings, tempFieldStrings, be_keyfield, bl_native, bl_name, is_eamcontext)
                tempFieldStrings = tempFieldStrings.replace("[s_param2]", submitStrings)

                tempFieldStrings = tempFieldStrings.replace("[bl_temp_table]", tempTableStrings)
                tempFieldStrings = tempFieldStrings.replace("[gbi_param]", gbiStrings)
                tempFieldStrings = tempFieldStrings.replace("bl_name", bl_name)
                tempFieldStrings = tempFieldStrings.replace("bl_native", bl_native)
                resultFieldStrings += tempFieldStrings

                tempFieldStrings = self.empty_strings(tempFieldStrings)
                gbiStrings       = self.empty_strings(gbiStrings)
                submitStrings    = self.empty_strings(submitStrings)

            if interface_method == 'I':

                tempFieldStrings            += tempFieldStrings.join(map(str,ci.initialize()[0]))
                tempFieldStrings            = self.replace_value(tempFieldStrings, bl_name, bl_dir_name, bl_interface_name, bl_native, bl_message_error)
                tempTableStrings            = self.assign_logical_to_native(tempTableStrings, tempFieldStrings, be_keyfield, be_native_keyfield, bl_native, bl_name, is_find=False)
                assigntoLogicalStrings      = self.assign_keyfield_to_logical(assigntoLogicalStrings, tempFieldStrings, be_keyfield, bl_native, bl_name, is_find=False)
                inputKeyFieldStrings        = self.replace_input_keyfield(inputKeyFieldStrings, tempFieldStrings, be_keyfield)
                methodNameStrings           = self.replace_method_name(methodNameStrings, bl_method, bl_method_value, interface_method)
                methodNamePrivateStrings    = self.replace_public_to_protected(methodNamePrivateStrings, methodNameStrings, interface_method)
                gbiStrings                  = self.replace_gbi(gbiStrings, tempFieldStrings, be_keyfield)

                tempFieldStrings = tempFieldStrings.replace("[bl_temp_table]", tempTableStrings)
                tempFieldStrings = tempFieldStrings.replace("[bl_logical_table]", assigntoLogicalStrings)
                tempFieldStrings = tempFieldStrings.replace("[input_keyfield]", inputKeyFieldStrings)
                tempFieldStrings = tempFieldStrings.replace("[bl_method]", methodNameStrings)
                tempFieldStrings = tempFieldStrings.replace("[bl_protected_method]", methodNamePrivateStrings)
                tempFieldStrings = tempFieldStrings.replace("[gbi_param]", gbiStrings)
                tempFieldStrings = tempFieldStrings.replace("bl_name", bl_name)
                tempFieldStrings = tempFieldStrings.replace("bl_native", bl_native)
                resultFieldStrings += tempFieldStrings

                tempFieldStrings            = self.empty_strings(tempFieldStrings)
                tempTableStrings            = self.empty_strings(tempTableStrings)
                inputKeyFieldStrings        = self.empty_strings(inputKeyFieldStrings)
                assigntoLogicalStrings      = self.empty_strings(assigntoLogicalStrings)
                methodNameStrings           = self.empty_strings(methodNameStrings)
                methodNamePrivateStrings    = self.empty_strings(methodNamePrivateStrings)
                gbiStrings                  = self.empty_strings(gbiStrings)

            if interface_method == 'V':

                tempFieldStrings            += tempFieldStrings.join(map(str,ci.isvalid()))
                tempFieldStrings            = self.replace_value(tempFieldStrings, bl_name, bl_dir_name, bl_interface_name, bl_native, bl_message_error)

                tempFieldStrings = tempFieldStrings.replace("legacy_be_name", bl_legacy_name)
                resultFieldStrings += tempFieldStrings

                tempFieldStrings            = self.empty_strings(tempFieldStrings)

            if interface_method == 'E':

                tempFieldStrings            += tempFieldStrings.join(map(str,ci.exists()))
                tempFieldStrings            = self.replace_value(tempFieldStrings, bl_name, bl_dir_name, bl_interface_name, bl_native, bl_message_error)
                assigntoLogicalStrings      = self.assign_keyfield_to_logical(assigntoLogicalStrings, tempFieldStrings, be_keyfield, bl_native, bl_name, is_find=True)
                inputKeyFieldStrings        = self.replace_input_keyfield(inputKeyFieldStrings, tempFieldStrings, be_keyfield)
                methodNameStrings           = self.replace_method_name(methodNameStrings, bl_method, bl_method_value, interface_method)

                tempFieldStrings = tempFieldStrings.replace("[bl_logical_table]", assigntoLogicalStrings)
                tempFieldStrings = tempFieldStrings.replace("[input_keyfield]", inputKeyFieldStrings)
                tempFieldStrings = tempFieldStrings.replace("[bl_method]", methodNameStrings)
                resultFieldStrings += tempFieldStrings

                tempFieldStrings            = self.empty_strings(tempFieldStrings)
                inputKeyFieldStrings        = self.empty_strings(inputKeyFieldStrings)
                assigntoLogicalStrings      = self.empty_strings(assigntoLogicalStrings)
                methodNameStrings           = self.empty_strings(methodNameStrings)

            if interface_method == 'G':

                tempFieldStrings            += tempFieldStrings.join(map(str,ci.getcontrolstates()))
                tempFieldStrings            = self.replace_value(tempFieldStrings, bl_name, bl_dir_name, bl_interface_name, bl_native, bl_message_error)
                tempTableStrings            = self.assign_logical_to_native(tempTableStrings, tempFieldStrings, be_keyfield, be_native_keyfield, bl_native, bl_name, is_find=False)
                gbiStrings                  = self.replace_gbi(gbiStrings, tempFieldStrings, be_keyfield)
                fieldEnableStrings          = self.replace_fieldenable(fieldEnableStrings, tempFieldStrings, be_keyfield, bl_native, bl_name)

                tempFieldStrings = tempFieldStrings.replace("[bl_temp_table]", tempTableStrings)
                tempFieldStrings = tempFieldStrings.replace("[gbi_param]", gbiStrings)
                tempFieldStrings = tempFieldStrings.replace("[fieldEnable_param]", fieldEnableStrings)
                tempFieldStrings = tempFieldStrings.replace("legacy_be_name", bl_legacy_name)
                tempFieldStrings = tempFieldStrings.replace("bl_name", bl_name)
                tempFieldStrings = tempFieldStrings.replace("bl_native", bl_native)
                resultFieldStrings += tempFieldStrings

                tempFieldStrings            = self.empty_strings(tempFieldStrings)
                tempTableStrings            = self.empty_strings(tempTableStrings)
                gbiStrings                  = self.empty_strings(gbiStrings)
                fieldEnableStrings          = self.empty_strings(fieldEnableStrings)

            if interface_method == 'GL':

                tempFieldStrings            += tempFieldStrings.join(map(str,ci.getlist()))
                tempFieldStrings            = self.replace_value(tempFieldStrings, bl_name, bl_dir_name, bl_interface_name, bl_native, bl_message_error)
                initPagingStrings           = self.replace_initpaging(initPagingStrings, tempFieldStrings, be_keyfield, bl_native, bl_name)

                tempFieldStrings = tempFieldStrings.replace("[initpaging_param]", initPagingStrings)
                tempFieldStrings = tempFieldStrings.replace("bl_name", bl_name)
                tempFieldStrings = tempFieldStrings.replace("bl_native", bl_native)
                resultFieldStrings += tempFieldStrings

                tempFieldStrings            = self.empty_strings(tempFieldStrings)
                initPagingStrings           = self.empty_strings(initPagingStrings)

            if interface_method == 'FL':

                tempFieldStrings            += tempFieldStrings.join(map(str,ci.fetchlist()))
                tempFieldStrings            = self.replace_value(tempFieldStrings, bl_name, bl_dir_name, bl_interface_name, bl_native, bl_message_error)
                initPagingStrings           = self.replace_initpaging(initPagingStrings, tempFieldStrings, be_keyfield, bl_native, bl_name)

                tempFieldStrings = tempFieldStrings.replace("[initpaging_param]", initPagingStrings)
                tempFieldStrings = tempFieldStrings.replace("bl_name", bl_name)
                tempFieldStrings = tempFieldStrings.replace("bl_native", bl_native)
                resultFieldStrings += tempFieldStrings

                tempFieldStrings            = self.empty_strings(tempFieldStrings)
                initPagingStrings           = self.empty_strings(initPagingStrings)

            if interface_method == 'CC':

                tempFieldStrings            += tempFieldStrings.join(map(str,ci.createconfirmation()))
                tempFieldStrings            = self.replace_value(tempFieldStrings, bl_name, bl_dir_name, bl_interface_name, bl_native, bl_message_error)

                resultFieldStrings += tempFieldStrings

                tempFieldStrings            = self.empty_strings(tempFieldStrings)

            if interface_method == 'UC':

                tempFieldStrings            += tempFieldStrings.join(map(str,ci.updateconfirmation()))
                tempFieldStrings            = self.replace_value(tempFieldStrings, bl_name, bl_dir_name, bl_interface_name, bl_native, bl_message_error)
                gbiStrings                  = self.replace_gbi_ds(gbiStrings, tempFieldStrings, be_keyfield)
                crsStrings                  = self.replace_crs(crsStrings, tempFieldStrings, be_keyfield, bl_native, bl_name)

                tempFieldStrings = tempFieldStrings.replace("[gbi_ds_param]", gbiStrings)
                tempFieldStrings = tempFieldStrings.replace("[crs_param]", crsStrings)
                tempFieldStrings = tempFieldStrings.replace("bl_name", bl_name)
                tempFieldStrings = tempFieldStrings.replace("bl_native", bl_native)
                resultFieldStrings += tempFieldStrings

                tempFieldStrings            = self.empty_strings(tempFieldStrings)
                gbiStrings                  = self.empty_strings(gbiStrings)
                crsStrings                  = self.empty_strings(crsStrings)

            if interface_method == 'DC':

                tempFieldStrings            += tempFieldStrings.join(map(str,ci.deleteconfirmation()))
                tempFieldStrings            = self.replace_value(tempFieldStrings, bl_name, bl_dir_name, bl_interface_name, bl_native, bl_message_error)
                gbiStrings                  = self.replace_gbi_ds(gbiStrings, tempFieldStrings, be_keyfield)
                submitStrings               = self.submit_procedure(submitStrings, tempFieldStrings, be_keyfield, bl_native, bl_name, is_eamcontext)

                tempFieldStrings = tempFieldStrings.replace("[gbi_ds_param]", gbiStrings)
                tempFieldStrings = tempFieldStrings.replace("[s_param]", submitStrings)
                tempFieldStrings = tempFieldStrings.replace("bl_name", bl_name)
                tempFieldStrings = tempFieldStrings.replace("bl_native", bl_native)
                resultFieldStrings += tempFieldStrings

                tempFieldStrings            = self.empty_strings(tempFieldStrings)
                gbiStrings                  = self.empty_strings(gbiStrings)
                submitStrings               = self.empty_strings(submitStrings)


        # Loop through Optional method
        for i in range(len(bl_optional_method)):

            if 'dataset' in bl_method_option[i]:

                tempFieldStrings            += tempFieldStrings.join(map(str,ci.optionalmethod()[0]))
                tempFieldStrings            = self.replace_value(tempFieldStrings, bl_name, bl_dir_name, bl_interface_name, bl_native, bl_message_error)
                methodNameStrings           = self.replace_optional_method_name(methodNameStrings, bl_optional_method_lists[i])
                methodNamePrivateStrings    = self.replace_optional_input_method(methodNamePrivateStrings, methodNameStrings, bl_optional_method[i], bl_name)
                methodPrivateStrings        = self.replace_optional_private_method(methodPrivateStrings, methodNameStrings)
                submitStrings               = self.submit_procedure(submitStrings, tempFieldStrings, be_keyfield, bl_native, bl_name, is_eamcontext)

                tempFieldStrings = tempFieldStrings.replace("[bl_optional_method_dataset]", methodNameStrings)
                tempFieldStrings = tempFieldStrings.replace("[bl_optional_private_input_method]", methodNamePrivateStrings)
                tempFieldStrings = tempFieldStrings.replace("[bl_optional_private_method]", methodPrivateStrings)
                resultFieldStrings += tempFieldStrings

                tempFieldStrings            = self.empty_strings(tempFieldStrings)
                methodNameStrings           = self.empty_strings(methodNameStrings)
                methodNamePrivateStrings    = self.empty_strings(methodNamePrivateStrings)
                methodPrivateStrings        = self.empty_strings(methodPrivateStrings)
                submitStrings               = self.empty_strings(submitStrings)


            if 'get' in bl_method_option[i]:
                tempFieldStrings            += tempFieldStrings.join(map(str,ci.optionalmethod()[1]))
                tempFieldStrings            = self.replace_value(tempFieldStrings, bl_name, bl_dir_name, bl_interface_name, bl_native, bl_message_error)
                methodNameStrings           = self.replace_optional_method_name(methodNameStrings, bl_optional_method_lists[i])

                tempFieldStrings = tempFieldStrings.replace("[bl_optional_method_get]", methodNameStrings)
                resultFieldStrings += tempFieldStrings

                tempFieldStrings            = self.empty_strings(tempFieldStrings)
                methodNameStrings           = self.empty_strings(methodNameStrings)


            if 'returnMessage' in bl_method_option[i]:
                tempFieldStrings            += tempFieldStrings.join(map(str,ci.optionalmethod()[2]))
                tempFieldStrings            = self.replace_value(tempFieldStrings, bl_name, bl_dir_name, bl_interface_name, bl_native, bl_message_error)
                methodNameStrings           = self.replace_optional_method_name(methodNameStrings, bl_optional_method_lists[i])
                gbiStrings                  = self.replace_gbi(gbiStrings, tempFieldStrings, be_keyfield)


                tempFieldStrings = tempFieldStrings.replace("[bl_optional_method_returnmessage]", methodNameStrings)
                tempFieldStrings = tempFieldStrings.replace("[gbi_param]", gbiStrings)
                tempFieldStrings = tempFieldStrings.replace("bl_name", bl_name)
                tempFieldStrings = tempFieldStrings.replace("bl_native", bl_native)
                resultFieldStrings += tempFieldStrings

                tempFieldStrings            = self.empty_strings(tempFieldStrings)
                methodNameStrings           = self.empty_strings(methodNameStrings)
                gbiStrings                  = self.empty_strings(gbiStrings)


        # Loop through bl implementation
        for bl_impl in bl_implementation:
            # print(bl_impl)
            if bl_impl == 'HC':

                tempFieldStrings            += tempFieldStrings.join(map(str,ci.handleconfirmation()))
                tempFieldStrings            = self.replace_value(tempFieldStrings, bl_name, bl_dir_name, bl_interface_name, bl_native, bl_message_error)
                assigntoConfStrings         = self.assign_keyfieldforconf(assigntoConfStrings, tempFieldStrings, be_keyfield, bl_name)

                tempFieldStrings = tempFieldStrings.replace("[bl_conf_table]", assigntoConfStrings)
                tempFieldStrings = tempFieldStrings.replace("bl_name", bl_name)
                resultFieldStrings += tempFieldStrings

                tempFieldStrings            = self.empty_strings(tempFieldStrings)
                assigntoConfStrings         = self.empty_strings(assigntoConfStrings)

            if bl_impl == 'RP':

                tempFieldStrings            += tempFieldStrings.join(map(str,ci.replaceunknowns()))
                tempFieldStrings            = self.replace_value(tempFieldStrings, bl_name, bl_dir_name, bl_interface_name, bl_native, bl_message_error)
                resultFieldStrings += tempFieldStrings

                tempFieldStrings            = self.empty_strings(tempFieldStrings)

            if bl_impl == 'CLN':

                tempFieldStrings            += tempFieldStrings.join(map(str,ci.copylogicaltonative()))
                tempFieldStrings            = self.replace_value(tempFieldStrings, bl_name, bl_dir_name, bl_interface_name, bl_native, bl_message_error)
                logicalNativeStrings        = self.replace_logical_native(logicalNativeStrings, bl_logicalfield, bl_nativefield, bl_native, bl_name)

                tempFieldStrings = tempFieldStrings.replace("[copy_logical_native]", logicalNativeStrings)
                resultFieldStrings += tempFieldStrings

                tempFieldStrings            = self.empty_strings(tempFieldStrings)
                logicalNativeStrings        = self.empty_strings(logicalNativeStrings)

            if bl_impl == 'CNL':

                tempFieldStrings            += tempFieldStrings.join(map(str,ci.copynativetological()))
                tempFieldStrings            = self.replace_value(tempFieldStrings, bl_name, bl_dir_name, bl_interface_name, bl_native, bl_message_error)
                nativeLogicalStrings        = self.replace_native_logical(nativeLogicalStrings, bl_logicalfield, bl_nativefield, bl_native, bl_name)

                tempFieldStrings = tempFieldStrings.replace("[copy_native_logical]", nativeLogicalStrings)
                resultFieldStrings += tempFieldStrings

                tempFieldStrings            = self.empty_strings(tempFieldStrings)
                nativeLogicalStrings        = self.empty_strings(nativeLogicalStrings)

            if bl_impl == 'GCS':

                tempFieldStrings            += tempFieldStrings.join(map(str,ci.getcomponentschema()))
                tempFieldStrings            = self.replace_value(tempFieldStrings, bl_name, bl_dir_name, bl_interface_name, bl_native, bl_message_error)
                resultFieldStrings += tempFieldStrings

                tempFieldStrings            = self.empty_strings(tempFieldStrings)

            if bl_impl == 'CLNF':

                tempFieldStrings            += tempFieldStrings.join(map(str,ci.convertlogicaltonativefieldname()))
                tempFieldStrings            = self.replace_value(tempFieldStrings, bl_name, bl_dir_name, bl_interface_name, bl_native, bl_message_error)
                convertLogicalFieldStrings  = self.replace_convert_logical_native(convertLogicalFieldStrings, bl_logicalfield, bl_nativefield, bl_native, bl_name)

                tempFieldStrings = tempFieldStrings.replace("[copy_logical_native_field]", convertLogicalFieldStrings)
                resultFieldStrings += tempFieldStrings

                tempFieldStrings            = self.empty_strings(tempFieldStrings)
                convertLogicalFieldStrings  = self.empty_strings(convertLogicalFieldStrings)

            if bl_impl == 'CNLF':

                tempFieldStrings            += tempFieldStrings.join(map(str,ci.convertnativetologicalfieldname()))
                tempFieldStrings            = self.replace_value(tempFieldStrings, bl_name, bl_dir_name, bl_interface_name, bl_native, bl_message_error)
                convertNativeFieldlStrings  = self.replace_convert_native_logical(convertNativeFieldlStrings, bl_logicalfield, bl_nativefield, bl_native, bl_name)

                tempFieldStrings = tempFieldStrings.replace("[copy_native_logical_field]", convertNativeFieldlStrings)
                resultFieldStrings += tempFieldStrings

                tempFieldStrings            = self.empty_strings(tempFieldStrings)
                convertNativeFieldlStrings  = self.empty_strings(convertNativeFieldlStrings)

            if bl_impl == 'CQKF':

                tempFieldStrings            += tempFieldStrings.join(map(str,ci.createquerywithkeyfield()))
                tempFieldStrings            = self.replace_value(tempFieldStrings, bl_name, bl_dir_name, bl_interface_name, bl_native, bl_message_error)
                resultFieldStrings += tempFieldStrings

                tempFieldStrings            = self.empty_strings(tempFieldStrings)


        # print(resultFieldStrings)
        # Open implementation and replace main-content
        with open(os.path.join(base_path, bl_name, implementation), 'r') as f_implementation:
            readFile = f_implementation.read()
            main_content = '[Main-Content]'
            content = readFile.replace(main_content, resultFieldStrings)

        # Update interface
        with open(os.path.join(base_path, bl_name, implementation), 'w') as f_implementation:
            f_implementation.write(content)

        # Replace export data text file
        if is_be:
            exportData = ''.join(ed.be_data())
        else:
            exportData = ''.join(ed.service_data())

        exportData = exportData.replace("bl_name", bl_name)
        exportData = exportData.replace("bl_dir_name", bl_dir_name)
        exportData = exportData.replace("bl_dir_parent", bl_dir_parent)
        exportData = exportData.replace("bl_parent", bl_parent)
        exportData = exportData.replace("be_browseuri", be_browseuri)
        with open(export_data_name, 'w') as d:
            d.write(exportData)

        # Create datalist file if its true
        if is_datalist:
            datalist_definition = 'datalistDefinition.txt'
            os.chdir('..')
            os.chdir(os.getcwd() + "\\" + bl_name)

            datalist = bl_name + "DataList.cls"
            d_implementation = open(datalist, 'w+')

            # Copy implementation definition
            with open(os.path.join(definition_path, datalist_definition), 'r') as d_Definition:
                readlines = d_Definition.read()
                d_readlines = readlines
                d_Definition.close()

            d_implementation.write(d_readlines)
            d_implementation.close()

            for line in fileinput.input(os.path.join(base_path, bl_name, datalist), inplace=True):
                print(line.replace('bl_name', bl_name), end='')

            for line in fileinput.input(os.path.join(base_path, bl_name, datalist), inplace=True):
                print(line.replace('bl_dir_name', bl_dir_name), end='')



##### METHOD DEFINITIONS #####

    # Empty string variables
    def empty_strings(self, string_to_empty):
        string_to_empty = ""
        return string_to_empty


    # Replace inline
    def replace_inline(self, base_path, bl_name, implementation, bl_using_import, bl_dataset_import, service_strings, be_discarded_lists, bl_dir_name, bl_interface_name, bl_native):

        for line in fileinput.input(os.path.join(base_path, bl_name, implementation), inplace=True):
            print(line.replace('[Import-Content]', bl_using_import), end='')

        for line in fileinput.input(os.path.join(base_path, bl_name, implementation), inplace=True):
            print(line.replace('[Dataset-Content]', bl_dataset_import), end='')

        for line in fileinput.input(os.path.join(base_path, bl_name, implementation), inplace=True):
            print(line.replace('[Service-Content]', service_strings), end='')

        for line in fileinput.input(os.path.join(base_path, bl_name, implementation), inplace=True):
            print(line.replace('bl_name', bl_name), end='')

        for line in fileinput.input(os.path.join(base_path, bl_name, implementation), inplace=True):
            print(line.replace('bl_dir_name', bl_dir_name), end='')

        for line in fileinput.input(os.path.join(base_path, bl_name, implementation), inplace=True):
            print(line.replace('bl_interface_name', bl_interface_name), end='')

        for line in fileinput.input(os.path.join(base_path, bl_name, implementation), inplace=True):
            print(line.replace('bl_native', bl_native), end='')

        for line in fileinput.input(os.path.join(base_path, bl_name, implementation), inplace=True):
            print(line.replace('[be_discarded_lists]', be_discarded_lists), end='')


    # Replace common
    def replace_value(self, tempFieldStrings, bl_name, bl_dir_name, bl_interface_name, bl_native, bl_message_error):
        tempFieldStrings = tempFieldStrings.replace("bl_name", bl_name)
        tempFieldStrings = tempFieldStrings.replace("bl_dir_name", bl_dir_name)
        tempFieldStrings = tempFieldStrings.replace("bl_interface_name", bl_interface_name)
        tempFieldStrings = tempFieldStrings.replace("bl_native", bl_native)
        tempFieldStrings = tempFieldStrings.replace("[bl_message_error]", bl_message_error)

        return tempFieldStrings


    # Replace [bl_temp_table]
    # e.g. ttNativeProject.site_code = ttProject.SiteCode
    def assign_logical_to_native(self, tempTableStrings, tempFieldStrings, be_keyfield, be_native_keyfield, bl_native, bl_name, is_find):
        # Find position of '['
        inputFieldStrings = tempFieldStrings
        inputLines        = inputFieldStrings.splitlines()
        fieldPosition     = 0
        fieldIndent       = ""
        indent            = " "
        and_separator     = ""

        for n, line in enumerate(inputLines, 1):
            if "[bl_temp_table]" in line:
                fieldPosition = line.index('[')

        for i in range(fieldPosition):
            fieldIndent += indent

        for i in range(len(be_keyfield)):

            # if is_find is true we only want to add 'and' for find or for first statement. For assign statement, we need to skip adding ' and'
            if is_find:
                if len(be_keyfield) > 0:
                    and_separator = " and"
                else: and_separator = ""

            if i < 1:
                tempTableStrings += "tt" + bl_native + bl_name + "." + be_native_keyfield[i] + " = " + "tt" + bl_name + "." + be_keyfield[i] + and_separator + "\n"
            else:
                tempTableStrings += fieldIndent + "tt" + bl_native + bl_name + "." + be_native_keyfield[i] + " = " + "tt" + bl_name + "." + be_keyfield[i] + and_separator + "\n"

        tempTableStrings = tempTableStrings.rstrip('and \n')

        return tempTableStrings


    # Replace [bl_native_table]
    # e.g. ttProject.site_code = siteCode
    def assign_keyfield_as_native(self, assigntoLogicalStrings, tempFieldStrings, be_keyfield, be_native_keyfield, bl_native, bl_name, is_find):
        # Find position of '['
        inputFieldStrings = tempFieldStrings
        inputLines        = inputFieldStrings.splitlines()
        fieldPosition     = ""
        fieldIndent       = ""
        indent            = " "
        and_separator     = ""

        for n, line in enumerate(inputLines, 1):
            if "[bl_native_table]" in line:
                fieldPosition = line.index('[')

        for i in range(fieldPosition):
            fieldIndent += indent

        for i in range(len(be_keyfield)):

            # if is_find is true we only want to add 'and' for find or for first statement. For assign statement, we need to skip adding ' and'
            if is_find:
                if len(be_keyfield) > 0:
                    and_separator = " and"
                else: and_separator = ""

            # lower case first letter of keyfield
            keyfield = be_keyfield[i][:1].lower() + be_keyfield[i][1:]
            if i < 1:
                assigntoLogicalStrings += "tt" + bl_name + "." + be_native_keyfield[i] + " = " + keyfield + and_separator + "\n"
            else:
                assigntoLogicalStrings += fieldIndent + "tt" + bl_name + "." + be_native_keyfield[i] + " = " + keyfield + and_separator + "\n"

        assigntoLogicalStrings = assigntoLogicalStrings.rstrip('and \n')

        return assigntoLogicalStrings


    # Replace [bl_logical_table]
    # e.g. ttProject.SiteCode = siteCode
    def assign_keyfield_to_logical(self, assigntoLogicalStrings, tempFieldStrings, be_keyfield, bl_native, bl_name, is_find):
        # Find position of '['
        inputFieldStrings = tempFieldStrings
        inputLines        = inputFieldStrings.splitlines()
        fieldPosition     = ""
        fieldIndent       = ""
        indent            = " "
        and_separator     = ""

        for n, line in enumerate(inputLines, 1):
            if "[bl_logical_table]" in line:
                fieldPosition = line.index('[')

        for i in range(fieldPosition):
            fieldIndent += indent

        for i in range(len(be_keyfield)):

            # if is_find is true we only want to add 'and' for find or for first statement. For assign statement, we need to skip adding ' and'
            if is_find:
                if len(be_keyfield) > 0:
                    and_separator = " and"
                else: and_separator = ""

            # lower case first letter of keyfield
            keyfield = be_keyfield[i][:1].lower() + be_keyfield[i][1:]
            if i < 1:
                assigntoLogicalStrings += "tt" + bl_name + "." + be_keyfield[i] + " = " + keyfield + and_separator + "\n"
            else:
                assigntoLogicalStrings += fieldIndent + "tt" + bl_name + "." + be_keyfield[i] + " = " + keyfield + and_separator + "\n"

        assigntoLogicalStrings = assigntoLogicalStrings.rstrip('and \n')

        return assigntoLogicalStrings


    # Replace [bl_conf_table]
    # e.g. ttProjectConf.site_code = ttProject.SiteCode
    def assign_keyfieldforconf(self, assigntoConfStrings, tempFieldStrings, be_keyfield, bl_name):
        # Find position of '['
        inputFieldStrings = tempFieldStrings
        inputLines        = inputFieldStrings.splitlines()
        fieldPosition     = ""
        fieldIndent       = ""
        indent            = " "

        for n, line in enumerate(inputLines, 1):
            if "[bl_conf_table]" in line:
                fieldPosition = line.index('[')

        for i in range(fieldPosition):
            fieldIndent += indent

        for i in range(len(be_keyfield)):
            # lower case first letter of keyfield
            # keyfield = be_keyfield[i][:1].lower() + be_keyfield[i][1:]
            if i < 1:
                assigntoConfStrings += "tt" + bl_name + "Conf." + be_keyfield[i] + " = " + "tt" + bl_name + "." + be_keyfield[i] + "\n"
            else:
                assigntoConfStrings += fieldIndent + "tt" + bl_name + "Conf." + be_keyfield[i] + " = " + "tt" + bl_name + "." + be_keyfield[i] + "\n"

        assigntoConfStrings = assigntoConfStrings.rstrip('\n')

        return assigntoConfStrings


    # Replace [copy_logical_native]
    def replace_logical_native(self, logicalNativeStrings, bl_logicalfield, bl_nativefield, bl_native, bl_name):
        fieldIndent = " " * 9

        for i in range(len(bl_logicalfield)):
            if i < 1:
                logicalNativeStrings += "tt" + bl_native + bl_name + "." + bl_nativefield[i] + " = tt" + bl_name + "." + bl_logicalfield[i] + "\n"
            else:
                logicalNativeStrings += fieldIndent + "tt" + bl_native + bl_name + "." + bl_nativefield[i] + " = tt" + bl_name + "." + bl_logicalfield[i] + "\n"

        logicalNativeStrings = logicalNativeStrings.rstrip('\n')

        return logicalNativeStrings


    # Replace [copy_native_logical]
    def replace_native_logical(self, nativeLogicalStrings, bl_logicalfield, bl_nativefield, bl_native, bl_name):
        fieldIndent = " " * 12

        for i in range(len(bl_nativefield)):
            if i < 1:
                nativeLogicalStrings += "tt" + bl_name + "." + bl_logicalfield[i] + " = tt" + bl_native + bl_name + "." + bl_nativefield[i] + "\n"
            else:
                nativeLogicalStrings += fieldIndent + "tt" + bl_name + "." + bl_logicalfield[i] + " = tt" + bl_native + bl_name + "." + bl_nativefield[i] + "\n"

        nativeLogicalStrings = nativeLogicalStrings.rstrip('\n')

        return nativeLogicalStrings


    # Replace [input_keyfield]
    def replace_input_keyfield(self, inputKeyFieldStrings, tempFieldStrings, be_keyfield):
        # Find position of '['
        inputFieldStrings = tempFieldStrings
        inputLines        = inputFieldStrings.splitlines()
        fieldPosition     = ""
        fieldIndent       = ""
        indent            = " "
        keyfield          = ""

        for n, line in enumerate(inputLines, 1):
            if "[input_keyfield]" in line:
                fieldPosition = line.index('[')

        for i in range(fieldPosition):
            fieldIndent += indent

        for i in range(len(be_keyfield)):
            # lower case first letter of keyfield
            keyfield = be_keyfield[i][:1].lower() + be_keyfield[i][1:]
            if i < 1:
                inputKeyFieldStrings += "input " + keyfield + ",\n"
            else:
                inputKeyFieldStrings += fieldIndent + "input " + keyfield + ",\n"

        inputKeyFieldStrings = inputKeyFieldStrings.rstrip('\n')

        return inputKeyFieldStrings


    # Replace [bl_method]
    def replace_method_name(self, methodNameStrings, bl_method, bl_method_value, interface_method):
        # Get method from list
        methodIndex = bl_method.index(interface_method)
        methodNameStrings  = bl_method_value[methodIndex]
        methodNameStrings  = methodNameStrings.rstrip('.\n    ')

        return methodNameStrings


    # Replace [bl_protected_method]
    # Change method public to method protected for Fetch and Initialize
    def replace_public_to_protected(self, methodNamePrivateStrings, methodNameStrings, interface_method):
        public_string    = "public"
        protected_string = "protected"

        methodNameStrings = methodNameStrings.replace(public_string, protected_string)

        if interface_method == 'R':
            methodNameStrings = methodNameStrings.replace('Fetch', 'FetchPrivate')
        elif interface_method == 'I':
            methodNameStrings = methodNameStrings.replace('Initialize', 'InitializePrivate')

        if "dataset-handle" in methodNameStrings:
            methodNameStrings = methodNameStrings.replace('dataset-handle', 'dataset')

        methodNameLines = methodNameStrings.splitlines()

        indent = " "
        methodIndent = " "
        indentPosition = len(protected_string)

        for i in range(indentPosition):
            methodIndent += indent

        for n, line in enumerate(methodNameLines, 1):
            if n > 1:
                methodNamePrivateStrings += methodIndent + line + "\n"
            else:
                methodNamePrivateStrings = methodNameLines[0] + "\n"

        methodNamePrivateStrings = methodNamePrivateStrings.rstrip('.\n')

        return methodNamePrivateStrings


    # replace [bl_optional_method_name]
    # simply removing . and newline at the end of optional method
    def replace_optional_method_name(self, method_name_string, optional_method):
        method_name_string  = optional_method.rstrip('.\n    ')
        return method_name_string


    # replace [bl_optional_private_input_method]
    """
    this-object:CheckCanPostLaborPrivate(input workorderNumber,
                                         input dataset-handle dsProjectTemp by-reference,
                                         output displayMessage).
    """
    def replace_optional_input_method(self, returnMethodStrings, methodNameStrings, optional_method_name, bl_name):

        methodIndent        = ""
        indent              = " "
        methodNamePrivateStrings = ""
        thisObjectLength    = len('      this-object:')
        methodLength        = len(optional_method_name)
        privateLength       = len('Private(')
        indentPosition      = thisObjectLength + methodLength + privateLength

        for i in range(indentPosition):
            methodIndent += indent

        # slices 'method public void '
        methodIndex = methodNameStrings.index(optional_method_name)
        methodNameStrings = methodNameStrings[methodIndex:]

        methodNameStrings = methodNameStrings.replace(optional_method_name, "      this-object:" + optional_method_name + 'Private')
        methodNameLines = methodNameStrings.splitlines()

        for n, line in enumerate(methodNameLines, 1):
            if n > 1:
                if ' as ' in line:
                    lineIndex = line.index(' as ')
                    line = line[:lineIndex]

                    methodNamePrivateStrings += methodIndent + line.lstrip() + ",\n"
                else:
                    line = line.rstrip(',')
                    methodNamePrivateStrings += methodIndent + line.lstrip() + "Temp by-reference,\n"

            else:
                if ' as ' in line:
                    lineIndex = line.index(' as ')
                    line = line[:lineIndex]

                    methodNamePrivateStrings += line + ",\n"
                else:
                    if line[-1] == ')':
                        line = line.rstrip(')')
                        methodNamePrivateStrings = line + "Temp by-reference\n"
                    else:
                        line = line.rstrip(',')
                        methodNamePrivateStrings = line + "Temp by-reference,\n"

        methodNamePrivateStrings = methodNamePrivateStrings.rstrip(',\n')
        methodNamePrivateStrings = methodNamePrivateStrings.replace(methodNamePrivateStrings, methodNamePrivateStrings + ").")

        if 'input-output dataset-handle' in methodNamePrivateStrings:
            inAndInOutDsPre = '{com/qad/qra/base/InAndInOutDSPre.i &dataset = "dsbl_name"}'
            inOutDsPost = '{com/qad/qra/base/InOutDSPost.i &dataset = "dsbl_name"}'

            returnMethodStrings = inAndInOutDsPre + "\n" + methodNamePrivateStrings + "\n      " + inOutDsPost

        elif 'output dataset-handle' in methodNamePrivateStrings:
            outDsPre = '{com/qad/qra/base/OutDSPre.i &dataset = "dsbl_name"}'
            outDsPost = '{com/qad/qra/base/OutDSPost.i &dataset = "dsbl_name"}'

            returnMethodStrings = outDsPre + "\n" + methodNamePrivateStrings + "\n      " + outDsPost

        elif 'input dataset-handle' in methodNamePrivateStrings:
            inAndInOutDsPre = '{com/qad/qra/base/InAndInOutDSPre.i &dataset = "dsbl_name"}'

            returnMethodStrings = inAndInOutDsPre + "\n" + methodNamePrivateStrings

        returnMethodStrings = returnMethodStrings.replace("bl_name", bl_name)
        return returnMethodStrings


    # Replace [bl_optional_private_method]
    """
    method private void CheckCanPostLabor(input workorderNumber as integer,
                                          input dataset dsProject,
                                          output displayMessage as character):
    """
    def replace_optional_private_method(self, methodPrivateStrings, optional_method):
        methodPrivateStrings = optional_method

        methodPrivateStrings  = methodPrivateStrings.replace('public', 'protected')
        methodPrivateStrings  = methodPrivateStrings.replace('dataset-handle', 'dataset')

        inputMethodStrings = methodPrivateStrings
        inputLines         = inputMethodStrings.splitlines()
        methodPrivateStrings = ""
        methodPosition       = ""
        methodIndent         = ""
        indent               = " "
        blankIndent          = indent * 4

        for n, line in enumerate(inputLines, 1):
            if n == 1:
                methodPosition = line.index('(')
                methodPrivateStrings = line + "\n"

                for i in range(methodPosition):
                    methodIndent += indent

            else:
                methodPrivateStrings += blankIndent + methodIndent + line.lstrip() + "\n"


        methodPrivateStrings = methodPrivateStrings.rstrip()
        return methodPrivateStrings


    # Replace [s_param]
    def submit_procedure(self, submitStrings, tempFieldStrings, be_keyfield, bl_native, bl_name, is_eamcontext):
        # Find position of '['
        inputFieldStrings = tempFieldStrings
        inputLines        = inputFieldStrings.splitlines()
        fieldPosition     = ""
        fieldIndent       = ""
        indent            = " "

        for n, line in enumerate(inputLines, 1):
            if "[s_param]" in line:
                fieldPosition = line.index('[')
                for i in range(fieldPosition):
                    fieldIndent += indent

                submitStrings += "input-output dataset ds" + bl_native + bl_name + " by-reference" + ",\n"

                if is_eamcontext == True:
                    submitStrings += fieldIndent + "output dataset dsSubmitError by-reference" + ",\n" + \
                        fieldIndent + "input-output validationStatus" + ",\n" + fieldIndent + \
                            "input-output dataset dsEAMContext by-reference" + ",\n" + fieldIndent
                else:
                    submitStrings += fieldIndent + "output dataset dsSubmitError by-reference" + ",\n" + \
                        fieldIndent + "input-output validationStatus" + ",\n" + fieldIndent

                return submitStrings

            elif "[s_param2]" in line:
                fieldPosition = line.index('[')
                for i in range(fieldPosition):
                    fieldIndent += indent

                submitStrings += "input-output dataset ds" + bl_native + bl_name + " by-reference" + ",\n"

                if is_eamcontext == True:
                    submitStrings += fieldIndent + "output dataset dsSubmitError by-reference" + ",\n" + \
                        fieldIndent + "input-output validationStatus" + ",\n" + fieldIndent + \
                            "input-output dataset dsEAMContext by-reference" + ",\n" + fieldIndent
                else:
                    submitStrings += fieldIndent + "output dataset dsSubmitError by-reference" + ",\n" + \
                        fieldIndent + "input-output validationStatus" + ",\n" + fieldIndent

                return submitStrings


    # Replace [gbi_param]
    # e.g. input siteCode
    def replace_gbi(self, gbiStrings, tempFieldStrings, be_keyfield):
        # Find position of '['
        inputFieldStrings = tempFieldStrings
        inputLines        = inputFieldStrings.splitlines()
        fieldPosition     = ""
        fieldIndent       = ""
        indent            = " "
        outputds          = "output dataset dsbl_nativebl_name by-reference"
        inputusersession  = "input userSessionService:SessionID"

        for n, line in enumerate(inputLines, 1):
            if "[gbi_param]" in line:
                fieldPosition = line.index('[')

        for i in range(fieldPosition):
            fieldIndent += indent

        for i in range(len(be_keyfield)):
            # lower case first letter of keyfield
            keyfield = be_keyfield[i][:1].lower() + be_keyfield[i][1:]
            if i < 1:
                gbiStrings += "input " + keyfield + ",\n"
            else:
                gbiStrings += fieldIndent + "input " + keyfield + ",\n"

        # gbiStrings = gbiStrings.rstrip('\n')
        gbiStrings += fieldIndent + outputds + ",\n"
        gbiStrings += fieldIndent + inputusersession

        return gbiStrings


    # Replace [crs_param]
    def replace_crs(self, crsStrings, tempFieldStrings, be_keyfield, bl_native, bl_name):
        # Find position of '['
        inputFieldStrings = tempFieldStrings
        inputLines        = inputFieldStrings.splitlines()
        fieldPosition     = 0
        fieldIndent       = ""
        indent            = " "

        for n, line in enumerate(inputLines, 1):
            if "[crs_param]" in line:
                fieldPosition = line.index('[')

        for i in range(fieldPosition):
            fieldIndent += indent

        crsStrings += "input dataset ds" + bl_native + bl_name + " by-reference" + ",\n"

        crsStrings += fieldIndent + "input dataset dsEAMContext by-reference" + ",\n" + \
            fieldIndent + "output dsMessage by-reference" + ",\n" + fieldIndent + \
                "input userSessionService:SessionID"

        return crsStrings


    # Replace [fieldEnable_param]
    def replace_fieldenable(self, fieldEnableStrings, tempFieldStrings, be_keyfield, bl_native, bl_name):
        # Find position of '['
        inputFieldStrings = tempFieldStrings
        inputLines        = inputFieldStrings.splitlines()
        fieldPosition     = ""
        fieldIndent       = ""
        indent            = " "

        for n, line in enumerate(inputLines, 1):
            if "[fieldEnable_param]" in line:
                fieldPosition = line.index('[')

        for i in range(fieldPosition):
            fieldIndent += indent

        fieldEnableStrings += "input dataset ds" + bl_native + bl_name + " by-reference" + ",\n"

        fieldEnableStrings += fieldIndent + "input-output dataset dsControlState by-reference" + ",\n" + \
            fieldIndent + "input userSessionService:SessionID"

        return fieldEnableStrings


    # Replace [initpaging_param]
    def replace_initpaging(self, initPagingStrings, tempFieldStrings, be_keyfield, bl_native, bl_name):
        # Find position of '['
        inputFieldStrings = tempFieldStrings
        inputLines        = inputFieldStrings.splitlines()
        fieldPosition     = ""
        fieldIndent       = ""
        indent            = " "

        for n, line in enumerate(inputLines, 1):
            if "[initpaging_param]" in line:
                fieldPosition = line.index('[')

        for i in range(fieldPosition):
            fieldIndent += indent

        initPagingStrings += 'input "",\n'

        initPagingStrings += fieldIndent + 'input "",\n' + fieldIndent + 'input -1,\n' + \
            fieldIndent + 'input dataset dsPageInitialization by-reference,\n' + fieldIndent + 'output isHaveLast,\n' + \
                fieldIndent + 'output dataset dsbl_nativebl_name by-reference,\n' + fieldIndent + 'output dataset dsMessage by-reference,\n' + \
                    fieldIndent + 'input userSessionService:SessionID'

        return initPagingStrings


    # Replace [gbi_ds_param]
    # e.g. input ttProject.SiteCode
    def replace_gbi_ds(self, gbiStrings, tempFieldStrings, be_keyfield):
        # Find position of '['
        inputFieldStrings = tempFieldStrings
        inputLines        = inputFieldStrings.splitlines()
        fieldPosition     = ""
        fieldIndent       = ""
        indent            = " "
        outputds          = "output dataset dsbl_nativebl_name by-reference"
        inputusersession  = "input userSessionService:SessionID"

        for n, line in enumerate(inputLines, 1):
            if "[gbi_ds_param]" in line:
                fieldPosition = line.index('[')

        for i in range(fieldPosition):
            fieldIndent += indent

        for i in range(len(be_keyfield)):
            # Not needed if from logical ds field. lower case first letter of keyfield
            # keyfield = be_keyfield[i][:1].lower() + be_keyfield[i][1:]
            keyfield = be_keyfield[i]
            if i < 1:
                gbiStrings += "input ttbl_name." + keyfield + ",\n"
            else:
                gbiStrings += fieldIndent + "input ttbl_name." + keyfield + ",\n"

        # gbiStrings = gbiStrings.rstrip('\n')
        gbiStrings += fieldIndent + outputds + ",\n"
        gbiStrings += fieldIndent + inputusersession

        return gbiStrings


    # Replace [copy_logical_native_field]
    def replace_convert_logical_native(self, convertLogicalFieldStrings, bl_logicalfield, bl_nativefield, bl_native, bl_name):
        fieldIndent = " " * 6

        for i in range(len(bl_logicalfield)):
            if i < 1:
                convertLogicalFieldStrings += 'if fieldName = ' + '"' + bl_logicalfield[i] + '" then fieldName = "' + bl_nativefield[i] + '".' + '\n'
            else:
                convertLogicalFieldStrings += fieldIndent + 'if fieldName = ' + '"' + bl_logicalfield[i] + '" then fieldName = "' + bl_nativefield[i] + '".' + '\n'

        convertLogicalFieldStrings = convertLogicalFieldStrings.rstrip('\n')

        return convertLogicalFieldStrings


    # Replace [copy_native_logical_field]
    def replace_convert_native_logical(self, convertNativeFieldStrings, bl_logicalfield, bl_nativefield, bl_native, bl_name):
        fieldIndent     = " " * 9
        tempFieldIndent = " " * 12

        for i in range(len(bl_nativefield)):
            logicalfield = bl_logicalfield[i][:1].lower() + bl_logicalfield[i][1:]
            if i < 1:
                convertNativeFieldStrings += 'if fieldName = ' + '"' + bl_nativefield[i] + '" then do:' + '\n'
                convertNativeFieldStrings += tempFieldIndent + 'tempFieldNames = tempFieldNames + ' + '"' + logicalfield + '" + AssetMgmtConstants:FIELD_DELIMITER.' + '\n'
                convertNativeFieldStrings += tempFieldIndent + 'tempFieldStates + fieldState + AssetMgmtConstants:FIELD_DELIMITER.' + '\n'
                convertNativeFieldStrings += fieldIndent + 'end.' + '\n'
            else:
                convertNativeFieldStrings += fieldIndent + 'if fieldName = ' + '"' + bl_nativefield[i] + '" then do:' + '\n'
                convertNativeFieldStrings += tempFieldIndent + 'tempFieldNames = tempFieldNames + ' + '"' + logicalfield + '" + AssetMgmtConstants:FIELD_DELIMITER.' + '\n'
                convertNativeFieldStrings += tempFieldIndent + 'tempFieldStates + fieldState + AssetMgmtConstants:FIELD_DELIMITER.' + '\n'
                convertNativeFieldStrings += fieldIndent + 'end.' + '\n'

        convertNativeFieldStrings = convertNativeFieldStrings.rstrip('\n')

        return convertNativeFieldStrings



# Uncomment to run this generator
img = Implementation_Generator()
