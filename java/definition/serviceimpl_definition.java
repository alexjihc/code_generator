/*
 *  Copyright 1986 QAD Inc. All rights reserved.
 *
 *  $Id: .java 308556 2020-11-11 14:58:29Z a3q $
 */
package com.qad.erp.assetmgmt.service.impl;

import java.util.ArrayList;
import java.util.List;

import commonj.sdo.DataGraph;
import com.progress.open4gl.ProDataGraph;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.qad.qra.ApiException;
import com.qad.qra.ConnectionManager;
import com.qad.qra.Context;
import com.qad.qra.Holder;
import com.qad.qracore.service.impl.QraEntityServiceBase;
import com.qad.qracore.util.ExceptionUtil;
import com.qad.qraview.dto.SubmitResultAndData;

import com.qad.assetmgmt.bl_dir_name.bl_name.FilterContainer;
import com.qad.assetmgmt.bl_dir_name.bl_name.impl.QraFilterContainer;
import com.qad.assetmgmt.bl_dir_name.bl_name;
import com.qad.assetmgmt.bl_dir_name.bl_nameService;
import com.qad.assetmgmt.bl_dir_name.bl_nameContainer;
import com.qad.assetmgmt.bl_dir_name.bl_nameFactory;
import com.qad.assetmgmt.bl_dir_name.impl.Qrabl_nameFactory;
import com.qad.assetmgmt.bl_dir_name.impl.Qrabl_nameContainer;

@Service("com.qad.erp.assetmgmt.service.impl.bl_nameServiceImpl")
public class bl_nameServiceImpl extends QraEntityServiceBase<bl_nameContainer, bl_nameFactory>
{
    public static final Logger logger = LoggerFactory.getLogger(bl_nameServiceImpl.class);
    [SERVICE_IMPLEMENTATION_METHODS]
    @Override
    public bl_nameContainer initialize() {
        return null;
    }
    [SERVICE_ENTITY_METHODS]
    @Override
	protected bl_nameFactory getEntityFactory(ConnectionManager connectionManager, Context context)
	{
		return new Qrabl_nameFactory(connectionManager, context);
    }

    @Override
    public List<bl_name> getEntityListFromContainer(bl_nameContainer container) {
        List<bl_name> entityList = new ArrayList<>();
        for (bl_name entity : container.bl_lowercases().values()) {
            entityList.add(entity);
        }
        return entityList;
    }

	@Override
	protected String getJsonFromEntity(bl_nameContainer entity)
	{
		bl_nameFactory factory = getEntityFactory();
		return factory.containerToJson(entity);
    }

}