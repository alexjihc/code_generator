/*
 *  Copyright 1986 QAD Inc. All rights reserved.
 *
 *  $Id: .java 308556 2020-11-11 14:58:29Z a3q $
 */
package com.qad.erp.assetmgmt.mvc.controller.data;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.servlet.View;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.DeleteMapping;

import com.qad.webshell.util.JsonDataHolder;
import com.qad.qra.ApiException;
import com.qad.qra.Holder;
import com.qad.qracore.mvc.controller.data.QraDataControllerBase;
import com.qad.qraview.dto.QueryParameters;
import com.qad.qraview.dto.SubmitResult;
import com.qad.qraview.dto.SubmitResultAndData;

import com.qad.erp.assetmgmt.StringCodeConstants;
import com.qad.erp.assetmgmt.AssetmgmtConstants;
import com.qad.erp.assetmgmt.service.impl.bl_nameServiceImpl;
import com.qad.assetmgmt.bl_dir_name.bl_name.FilterContainer;
import com.qad.assetmgmt.bl_dir_name.bl_name.Filter;
import com.qad.assetmgmt.bl_dir_name.bl_name;
import com.qad.assetmgmt.bl_dir_name.bl_nameContainer;
import com.qad.assetmgmt.bl_dir_name.bl_nameFactory;


@Controller("com.qad.erp.assetmgmt.mvc.controller.data.bl_nameDataController")
public class bl_nameDataController extends QraDataControllerBase<bl_nameContainer>
{
    public static final Logger logger = LoggerFactory.getLogger(bl_nameDataController.class);

	@Autowired
	public void setbl_nameService(bl_nameServiceImpl service)
	{
		crudProviderService = service;
	}

	@Override
	public String getServiceProviderName()
	{
		return com.qad.erp.assetmgmt.Plugin.PLUGIN_NAME;
    }
    [DATACONTROLLER_API]
	@Override
	protected bl_nameContainer getEntityFromJson(String json)
	{
		bl_nameServiceImpl service = (bl_nameServiceImpl)crudProviderService;
		bl_nameFactory factory = service.getEntityFactory();
		bl_nameContainer container = null;

		try
		{
			container = factory.createbl_nameContainer(json);
		}
		catch (Exception e)
		{
			throw(new RuntimeException(e));
		}

		return container;
	}

	@Override
	protected String getJsonFromEntity(bl_nameContainer entity)
	{
		bl_nameFactory factory = ((bl_nameServiceImpl)crudProviderService).getEntityFactory();
		return factory.containerToJson(entity);
	}

	@Override
	protected int getEntityCount(bl_nameContainer entity)
	{
		return entity.bl_lowercases().size();
	}

}